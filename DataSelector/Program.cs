﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace DataSelector
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Start");
            try
            {
                //string path = @"\\172.16.0.216\www_scan_directives\Year {0}\{1} {0}\{2} - {3}";
                string path = @"D:\DIRECTIVESDATA2017";
                string input = @"D:\filter.csv";
                string download_path = @"D:\SelectiveFiles";

                string[] list = File.ReadAllLines(input);

                foreach(string file in list)
                {
                    string[] files = Directory.GetFiles(path, string.Format("{0}.*", file), SearchOption.AllDirectories);
                    if (files.Length > 0)
                    {
                        File.Copy(files[0], Path.Combine(download_path, Path.GetFileName(files[0])), true);
                        //Console.WriteLine(files[0]);
                    }
                    else
                    {
                        Console.WriteLine("Not found: " + file);
                    }
                }
                
            }
            catch(Exception ex)
            {
                Console.WriteLine("Exception: {0}\n{1}", ex.Message, ex.StackTrace);
            }
            Console.WriteLine("End");
            Console.Read();
        }
    }
}

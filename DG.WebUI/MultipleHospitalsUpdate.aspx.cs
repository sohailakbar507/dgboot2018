﻿using DGModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DG.WebUI
{
    public partial class MultipleHospitalsUpdate : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                uploadhospitals();
                UploadGridMultiple();
            }
        }
        private void uploadhospitals()
        {            
        }
        private void UploadGridMultiple()
        {
            DGEntities dg = new DGEntities();
            int fanos = Convert.ToInt32(Request["fano"]);
            var getfano = (from a in dg.MAINCORRs
                           from r in dg.Reffereds
                           where a.senttoid == r.RefId && a.fano==fanos
                           orderby a.DGId descending
                           select new
                           {
                               a.DGId,
                               a.fano,
                               a.appname,
                               a.chqno,
                               a.chqdate,
                               a.amount,
                               a.senttoid,
                               a.Expense,
                               a.AmountIncured,
                               a.refunded,
                               a.fyear
                           }
                           ).ToList();
           DGVHospitals.DataSource = getfano;
           DGVHospitals.DataBind();
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            DGEntities dg = new DGEntities();
            int fanos = Convert.ToInt32(Request["fano"]);
            Cls_Login login = (Cls_Login)Session["Login"];
            if (login.Groupid == 2 || login.Groupid == 6)
            {
                Response.Redirect("HealthForm.aspx?fano=" + fanos);
            }
            else
            {
              Response.Redirect("DGHealthFormMultipleHospitals.aspx?fano=" + fanos);
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            DGEntities dg = new DGEntities();
            foreach (GridViewRow row in DGVHospitals.Rows)
            {
                DropDownList ddlref = (row.FindControl("DDLRef") as DropDownList);
                TextBox chqno = (TextBox)row.FindControl("txtchqno");
                TextBox chqdate = (TextBox)row.FindControl("txtchqdate");               
                Label amount = (Label)row.FindControl("lblAmount");                
                TextBox expens = (TextBox)row.FindControl("txtexp");
                Label DGID = (Label)row.FindControl("lblDGId");
                int dgid = Convert.ToInt32(DGID.Text);
                MAINCORR exp = new MAINCORR();
                MAINCORR update = (from a in dg.MAINCORRs where a.DGId == dgid select a).First();
                update.senttoid = Convert.ToInt32(ddlref.SelectedValue) ; 
                update.Expense = expens.Text == string.Empty ? 0 : Convert.ToInt32(expens.Text);
                int amnt = amount.Text == string.Empty ? 0 : Convert.ToInt32(amount.Text);
                int expenses = expens.Text == string.Empty ? 0 : Convert.ToInt32(expens.Text);
                update.Balance = amnt - expenses;             
                update.chqno = chqno.Text;
                if (!
                   string.IsNullOrEmpty(chqdate.Text))
                {
                    update.chqdate = Convert.ToDateTime(chqdate.Text);
                }               
                dg.SaveChanges();
            }
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "successalert();", true);
        }
        protected void DGVHospitals_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DropDownList ddlref = (e.Row.FindControl("DDLRef") as DropDownList);
                DGEntities dg = new DGEntities();
                var sentto = (from a in dg.Reffereds orderby a.Refto select a).ToList();
                ddlref.DataSource = sentto;
                ddlref.DataValueField = "RefId";
                ddlref.DataTextField = "Refto";
                ddlref.DataBind();
                int refid = Convert.ToInt32((e.Row.FindControl("lblRefId") as Label).Text);
                ddlref.Items.FindByValue(refid.ToString()).Selected = true;
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DGModel;

namespace DG.WebUI
{
    public partial class ReportPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
              
                if (Request["Report"] == "MpaWise")
                {
                    lblFanoFrom.Enabled = false;
                    lblFanoTo.Enabled = false;
                    txtFANoFrom.Enabled = false;
                    txtFaNoTo.Enabled = false;
                    lblFileNo.Enabled = false;
                    txtFileNo.Enabled = false;
                    lblDateFrom.Enabled = false;
                    lblDateTo.Enabled = false;
                    DateFrom.Enabled = false;
                    DateTo.Enabled = false;
                    lblLetCode.Enabled = false;
                    txtLetCode.Enabled = false;
                    lblCatCode.Enabled = false;
                    txtCatCode.Enabled = false;
                    lblCons.Enabled = false;
                    txtCons.Enabled = false;
               }


                    if (Request["Report"] == "MPAWiseReport2")
                    {
                        lblFanoFrom.Enabled = false;
                        lblFanoTo.Enabled = false;
                        txtFANoFrom.Enabled = false;
                        txtFaNoTo.Enabled = false;
                        lblFileNo.Enabled = false;
                        txtFileNo.Enabled = false;
                        lblDateFrom.Enabled = false;
                        lblDateTo.Enabled = false;
                        DateFrom.Enabled = false;
                        DateTo.Enabled = false;
                        lblLetCode.Enabled = false;
                        txtLetCode.Enabled = false;
                        lblCatCode.Enabled = false;
                        txtCatCode.Enabled = false;
                        lblCons.Enabled = false;
                        txtCons.Enabled = false;
                    }
                    if (Request["Report"] == "Letter")
                    {
                        lblFanoFrom.Enabled = true;
                        lblFanoTo.Enabled = true;
                        txtFANoFrom.Text = Convert.ToString(1);
                        txtFaNoTo.Text = Convert.ToString(99999);
                        lblFileNo.Enabled = false;
                        txtFileNo.Enabled = false;
                        lblDateFrom.Enabled = false;
                        lblDateTo.Enabled = false;
                        DateFrom.Enabled = false;
                        DateTo.Enabled = false;
                        lblLetCode.Enabled = false;
                        txtLetCode.Enabled = false;
                        lblCatCode.Enabled = false;
                        txtCatCode.Enabled = false;
                        lblCons.Enabled = false;
                        txtCons.Enabled = false;
                    }
               
                if (Request["Report"] == "GRANTINAID")
                    {
                        lblFanoFrom.Enabled = true;
                        lblFanoTo.Enabled = true;
                        txtFANoFrom.Text = Convert.ToString(1);
                        txtFaNoTo.Text = Convert.ToString(99999);
                        lblFileNo.Enabled = false;
                        txtFileNo.Enabled = false;
                        lblDateFrom.Enabled = false;
                        lblDateTo.Enabled = false;
                        DateFrom.Enabled = false;
                        DateTo.Enabled = false;
                        lblLetCode.Enabled = false;
                        txtLetCode.Enabled = false;
                        lblCatCode.Enabled = false;
                        txtCatCode.Enabled = false;
                        lblCons.Enabled = false;
                        txtCons.Enabled = false;
                    }
                    if (Request["Report"] == "Letter6treasury")
                    {
                        lblFanoFrom.Enabled = true;
                        lblFanoTo.Enabled = false;
                        txtFANoFrom.Enabled = true;
                        txtFaNoTo.Enabled = false;
                        lblFileNo.Enabled = true;
                        txtFileNo.Enabled = true;
                        lblDateFrom.Enabled = false;
                        lblDateTo.Enabled = false;
                        DateFrom.Enabled = false;
                        DateTo.Enabled = false;
                        lblLetCode.Enabled = false;
                        txtLetCode.Enabled = false;
                        lblCatCode.Enabled = false;
                        txtCatCode.Enabled = false;
                        lblCons.Enabled = false;
                        txtCons.Enabled = false;

                    }
                   
                    if (Request["Report"] == "ForHospital")
                    {
                        lblFanoFrom.Enabled = true;
                        lblFanoTo.Enabled = true;
                        txtFANoFrom.Text = Convert.ToString(1);
                        txtFaNoTo.Text = Convert.ToString(99999);
                        lblFileNo.Enabled = false;
                        txtFileNo.Enabled = false;
                        lblDateFrom.Enabled = false;
                        lblDateTo.Enabled = false;
                        DateFrom.Enabled = false;
                        DateTo.Enabled = false;
                        lblLetCode.Enabled = false;
                        txtLetCode.Enabled = false;
                        lblCatCode.Enabled = false;
                        txtCatCode.Enabled = false;
                        lblCons.Enabled = false;
                        txtCons.Enabled = false;
                    }
                    if (Request["Report"] == "Letter12")
                    {
                        lblFanoFrom.Enabled = true;
                        lblFanoTo.Enabled = true;
                        txtFANoFrom.Text = Convert.ToString(1);
                        txtFaNoTo.Text = Convert.ToString(99999);
                        lblFileNo.Enabled = false;
                        txtFileNo.Enabled = false;
                        lblDateFrom.Enabled = false;
                        lblDateTo.Enabled = false;
                        DateFrom.Enabled = false;
                        DateTo.Enabled = false;
                        lblLetCode.Enabled = false;
                        txtLetCode.Enabled = false;
                        lblCatCode.Enabled = false;
                        txtCatCode.Enabled = false;
                        lblCons.Enabled = false;
                        txtCons.Enabled = false;
                    }
                    if (Request["Report"] == "LetterforTreasury")
                    {
                        lblFanoFrom.Enabled = false;
                        lblFanoTo.Enabled = false;
                        txtFANoFrom.Enabled = false;
                        txtFaNoTo.Enabled = false;
                        lblFileNo.Enabled = true;
                        txtFileNo.Enabled = true;
                        lblDateFrom.Enabled = true;
                        lblDateTo.Enabled = true;
                        DateFrom.Enabled = true;
                        DateTo.Enabled = true;
                        lblLetCode.Enabled = false;
                        txtLetCode.Enabled = false;
                        lblCatCode.Enabled = false;
                        txtCatCode.Enabled = false;
                        lblCons.Enabled = false;
                        txtCons.Enabled = false;
                    }
                    if (Request["Report"] == "Note6")
                    {
                        lblFanoFrom.Enabled = true;
                        lblFanoTo.Enabled = true;
                        txtFANoFrom.Text = Convert.ToString(1);
                        txtFaNoTo.Text = Convert.ToString(99999);
                        lblFileNo.Enabled = false;
                        txtFileNo.Enabled = false;
                        lblDateFrom.Enabled = false;
                        lblDateTo.Enabled = false;
                        DateFrom.Enabled = false;
                        DateTo.Enabled = false;
                        lblLetCode.Enabled = false;
                        txtLetCode.Enabled = false;
                        lblCatCode.Enabled = false;
                        txtCatCode.Enabled = false;
                        lblCons.Enabled = false;
                        txtCons.Enabled = false;

                    }

                    if (Request["Report"] == "Note6Many")
                    {
                        lblFanoFrom.Enabled = false;
                        lblFanoTo.Enabled = false;
                        txtFANoFrom.Enabled = false;
                        txtFaNoTo.Enabled = false;
                        lblFileNo.Enabled = true;
                        txtFileNo.Enabled = true;
                        lblDateFrom.Enabled = true;
                        lblDateTo.Enabled = true;
                        DateFrom.Enabled = true;
                        DateTo.Enabled = true;
                        lblLetCode.Enabled = false;
                        txtLetCode.Enabled = false;
                        lblCatCode.Enabled = false;
                        txtCatCode.Enabled = false;
                        lblCons.Enabled = false;
                        txtCons.Enabled = false;
                    }


                    if (Request["Report"] == "Note7")
                    {
                        lblFanoFrom.Enabled = true;
                        lblFanoTo.Enabled = true;
                        txtFANoFrom.Text = Convert.ToString(1);
                        txtFaNoTo.Text = Convert.ToString(99999);
                        lblFileNo.Enabled = false;
                        txtFileNo.Enabled = false;
                        lblDateFrom.Enabled = false;
                        lblDateTo.Enabled = false;
                        DateFrom.Enabled = false;
                        DateTo.Enabled = false;
                        lblLetCode.Enabled = false;
                        txtLetCode.Enabled = false;
                        lblCatCode.Enabled = false;
                        txtCatCode.Enabled = false;
                        lblCons.Enabled = false;
                        txtCons.Enabled = false;
                    }

                    if (Request["Report"] == "Note7all")
                    {
                        lblFanoFrom.Enabled = false;
                        lblFanoTo.Enabled = false;
                        txtFANoFrom.Enabled = false;
                        txtFaNoTo.Enabled = false;
                        lblFileNo.Enabled = false;
                        txtFileNo.Enabled = false;
                        lblDateFrom.Enabled = true;
                        lblDateTo.Enabled = true;
                        DateFrom.Enabled = true;
                        DateTo.Enabled = true;
                        lblLetCode.Enabled = false;
                        txtLetCode.Enabled = false;
                        lblCatCode.Enabled = false;
                        txtCatCode.Enabled = false;
                        lblCons.Enabled = false;
                        txtCons.Enabled = false;
                    }
                    if (Request["Report"] == "RevalAdviceSingle")
                    {
                        lblFanoFrom.Enabled = true;
                        lblFanoTo.Enabled = false;
                        txtFANoFrom.Enabled = true;
                        txtFaNoTo.Enabled = false;
                        lblFileNo.Enabled = false;
                        txtFileNo.Enabled = false;
                        lblDateFrom.Enabled = false;
                        lblDateTo.Enabled = false;
                        DateFrom.Enabled = false;
                        DateTo.Enabled = false;
                        lblLetCode.Enabled = false;
                        txtLetCode.Enabled = false;
                        lblCatCode.Enabled = false;
                        txtCatCode.Enabled = false;
                        lblCons.Enabled = false;
                        txtCons.Enabled = false;
                    }
                    if (Request["Report"] == "RevalAdviceMulti")
                    {
                        lblFanoFrom.Enabled = false;
                        lblFanoTo.Enabled = false;
                        txtFANoFrom.Enabled = false;
                        txtFaNoTo.Enabled = false;
                        lblFileNo.Enabled = false;
                        txtFileNo.Enabled = false;
                        lblDateFrom.Enabled = true;
                        lblDateTo.Enabled = true;
                        DateFrom.Enabled = true;
                        DateTo.Enabled = true;
                        lblLetCode.Enabled = false;
                        txtLetCode.Enabled = false;
                        lblCatCode.Enabled = false;
                        txtCatCode.Enabled = false;
                        lblCons.Enabled = false;
                        txtCons.Enabled = false;
                    }



                    if (Request["Report"] == "Note17toASA")
                    {
                        lblFanoFrom.Enabled = true;
                        lblFanoTo.Enabled = true;
                        txtFANoFrom.Text = Convert.ToString(1);
                        txtFaNoTo.Text = Convert.ToString(99999);
                        lblFileNo.Enabled = false;
                        txtFileNo.Enabled = false;
                        lblDateFrom.Enabled = true;
                        lblDateTo.Enabled = true;
                        DateFrom.Enabled = true;
                        DateTo.Enabled = true;
                        lblLetCode.Enabled = false;
                        txtLetCode.Enabled = false;
                        lblCatCode.Enabled = false;
                        txtCatCode.Enabled = false;
                        lblCons.Enabled = false;
                        txtCons.Enabled = false;
                    }
                    if (Request["Report"] == "Note18toASA")
                    {
                        lblFanoFrom.Enabled = true;
                        lblFanoTo.Enabled = true;
                        txtFANoFrom.Text = Convert.ToString(1);
                        txtFaNoTo.Text = Convert.ToString(99999);
                        lblFileNo.Enabled = false;
                        txtFileNo.Enabled = false;
                        lblDateFrom.Enabled = true;
                        lblDateTo.Enabled = true;
                        DateFrom.Enabled = true;
                        DateTo.Enabled = true;
                        lblLetCode.Enabled = false;
                        txtLetCode.Enabled = false;
                        lblCatCode.Enabled = false;
                        txtCatCode.Enabled = false;
                        lblCons.Enabled = false;
                        txtCons.Enabled = false;

                    }
                    if (Request["Report"] == "Note19toASA")
                    {
                        lblFanoFrom.Enabled = true;
                        lblFanoTo.Enabled = true;
                        txtFANoFrom.Text = Convert.ToString(1);
                        txtFaNoTo.Text = Convert.ToString(99999);
                        lblFileNo.Enabled = false;
                        txtFileNo.Enabled = false;
                        lblDateFrom.Enabled = true;
                        lblDateTo.Enabled = true;
                        DateFrom.Enabled = true;
                        DateTo.Enabled = true;
                        lblLetCode.Enabled = false;
                        txtLetCode.Enabled = false;
                        lblCatCode.Enabled = false;
                        txtCatCode.Enabled = false;
                        lblCons.Enabled = false;
                        txtCons.Enabled = false;
                    }
                    if (Request["Report"] == "Note21toASA")
                    {
                        lblFanoFrom.Enabled = true;
                        lblFanoTo.Enabled = true;
                        txtFANoFrom.Text = Convert.ToString(1);
                        txtFaNoTo.Text = Convert.ToString(99999);
                        lblFileNo.Enabled = false;
                        txtFileNo.Enabled = false;
                        lblDateFrom.Enabled = false;
                        lblDateTo.Enabled = false;
                        DateFrom.Enabled = false;
                        DateTo.Enabled = false;
                        lblLetCode.Enabled = false;
                        txtLetCode.Enabled = false;
                        lblCatCode.Enabled = false;
                        txtCatCode.Enabled = false;
                        lblCons.Enabled = false;
                        txtCons.Enabled = false;

                    }
                    if (Request["Report"] == "NotetoSCM12")
                    {
                        lblFanoFrom.Enabled = true;
                        lblFanoTo.Enabled = true;
                        txtFANoFrom.Text = Convert.ToString(1);
                        txtFaNoTo.Text = Convert.ToString(99999);
                        lblFileNo.Enabled = false;
                        txtFileNo.Enabled = false;
                        lblDateFrom.Enabled = false;
                        lblDateTo.Enabled = false;
                        DateFrom.Enabled = false;
                        DateTo.Enabled = false;
                        lblLetCode.Enabled = false;
                        txtLetCode.Enabled = false;
                        lblCatCode.Enabled = false;
                        txtCatCode.Enabled = false;
                        lblCons.Enabled = false;
                        txtCons.Enabled = false;

                    }
                    if (Request["Report"] == "DGReport")
                    {
                        lblFanoFrom.Enabled = false;
                        lblFanoTo.Enabled = false;
                        txtFANoFrom.Enabled = false;
                        txtFaNoTo.Enabled = false;
                        lblFileNo.Enabled = false;
                        txtFileNo.Enabled = false;
                        lblDateFrom.Enabled = false;
                        lblDateTo.Enabled = false;
                        DateFrom.Enabled = false;
                        DateTo.Enabled = false;
                        lblLetCode.Enabled = false;
                        txtLetCode.Enabled = false;
                        lblCatCode.Enabled = false;
                        txtCatCode.Enabled = false;
                        lblCons.Enabled = false;
                        txtCons.Enabled = false;
                    }
                    if (Request["Report"] == "YearWise")
                    {
                        lblFanoFrom.Enabled = false;
                        lblFanoTo.Enabled = false;
                        txtFANoFrom.Enabled = false;
                        txtFaNoTo.Enabled = false;
                        lblFileNo.Enabled = false;
                        txtFileNo.Enabled = false;
                        lblDateFrom.Enabled = false;
                        lblDateTo.Enabled = false;
                        DateFrom.Enabled = false;
                        DateTo.Enabled = false;
                        lblLetCode.Enabled = false;
                        txtLetCode.Enabled = false;
                        lblCatCode.Enabled = false;
                        txtCatCode.Enabled = false;
                        lblCons.Enabled = false;
                        txtCons.Enabled = false;

                    }
                    if (Request["Report"] == "YearWiseDG")
                    {
                        lblFanoFrom.Enabled = false;
                        lblFanoTo.Enabled = false;
                        txtFANoFrom.Enabled = false;
                        txtFaNoTo.Enabled = false;
                        lblFileNo.Enabled = false;
                        txtFileNo.Enabled = false;
                        lblDateFrom.Enabled = false;
                        lblDateTo.Enabled = false;
                        DateFrom.Enabled = false;
                        DateTo.Enabled = false;
                        lblLetCode.Enabled = false;
                        txtLetCode.Enabled = false;
                        lblCatCode.Enabled = false;
                        txtCatCode.Enabled = false;
                        lblCons.Enabled = false;
                        txtCons.Enabled = false;

                    }

                    if (Request["Report"] == "LettertoDCO")
                    {
                        lblFanoFrom.Enabled = false;
                        lblFanoTo.Enabled = false;
                        txtFANoFrom.Enabled = false;
                        txtFaNoTo.Enabled = false;
                        lblFileNo.Enabled = false;
                        txtFileNo.Enabled = false;
                        lblDateFrom.Enabled = true;
                        lblDateTo.Enabled = true;
                        DateFrom.Enabled = true;
                        DateTo.Enabled = true;
                        lblLetCode.Enabled = true;
                        txtLetCode.Enabled = true;
                        lblCatCode.Enabled = false;
                        txtCatCode.Enabled = false;
                        lblCons.Enabled = false;
                        txtCons.Enabled = false;


                    }

          


                    if (Request["Report"] == "BaitLetter")
                    {
                        lblFanoFrom.Enabled = false;
                        lblFanoTo.Enabled = false;
                        txtFANoFrom.Enabled = false;
                        txtFaNoTo.Enabled = false;
                        lblFileNo.Enabled = false;
                        txtFileNo.Enabled = false;
                        lblDateFrom.Enabled = true;
                        lblDateTo.Enabled = true;
                        DateFrom.Enabled = true;
                        DateTo.Enabled = true;
                        lblLetCode.Enabled = false;
                        txtLetCode.Enabled = false;
                        lblCatCode.Enabled = false;
                        txtCatCode.Enabled = false;
                        lblCons.Enabled = false;
                        txtCons.Enabled = false;


                    }

                    if (Request["Report"] == "DGAnalysis1")
                    {
                        lblFanoFrom.Enabled = false;
                        lblFanoTo.Enabled = false;
                        txtFANoFrom.Enabled = false;
                        txtFaNoTo.Enabled = false;
                        lblFileNo.Enabled = false;
                        txtFileNo.Enabled = false;
                        lblDateFrom.Enabled = true;
                        lblDateTo.Enabled = true;
                        DateFrom.Enabled = true;
                        DateTo.Enabled = true;
                        lblLetCode.Enabled = false;
                        txtLetCode.Enabled = false;
                        lblCatCode.Enabled = false;
                        txtCatCode.Enabled = false;
                        lblCons.Enabled = false;
                        txtCons.Enabled = false;

                    }

                    if (Request["Report"] == "TimeLimitCase")
                    {
                        lblFanoFrom.Enabled = false;
                        lblFanoTo.Enabled = false;
                        txtFANoFrom.Enabled = false;
                        txtFaNoTo.Enabled = false;
                        lblFileNo.Enabled = false;
                        txtFileNo.Enabled = false;
                        lblDateFrom.Enabled = false;
                        lblDateTo.Enabled = false;
                        DateFrom.Enabled = false;
                        DateTo.Enabled = false;
                        lblLetCode.Enabled = false;
                        txtLetCode.Enabled = false;
                        lblCatCode.Enabled = false;
                        txtCatCode.Enabled = false;
                        lblCons.Enabled = false;
                        txtCons.Enabled = false;
                    }
                    if (Request["Report"] == "TargetCasesComplete")
                    {
                        lblFanoFrom.Enabled = false;
                        lblFanoTo.Enabled = false;
                        txtFANoFrom.Enabled = false;
                        txtFaNoTo.Enabled = false;
                        lblFileNo.Enabled = false;
                        txtFileNo.Enabled = false;
                        lblDateFrom.Enabled = false;
                        lblDateTo.Enabled = false;
                        DateFrom.Enabled = false;
                        DateTo.Enabled = false;
                        lblLetCode.Enabled = false;
                        txtLetCode.Enabled = false;
                        lblCatCode.Enabled = false;
                        txtCatCode.Enabled = false;
                        lblCons.Enabled = false;
                        txtCons.Enabled = false;
                    }
                    if (Request["Report"] == "DGAnalysis")
                    {
                        lblFanoFrom.Enabled = false;
                        lblFanoTo.Enabled = false;
                        txtFANoFrom.Enabled = false;
                        txtFaNoTo.Enabled = false;
                        lblFileNo.Enabled = false;
                        txtFileNo.Enabled = false;
                        lblDateFrom.Enabled = false;
                        lblDateTo.Enabled = false;
                        DateFrom.Enabled = false;
                        DateTo.Enabled = false;
                        lblLetCode.Enabled = false;
                        txtLetCode.Enabled = false;
                        lblCatCode.Enabled = false;
                        txtCatCode.Enabled = false;
                        lblCons.Enabled = false;
                        txtCons.Enabled = false;
                    }

                    if (Request["Report"] == "DGAnalysis2")
                    {
                        lblFanoFrom.Enabled = false;
                        lblFanoTo.Enabled = false;
                        txtFANoFrom.Enabled = false;
                        txtFaNoTo.Enabled = false;
                        lblFileNo.Enabled = false;
                        txtFileNo.Enabled = false;
                        lblDateFrom.Enabled = false;
                        lblDateTo.Enabled = false;
                        DateFrom.Enabled = false;
                        DateTo.Enabled = false;
                        lblLetCode.Enabled = false;
                        txtLetCode.Enabled = false;
                        lblCatCode.Enabled = false;
                        txtCatCode.Enabled = false;
                        lblCons.Enabled = false;
                        txtCons.Enabled = false;
                    }
                    if (Request["Report"] == "MPAWISEREPO")
                    {
                        lblFanoFrom.Enabled = false;
                        lblFanoTo.Enabled = false;
                        txtFANoFrom.Enabled = false;
                        txtFaNoTo.Enabled = false;
                        lblFileNo.Enabled = false;
                        txtFileNo.Enabled = false;
                        lblDateFrom.Enabled = false;
                        lblDateTo.Enabled = false;
                        DateFrom.Enabled = false;
                        DateTo.Enabled = false;
                        lblLetCode.Enabled = false;
                        txtLetCode.Enabled = false;
                        lblCatCode.Enabled = true;
                        txtCatCode.Enabled = true;
                        lblCons.Enabled = true;
                        txtCons.Enabled = true;
                    }
                    if (Request["Report"] == "MPAMNAWISEREPO")
                    {
                        lblFanoFrom.Enabled = false;
                        lblFanoTo.Enabled = false;
                        txtFANoFrom.Enabled = false;
                        txtFaNoTo.Enabled = false;
                        lblFileNo.Enabled = false;
                        txtFileNo.Enabled = false;
                        lblDateFrom.Enabled = false;
                        lblDateTo.Enabled = false;
                        DateFrom.Enabled = false;
                        DateTo.Enabled = false;
                        lblLetCode.Enabled = false;
                        txtLetCode.Enabled = false;
                        lblCatCode.Enabled = true;
                        txtCatCode.Enabled = true;
                        lblCons.Enabled = true;
                        txtCons.Enabled = true;
                    }
                    if (Request["Report"] == "DGYearWiseStatus")
                    {
                        lblFanoFrom.Enabled = false;
                        lblFanoTo.Enabled = false;
                        txtFANoFrom.Enabled = false;
                        txtFaNoTo.Enabled = false;
                        lblFileNo.Enabled = false;
                        txtFileNo.Enabled = false;
                        lblDateFrom.Enabled = false;
                        lblDateTo.Enabled = false;
                        DateFrom.Enabled = false;
                        DateTo.Enabled = false;
                        lblLetCode.Enabled = false;
                        txtLetCode.Enabled = false;
                        lblCatCode.Enabled = false;
                        txtCatCode.Enabled = false;
                        lblCons.Enabled = false;
                        txtCons.Enabled = false;
                    }

                    if (Request["Report"] == "SenttoHospital")
                    {
                        lblFanoFrom.Enabled = false;
                        lblFanoTo.Enabled = false;
                        txtFANoFrom.Enabled = false;
                        txtFaNoTo.Enabled = false;
                        lblFileNo.Enabled = true;
                        txtFileNo.Enabled = true;
                        lblDateFrom.Enabled = false;
                        lblDateTo.Enabled = false;
                        DateFrom.Enabled = false;
                        DateTo.Enabled = false;
                        lblLetCode.Enabled = false;
                        txtLetCode.Enabled = false;
                        lblCatCode.Enabled = false;
                        txtCatCode.Enabled = false;
                        lblCons.Enabled = false;
                        txtCons.Enabled = false;
                    }
                    if (Request["Report"] == "Receipt")
                    {
                        lblFanoFrom.Enabled = true;
                        lblFanoTo.Enabled = false;
                        txtFANoFrom.Enabled = true;
                        txtFaNoTo.Enabled = false;
                        lblFileNo.Enabled = false;
                        txtFileNo.Enabled = false;
                        lblDateFrom.Enabled = false;
                        lblDateTo.Enabled = false;
                        DateFrom.Enabled = false;
                        DateTo.Enabled = false;
                        lblLetCode.Enabled = false;
                        txtLetCode.Enabled = false;
                        lblCatCode.Enabled = false;
                        txtCatCode.Enabled = false;
                        lblCons.Enabled = false;
                        txtCons.Enabled = false;
                    }
                    if (Request["Report"] == "ChqReceipt")
                    {
                        lblFanoFrom.Enabled = false;
                        lblFanoTo.Enabled = false;
                        txtFANoFrom.Enabled = false;
                        txtFaNoTo.Enabled = false;
                        lblFileNo.Enabled = false;
                        txtFileNo.Enabled = false;
                        lblDateFrom.Enabled = true;
                        lblDateTo.Enabled = true;
                        DateFrom.Enabled = true;
                        DateTo.Enabled = true;
                        lblLetCode.Enabled = false;
                        txtLetCode.Enabled = false;
                        lblCatCode.Enabled = false;
                        txtCatCode.Enabled = false;
                        lblCons.Enabled = false;
                        txtCons.Enabled = false;
                    }
                    if (Request["Report"] == "MeetingReport")
                    {
                        lblFanoFrom.Enabled = false;
                        lblFanoTo.Enabled = false;
                        txtFANoFrom.Enabled = false;
                        txtFaNoTo.Enabled = false;
                        lblFileNo.Enabled = false;
                        txtFileNo.Enabled = false;
                        lblDateFrom.Enabled = true;
                        lblDateTo.Enabled = false;
                        DateFrom.Enabled = true;
                        DateTo.Enabled = false;
                        lblLetCode.Enabled = false;
                        txtLetCode.Enabled = false;
                        lblCatCode.Enabled = false;
                        txtCatCode.Enabled = false;
                        lblCons.Enabled = false;
                        txtCons.Enabled = false;
                    }
                if (Request["Report"] == "Note32toASA")
                {
                    lblFanoFrom.Enabled = true;
                    lblFanoTo.Enabled = true;
                    txtFANoFrom.Text = Convert.ToString(1);
                    txtFaNoTo.Text = Convert.ToString(99999);
                    lblFileNo.Enabled = false;
                    txtFileNo.Enabled = false;
                    lblDateFrom.Enabled = true;
                    lblDateTo.Enabled = true;
                    DateFrom.Enabled = true;
                    DateTo.Enabled = true;
                    lblLetCode.Enabled = false;
                    txtLetCode.Enabled = false;
                    lblCatCode.Enabled = false;
                    txtCatCode.Enabled = false;
                    lblCons.Enabled = false;
                    txtCons.Enabled = false;
                }


                // GetRptNames();
            }
        }


        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            DateTime Fromdate = Convert.ToDateTime(DateFrom.DateInput.SelectedDate);
            DateTime Todate = Convert.ToDateTime(DateTo.DateInput.SelectedDate);
            string FileNo = txtFileNo.Text;
            string mhosp = txtFileNo.Text;
            string fanoFrom = txtFANoFrom.Text;
            string fanoTo = txtFaNoTo.Text;
            string mfileno = txtFileNo.Text;
            string letcode = txtLetCode.Text;
            string mcatcode = txtCatCode.Text;
            string mcons = txtCons.Text;
            //string  = ddlDepartment.SelectedValue;
            string rpt = Request["Report"].Replace(" ", string.Empty); //ddlRepotsName.SelectedValue.Replace(" ", string.Empty);
            //string test = ddlReportsName.SelectedValue.Replace(" ", string.Empty);
            //txtName.Text = test;
            
            if (rpt == "MpaWise")
            {
                Response.Redirect("./ReportsPages/ReportMainPage.aspx?reportid=" + rpt);
            }
            if (rpt == "MPAWiseReport2")
            {
                Response.Redirect("./ReportsPages/ReportMainPage.aspx?reportid=" + rpt);
            }
            if (rpt == "Letter")
            {
                Response.Redirect("./ReportsPages/ReportMainPage.aspx?reportid=" + rpt + "&FromFANo=" + fanoFrom + "&ToFANo=" + fanoTo);
            }
           
            if (rpt == "GRANTINAID")
            {
                Response.Redirect("./ReportsPages/ReportMainPage.aspx?reportid=" + rpt + "&FromFANo=" + fanoFrom + "&ToFANo=" + fanoTo);
            }
            if (rpt == "Letter6treasury")
            {
                Response.Redirect("./ReportsPages/ReportMainPage.aspx?reportid=" + rpt + "&FANos=" + fanoFrom + "&mfileno=" + mfileno);
            }
         
            if (rpt == "ForHospital")
            {
                Response.Redirect("./ReportsPages/ReportMainPage.aspx?reportid=" + rpt + "&FromFANo=" + fanoFrom + "&ToFANo=" + fanoTo);
            }
            if (rpt == "Letter12")
            {
                Response.Redirect("./ReportsPages/ReportMainPage.aspx?reportid=" + rpt + "&FromFANo=" + fanoFrom + "&ToFANo=" + fanoTo);
            }
            if (rpt == "LetterforTreasury")
            {
                Response.Redirect("./ReportsPages/ReportMainPage.aspx?reportid=" + rpt + "&StartDate=" + Fromdate + "&EndDate=" + Todate + "&mfileno=" + mfileno);
            }
           
            if (rpt == "RevalAdviceSingle")
            {
                Response.Redirect("./ReportsPages/ReportMainPage.aspx?reportid=" + rpt + "&FromFANo="+fanoFrom);
            }
            if (rpt == "RevalAdviceMulti")
            {
                Response.Redirect("./ReportsPages/ReportMainPage.aspx?reportid=" + rpt + "&StartDate=" + Fromdate + "&EndDate=" + Todate);
            }

            if (rpt == "Note7")
            {
                Response.Redirect("./ReportsPages/ReportMainPage.aspx?reportid=" + rpt + "&FromFANo=" + fanoFrom + "&ToFANo=" + fanoTo);
            }
            if (rpt == "Note7all")
            {
                Response.Redirect("./ReportsPages/ReportMainPage.aspx?reportid=" + rpt + "&StartDate=" + Fromdate + "&EndDate=" + Todate);
            }

            if (rpt == "Note17toASA")
            {
                Response.Redirect("./ReportsPages/ReportMainPage.aspx?reportid=" + rpt + "&FromFANo=" + fanoFrom + "&ToFANo=" + fanoTo + "&StartDate=" + Fromdate + "&EndDate=" + Todate);
            }
            if (rpt == "Note18toASA")
            {
                Response.Redirect("./ReportsPages/ReportMainPage.aspx?reportid=" + rpt + "&FromFANo=" + fanoFrom + "&ToFANo=" + fanoTo + "&StartDate=" + Fromdate + "&EndDate=" + Todate);
            }
            if (rpt == "Note19toASA")
            {
                Response.Redirect("./ReportsPages/ReportMainPage.aspx?reportid=" + rpt + "&FromFANo=" + fanoFrom + "&ToFANo=" + fanoTo + "&StartDate=" + Fromdate + "&EndDate=" + Todate);
            }
            if (rpt == "Note21toASA")
            {
                Response.Redirect("./ReportsPages/ReportMainPage.aspx?reportid=" + rpt + "&FromFANo=" + fanoFrom + "&ToFANo=" + fanoTo);
            }
            if (rpt == "NotetoSCM12")
            {
                Response.Redirect("./ReportsPages/ReportMainPage.aspx?reportid=" + rpt + "&FromFANo=" + fanoFrom + "&ToFANo=" + fanoTo);
            }
            if (rpt == "DGReport")
            {
                Response.Redirect("./ReportsPages/ReportMainPage.aspx?reportid=" + rpt);
            }
            if (rpt == "YearWise")
            {
                Response.Redirect("./ReportsPages/ReportMainPage.aspx?reportid=" + rpt);
            }
            if (rpt == "YearWiseDG")
            {
                Response.Redirect("./ReportsPages/ReportMainPage.aspx?reportid=" + rpt);
            }
            if (rpt == "LettertoDCO")
            {
                Response.Redirect("./ReportsPages/ReportMainPage.aspx?reportid=" + rpt + "&StartDate=" + Fromdate + "&EndDate=" + Todate + "&LetCode="+ letcode);
                
            }
            //if (rpt == "LetterDCOSingle")
            //{
            //    Response.Redirect("./ReportsPages/ReportMainPage.aspx?reportid=" + rpt + "&FANo=" + fanoFrom + "&mFileNo=" + mfileno);

            //}

            if (rpt == "BaitLetter")
            {
                Response.Redirect("./ReportsPages/ReportMainPage.aspx?reportid=" + rpt + "&StartDate=" + Fromdate + "&EndDate=" + Todate);

            }

            if (rpt == "DGAnalysis1")
            {
                Response.Redirect("./ReportsPages/ReportMainPage.aspx?reportid=" + rpt + "&StartDate=" + Fromdate + "&EndDate=" + Todate);

            }

            if (rpt == "TimeLimitCase")
            {
                Response.Redirect("./ReportsPages/ReportMainPage.aspx?reportid=" + rpt);

            }
            if (rpt == "TargetCasesComplete")
            {
                Response.Redirect("./ReportsPages/ReportMainPage.aspx?reportid=" + rpt);

            }
                        
            if (rpt == "DGAnalysis")
            {
                Response.Redirect("./ReportsPages/ReportMainPage.aspx?reportid=" + rpt);

            }
            if (rpt == "DGAnalysis2")
            {
                Response.Redirect("./ReportsPages/ReportMainPage.aspx?reportid=" + rpt);

            }
            if (rpt == "SenttoHospital")
            {
                Response.Redirect("./ReportsPages/ReportMainPage.aspx?reportid=" + rpt + "&Hosp=" + mhosp);

            }
            if (rpt == "MPAWISEREPO")
            {
                Response.Redirect("./ReportsPages/ReportMainPage.aspx?reportid=" + rpt + "&CatCode=" + mcatcode + "&Cons=" + mcons);

            }
            if (rpt == "MedicalBoard")
            {
                Response.Redirect("./ReportsPages/ReportMainPage.aspx?reportid=" + rpt + "&FromFANo=" + fanoFrom + "&ToFANo=" + fanoTo);

            }
            if (rpt == "Receipt")
            {
                Response.Redirect("./ReportsPages/ReportMainPage.aspx?reportid=" + rpt + "&FromFANo=" + fanoFrom);
            }
            if (rpt == "ChqReceipt")
            {
                Response.Redirect("./ReportsPages/ReportMainPage.aspx?reportid=" + rpt + "&StartDate=" + Fromdate + "&EndDate=" + Todate);
            }
            if (rpt == "MeetingReport")
            {
                Response.Redirect("./ReportsPages/ReportMainPage.aspx?reportid=" + rpt + "&StartDate=" + Fromdate);
            }
            if (rpt == "Note32toASA")
            {
                Response.Redirect("./ReportsPages/ReportMainPage.aspx?reportid=" + rpt + "&FromFANo=" + fanoFrom + "&ToFANo=" + fanoTo + "&StartDate=" + Fromdate + "&EndDate=" + Todate);
            }
        }
    }
}
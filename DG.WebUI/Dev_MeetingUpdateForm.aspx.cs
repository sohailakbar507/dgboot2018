﻿using DGModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DG.WebUI
{
    public partial class Dev_MeetingUpdateForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                UploadData();
                //NoEdit();
               txtMeetingId.Text = getMeetingId().ToString();
                if (Session["officer"] != null)
                {
                    Cls_Login login = (Cls_Login)Session["officer"];
                    UploadStatus(login.OfficerName, login.Type);
                }
                else
                {
                    Response.Redirect("/dgpublish/LoginPage.aspx");
                }
            }

        }

        public void UploadStatus(string user, string type)
        {
            cmsadbEntities ent = new cmsadbEntities();

            if (type == "officer")
            {
                var chk = (from a in ent.cmsadb_District where a.Password == user select a).ToList();
                if (chk.Count() > 0)
                {
                    ddldistricts.DataSource = chk;
                    ddldistricts.DataTextField = "DistrictName";
                    ddldistricts.DataValueField = "DistrictId";
                    ddldistricts.DataBind();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Alert", "alert('You are not authorized here ..');", true);
                    Response.Redirect("/dgpublish/LoginPage.aspx");
                }
            }


            if (type == "district")
            {
                var chk = (from a in ent.cmsadb_District where a.DistUserName == user select a).ToList();
                if (chk.Count() > 0)
                {
                    ddldistricts.DataSource = chk;
                    ddldistricts.DataTextField = "DistrictName";
                    ddldistricts.DataValueField = "DistrictId";
                    ddldistricts.DataBind();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Alert", "alert('You are not authorized here ..');", true);
                    Response.Redirect("/dgpublish/LoginPage.aspx");
                }
            }
        }
        private int getMeetingId()
        {
            DGEntities ent = new DGEntities();
            var id = ent.Dev_MeetingUpdate.Max(x => x.MeetingNumber);
            if (id > 0)
                return id+1??0;
            else
                return 1;
        }

        private void NoEdit()
        {
            txtMeetingId.Enabled = false;
            txtfilename.Enabled = false;
            dtMeetingDate.Enabled = false;
        }

        private void UploadData()
        {
            DGEntities DG = new DGEntities();
        }

        protected void lnkbtnSave_Click(object sender, EventArgs e)
        {
            DGEntities dg = new DGEntities();
                        
            if (FileReceiptUpload.HasFile)
            {
                foreach (HttpPostedFile file in FileReceiptUpload.PostedFiles)
                {
                    file.SaveAs(Server.MapPath("~/MeetingUploadFiles/" + txtMeetingId.Text + file.FileName));
                    Dev_MeetingUpdate dev = new Dev_MeetingUpdate();
                    dev.MeetingDate = dtMeetingDate.SelectedDate;
                    dev.MeetingNumber = Convert.ToInt32(txtMeetingId.Text);
                    dev.FileName = txtMeetingId.Text + file.FileName;
                    dev.FilePath = "MeetingUploadFiles/" + txtMeetingId.Text +"-"+ file.FileName;
                    dev.UpdatedOn = Convert.ToDateTime(DateTime.Now.ToShortDateString());
                    Cls_Login login = (Cls_Login)Session["officer"];
                    dev.OfficerName = login.OfficerName;
                    dev.DistrictName = ddldistricts.Text;
                    dev.DistrictId = Convert.ToInt32(ddldistricts.SelectedValue);
                    dg.Dev_MeetingUpdate.Add(dev);
                    dg.SaveChanges();
                    txtfilename.Text = dev.FileName;
                    ScriptManager.RegisterStartupScript(this, GetType(), "Alert", "alert('File uploaded and Saved Successfully ..');", true);
                }
            }

            //try
            //{

            //    System.Threading.Thread.Sleep(500);
            //    int meetingid = Convert.ToInt32(Request["Meeting_Id"]);
            //    var getfno = from h in dg.Dev_MeetingUpdate where h.Meeting_Id == meetingid select h;

            //    if (getfno.Count() > 0)
            //    {
            //        var h = getfno.First();
            //        h.MeetingDate = dtMeetingDate.DateInput.SelectedDate;
            //        h.UpdatedOn = DateTime.Now;                               
            //        dg.SaveChanges();
            //        //Cls_Login login = (Cls_Login)Session["Login"];
            //        ScriptManager.RegisterStartupScript(this, GetType(), "Alert", "alert('Changes Saved Successfully ..');", true);
            //    }


            //}

            //catch (Exception ex)
            //{
            //    ScriptManager.RegisterStartupScript(this, GetType(), "Alert", "alert('" + ex.Message.ToString() + "');", true);
            //}
        }

        protected void lnkbtnCancel_Click(object sender, EventArgs e)
        {

        }
    }

    
}
//}
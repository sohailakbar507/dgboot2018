﻿using DGModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DG.WebUI
{
    public partial class ApprovedForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Login"] == null)
                {
                    Response.Redirect("~/LoginPage.aspx");
                }

                txtfind.Focus();
                NoEdit();
            }
            lnkbtnSave.Attributes.Add("onclick", "return confirm('Do you want to Save this record[Yes / No]?');");

        }
        private void NoEdit()
        {            
        }
        protected void lnkBtnSearch_Click(object sender, EventArgs e)
        {
            GetRecord();
        }
        private void GetRecord()
        {
            DGEntities ent = new DGEntities();
            int fanos = Convert.ToInt32(txtfind.Text);
            var getfano = from a in ent.Approveds where a.fanoApproved == fanos select a;
            var getfanom = from m in ent.MAINCORRs where m.fano == fanos select m;
            
            if (getfano.Count() > 0)
            {
                var meetinginfo = getfano.First();
                txtFano.Text = meetinginfo.fanoApproved.ToString();
                txtApproved.Text = meetinginfo.Approved1.ToString();
                txtPayable.Text = meetinginfo.Payable.ToString();
                txtIssued.Text = meetinginfo.Issued.ToString();
                txtPayable.Text = meetinginfo.Payable.ToString();
                txtRemarks.Text = meetinginfo.Remarks;                
           }
            else
            {
                if (getfanom.Count() > 0)
                {
                    var maininfo = getfanom.First();
                    txtFano.Text = Convert.ToInt32(maininfo.fano).ToString();
                    txtAppname.Text = maininfo.appname;
                    txtAddress.Text = maininfo.address;
                    txtIssued.Text = Convert.ToInt32(maininfo.amount).ToString();
                    txtrefno.Text = maininfo.refno;
                    //refdate.DateInput.SelectedDate = maininfo.Sancdate;
                    refdate.DateInput.DisplayText = maininfo.Sancdate.ToString();
                    txtFyear.Text = maininfo.fyear;
                    txtRemarks.Text = maininfo.remarks;
                }
            }
        }

        protected void lnkbtnSave_Click(object sender, EventArgs e)
        {
            DGEntities dg = new DGEntities();
            int fanos = Convert.ToInt32(txtfind.Text);
            var getfano = from a in dg.Approveds where a.fanoApproved == fanos select a;
            if (getfano.Count() > 0)
            {
                var m = getfano.First();
                //m.fanoApproved = Convert.ToInt32(txtFano.Text);
                m.Approved1 = Convert.ToInt32(txtApproved.Text);
                m.Payable = Convert.ToInt32(txtPayable.Text == string.Empty ? 0 : Convert.ToInt32(this.txtPayable.Text));
                m.Remarks = txtRemarks.Text;  
                dg.SaveChanges();
            }
            else
            {
                Approved m = new Approved();
                m.fanoApproved = Convert.ToInt32(txtFano.Text);
                m.Approved1 = Convert.ToInt32(txtApproved.Text == "" ? "0" : this.txtApproved.Text);
                m.Payable = Convert.ToInt32(txtPayable.Text == "" ? "0" : this.txtPayable.Text);
                m.Issued = Convert.ToInt32(txtIssued.Text == "" ? "0" : this.txtIssued.Text);
                m.SanctionDate = refdate.DateInput.SelectedDate;
                m.Remarks = txtRemarks.Text;
                dg.Approveds.Add(m);
                dg.SaveChanges();
            }

        }

        protected void lnkbtnCancel_Click(object sender, EventArgs e)
        {

        }
    }
}
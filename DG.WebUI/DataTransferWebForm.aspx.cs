﻿using Aspose.Cells;
using DGModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DG.WebUI
{
    public partial class DataTransferWebForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LicenseHelper.ModifyInMemory.ActivateMemoryPatching();
            }
        }

        protected void btnExcel_Click(object sender, EventArgs e)
        {
            DGEntities dg = new DGEntities();
            int frmno = Convert.ToInt32(txtfanofrom.Text);
            int tofano = Convert.ToInt32(txtfanoto.Text);

            var result = dg.datadgtransfer(frmno, tofano).ToList();

            Workbook workbook = new Workbook();
            Worksheet sheet = workbook.Worksheets[0];
            //sheet.Cells.ImportCustomObjects(result.ToArray(), new string[] { "fano", "refdate", "pageno", "cons", "catcode", "appname", "subjcode", "address", "subject", "targetdate", "catlet", "deptcode", "subdept", "offcode", "cataid", "catcs", "catas", "mrefno" }, true, 0, 0, result.Count, true, "dd/mm/yyyy", false);
            sheet.Cells.ImportCustomObjects(result.ToArray(), new string[] {
                "fano",
                "refdate",
                "pageno",
                "cons",
                "catcode",
                "appname",
                "subjcode",
               // "address",
                "subject",
                "targetdate",
                "catlet",
                "deptid",
                "subdept",
                "offcode",
                "cataid",
                "catcs",
                "catas",
                "mrefno"
            }, true, 0, 0, result.Count, true, "dd/mm/yyyy", false);
            sheet.AutoFitColumns();
            workbook.Save(this.Response, "JamalData.xls", ContentDisposition.Attachment, new XlsSaveOptions(SaveFormat.Excel97To2003));
            Response.End();


        }
    }
}
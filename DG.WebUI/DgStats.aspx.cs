﻿using DGModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DG.WebUI
{
    public partial class DgStats : System.Web.UI.Page
    {
        private const string DGDetailedList =    "DG Detailed List";       
        private const string StatsCategoryWise = "Stats Category Wise";
        private const string StatsDeptWise =     "Stats DeptWise";
        private const string StatsMPAWise =      "Stats MPA Wise";
        private const string StatsOverall =      "Stats Overall";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Login"] == null)
                {
                    Session["LoginMessage"] = "You are not properly logged in.";
                    Response.Redirect("~/LoginPage.aspx");
                }
                List<string> items = new List<string>();
                items.Add(DGDetailedList);               
                items.Add(StatsCategoryWise);
                items.Add(StatsDeptWise);
                items.Add(StatsMPAWise);
                items.Add(StatsOverall);
                rblReportCategories.DataSource = items;
                rblReportCategories.DataBind();
                RadDatePicker1.SelectedDate = DateTime.Now;
                RadDatePicker2.SelectedDate = DateTime.Now;
            }
        }

        protected void rblReportCategories_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (((System.Web.UI.WebControls.RadioButtonList)sender).SelectedValue)
            {
                case "DG Detailed List":
                    Response.Redirect("STIReportsView.aspx?reportid=DGDetailedList&fromdate="+RadDatePicker1.DateInput.SelectedDate+"&todate="+RadDatePicker2.DateInput.SelectedDate);
                    break;                            
                case "DG HEALTH STATS":
                    Response.Redirect("STIReportsView.aspx?reportid=DGHEALTHCOMPLETESTATS");
                    break;
                case "Stats Category Wise":
                    Response.Redirect("STIReportsView.aspx?reportid=StatsCategoryWise");
                    break;
                case "Stats DeptWise":
                    Response.Redirect("STIReportsView.aspx?reportid=StatsDeptWise");
                    break;               
                case "Stats MPA Wise":
                    Response.Redirect("STIReportsView.aspx?reportid=StatsMPAWise");
                    break;
                case "Stats Overall":
                    Response.Redirect("STIReportsView.aspx?reportid=StatsOverall");
                    break;

            }
        }
    }
}
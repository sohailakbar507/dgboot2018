﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DGMasterPage.Master" AutoEventWireup="true" CodeBehind="ChangePassword.aspx.cs" Inherits="DG.WebUI.ChangePassword" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<h1 align = "center"> Change Password </h1>
<table style="border:1px dashed white;border-collapse:collapse;" align="center" width="600" cellpadding="2" cellspacing="1">
    
    <tr>
        <td>
            User Name
        </td>
        <td>
            <asp:TextBox ID="txtUserName" runat="server" CssClass="form-control" Width="220px"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            Old Password
        </td>
        <td>
            <asp:TextBox ID="txtOldPassword" runat="server" CssClass="form-control" Width="220px" 
                TextMode="Password"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            New Password
        </td>
        <td>
            <asp:TextBox ID="txtNewPassword" runat="server" CssClass="form-control" Width = "220px" 
                TextMode="Password"></asp:TextBox>
            <br />
            <br />
        </td>

    </tr>
    <tr>
        <td align = "center" colspan = "2">
            <asp:Button ID="Change" runat="server" Text="Change Password" CssClass="btn btn-primary btn-lg"
                onclick="Change_Click" />
        </td>
    </tr>
    
</table>

</asp:Content>

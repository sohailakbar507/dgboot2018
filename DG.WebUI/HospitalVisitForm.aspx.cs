﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DG.WebUI
{
    public partial class HospitalVisitForm : System.Web.UI.Page
    {
        private const string StatsOverall = "STATS OVERALL";
        private const string DetailedList = "DETAILED LIST";
        private const string SearchForm = "SEARCHING FORM";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Login"] == null)
                {
                    Session["LoginMessage"] = "You are Not Properly Logged in:  ";
                    Response.Redirect("~/LoginPage.aspx");
                }
                List<string> items = new List<string>();
                items.Add(StatsOverall);
                items.Add(DetailedList);
                items.Add(SearchForm);
                rblReportCategories.DataSource = items;
                rblReportCategories.DataBind();
                
                //RadDatePicker1.SelectedDate = DateTime.Now;
                //RadDatePicker2.SelectedDate = DateTime.Now;
            }

        }
        protected void rblReportCategories_SelectedIndexChanged(object sender, EventArgs e)
        {
            //int hspid = Convert.ToInt32(txtHospId.Text);
            string report = Request["Report"];
            switch (((System.Web.UI.WebControls.RadioButtonList)sender).SelectedValue)
            {                
                case "STATS OVERALL":
                    Response.Redirect("HospitalsStatsPage.aspx?reportid=StatsOverall");
                    break;
                case "DETAILED LIST":
                    Response.Redirect("HospitalsStatsPage.aspx?reportid=DetailedList");
                    break;
                case "SEARCHING FORM":
                    Response.Redirect("HospitalSearchingForm.aspx");
                    break;

            }
        }
    }
}
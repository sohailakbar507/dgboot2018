﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DGMasterPage.Master" AutoEventWireup="true" CodeBehind="DGSearching.aspx.cs" Inherits="DG.WebUI.DGSearching" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
               
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    
                            <div class="dataTable_wrapper">

         <asp:GridView ID="DgSearch" runat="server" AllowPaging="true" PageSize="20"
            AutoGenerateColumns="False"   
                    onrowupdating="dgvDG_RowUpdating" DataKeyNames="DGId" 
                    CssClass="table table-striped table-bordered table-hover" onprerender="DgSearch_PreRender" 
                                    onrowcommand="DgSearch_RowCommand">
           
               <Columns>
                   
                   <asp:TemplateField>
                        <ItemTemplate>
                            <asp:CheckBox ID = "cbRows" runat = "server" />
                        </ItemTemplate>
                   </asp:TemplateField>
                   
                   <asp:TemplateField HeaderText="DGId" Visible="False">
                   <ItemTemplate>
                       <asp:Label ID="lblDGId" runat="server" Text= '<%#Eval("DGId")%>'></asp:Label>
                   </ItemTemplate>
                   
                   </asp:TemplateField>
                   
                   
                   <asp:TemplateField HeaderText="FANo.">
                   <ItemTemplate>
                       <asp:Label ID="lblFaNo" runat="server" Text='<%#Eval("fano")%>'></asp:Label>
                   </ItemTemplate>
                   </asp:TemplateField>
                   
                   <asp:TemplateField HeaderText="Refdate">
                   <ItemTemplate>
                       <asp:Label ID="lblRefdate" runat="server" Text='<%#Eval("refdate","{0:dd/MM/yyyy}") %>'></asp:Label>
                   </ItemTemplate>
                       
                       <ControlStyle Width="80px" />
                       
                   </asp:TemplateField>
                   
                   <asp:TemplateField HeaderText="Applicant Name">
                   <ItemTemplate>
                       <asp:Label ID="lblAppname" runat="server" Text='<%#Eval("appname")%>'></asp:Label>
                   </ItemTemplate>
                   
                       <ControlStyle Width="120px" />
                   
                   </asp:TemplateField>
                   
                   <asp:TemplateField HeaderText="Address">
                   <ItemTemplate>
                       <asp:Label ID = "lblAddress" runat="server" Text='<%#Eval("address")%>'></asp:Label>
                       
                   </ItemTemplate>
                       <ControlStyle Width="200px" />
                   </asp:TemplateField>
                   
                   <asp:TemplateField HeaderText="CNIC">
                       <ItemTemplate>
                           <asp:Label ID="lblCNIC" runat="server" Text='<%#Eval("CNIC")%>'></asp:Label>
                       </ItemTemplate>
                   </asp:TemplateField>
                   
                   
                   <asp:TemplateField HeaderText="Cheque No.">
                       <ItemTemplate>
                           <asp:Label ID="lblChequeno" runat="server" Text='<%#Eval("chqno")%>'></asp:Label>
                       </ItemTemplate>
                   </asp:TemplateField>

                   <asp:TemplateField HeaderText="Cheque Date">
                   <ItemTemplate>
                       <asp:Label ID="lblChqDate" runat="server" Text='<%#Eval("chqdate","{0:dd/MM/yyyy}") %>'></asp:Label>
                   </ItemTemplate>
                   </asp:TemplateField>

                   
                   <asp:TemplateField HeaderText="Amount">
                   
                     <ItemTemplate>
                         <asp:Label ID="lblAmount" runat="server" Text='<%#Eval("Amount")%>'></asp:Label>
                       </ItemTemplate>
                   
                   
                   </asp:TemplateField>

                   
                   <asp:TemplateField HeaderText="Department Name">
                   <ItemTemplate>
                       <asp:Label ID="lblDeptName" runat="server" Text='<%#Eval("deptname") %>'></asp:Label>
                   </ItemTemplate>
                   <ItemStyle HorizontalAlign = "Center" />
                   </asp:TemplateField>
                   
                                  
                   <asp:TemplateField HeaderText="Status">
                   <ItemTemplate>
            
            
                       <asp:Label ID="lblStatus" runat="server" Text='<%#Eval("Description") %>'></asp:Label>
                   </ItemTemplate>
                   <ItemStyle HorizontalAlign = "Center" />
                   </asp:TemplateField>
                   
                   <asp:TemplateField HeaderText="Edit">
                   <ItemTemplate>
                       <asp:LinkButton ID="LnkBtnEdit" CommandName = "update" runat="server"><img src = "Images/Text-Edit-icon.png" alt = "" /> </asp:LinkButton>
                   </ItemTemplate>
                   </asp:TemplateField>
                   
                   
               </Columns>
                

        </asp:GridView>
                                

                                        </div>
                                        </div>
                                        </div>
                                        
                                        
                                        
</asp:Content>

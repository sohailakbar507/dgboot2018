﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DG.WebUI
{
    public partial class DGHealthPage : System.Web.UI.Page
    {
        private const string DGDetailedListGRANT = "DG Detailed List GRANT";
        private const string DGHEALTHSTATS = "DG HEALTH STATS";
        private const string DGHEALTHSTATSTHISTENURE = "DG HEALTH STATS THIS TENURE";
        private const string DGHealthApproved = "DG Health Amount Issued/Approved";
        private const string DGHealthApproved2 = "DG Health Part II Amount Issued/Approved";
        private const string StatsDiseasesWise = "Stats Diseases Wise";
        private const string StatsHospitalWise = "Stats Hospital Wise";
       protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Login"] == null)
                {
                    Session["LoginMessage"] = "You are not properly logged in.";
                    Response.Redirect("~/LoginPage.aspx");
                }

                List<string> items = new List<string>();
                items.Add(DGDetailedListGRANT);
                items.Add(DGHEALTHSTATS);
                items.Add(DGHEALTHSTATSTHISTENURE);
                items.Add(DGHealthApproved);
                items.Add(DGHealthApproved2);
                items.Add(StatsDiseasesWise);
                items.Add(StatsHospitalWise);
                rblReportCategories.DataSource = items;
                rblReportCategories.DataBind();
                RadDatePicker1.SelectedDate = DateTime.Now;
                RadDatePicker2.SelectedDate = DateTime.Now;
                
            }


        }
        protected void rblReportCategories_SelectedIndexChanged(object sender, EventArgs e)
        {
            var mfyear = txtfyear.Text;
            switch (((System.Web.UI.WebControls.RadioButtonList)sender).SelectedValue)
            {
                case "DG Detailed List GRANT":
                    Response.Redirect("STIReportsView.aspx?reportid=DGDetailedListGRANT&mfromdate=" + RadDatePicker1.DateInput.SelectedDate + "&mtodate=" + RadDatePicker2.DateInput.SelectedDate);
                    break;
                case "DG Health Amount Issued/Approved":
                    Response.Redirect("STIReportsView.aspx?reportid=DGHealthApproved&fromdate=" + RadDatePicker1.DateInput.SelectedDate + "&todate=" + RadDatePicker2.DateInput.SelectedDate);
                    break;
                case "DG Health Part II Amount Issued/Approved":
                    Response.Redirect("STIReportsView.aspx?reportid=DGHealthApproved2&fromdate=" + RadDatePicker1.DateInput.SelectedDate + "&todate=" + RadDatePicker2.DateInput.SelectedDate);
                    break;
                case "DG HEALTH STATS":
                    Response.Redirect("STIReportsView.aspx?reportid=DGHEALTHCOMPLETESTATS&fy=" + mfyear);
                    break;
                case "DG HEALTH STATS THIS TENURE":
                    Response.Redirect("STIReportsView.aspx?reportid=DGHEALTHSTATSTHISTENURE");
                    break;
                case "Stats Diseases Wise":
                    Response.Redirect("STIReportsView.aspx?reportid=StatsDiseasesWise");
                    break;
                case "Stats Hospital Wise":
                    Response.Redirect("STIReportsView.aspx?reportid=StatsHospitalWise");
                    break;
              

            }
        }
    }
}
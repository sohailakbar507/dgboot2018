﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DGModel;

namespace DG.WebUI
{
    public partial class SearchingWebForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["login"] != null)
            {

                Cls_Login val = (Cls_Login)Session["login"];
                if (val.Isdel == false)
                {
                    lnkBtnDelete.Enabled = false;
                }

            }
            if (!IsPostBack)
            {
                LoadGrid();
                //lblbyName.Visible = false;
                txtbyName.Visible = false;
                txtAddress.Visible = false;
                txtName.Visible = false;
                //lblname.Visible = false;
                lblAddress.Visible = false;
                lblFaNo.Visible = true;                
                // dgvDG.Visible = false;
            }
            
        }
        private void LoadGrid()
        {
            DGEntities dgent = new DGEntities();

            var dglist = (from a in dgent.MAINCORRs
                          from b in dgent.officers
                          from d in dgent.DEPTs
                          from s in dgent.status
                          where a.offcode == b.OffCode && a.deptcode == d.deptcode && a.statcode == s.StatusCode && a.fano>0
                          orderby a.fano
                          select new { a.DGId, a.fano, a.refdate, a.appname,a.contactno, a.PatientName, a.address, a.subject, a.cnic, a.chqno, a.chqdate, a.amount, b.Name, d.deptname, s.Description }).ToList();

            dgvDG.DataSource = dglist;
            dgvDG.DataBind();
            Session["dtData"] = dglist;
        }

        protected void dgvDG_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            dgvDG.PageIndex = e.NewPageIndex;
            dgvDG.DataSource = Session["dtData"];
            dgvDG.DataBind();           
        }
        protected void dgvDG_SelectedIndexChanged(object sender, EventArgs e)
        {
        }
        protected void dgvDG_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
        }
        protected void lnkBtnSearch_Click(object sender, EventArgs e)
        {
            DGEntities dgent = new DGEntities();

            if (RadioButtonList1.SelectedItem.Value == "rdoFaNowise")
            {
                if (txtFaNo.Text != "")
                {
                    int fano = Convert.ToInt32(txtFaNo.Text);
                    var fanowise = dgent.SEARCHFANOWISE(fano);
                    Session["dtData"] = fanowise;
                    dgvDG.DataSource = fanowise;
                    dgvDG.DataBind();
                }
            }
            if (RadioButtonList1.SelectedItem.Value == "rdoSearchbyName")
            {
                if (txtbyName.Text != "")
                {

                    string name = Convert.ToString(txtbyName.Text);
                    var byname = dgent.SEARCHBYNAME(name);
                    Session["dtData"] = byname;
                    dgvDG.DataSource = byname;
                    dgvDG.DataBind();
                }
            }
            if (RadioButtonList1.SelectedItem.Value == "rdoSearchbyPtName")
            {
                if (txtbyName.Text != "")
                {

                    string name = Convert.ToString(txtbyName.Text);
                    var byname = dgent.SEARCHBYPATIENTNAME(name);
                    Session["dtData"] = byname;
                    dgvDG.DataSource = byname;
                    dgvDG.DataBind();
                }
            }
            if (RadioButtonList1.SelectedItem.Value == "rdoSearchbySubject")
            {
                if (txtbyName.Text != "")
                {
                    string subject = Convert.ToString(txtbyName.Text);
                    var bysubject = dgent.SEARCHBYSUBJECT(subject);
                    Session["dtData"] = bysubject;
                    dgvDG.DataSource = bysubject;
                    dgvDG.DataBind();
                }
            }

            if (RadioButtonList1.SelectedItem.Value == "rdochqwise")
            {
                if (txtbyName.Text != null)
                {
                    string chqno = Convert.ToString(txtbyName.Text);
                    var chqwise = dgent.SEARCHBYCHQNO(chqno);
                    Session["dtData"] = chqwise;
                    dgvDG.DataSource = chqwise;
                    dgvDG.DataBind();
                }
            }

            if (RadioButtonList1.SelectedItem.Value == "rdocnicwise")

            {
                if (txtbyName != null)
                {
                    string cnic = Convert.ToString(txtbyName.Text);
                    var cnicwise = dgent.SEARCHBYCNIC(cnic);
                    Session["dtData"] = cnicwise;
                    dgvDG.DataSource = cnicwise;
                    dgvDG.DataBind();
                }
            }
            if (RadioButtonList1.SelectedItem.Value == "rdoNameandAddress")
            {
                if (txtName != null && txtAddress != null)
                {
                    string name = Convert.ToString(txtName.Text);
                    string address = Convert.ToString(txtAddress.Text);
                    var nameaddress = dgent.SEARCHBYNAMEANDADDRESS(name, address);
                    Session["dtData"] = nameaddress;
                    dgvDG.DataSource = nameaddress;
                    dgvDG.DataBind();
                }
            }
            if(RadioButtonList1.SelectedItem.Value == "rdocontactnowise")
            {
                if(txtFaNo != null)
                {
                    string contactnumber = Convert.ToString(txtFaNo.Text);
                    var contact = dgent.SearchContactNumberWise(contactnumber);
                    Session["dtData"] = contact;
                    dgvDG.DataSource = contact;
                    dgvDG.DataBind();

                }
            }

            if (RadioButtonList1.SelectedItem.Value == "rdoAmountwise")
            {
                if (txtbyName.Text != null && txtName.Text!=null)
                {

                    int amount1=0;
                    if (txtbyName.Text != ""){amount1 = Convert.ToInt32(txtbyName.Text); }
                    string apname = Convert.ToString(txtName.Text);
                    var amountwise = dgent.SEARCHAMOUNTWISE(amount1,apname);
                    Session["dtData"] = amountwise;
                    dgvDG.DataSource = amountwise;
                    dgvDG.DataBind();
                }
            }
        }
        protected void RadioButtonList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (RadioButtonList1.SelectedItem.Value == "rdoFaNowise")
            {
               // lblbyName.Visible = false;
                txtbyName.Visible = false;
                txtFaNo.Visible = true;
                lblFaNo.Visible = true;
                txtAddress.Visible = false;
                txtName.Visible = false;
                //lblname.Visible = false;
                lblAddress.Visible = false;

            }
            if (RadioButtonList1.SelectedItem.Value == "rdoAmountwise")
            {
                //lblbyName.Visible = true;
                txtbyName.Visible = true;
                txtFaNo.Visible = false;
                lblFaNo.Visible = false;
                txtAddress.Visible = false;
                txtName.Visible = true;
               // lblname.Visible = true;
                lblAddress.Visible = false;

            }

            if (RadioButtonList1.SelectedItem.Value == "rdoSearchbyName")
            {
                lblFaNo.Visible = false;
                txtFaNo.Visible = false;
                txtbyName.Visible = true;
              //  lblbyName.Visible = true;
                txtAddress.Visible = false;
                txtName.Visible = false;
                //lblname.Visible = false;
                lblAddress.Visible = false;
                
            }
            if(RadioButtonList1.SelectedItem.Value == "rdocontactnowise")
            {
                lblFaNo.Visible = true;
                txtFaNo.Visible = true;
                txtbyName.Visible = false;
                //  lblbyName.Visible = true;
                txtAddress.Visible = false;
                txtName.Visible = false;
                //lblname.Visible = false;
                lblAddress.Visible = false;

            }
            if (RadioButtonList1.SelectedItem.Value == "rdoSearchbyPtName")
            {
                lblFaNo.Visible = false;
                txtFaNo.Visible = false;
                txtbyName.Visible = true;
                //  lblbyName.Visible = true;
                txtAddress.Visible = false;
                txtName.Visible = false;
                //lblname.Visible = false;
                lblAddress.Visible = false;

            }
            if (RadioButtonList1.SelectedItem.Value == "rdoSearchbySubject")
            {
                lblFaNo.Visible = false;
                txtFaNo.Visible = false;
                txtbyName.Visible = true;
               // lblbyName.Visible = true;
                txtAddress.Visible = false;
                txtName.Visible = false;
               // lblname.Visible = false;
                lblAddress.Visible = false;

            }

            if (RadioButtonList1.SelectedItem.Value == "rdochqwise")
            {
                lblFaNo.Visible = false;
                txtFaNo.Visible = false;
                txtbyName.Visible = true;
                //lblbyName.Visible = true;
                txtAddress.Visible = false;
                txtName.Visible = false;
                //lblname.Visible = false;
                lblAddress.Visible = false;
            }
            
            if (RadioButtonList1.SelectedItem.Value == "rdocnicwise")
            {
                lblFaNo.Visible = false;
                txtFaNo.Visible = false;
                txtbyName.Visible = true;
               // lblbyName.Visible = true;
                txtAddress.Visible = false;
                txtName.Visible = false;
               // lblname.Visible = false;
                lblAddress.Visible = false;
            }
            if (RadioButtonList1.SelectedItem.Value == "rdoNameandAddress")
            {
                txtAddress.Visible = true;
                txtName.Visible = true;
               // lblname.Visible = true;
                lblAddress.Visible = true;
                lblFaNo.Visible = false;
                txtFaNo.Visible = false;
                txtbyName.Visible = false;
               // lblbyName.Visible = false;
            }
        }

        protected void dgvDG_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            
            //Cls_Login login = (Cls_Login)Session["Login"];
            //if (login.Isedit != true)
            //{
            //    LinkButton editbutton = (LinkButton)dgvDG.Rows[e.RowIndex].FindControl("LnkBtnEdit");
            //    editbutton.Enabled = false;
            //    return;
            //}

            //else
            //{
            //    Label dgid = (Label)dgvDG.Rows[e.RowIndex].FindControl("lblDGId");
            //    int fano = Convert.ToInt32(dgid.Text);
            //    Response.Redirect("DGForm.aspx?DGID=" + fano);
            //}
        }

        protected void lnkBtnFirst_Click(object sender, EventArgs e)
        {
            //LoadGrid();
            if (Session["dtData"] != null)
                dgvDG.DataSource = Session["dtData"];
            this.dgvDG.PageIndex = 0;
            this.dgvDG.DataBind();


        }

        protected void lnkBtnLast_Click(object sender, EventArgs e)
        {
            //LoadGrid();
            if (Session["dtData"] != null)
                dgvDG.DataSource = Session["dtData"];
            this.dgvDG.PageIndex = Int32.MaxValue;
            this.dgvDG.DataBind();


        }

        protected void lnkBtnDelete_Click(object sender, EventArgs e)
        {
            DGEntities ent = new DGEntities();
                        
            foreach (GridViewRow row in dgvDG.Rows)
            {
                CheckBox cb = (CheckBox)row.FindControl("cbRows");
                if (cb != null && cb.Checked)
                {
                    int dgid = Convert.ToInt32(dgvDG.DataKeys[row.RowIndex].Value);
                    ent.DELETEARECORD(dgid);

                }
             }

            
            LoadGrid();
            
        }

        protected void dgvDG_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if(e.CommandName == "update")
            {
                int fano = Convert.ToInt32(e.CommandArgument);
                Response.Redirect("DGForm.aspx?DGID=" + fano);
            }
        }
    }
}
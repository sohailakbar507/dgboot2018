﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DGMasterPage.Master" AutoEventWireup="true" CodeBehind="SearchingWebForm.aspx.cs" Inherits="DG.WebUI.SearchingWebForm" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1> Searching Form </h1>
<%--<asp:ScriptManager ID="ScriptManager1" runat="server">
</asp:ScriptManager>--%>

<div>
        <table width="80%" cellpadding = "1" cellspacing = "0" border = "0px" style= "border-collapse:collapse">
        <tr> 
            <td>
                
                <asp:LinkButton ID="lnkBtnDelete" runat="server" CommandName = "" 
                    ToolTip = "Delete a Record" onclick="lnkBtnDelete_Click"><img src="Images/Misc-Delete-Database-icon.png" border = "" alt=""/></asp:LinkButton>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <asp:LinkButton ID="lnkBtnFirst" CommandName="" runat="server" 
                    onclick="lnkBtnFirst_Click" ToolTip = "Goto First Page">  <img src="Images/Actions-go-first-icon.png" border = "" alt = ""/></asp:LinkButton>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <asp:LinkButton ID="lnkBtnLast" runat="server" onclick="lnkBtnLast_Click" ToolTip= "Goto Last Page"><img src="Images/Actions-go-last-icon.png" border = "" alt = ""/></asp:LinkButton>
                
            </td></tr></table>
       </div>
<div>
    <table align="center" width="30%" cellpadding = "1" cellspacing = "1" border = "0px" style= "border-collapse:collapse">
            <tr>
                <td colspan="2">
                    <asp:Label ID="lblFaNo" runat="server" Text="FaNo."></asp:Label></td>
                <td colspan="2" align="center"><asp:TextBox ID="txtFaNo" runat="server" CssClass="form-control" Width="250px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                   <asp:TextBox ID="txtbyName" runat="server" CssClass="form-control" placeholder="Enter Your Text Here" required="required" Width="250px"></asp:TextBox></td>
                 <td colspan="2"></td> 
        </tr>
        <tr>
            <td>
               </td> 
            <td><asp:TextBox ID="txtName" runat="server" Width = "250px" CssClass="form-control" placeholder="Enter Name" required="required"></asp:TextBox></td>
            <td><asp:Label ID="lblAddress" runat="server" Text="Address"></asp:Label></td> 
            <td><asp:TextBox ID="txtAddress" runat="server" Width = "250px" CssClass="form-control"></asp:TextBox></td>
        </tr></table>
    
    <table  width="100%" cellpadding = "0" cellspacing = "0" border = "0px" style= "border-collapse:collapse">
        <tr>
            <td colspan="4" align="center">
            <br />
            <asp:RadioButtonList ID="RadioButtonList1" Width="1050px" runat="server" 
            RepeatDirection="Horizontal" AutoPostBack="True" 
            onselectedindexchanged="RadioButtonList1_SelectedIndexChanged" >
            
            <asp:ListItem Value = "rdoFaNowise" Selected = "True">FaNo Wise</asp:ListItem>
            <asp:ListItem Value = "rdoSearchbyName">By App.Name</asp:ListItem>
            <asp:ListItem Value = "rdoSearchbyPtName">By Patient Name</asp:ListItem>
            <asp:ListItem Value = "rdochqwise">Cheque Wise</asp:ListItem>
            <asp:ListItem Value = "rdocnicwise">CNIC Wise</asp:ListItem>
            <asp:ListItem Value = "rdocontactnowise">Contact Wise</asp:ListItem>
            <asp:ListItem  Value = "rdoNameandAddress">App.Name & Address</asp:ListItem>
            <asp:ListItem Value = "rdoSearchbySubject">By Subject</asp:ListItem>
            <asp:ListItem Value = "rdoAmountwise">By Amount</asp:ListItem>
        </asp:RadioButtonList>
       
    </td>
</tr>
        <tr>
                <td colspan="4" align="center">
                    <asp:LinkButton ID="lnkBtnSearch" runat="server" ToolTip = "Search Button" onclick="lnkBtnSearch_Click"><img src="Images/Zoom-icon.png" alt = ""/> </asp:LinkButton>
                </td>
            </tr>
        <tr>
            <td><div class="dataTable_wrapper">
            <asp:GridView ID="dgvDG" runat="server" AllowPaging="True" 
            onpageindexchanging="dgvDG_PageIndexChanging" 
            onselectedindexchanged="dgvDG_SelectedIndexChanged" 
            AutoGenerateColumns="False" onrowcancelingedit="dgvDG_RowCancelingEdit" 
            onrowupdating="dgvDG_RowUpdating" DataKeyNames="DGId" 
            CssClass="table table-striped table-bordered table-hover" OnRowCommand="dgvDG_RowCommand" BackColor="White" BorderColor="#999999" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Vertical">
           
                <AlternatingRowStyle BackColor="#DCDCDC" />
           
               <Columns>
                   
                   <asp:TemplateField>
                        <ItemTemplate>
                            <asp:CheckBox ID = "cbRows" runat = "server" />
                        </ItemTemplate>
                   </asp:TemplateField>
                   
                   <asp:TemplateField HeaderText="DGId" Visible="False">
                   <ItemTemplate>
                       <asp:Label ID="lblDGId" runat="server" Text= '<%#Eval("DGId")%>'></asp:Label>
                   </ItemTemplate>                   
                   </asp:TemplateField>
                   
                   
                   <asp:TemplateField HeaderText="FANo.">
                   <ItemTemplate>
                       <asp:Label ID="lblFaNo" runat="server" Text='<%#Eval("fano")%>'></asp:Label>
                   </ItemTemplate>
                   </asp:TemplateField>
                   
                   <asp:TemplateField HeaderText="Refdate">
                   <ItemTemplate>
                       <asp:Label ID="lblRefdate" runat="server" Text='<%#Eval("refdate","{0:dd/MM/yyyy}") %>'></asp:Label>
                   </ItemTemplate>
                       
                   <ControlStyle Width="80px" />
                       
                   </asp:TemplateField>
                   
                   <asp:TemplateField HeaderText="Applicant Name">
                   <ItemTemplate>
                       <asp:Label ID="lblAppname" runat="server" Text='<%#Eval("appname")%>'></asp:Label>
                   </ItemTemplate>
                   
                       <ControlStyle Width="120px" />                   
                   </asp:TemplateField>
                    <asp:TemplateField HeaderText="Patient Name">
                   <ItemTemplate>
                       <asp:Label ID = "lblptname" runat="server" Text='<%#Eval("PatientName")%>'></asp:Label>                       
                   </ItemTemplate>
                       <ControlStyle Width="100px" />
                   </asp:TemplateField>
                   <asp:TemplateField HeaderText="Subejct">
                   <ItemTemplate>
                       <asp:Label ID="lblSubject" runat="server" Text='<%#Eval("subject")%>'></asp:Label>
                   </ItemTemplate>
                   
                       <ControlStyle Width="120px" />
                   
                   </asp:TemplateField>
                   
                   <asp:TemplateField HeaderText="Address">
                   <ItemTemplate>
                       <asp:Label ID = "lblAddress" runat="server" Text='<%#Eval("address")%>'></asp:Label>                       
                   </ItemTemplate>
                       <ControlStyle Width="100px" />
                   </asp:TemplateField>
                  
                   <asp:TemplateField HeaderText="CNIC">
                       <ItemTemplate>
                           <asp:Label ID="lblCNIC" runat="server" Text='<%#Eval("CNIC")%>'></asp:Label>
                       </ItemTemplate>
                   </asp:TemplateField>                  
                   <asp:TemplateField HeaderText="Amount">
                       <ItemTemplate>
                           <asp:Label ID="lblAmount" runat="server" Text='<%#Eval("amount")%>'></asp:Label>
                       </ItemTemplate>
                   </asp:TemplateField>
                   <asp:TemplateField HeaderText="Cheque No.">
                       <ItemTemplate>
                           <asp:Label ID="lblChequeno" runat="server" Text='<%#Eval("chqno")%>'></asp:Label>
                       </ItemTemplate>
                   </asp:TemplateField>

                   <asp:TemplateField HeaderText="Cheque Date">
                   <ItemTemplate>
                       <asp:Label ID="lblChqDate" runat="server" Text='<%#Eval("chqdate","{0:dd/MM/yyyy}") %>'></asp:Label>
                   </ItemTemplate>
                   </asp:TemplateField>

                   <asp:TemplateField HeaderText="Department Name">
                   <ItemTemplate>
                       <asp:Label ID="lblDeptName" runat="server" Text='<%#Eval("deptname") %>'></asp:Label>
                   </ItemTemplate>
                       <ControlStyle Width="60px" />
                   <ItemStyle HorizontalAlign = "Left" />
                   </asp:TemplateField>
                   
                                  
                   <asp:TemplateField HeaderText="Status">
                   <ItemTemplate>
            
            
                       <asp:Label ID="lblStatus" runat="server" Text='<%#Eval("Description") %>'></asp:Label>
                   </ItemTemplate>
                   <ItemStyle HorizontalAlign = "Center" />
                   </asp:TemplateField>
                   
                   <asp:TemplateField HeaderText="Edit">
                   <ItemTemplate>
                       <asp:LinkButton ID="LnkBtnEdit" CommandName = "update" CommandArgument='<%#Eval("DGId")%>' runat="server"><img src = "Images/Text-Edit-icon.png" alt = "" /> </asp:LinkButton>
                   </ItemTemplate>
                   </asp:TemplateField>
                   
                   
               </Columns>
              
                 <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                <HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White" />
              
                 <PagerSettings FirstPageText=">>" LastPageText="<<" />
                <PagerStyle HorizontalAlign = "Center" CssClass = "pagination-ys" BackColor="#999999" ForeColor="Black" />

               
                <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
                <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                <SortedAscendingHeaderStyle BackColor="#0000A9" />
                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                <SortedDescendingHeaderStyle BackColor="#000065" />

               
        </asp:GridView>
        </div>
        </td>
      </tr>

    </table>
</div>
</asp:Content>

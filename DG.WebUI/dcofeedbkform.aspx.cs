﻿using DGModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DG.WebUI
{
    public partial class dcofeedbkform : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Login"] == null)
                {
                    Response.Redirect("~/LoginPage.aspx");
                }
                LoadData();
                UploadStatus();
                NoEdit();
                this.ddlStatus.Items.Insert(0, new ListItem("Select Status"));

            }
        }

        private void UploadStatus()
        {
            DGEntities dg = new DGEntities();
            List<int> findStatusCode = new List<int> { 1, 5 };
            var stat = (from b in dg.status where findStatusCode.Contains(b.StatusCode) select b).ToList();
            ddlStatus.DataSource = stat;
            ddlStatus.DataValueField = "StatusCode";
            ddlStatus.DataTextField = "Description";
            //ddlStatus.SelectedIndex = 0;
            ddlStatus.DataBind();
        }

        private void LoadData()
        {
            DGEntities dg = new DGEntities();
            int fano = Convert.ToInt32(Request["DGId"]);
            var result = from h in dg.MAINCORRs where h.DGId == fano && h.statcode==11 select h;
            
            if (result.Count() > 0)
            {
                var record = result.First();
                txtFano.Text = record.fano.ToString();
                txtAppname.Text = record.appname;
                txtAddress.Text = record.address;
                txtLetterNo.Text = record.refno;
                txtPageNo.Text = record.pageno.ToString();
                txtRecvdChqno.Text = record.chqno;
                dtLetterDate.SelectedDate = record.refdate;
                rdRecvdChqDate.SelectedDate = record.chqdate;
                DDLChqStatus.SelectedIndex = DDLChqStatus.Items.IndexOf(DDLChqStatus.Items.FindByValue(record.chqstatusID.ToString()));
                ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByValue(record.statcode.ToString()));
                txtAmount.Text = Convert.ToInt32(record.amount).ToString();
                txtRemarks.Text = record.dcremarks;
                var filename = from f in dg.tblUploadFiles where f.FaNo == record.fano select f;
                if (filename.Count() > 0)
                {
                    var fname = filename.First();
                    txtfilename.Text = fname.FileName;
               }

            }
        }

        private void NoEdit()
            {
                txtFano.Enabled = false;
                txtAppname.Enabled = false;
                txtAddress.Enabled = false;
                txtPageNo.Enabled = false;
                txtLetterNo.Enabled = false;
                dtLetterDate.Enabled = false;
                rdtUpdatedOn.Enabled = false;
            }

            private void Editable()
            {
                txtPageNo.Enabled = true;
                txtLetterNo.Enabled = true;
                dtLetterDate.Enabled = true;
            
            }
            protected void btnFind_Click(object sender, EventArgs e)
            {
            }

            private void ClearFields()
            {
                txtPageNo.Text = "";
                txtLetterNo.Text = "";
                dtLetterDate.SelectedDate = null;
                rdtUpdatedOn.SelectedDate = null;
            }
            protected void btnSave_Click(object sender, EventArgs e)
            {
            }

            protected void btnCancel_Click(object sender, EventArgs e)
            {
            }

            protected void LinkButton1_Click(object sender, EventArgs e)
            {
            }

            protected void lnkbtnCancel_Click(object sender, EventArgs e)
            {
                Response.Redirect("LoginPage.aspx");

            }

        protected void lnkbtnSave_Click(object sender, EventArgs e)
        {
            DGEntities dg = new DGEntities();
            if (FileReceiptUpload.HasFile)
            {
                foreach (HttpPostedFile file in FileReceiptUpload.PostedFiles)
                {

                    file.SaveAs(Server.MapPath("~/UploadFiles/" + txtFano.Text + file.FileName));
                    tblUploadFile file1 = new tblUploadFile();
                    file1.FaNo = Convert.ToInt32(txtFano.Text);
                    file1.FileName = txtFano.Text + file.FileName;
                    file1.FilePath = "UploadFiles/" + txtFano.Text + file.FileName;
                    dg.tblUploadFiles.Add(file1);
                    dg.SaveChanges();
                    txtfilename.Text = file1.FileName;
                }
            }

            try
            {

                System.Threading.Thread.Sleep(500);
                int dgid = Convert.ToInt32(Request["DGId"]);
                var getdgid = from h in dg.MAINCORRs where h.DGId == dgid select h;

                if (getdgid.Count() > 0)
                {
                    var h = getdgid.First();
                    h.pageno = txtPageNo.Text == string.Empty ? 0 : Convert.ToInt32(txtPageNo.Text);
                    h.refdate = dtLetterDate.DateInput.SelectedDate;
                    //h.fano = Convert.ToInt32(txtPageNo.Text);
                    h.amount = txtAmount.Text == string.Empty ? 0 : Convert.ToInt32(txtAmount.Text);
                    h.refno = txtLetterNo.Text;
                    h.refdate = dtLetterDate.DateInput.SelectedDate;
                    h.UpdatedOn = DateTime.Now;
                    h.dcremarks = txtRemarks.Text;
                    h.chqno = txtRecvdChqno.Text;
                    h.chqdate = rdRecvdChqDate.DateInput.SelectedDate;
                    h.chqstatusID = int.Parse(DDLChqStatus.SelectedValue);
                   // h.statcode = int.Parse(ddlStatus.SelectedValue);
                    if (string.IsNullOrEmpty(txtAmount.Text))
                    {
                        txtAmount.Text = "0";
                    }
                   
                    dg.SaveChanges();
                    Cls_Login login = (Cls_Login)Session["Login"];
                    ScriptManager.RegisterStartupScript(this, GetType(), "Alert", "alert('Changes Saved Successfully ..');", true);
                }
                   
                
            }

            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Alert", "alert('" + ex.Message.ToString() + "');", true);
            }
        }
            
        
    }
}

﻿using DGModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DG.WebUI.Api
{
    public partial class AddMPA_Api : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            {
                if (!this.IsPostBack)
                {
                    int mpa_id = 0;
                    DGEntities ent = new DGEntities();
                    mpamna response = new mpamna();
                    string id = Request["Refid"];
                    Response.Headers["Content-type"] = "application/json";
                    if (string.IsNullOrEmpty(Request["action"]))
                    {
                        string r = JsonConvert.SerializeObject("No action defined");
                        Response.Write(r);
                        return;
                    }

                    if (string.IsNullOrEmpty(Request["Refid"]))
                        mpa_id = 0;
                    else
                        int.TryParse(Request["Refid"], out mpa_id);

                    switch (Request["action"])
                    {
                        case "save":
                            if (mpa_id == 0)
                            {
                                response = new mpamna();
                                response.RefId = Convert.ToInt32(Request["Refid"]);
                                response.CatCons = Request["catcons"];
                                response.Name = Request["Name"];
                                response.Address1 = Request["address1"];
                                response.Address2 = Request["address2"];
                                response.District = Request["district"];
                                ent.mpamnas.Add(response);
                                ent.SaveChanges();
                            }

                            else
                            {

                                response = (from a in ent.mpamnas where a.RefId == mpa_id select a).First();
                                response.RefId = Convert.ToInt32(Request["Refid"]);
                                response.CatCons = Request["catcons"];
                                response.Name = Request["Name"];
                                response.Address1 = Request["address1"];
                                response.Address2 = Request["address2"];
                                response.District = Request["district"];
                                ent.SaveChanges();
                            }

                            Response.Write(JsonConvert.SerializeObject(response));
                            break;

                        case "delete":
                            response = (from a in ent.mpamnas where a.RefId == mpa_id select a).First();
                            ent.mpamnas.Remove(response);
                            ent.SaveChanges();
                            Response.Write(JsonConvert.SerializeObject(response));
                            break;
                        case "get":
                            var response1 = (from a in ent.mpamnas where a.RefId == mpa_id select new {a.CatCons, a.Name, a.Address1, a.Address2, a.District }).First();
                            Response.Write(JsonConvert.SerializeObject(response1));
                            break;

                    }
                }
            }

        }
    }
}
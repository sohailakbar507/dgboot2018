﻿using DGModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DG.WebUI.Api
{
    public partial class DeptApi : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                int dept_id = 0;
                
                DGEntities ent = new DGEntities();
                DEPT response = new DEPT();
                string id = Request["deptcode"];
                Response.Headers["Content-type"] = "application/json";
                if (string.IsNullOrEmpty(Request["action"]))
                {
                    string r = JsonConvert.SerializeObject("No action defined");
                    Response.Write(r);
                    return;
                }

                if (string.IsNullOrEmpty(Request["deptcode"]))
                    dept_id = 0;
                else
                    int.TryParse(Request["deptcode"], out dept_id);

                switch (Request["action"])
                {
                    case "save":
                        if (dept_id == 0)
                        {
                            response = new DEPT();
                            response.deptname = Request["deptname"];
                            response.contact = Request["contact"];
                            response.email = Request["email"];
                            ent.DEPTs.Add(response);
                            ent.SaveChanges();
                        }

                        else
                        {

                            response = (from a in ent.DEPTs where a.deptcode == dept_id select a).First();
                            response.deptname = Request["deptname"];
                            response.contact = Request["contact"];
                            response.email = Request["email"];
                            ent.SaveChanges();
                        }

                        Response.Write(JsonConvert.SerializeObject("Operation Performed Successfully...."));
                        break;

                    case "delete":
                        response = (from a in ent.DEPTs where a.deptcode == dept_id select a).First();
                        ent.DEPTs.Remove(response);
                        ent.SaveChanges();
                        Response.Write(JsonConvert.SerializeObject("Operation Performed Successfully...."));
                        break;
                    case "get":
                        var response1 = (from a in ent.DEPTs  select new { a.deptcode, a.deptname, a.contact, a.email});
                        Response.Write(JsonConvert.SerializeObject(response1));
                        break;

                }
            }
        }
    }
}
﻿using DGModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DG.WebUI
{
    public partial class UpdateApproved : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            

        }

        private void UploadGrid(int fano)
        {
            DGEntities dg = new DGEntities();
            var getfano = (from a in dg.MAINCORRs
                           where a.fano == fano
                           from d in dg.Approveds
                           where d.fanoApproved == a.fano
                           orderby a.DGId descending
                           select new

                           {
                               a.DGId,
                               a.fano,
                               a.appname,
                               a.amount,
                               ApprovedAmount =d.Approved1,
                               a.fyear
                           }
                           ).ToList();
            DGVApproved.DataSource = getfano;
            DGVApproved.DataBind();

        }
        protected void DGVApproved_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
        }
        protected void DGVApproved_RowEditing(object sender, GridViewEditEventArgs e)
        {
        }
        protected void DGVApproved_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
        }
        protected void DGVApproved_RowCommand(object sender, GridViewCommandEventArgs e)
        {
        }
        protected void DGVApproved_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {

        }

        protected void btnFind_Click(object sender, EventArgs e)
        {
            
            int fanos = Convert.ToInt32(txtFano.Text);
            UploadGrid(fanos);
        }
    }
}
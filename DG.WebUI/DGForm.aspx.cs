﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DGModel;

namespace DG.WebUI
{
    public partial class DGForm : System.Web.UI.Page
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {
            this.txtAmount.Attributes.Add("onkeyup", "OnFiguresChange()");
            if (!IsPostBack)
            {
               // txtSearch.Visible = false;
                GetDepartments();
                GetSubject();
                GetStatus();
                //SentOn.DateInput.DisplayText = datetime.now.toshortdatestring(); 
                if (Request["DGID"] != null)
                {
                    this.ButtonsForEdit();
                    EditRecord();
                }
                else
                {
                    GetFaNo();
                    DisabledFields();
                    this.ButtonsForAdd();
                }


            }
            btnSave.Attributes.Add("onclick", "return confirm('Do you want to Save this record[Yes / No]?');");

            //txtSCMDate.Text = DateTime.Now.ToShortDateString();
            //txtChqDate.Text = DateTime.Now.ToShortDateString();

        }
        private void ButtonsForAdd()
        {
            this.btnAdd.Visible = true;
            this.btnCancel.Visible = true;
            this.btnEdit.Visible = false;
            this.btnSave.Visible = false;
            this.btnSearch.Visible = false;
        }
        private void ButtonsForEdit()
        {
            this.btnAdd.Visible = false;
            this.btnCancel.Visible = true;
            this.btnEdit.Visible = false;
            this.btnSave.Visible = true;
            this.btnSearch.Visible = false;
        }
        private void DisabledFields()
        {
            txtCons.Enabled = false;
            txtCatcode.Enabled = false;
            txtName.Enabled = false;
            txtFaNo.Enabled = false;
            txtAppName.Enabled = false;
            txtContact.Enabled = false;
            txtAddress.Enabled = false;
            txtActionyBy.Enabled = false;
            txtDistrict.Enabled = false;
            txtCPNo.Enabled = false;
            SentOn.Enabled = false;
            txtFileNo.Enabled = false;
            txtCnic.Enabled = false;
            Chqdate.Enabled = false;
            IssueDate.Enabled = false;
            ddlType.Enabled = false;
            txtChqno.Enabled = false;
            txtLetterNo.Enabled = false;
            ddlDepartment.Enabled = false;
            ddlStatus.Enabled = false;
            scmdate.Enabled = false;
            txtOldFileNo.Enabled = false;
            OldChqDate.Enabled = false;
            txtOldChqNumber.Enabled = false;
            txtRemarks.Enabled = false;
            txtAmount.Enabled = false;
            txtAmtinWords.Enabled = false;
            TreasuryDate.Enabled = false;
            BankDate.Enabled = false;
            OldAknDate.Enabled = false;
            NewAknDate.Enabled = false;
            btnSave.Visible = false;
            btnAdd.Visible = true;
        }
        private void GetFaNo()
        {
            DGEntities dg = new DGEntities();
            int fano;
            var getfano = from a in dg.MAINCORRs select a.fano;
            if (getfano.Count() > 0)
            {
                var fansso = getfano.Max() + 1;
                fano = Convert.ToInt32(fansso);
            }
            else
            {
                fano = 1;
            }

            txtFaNo.Text = fano.ToString();

        }

        private void EditRecord()
        {
            DGEntities dg = new DGEntities();
            int dgid = Convert.ToInt32(Request["DGID"]);
            var abc = from a in dg.MAINCORRs
                      from s in dg.subjects
                      from st in dg.status
                      from d in dg.DEPTs
                      where a.DGId == dgid && a.subjcode == s.SubjectCode && a.statcode == st.StatusCode && a.deptcode == d.deptcode
                      select new
                      {
                          a.fano,a.catcode,a.cons,a.name,a.appname,a.address,a.contactno,a.district,a.subjcode,a.deptcode,a.statcode,a.subdept,a.comdate,a.fileno,
                          a.cpno,a.amount,a.amtinwords,a.chqno,a.chqdate,a.cnic,a.dispdate,a.letcode,a.scmdate,a.oldfileno,a.oldchqno,a.oldchqdate,a.remarks,a.chqremarks,
                          a.fyear,a.akndate,a.oldakndate,a.cleardate,a.recondate,a.fremarks,s.SubjectCode,s.Description,s.Description2,st.StatusCode,
                          Statusdescription = st.Description,d.deptname
                      };
            if (abc.Count() > 0)
            {
                var bbc = abc.First();
                txtFaNo.Text = Convert.ToInt32(bbc.fano).ToString();
                txtCatcode.Text = Convert.ToInt32(bbc.catcode).ToString();
                txtCons.Text = Convert.ToInt32(bbc.cons).ToString();
                txtAppName.Text = bbc.appname;
                txtAddress.Text = bbc.address;
                txtContact.Text = bbc.contactno;
                txtDistrict.Text = bbc.district;
                ddlDepartment.SelectedIndex = ddlDepartment.Items.IndexOf(ddlDepartment.Items.FindByValue(bbc.deptcode.ToString()));
                ddlType.SelectedIndex = ddlType.Items.IndexOf(ddlType.Items.FindByValue(bbc.SubjectCode.ToString()));
                txtActionyBy.Text = bbc.subdept;
                txtFileNo.Text = bbc.fileno;
                txtCPNo.Text = bbc.cpno;
                txtAmount.Text = Convert.ToInt32(bbc.amount).ToString();
                txtChqno.Text = bbc.chqno;
                //SentOn.DateInput.DisplayText = bbc.comdate.ToString();
                SentOn.SelectedDate = bbc.comdate;
                Chqdate.SelectedDate = bbc.chqdate;
                IssueDate.SelectedDate = bbc.dispdate;
                scmdate.SelectedDate = bbc.scmdate;
                OldChqDate.SelectedDate = bbc.oldchqdate;
                txtCnic.Text = bbc.cnic;
                ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByValue(bbc.StatusCode.ToString()));
                txtLetterNo.Text = bbc.letcode.ToString();
                txtOldFileNo.Text = bbc.oldfileno;
                txtRemarks.Text = bbc.remarks;
                txtChqRemarks.Text = bbc.chqremarks;
                txtFyear.Text = bbc.fyear;
                TreasuryDate.SelectedDate = bbc.cleardate;
                BankDate.SelectedDate = bbc.recondate;
                OldAknDate.SelectedDate = bbc.oldakndate;
                NewAknDate.SelectedDate = bbc.akndate;
                txtAmtinWords.Text = bbc.amtinwords;
            }
        }
        private void GetStatus()
        {
            DGEntities dg = new DGEntities();
            var stat = (from st in dg.status select st).ToList();
            ddlStatus.DataSource = stat;
            ddlStatus.DataValueField = "StatusCode";
            ddlStatus.DataTextField = "Description";
            //Session["StatusValue"] = ddlStatus.DataValueField; 
            ddlStatus.SelectedIndex = 0;
            ddlStatus.DataBind();

        }

        private void GetSubject()
        {
            DGEntities dg = new DGEntities();
            var subj = (from s in dg.subjects select s).ToList();
            ddlType.DataSource = subj;
            ddlType.DataValueField = "SubjectCode";
            ddlType.DataTextField = "Description";
            ddlType.SelectedIndex = 0;
            ddlType.DataBind();
        }
        private void GetDepartments()
        {
            DGEntities dg = new DGEntities();
            var dept = (from d in dg.DEPTs select d).ToList();
            ddlDepartment.DataSource = dept;
            ddlDepartment.DataValueField = "DeptCode";
            ddlDepartment.DataTextField = "DeptName";
            ddlDepartment.SelectedIndex = 0;
            ddlDepartment.DataBind();
        }


        protected void btnSave_Click(object sender, EventArgs e)
        {
            DGEntities dg = new DGEntities();
            if (Request["DGID"] == null)
            {
                MAINCORR M = new MAINCORR();
                M.catcode = txtCatcode.Text == string.Empty ? 0 : Convert.ToInt32(txtCatcode.Text);
                M.cons = txtCons.Text == string.Empty ? 0 : Convert.ToInt32(txtCons.Text);
                int catcodes = Convert.ToInt32(txtCatcode.Text == "" ? "0" : this.txtCatcode.Text);
                int conss = Convert.ToInt32(txtCons.Text == "" ? "0" : this.txtCons.Text);
                var getref = from a in dg.mpamnas where (a.CatCode == catcodes && a.Cons == conss) && a.Active==true select a;
                if (getref.Count() > 0)
                {
                    var mna = getref.First();
                    M.RefId = mna.RefId;
                    M.CATCONSMAIN = mna.CatCons;
                }
                M.fano = txtFaNo.Text == string.Empty ? 0 : Convert.ToInt32(txtFaNo.Text);
                M.catcode = txtCatcode.Text == string.Empty ? 0 : Convert.ToInt32(txtCatcode.Text);
                M.cons = txtCons.Text == string.Empty ? 0 : Convert.ToInt32(txtCons.Text);
                M.appname = txtAppName.Text;
                M.address = txtAddress.Text;
                M.contactno = txtContact.Text;
                M.district = txtDistrict.Text;
                M.subjcode = Convert.ToInt32(ddlType.SelectedItem.Value);
                M.deptcode = Convert.ToInt32(ddlDepartment.SelectedItem.Value);
                M.subdept = txtActionyBy.Text;
                M.comdate = SentOn.DateInput.SelectedDate;
                M.fileno = txtFileNo.Text;
                M.cpno = txtCPNo.Text;
                M.amount = txtAmount.Text == string.Empty ? 0 : Convert.ToInt32(txtAmount.Text);
                M.amtinwords = txtAmtinWords.Text;
                M.chqno = txtChqno.Text;
                M.chqdate = Chqdate.DateInput.SelectedDate;
                M.cnic = txtCnic.Text;
                M.statcode = Convert.ToInt32(ddlStatus.SelectedItem.Value);
                M.dispdate = IssueDate.DateInput.SelectedDate;
                //if (txtLetterNo.Text == "")
                //{
                //    RequiredFieldValidator1.Text = "Letter No. must be Entered";
                //    txtLetterNo.Focus();
                //    return;
                //}

                M.letcode = txtLetterNo.Text == string.Empty ? 0 : Convert.ToInt32(txtLetterNo.Text);
                M.scmdate = scmdate.DateInput.SelectedDate;
                M.oldfileno = txtOldFileNo.Text;
                M.oldchqno = txtOldChqNumber.Text;
                M.oldchqdate = OldChqDate.DateInput.SelectedDate;
                M.remarks = txtRemarks.Text;
                M.chqremarks = txtChqRemarks.Text;
                M.fyear = txtFyear.Text;
                M.cleardate = TreasuryDate.DateInput.SelectedDate;
                M.recondate = BankDate.DateInput.SelectedDate;
                M.oldakndate = OldAknDate.DateInput.SelectedDate;
                M.akndate = NewAknDate.DateInput.SelectedDate;
                M.fremarks = txtFinalRemarks.Text;
                if (M.letcode == 0) M.letcode = null;
                if (M.deptcode == 0) M.deptcode = null;
                if (M.statcode == 0) M.statcode = null;
                if (M.offcode == 0) M.offcode = null;
                if (M.subjcode == 0) M.subjcode = null;
                if (M.RefId == 0) M.RefId = null;
                dg.MAINCORRs.Add(M);
                dg.SaveChanges();
                txtAddress.Text = "";
                txtAppName.Text = "";
                txtContact.Text = "";
                DisabledFields();
            }
            else
            {
                int dgid = Convert.ToInt32(Request["DGID"]);
                MAINCORR M = new MAINCORR();
                M = (from a in dg.MAINCORRs where a.DGId == dgid select a).First();
                int conss = Convert.ToInt32(txtCons.Text == "" ? "0" : this.txtCons.Text);
                int catcodes = Convert.ToInt32(txtCatcode.Text == "" ? "0" : this.txtCatcode.Text);
                var getrefid = from a in dg.mpamnas where (a.CatCode == catcodes && a.Cons == conss) && a.Active==true select a;
                if (getrefid.Count() > 0)
                {
                    var mna = getrefid.First();
                    M.RefId = mna.RefId;
                    M.CATCONSMAIN = mna.CatCons;
                }
                M.catcode = txtCatcode.Text == string.Empty ? 0 : Convert.ToInt32(txtCatcode.Text);
                M.cons = txtCons.Text == string.Empty ? 0 : Convert.ToInt32(txtCons.Text);
                M.appname = txtAppName.Text;
                M.address = txtAddress.Text;
                M.contactno = txtContact.Text;
                M.district = txtDistrict.Text;
                M.subjcode = Convert.ToInt32(ddlType.SelectedItem.Value);
                M.deptcode = Convert.ToInt32(ddlDepartment.SelectedItem.Value);
                M.subdept = txtActionyBy.Text;
                M.comdate = SentOn.DateInput.SelectedDate;
                M.fileno = txtFileNo.Text;
                M.cpno = txtCPNo.Text;
                M.amount = txtAmount.Text == string.Empty ? 0 : Convert.ToInt32(txtAmount.Text);
                M.amtinwords = txtAmtinWords.Text;
                M.chqno = txtChqno.Text;
                M.chqdate = Chqdate.DateInput.SelectedDate;
                M.cnic = txtCnic.Text;
                M.statcode = Convert.ToInt32(ddlStatus.SelectedItem.Value);
                M.dispdate = IssueDate.DateInput.SelectedDate;
                M.letcode = Convert.ToInt32(txtLetterNo.Text);
                M.scmdate = scmdate.DateInput.SelectedDate;
                M.oldfileno = txtOldFileNo.Text;
                M.oldchqno = txtOldChqNumber.Text;
                M.oldchqdate = OldChqDate.DateInput.SelectedDate;
                M.remarks = txtRemarks.Text;
                M.chqremarks = txtChqRemarks.Text;
                M.fyear = txtFyear.Text;
                M.cleardate = TreasuryDate.DateInput.SelectedDate;
                M.recondate = BankDate.DateInput.SelectedDate;
                M.oldakndate = OldAknDate.DateInput.SelectedDate;
                M.akndate = NewAknDate.DateInput.SelectedDate;
                M.fremarks = txtFinalRemarks.Text;
                dg.SaveChanges();
                DisabledFields();

                Response.Redirect("SearchingWebForm.aspx");
            }


        }

        protected void txtCons_TextChanged(object sender, EventArgs e)
        {
            int catcodes = Convert.ToInt32(txtCatcode.Text);
            int consss = Convert.ToInt32(txtCons.Text);
            DGEntities ent = new DGEntities();
            var getmnampa = from a in ent.mpamnas where (a.CatCode == catcodes && a.Cons == consss) && a.Active==true select a;
            if (getmnampa.Count() > 0)
            {
                var mna = getmnampa.First();
                txtName.Text = mna.Name;
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("MainPage.aspx");
        }



        protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {

            EnabledValues();
            btnSave.Visible = true;
            btnAdd.Visible = false;
            ClearField();
            txtCatcode.Focus();
            GetFaNo();
        }

        private void ClearField()
        {
            txtCatcode.Text = "";
            txtCons.Text = "";
            txtName.Text = "";
            txtAppName.Text = "";
            txtAddress.Text = "";
            txtContact.Text = "";
            txtDistrict.Text = "";
            //ddlType.Text = "";
            //ddlDepartment.Text = "";
            txtActionyBy.Text = "";
            txtFileNo.Text = "";
            txtCPNo.Text = "";
            txtAmount.Text = "";
            txtAmtinWords.Text = "";
            txtChqno.Text = "";
            txtCnic.Text = "";
            //ddlStatus.Text = "";
            txtLetterNo.Text = "";
            txtOldFileNo.Text = "";
            txtOldChqNumber.Text = "";
            txtRemarks.Text = "";
            txtFinalRemarks.Text = "";


        }

        private void EnabledValues()
        {
            txtCons.Enabled = true;
            txtCatcode.Enabled = true;
            txtName.Enabled = true;
            txtFaNo.Enabled = true;
            txtAppName.Enabled = true;
            txtContact.Enabled = true;
            txtAddress.Enabled = true;
            txtActionyBy.Enabled = true;
            txtDistrict.Enabled = true;
            txtCPNo.Enabled = true;
            ddlType.Enabled = true;
            SentOn.Enabled = true;
            txtChqno.Enabled = true;
            txtFileNo.Enabled = true;
            txtCnic.Enabled = true;
            txtLetterNo.Enabled = true;
            ddlDepartment.Enabled = true;
            ddlStatus.Enabled = true;
            Chqdate.Enabled = true;
            IssueDate.Enabled = true;
            scmdate.Enabled = true;
            txtOldFileNo.Enabled = true;
            OldChqDate.Enabled = true;
            txtOldChqNumber.Enabled = true;
            SentOn.Enabled = true;
            txtRemarks.Enabled = true;
            txtAmount.Enabled = true;
            txtAmtinWords.Enabled = true;
            TreasuryDate.Enabled = true;
            BankDate.Enabled = true;
            OldAknDate.Enabled = true;
            NewAknDate.Enabled = true;

            btnSearch.Visible = false;
            btnEdit.Visible = false;
            btnCancel.Visible = false;
            btnAdd.Visible = false;
            btnSave.Visible = true;
            btnCancel.Visible = true;


            //txtAddress.Text = "";
            //txtAppName.Text = "";
            //txtContact.Text = "";
        }


        protected void btnRevert_Click(object sender, EventArgs e)
        {
            Response.Redirect("DGForm.aspx");
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            Response.Redirect("ShowDGList.aspx");
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {

        }

        
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DGMasterPage.Master" AutoEventWireup="true" CodeBehind="NewEntryForm.aspx.cs" Inherits="DG.WebUI.NewEntryForm" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <!--<link href="Styles/bootstrap.css" rel="stylesheet" type="text/css"/> -->
    <style>
        .top-buffer { margin-top:20px; }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%--    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>--%>
 <script language="javascript">

 
     function OnFiguresChange() {
         var obj1 = document.getElementById('<%=txtAmount.ClientID %>');
         var obj2 = document.getElementById('<%=txtAmountinWords.ClientID %>');
         obj2.value = ConvertToWords(obj1.value);
         //alert("test");
     }
     function GetWords(figure) {
         var singles = new Array();
         singles[0] = "";
         singles[1] = "One";
         singles[2] = "Two";
         singles[3] = "Three";
         singles[4] = "Four";
         singles[5] = "Five";
         singles[6] = "Six";
         singles[7] = "Seven";
         singles[8] = "Eight";
         singles[9] = "Nine";
         singles[10] = "Ten";
         singles[11] = "Eleven";
         singles[12] = "Twelve";
         singles[13] = "Thirteen";
         singles[14] = "Fourteen";
         singles[15] = "Fifteen";
         singles[16] = "Sixteen";
         singles[17] = "Seventeen";
         singles[18] = "Eighteen";
         singles[19] = "Nineteen";
         singles[20] = "Twenty";

         var tens = new Array();
         tens[2] = "Twenty";
         tens[3] = "Thrity";
         tens[4] = "Forty";
         tens[5] = "Fifty";
         tens[6] = "Sixty";
         tens[7] = "Seventy";
         tens[8] = "Eighty";
         tens[9] = "Ninty";

         if (figure <= 20) { return singles[figure]; }
         else { return tens[Math.floor(figure / 10)] + " " + singles[figure % 10]; }
     }
     function ConvertToWords(amount) {
         var units = new Array();
         units[0] = "Hundred";
         units[1] = "Thousand";
         units[2] = "Lacs";
         units[3] = "Crore";
         var returnValue = "";


         var intTens = amount % 100;
         amount /= 100;
         amount = Math.floor(amount);

         var inHunderds = amount % 10;
         amount /= 10;
         amount = Math.floor(amount);

         var inThousands = amount % 100;
         amount /= 100;
         amount = Math.floor(amount);

         var inLacs = amount % 100;
         amount /= 100;
         amount = Math.floor(amount);

         var inCrores = amount;

         if (inCrores > 0) {
             returnValue += " " + GetWords(inCrores) + " " + units[3];
         }

         if (inLacs > 0) {
             returnValue += " " + GetWords(inLacs) + " " + units[2];
         }

         if (inThousands > 0) {
             returnValue += " " + GetWords(inThousands) + " " + units[1];
         }

         if (inHunderds > 0) {
             returnValue += " " + GetWords(inHunderds) + " " + units[0];
         }

         if (intTens > 0) {
             if (returnValue != "") {
                 returnValue += " and";
             }
             returnValue += " " + GetWords(intTens);
         }

         return returnValue;
     }
    </script>    
        <div class="col-lg-offset-1">
            <div class="col-md-11">
            <div class="panel panel-info">
                <div class="panel-heading text-center">
                    <h3>New Entry Form </h3>
                </div>
                <div class="panel-body">
                    <div class="col-lg-5 col-md-offset-2">
                     <div class="form-group">
                <asp:Label ID="lblOfficer" runat="server" Text="Select Officer"></asp:Label>
                <asp:DropDownList ID="DDLOfficer"  CssClass="form-control" runat="server"
                    BackColor="#FF6666" Font-Size="Large" ForeColor="White" OnSelectedIndexChanged="DDLOfficer_SelectedIndexChanged"></asp:DropDownList>
                     
                 </div></div>
                   <div class ="row">
                           <div class="col-lg-4">
                           <div class="form-group">
                                <label for="txtFind">Search</label>
                           <div class="input-group">
                <asp:TextBox ID="txtFind" Width="150" Height="34px" CssClass="form-control" runat="server"></asp:TextBox>
                <span class="input-group-btn" style="float: left;padding-left: 0px;">
                <asp:Button ID="btnFind" Width="50" runat="server" Text="Find" CssClass="btn btn-primary btn-md" onclick="btnFind_Click" />
                </span>
                 </div></div></div>

                   </div>
    </div>
      
    <table  align = "center" cellpadding = "0" cellspacing = "0" style = "border:1px dashed white">
           
        <tr>
            <td colspan = "2" align = "center" height = "30">
                Constituency
           </td>
            <td align = "center">Name</td>
        </tr>
        <tr>
            
            <td> <asp:TextBox ID="txtCatcode" CssClass="form-control" runat="server" 
                    Width = "100px" ToolTip="CatCode"></asp:TextBox> </td>
            
            <td> <asp:TextBox ID="txtCons" CssClass="form-control" runat="server" 
                    Width = "100px" AutoPostBack="True" 
                    ontextchanged="txtCons_TextChanged" ToolTip="Cons"></asp:TextBox>
            </td>
            <td> <asp:TextBox ID="txtName" CssClass="form-control" runat="server" 
                    Width = "250px" ToolTip="MPA/MNA Name"></asp:TextBox> </td>
        </tr>
        </table>
                <br />
                </div>
            </div>
        </div>

    <div class="row">
        <div class="col-md-10 col-md-offset-1">
        <div class="container">
        <ul class="list-group">
        <asp:RequiredFieldValidator  ID="RequiredFieldValidator3" runat="server" Display="None"
            ErrorMessage="CatCode must be required" ControlToValidate="txtCatcode" ValidationGroup="save" CssClass="alert-danger"></asp:RequiredFieldValidator>
        <asp:RequiredFieldValidator  ID="RequiredFieldValidator4" runat="server" Display="None"
            ErrorMessage="Cons must be required" ControlToValidate="txtCons" CssClass="alert-danger"  ValidationGroup="save"></asp:RequiredFieldValidator>
        <asp:RequiredFieldValidator  ID="RequiredFieldValidator7" Display="None"
                         runat="server" ErrorMessage="Office Code Must be Required" ControlToValidate="txtLetterNo" CssClass="alert-danger"  ValidationGroup="save"></asp:RequiredFieldValidator>
   </ul>
            </div>
        
    <asp:ValidationSummary ID="ValidationSummary1" CssClass="alert-danger" DisplayMode="BulletList" ShowSummary="true" ValidationGroup="save" HeaderText="Please Remove these errors" runat="server" />
    </div>
        </div>
    
    <div class="row">
        <div class="col-md-offset-1">
            
            <div class="col-md-11">
                <table class="table table-bordered">
            <tr>
                <td>
                     <asp:Label ID="lblFaNo" runat="server" Text="FaNo" ForeColor="Black" Font-Size="Medium"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtFaNo" runat="server"  CssClass="form-control" ReadOnly="True"></asp:TextBox>
                </td>
                <td>
                    <label for="Date1" class="top-buffer">Ref Date</label>
                </td>
                <td>
                    <telerik:RadDateTimePicker ID="Date1" Runat="server" CssClass="form-control top-buffer" Skin="Office2007" Width="250px">                       
                        <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x" 
                                    skin="Office2007" runat="server"></Calendar>
                        <DateInput DisplayDateFormat="dd-MMM-yy" DateFormat="dd-MMM-yy" runat="server"></DateInput>
                        <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                            </telerik:RadDateTimePicker>
                        <asp:RequiredFieldValidator  ID="RequiredFieldValidator2" runat="server" ErrorMessage="RefDate must be required"
                            ControlToValidate="Date1" CssClass="alert-danger"  ValidationGroup="save" Display="None"></asp:RequiredFieldValidator>
                </td>
            </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblAppName" runat="server" Text="Appname" ForeColor="Black" 
                                Font-Size="Medium"></asp:Label>
                        </td>
                        
                        <td>
                             <asp:TextBox ID="txtAppName" runat="server" CssClass="form-control"
                                ></asp:TextBox>
                        </td>
                        
                        <td>
                        <asp:Label ID="lblContact" runat="server" Text="Contact" ForeColor="Black" Font-Size="Medium"></asp:Label>

                        </td>

                        <td>
                            <asp:TextBox ID="txtContact" runat="server"  CssClass="form-control"></asp:TextBox>

                        </td>

                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblPatientName" runat="server" Text="Patient Name" ForeColor="Black" 
                                Font-Size="Medium"></asp:Label>
                        </td>
                        <td>
                             <asp:TextBox ID="txtPtName" runat="server" CssClass="form-control"
                                ></asp:TextBox>
                        </td>
                        <td>
                             <asp:Label ID="lblAddress" runat="server" Text="Address" ForeColor="Black" Font-Size="Medium"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtAddress" runat="server"  CssClass="form-control" 
                                   TextMode="MultiLine"></asp:TextBox>
                        </td>
                    </tr>

                    <tr>
                        <td>
                         <asp:Label ID="lblRefNo" runat="server" Text="Reference No" ForeColor="Black" Font-Size="Medium"></asp:Label>

                        </td>
                         <td colspan="3">
                              <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                
                                <asp:TextBox ID="txtRef" runat="server"  CssClass="form-control" Enabled="False"></asp:TextBox>
                                 </ContentTemplate>
                            </asp:UpdatePanel> 
                        </td>
                        </tr>
                    <tr>
                         <td>
                          <asp:Label ID="lblDistrict" runat="server" Text="District" ForeColor="Black" Font-Size="Medium"></asp:Label>

                        </td>
                         <td>
                            <asp:TextBox ID="txtDistrict" runat="server"  CssClass="form-control"></asp:TextBox>

                        </td>
                        <td>
                        <asp:Label ID="lblType" runat="server" Text="Type" ForeColor="Black" Font-Size="Medium"></asp:Label>

                        </td>
                        <td>
                             <asp:DropDownList ID="ddlType" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlType_SelectedIndexChanged" AutoPostBack="True">
                            </asp:DropDownList>
                            <asp:TextBox ID="txtSubject" runat="server"  
                                CssClass="form-control"></asp:TextBox>
                        </td>
                    </tr>

                    <tr>
                        <td>
                             <asp:Label ID="lblCNIC" runat="server" Text="CNIC" ForeColor="Black" Font-Size="Medium"></asp:Label>
                        </td>
                         <td>
                            <asp:TextBox ID="txtCNIC" runat="server" CssClass="form-control"></asp:TextBox>

                        </td>
                         <td>
                         <asp:Label ID="lblLetterNo" runat="server" Text="Letter No." ForeColor="Black" Font-Size="Medium"></asp:Label>

                        </td>
                         <td>
                              <asp:TextBox ID="txtLetterNo" runat="server" CssClass="form-control"></asp:TextBox>
                        <asp:RequiredFieldValidator  ID="RequiredFieldValidator1" runat="server" Display="None"
                            ErrorMessage="Letter Code Must be Required" ControlToValidate="txtLetterNo" CssClass="alert-danger"  ValidationGroup="save"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                           <asp:Label ID="lblDepartment" runat="server" Text="Department" ForeColor="Black" Font-Size="Medium"></asp:Label>

                        </td>
                         <td>
                              <asp:DropDownList ID="ddlDepartment" runat="server"  CssClass="form-control">
                            </asp:DropDownList>
                             <asp:RequiredFieldValidator  ID="RequiredFieldValidator5"
                                 runat="server" ErrorMessage="Department Code Must be Required" InitialValue="0"
                                 ControlToValidate="txtLetterNo" CssClass="alert-danger" Display="None" ValidationGroup="save">

                             </asp:RequiredFieldValidator>
                        </td>
                         <td>
                        <label for="SentOn">Sent On Date</label>

                        </td>
                         <td>
                        <telerik:RadDatePicker ID="SentOn" CssClass="form-control" Width="250px" Skin="Office2007" runat="server"></telerik:RadDatePicker>

                        </td>
                    </tr>
                    <tr>
                        <td>
                           <asp:Label ID="lblTargetDate" runat="server" Text="Target Date" ForeColor="Black" Font-Size="Medium"></asp:Label>

                        </td>
                        <td>
                            <telerik:RadDateTimePicker ID="TargetDate"  runat="server" CssClass="form-control" 
                                Skin="Office2007" Width="250px">
                            <TimeView CellSpacing="-1" runat="server"></TimeView>

                            <TimePopupButton ImageUrl="" HoverImageUrl=""></TimePopupButton>

                            <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x" 
                                    Skin="Office2007" runat="server"></Calendar>

                            <DateInput DisplayDateFormat="dd-MMM-yy" DateFormat="dd-MMM-yy" LabelWidth="40%" Height="22px" runat="server"></DateInput>

                            <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                            </telerik:RadDateTimePicker>
                        </td>
                        <td>
                         <asp:Label ID="lblMode" runat="server" Text="Mode of F.Aid (D,G,F)" ForeColor="Black" Font-Size="Medium"></asp:Label>

                        </td>
                        <td>
                            <asp:TextBox ID="txtMode" runat="server" CssClass="form-control"></asp:TextBox>

                        </td>
                    </tr>
                    <tr>
                        <td>
                        <asp:Label ID="lblpc1" runat="server" Text="PC 1" ForeColor="Black" Font-Size="Medium"></asp:Label>

                        </td>
                        <td>
                              <asp:TextBox ID="txtPC1" runat="server" CssClass="form-control"></asp:TextBox>

                        </td>
                        <td>
                         <asp:Label ID="lblpc2" runat="server" Text="PC 2" ForeColor="Black" Font-Size="Medium"></asp:Label>

                        </td>
                        <td>
                        <asp:TextBox ID="txtPC2" runat="server"  CssClass="form-control"></asp:TextBox>
                        </td>

                    </tr>
                    <tr>
                        <td class="col-xs-1">
                        <asp:Label ID="lblpc3" runat="server" Text="PC 3" ForeColor="Black" Font-Size="Medium"></asp:Label>
                            
                        </td>
                        <td class="col-xs-3">
                        <asp:TextBox ID="txtPC3" runat="server"  CssClass="form-control"></asp:TextBox>

                        </td>
                        <td class="col-xs-1">
                         <asp:Label ID="lblpc43" runat="server" Text="PC 4" ForeColor="Black" Font-Size="Medium"></asp:Label>

                        </td>
                        <td class="col-xs-3">
                         <asp:TextBox ID="txtPC4" runat="server" CssClass="form-control"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="col-xs-1">
                       <asp:Label ID="lblReplyNo" runat="server" Text="Reply #" ForeColor="Black" Font-Size="Medium"></asp:Label>

                        </td>
                        <td class="col-xs-3">
                       <asp:TextBox ID="txtReplyNo" runat="server" CssClass="form-control"></asp:TextBox>

                        </td>
                        <td class="col-xs-1">
                          <asp:Label ID="lblReplyDate" runat="server" Text="Reply Date" ForeColor="Black" Font-Size="Medium"></asp:Label>

                        </td>
                        <td class="col-xs-3">
                               <telerik:RadDateTimePicker ID="ReplyDate" runat="server" 
                                Skin="Office2007" CssClass="form-control" width="250">
                        <TimeView CellSpacing="-1" runat="server"></TimeView>

                        <TimePopupButton ImageUrl="" HoverImageUrl="" runat="server"></TimePopupButton>

                        <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x" 
                                    Skin="Office2007" runat="server"></Calendar>

                        <DateInput DisplayDateFormat="dd-MMM-yy" DateFormat="dd-MMM-yy" LabelWidth="40%" 
                                    runat="server" Height="23px"></DateInput>

                        <DatePopupButton ImageUrl="" HoverImageUrl="" runat="server"></DatePopupButton>
                            </telerik:RadDateTimePicker>
                        </td>
                    </tr>
                    <tr>
                        <td class="col-xs-1">
                        <asp:Label ID="lblStatus" runat="server" Text="Status" ForeColor="Black" Font-Size="Medium"></asp:Label>                           

                        </td>
                        <td class="col-xs-3">
                             <asp:DropDownList ID="ddlStatus" runat="server" CssClass="form-control" AutoPostBack="True" 
                                onselectedindexchanged="ddlStatus_SelectedIndexChanged"> </asp:DropDownList>
                         <asp:RequiredFieldValidator  ID="RequiredFieldValidator6" runat="server" InitialValue="0" 
                             ErrorMessage="Status Code Must be Required" ControlToValidate="ddlStatus" CssClass="alert-danger"  
                             ValidationGroup="save" Display="Dynamic"></asp:RequiredFieldValidator>
                        </td>
                        <td class="col-xs-1">
                         <asp:Label ID="lblRecommendation" runat="server" Text="Recommendation of DCO" ForeColor="Black" Font-Size="Medium"></asp:Label>                       

                        </td>
                        <td class="col-xs-3">
                            <asp:TextBox ID="txtRecommendation" runat="server"  CssClass="form-control"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="col-xs-1">
                        <asp:Label ID="lblMeeting" runat="server" Text="Meeting" ForeColor="Black" Font-Size="Medium"></asp:Label>

                        </td>
                         <td class="col-xs-3">
                            <asp:TextBox ID="txtMeeting" runat="server" CssClass="form-control"></asp:TextBox>

                        </td>
                         <td class="col-xs-1">
                          <asp:Label ID="lblAmount" runat="server" Text="Amount" ForeColor="Black" Font-Size="Medium"></asp:Label>

                        </td>
                         <td class="col-xs-3">
                             <asp:TextBox ID="txtAmount" runat="server" CssClass="form-control"></asp:TextBox> 
                            <asp:TextBox ID="txtAmountinWords" runat="server" CssClass="form-control"></asp:TextBox>
                        </td>
                    </tr>

                    <tr>
                        <td class="col-xs-1">
                        <asp:Label ID="lblCMOrders" runat="server" Text="CM Orders" ForeColor="Black" Font-Size="Medium"></asp:Label>                        

                        </td>
                        <td class="col-xs-3">
                            <asp:TextBox ID="txtCMOrders" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>

                        </td>
                        <td class="col-xs-1">
                           <asp:Label ID="lblRemarks" runat="server" Text="Remarks" ForeColor="Black" Font-Size="Medium"></asp:Label>

                        </td>
                        <td class="col-xs-3">
                           <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control"></asp:TextBox>

                        </td>
                    </tr>
                    <tr>
                        <td class="col-xs-1">
                           <asp:Label ID="lblMeetingDate" runat="server" Text="Meeting Date" ForeColor="Black" Font-Size="Medium"></asp:Label>

                        </td>
                        <td class="col-xs-3">
                             <telerik:RadDateTimePicker ID="MeetingDate" runat="server" CssClass="form-control" Skin="Office2007" Width="206px">
                            <TimeView CellSpacing="-1" runat="server"></TimeView>
                            <TimePopupButton ImageUrl="" HoverImageUrl=""></TimePopupButton>
                            <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x" 
                                    Skin="Office2007" runat="server"></Calendar>
                            <DateInput DisplayDateFormat="dd-MMM-yy" DateFormat="dd-MMM-yy" LabelWidth="40%" Height="22px" runat="server"></DateInput>
                            <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                            </telerik:RadDateTimePicker>
                        </td>
                        <td class="col-xs-1">
                         <asp:Label ID="lblRefferedTo" runat="server" Text="Ref.to Committee On" ForeColor="Black" Font-Size="Medium"></asp:Label>

                        </td>
                        <td class="col-xs-3">
                             <telerik:RadDateTimePicker ID="ReftoCommDate" runat="server" CssClass="form-control" Skin="Office2007" Width="250" >
                            <TimeView CellSpacing="-1" runat="server"></TimeView>
                            <TimePopupButton ImageUrl="" HoverImageUrl=""></TimePopupButton>
                            <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x" 
                                    Skin="Office2007" runat="server"></Calendar>
                            <DateInput DisplayDateFormat="dd-MMM-yy" DateFormat="dd-MMM-yy" CssClass="form-control" Height="22px" runat="server"></DateInput>
                            <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                            </telerik:RadDateTimePicker>
                        </td>
                    </tr>
        </table>
            </div>
        </div>
        </div>
   
    
    
            
   
           
                 <table width = "80%" align = "center" cellpadding = "0" cellspacing = "0" style = "border:1px dashed white">                               
            
                    <tr>
                    <td align = "center" colspan = "4">
                        <br />
                        <asp:Button ID="btnAdd" runat="server" Text="Add" Font-Size="Medium" CssClass="btn btn-primary btn-lg" Width="85px" onclick="btnAdd_Click" />
                        <asp:Button ID="btnEdit" runat="server" Text="Edit" Font-Size="Medium" CssClass="btn btn-primary btn-lg" Width="85px" onclick="btnEdit_Click" />
                        <asp:Button ID="btnSave" runat="server" Text="Save" Font-Size="Medium" CssClass="btn btn-primary btn-lg" Width="85px" onclick="btnSave_Click" ValidationGroup="save" />
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" Font-Size="Medium" CssClass="btn btn-primary btn-lg" Width="85px" onclick="btnCancel_Click" />
                    </td>
                    </tr>                

        </table>             
</asp:Content>
<asp:Content ID="Content3" runat="server" contentplaceholderid="cphScripts">
    <script type="text/javascript">

    </script>
</asp:Content>


﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MpaReport.aspx.cs" Inherits="DG.WebUI.MpaReport" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Sohail Akbar">
    <link  href="styles/default.css" rel="stylesheet" />
     <!-- Bootstrap Core CSS -->
    <link  href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
    <!-- MetisMenu CSS -->
    <link  href="bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet" />
    <!-- Timeline CSS -->
    <link  href="dist/css/timeline.css" rel="stylesheet" />
    <!-- Custom CSS -->
    <link href="dist/css/sb-admin-2.css" rel="stylesheet" />
    <!-- DataTables CSS -->
    <link href="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet" />
    <!-- DataTables Responsive CSS -->
    <link href="../bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet" />
    <!-- Custom Fonts -->
    <link  href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
        <style>
        body {
    background: url('/Images/cool_bg1.jpg') no-repeat, no-repeat; 
    background-repeat: no-repeat;
    background-position: center center;
    background-attachment: fixed;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
}
            </style>
    </head>
    <body> 
     <form id="form1" runat="server">         
   
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>   
        
     <p> &nbsp;</p>
<asp:Panel ID = "ReportPanel" runat="server" DefaultButton="btnSubmit"> 
<table style="border:1px dashed white;border-collapse:collapse;" align="center" width="700" cellpadding="2" cellspacing="1">
    <tr>
        <td align="center" colspan="2"><asp:Label ID="Label1" runat="server" Text="Ex MPAs Report" Font-Names="Arial Black" Font-Size="X-Large" ForeColor="#3366CC"></asp:Label>
            <br /> <br /> <br />
        </td>
        <td align = "left"> 
            <br />
            <br />
        </td>
    </tr>

    <tr>
        <td align ="center" width="80px">
            <asp:Label ID="lblText" runat="server" Text="Enter Name" ForeColor="#3366CC"></asp:Label> </td>
        <td align = "left">
            
            <asp:TextBox ID="txtExmpaName" runat="server" CssClass="form-control"></asp:TextBox>             
        </td>            
    </tr>   
        <tr>
            <td></td>
                    <td align="center">
                <br /><br />
                <asp:Button ID="btnSubmit" runat="server" Text="SUBMIT" CssClass="btn btn-danger btn-lg" onclick="btnSubmit_Click" />
            </td>
        </tr>     

</table>
<br /></asp:Panel>
    <!-- jQuery -->
    <script type="text/javascript" src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script type="text/javascript" src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script type="text/javascript" src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script type="text/javascript" src="../bower_components/raphael/raphael-min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script type="text/javascript" src="../dist/js/sb-admin-2.js"></script>

    <script type="text/javascript" src="../bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" src="JS/Menu-Collapse.js"></script>

   <!--<script>
       $(document).ready(function () {
       $('#ContentPlaceHolder1_DgSearch').DataTable({
       "responsive": true

       });
       $("#menu-toggle").click(function () {
       $("#side-menu").hide();
       });
       });

    </script>-->
         </form></body>
</html>

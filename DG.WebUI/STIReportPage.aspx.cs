﻿using DGModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DG.WebUI
{
    public partial class STIReportPage : System.Web.UI.Page
    {
        private const string FaNoWise = "FaNoWise";
        private const string Noting32 = "Noting 32";
        private const string Noting33 = "Noting 33";
        private const string DailyList = "Daily List";
        private const string MPAWiseReport = "MPA Wise Report";
        private const string NewLetter = "New Letter";
        private const string MadamLetter = "MADAM Letter";
        private const string MEDICALBOARD = "MEDICAL BOARD";
        private const string FMT46 = "FMT - 46";
        private const string FMT47 = "FMT - 47";
        private const string FMT48 = "FMT - 48";
        private const string FMT49 = "FMT - 49";
        private const string LettertoDCOMulti = "Letter to DCO Multiple";
        private const string LettertoDCOSingle = "Letter to DCO Single";
        private const string SingleNoting = "Single Noting";
        private const string TreasuryLetterSingle = "Treasury Letter Single";
        private const string Treasury12 = "Treasury Letter 12";
        private object rblReportCategories;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (Request["Report"] == "DailyList")
                {
                    txtFANoFrom.Enabled = false;
                    txtFaNoTo.Enabled = false;
                    RadDateTimePicker1.Enabled = false;
                }
                if (Request["Report"] == "FMT46" || Request["Report"] == "FMT47" || Request["Report"] == "FMT48" || Request["Report"] == "FMT49")
                {
                    txtFANoFrom.Enabled = true;
                    txtFaNoTo.Enabled = true;
                    RadDateTimePicker1.Enabled = false;
                    RadDateTimePicker2.Enabled = false;
                    txtCatCode.Enabled = false;
                    txtCons.Enabled = false;
                }
                if (Request["Report"] == "FaNoWise")
                {
                    txtCatCode.Visible = false;
                    txtCons.Visible = false;
                    RadDateTimePicker1.Visible = false;
                    RadDateTimePicker2.Visible = false;
                    lblCatCode.Visible = false;
                    lblCons.Visible = false;
                    lblDateFrom.Visible = false;
                    lblDateTo.Visible = false;

                }
                if (Request["Report"] == "LettertoDCOSingle")
                {
                    txtFANoFrom.Visible = true;
                    lblFanoFrom.Text = "FaNo";
                    txtFaNoTo.Visible = false;
                    lblFanoTo.Visible = false;
                    txtCatCode.Visible = true;
                    txtCons.Visible = false;
                    RadDateTimePicker1.Visible = false;
                    RadDateTimePicker2.Visible = false;
                    lblCatCode.Visible = true;
                    lblCatCode.Text = "File No.";
                    lblCons.Visible = false;
                    lblDateFrom.Visible = false;
                    lblDateTo.Visible = false;

                }
                if (Request["Report"] == "LettertoDCOMulti")
                {
                    RadDateTimePicker1.Visible = true;
                    RadDateTimePicker2.Visible = true;
                    lblCatCode.Visible = true;
                    txtCatCode.Visible = true;
                    lblCatCode.Text = "Letter Code";                   
                    lblDateFrom.Visible = true;
                    lblDateTo.Visible = true;
                    lblFanoFrom.Visible = false;
                    txtFANoFrom.Visible = false;
                    txtFaNoTo.Visible = false;
                    lblFanoTo.Visible = false;
                    txtCons.Visible = false;
                    lblCons.Visible = false;
                }
                if (Request["Report"] == "Treasury12")
                {
                    RadDateTimePicker1.Visible = true;
                    RadDateTimePicker2.Visible = true;
                    lblCatCode.Visible = true;
                    txtCatCode.Visible = true;
                    lblCatCode.Text = "File #";
                    lblDateFrom.Visible = true;
                    lblDateTo.Visible = true;
                    lblFanoFrom.Visible = false;
                    txtFANoFrom.Visible = false;
                    txtFaNoTo.Visible = false;
                    lblFanoTo.Visible = false;

                    txtCons.Visible = false;
                    lblCons.Visible = false;

                }
                if (Request["Report"] == "Noting33")
                {
                    txtFANoFrom.Text = "1";
                    txtFaNoTo.Text = "999999";
                                       
                }
                List<string> menuOptions = new List<string>();
                menuOptions.Add(FaNoWise);
                menuOptions.Add(Noting32);
                menuOptions.Add(Noting33);
                menuOptions.Add(DailyList);
                menuOptions.Add(MPAWiseReport);
                menuOptions.Add(NewLetter);
                menuOptions.Add(MadamLetter);
                menuOptions.Add(MEDICALBOARD);
                menuOptions.Add(SingleNoting);
                menuOptions.Add(FMT46);
                menuOptions.Add(FMT47);
                menuOptions.Add(FMT48);
                menuOptions.Add(FMT49);
                menuOptions.Add(LettertoDCOSingle);
                menuOptions.Add(LettertoDCOMulti);
                menuOptions.Add(TreasuryLetterSingle);
                menuOptions.Add(Treasury12);

               
            }
            using (DGEntities dg = new DGEntities())
            {    
            }
        
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (Request["Report"] == "Noting32")
            {
                int fanofrom = Convert.ToInt32(this.txtFANoFrom.Text);
                int fanoto = Convert.ToInt32(this.txtFaNoTo.Text);
                DateTime fromdate = this.RadDateTimePicker1.SelectedDate.Value;
                DateTime todate = this.RadDateTimePicker2.SelectedDate.Value;
                string report = Request["Report"];
                Response.Redirect("STIReportsView.aspx?reportid=" + report + "&fromfano=" + fanofrom + "&tofano=" + fanoto + "&fromdate=" + fromdate + "&todate=" + todate);
            }
            if (Request["Report"] == "Noting33")
            {
                int fanofrom = Convert.ToInt32(this.txtFANoFrom.Text);
                int fanoto = Convert.ToInt32(this.txtFaNoTo.Text);
                DateTime fromdate = Convert.ToDateTime(this.RadDateTimePicker1.SelectedDate.Value);
                DateTime todate = Convert.ToDateTime(this.RadDateTimePicker2.SelectedDate.Value);
                DGEntities ent = new DGEntities();
                var result = from a in ent.MAINCORRs where a.Printing == false && a.asadate >= fromdate && a.asadate <= todate select a;
                if(result.Count()>0)
                {
                    foreach(var val in result.ToList())
                    {
                        val.Printing = true;
                        ent.SaveChanges();
                    }
                }
                string report = Request["Report"];
                Response.Redirect("STIReportsView.aspx?reportid=" + report + "&fromfano=" + fanofrom + "&tofano=" + fanoto + "&fromdate=" + fromdate + "&todate=" + todate);
            }
            if (Request["Report"] == "DailyList")
            {
                DateTime todate = this.RadDateTimePicker2.SelectedDate.Value;
                string report = Request["Report"];
                Response.Redirect("STIReportsView.aspx?reportid=" + report + "&todate=" + todate);
            }
            if (Request["Report"] == "FaNoWise")
            {
                int fanofrom = Convert.ToInt32(this.txtFANoFrom.Text);
                int fanoto = Convert.ToInt32(this.txtFaNoTo.Text);
                string report = Request["Report"];
                Response.Redirect("STIReportsView.aspx?reportid=" + report + "&fromfano=" + fanofrom  + "&tofano=" + fanoto);
            }
            if (Request["Report"] == "MPAWiseReport")
            {
                int catcode = Convert.ToInt32(this.txtCatCode.Text);
                int cons = Convert.ToInt32(this.txtCons.Text);
                string report = Request["Report"];
                Response.Redirect("STIReportsView.aspx?reportid=" + report + "&catcode=" + catcode + "&cons=" + cons);
            }
            if (Request["Report"] == "Letter2017")
            {
                RadDateTimePicker1.Visible = false;
                RadDateTimePicker2.Visible = false;
                txtCatCode.Visible = false;
                txtCons.Visible = false;
                int fanofrom = Convert.ToInt32(this.txtFANoFrom.Text);
                int fanoto   = Convert.ToInt32(this.txtFaNoTo.Text);
                string report = Request["Report"];
                Response.Redirect("STIReportsView.aspx?reportid=" + report + "&fromfano=" + fanofrom + "&tofano=" + fanoto);
            }
            if (Request["Report"] == "SingleNoting")
            {
                txtCatCode.Visible = false;
                txtCons.Enabled = false;
                txtFANoFrom.Enabled = true;
                txtFaNoTo.Enabled = true;
                int fanofrom = Convert.ToInt32(this.txtFANoFrom.Text);
                int fanoto = Convert.ToInt32(this.txtFaNoTo.Text);
                string report = Request["Report"];
                Response.Redirect("STIReportsView.aspx?reportid=" + report + "&fromfano=" + fanofrom + "&tofano=" + fanoto);
            }
            if (Request["Report"] == "NotingMultiple")
            {
                txtCatCode.Visible = false;
                txtCons.Enabled = false;
                txtFANoFrom.Enabled = true;
                txtFaNoTo.Enabled = true;
                DateTime fromdate = this.RadDateTimePicker1.SelectedDate.Value;
                DateTime todate = this.RadDateTimePicker2.SelectedDate.Value;
                string mfileno = this.txtCatCode.Text;
                string report = Request["Report"];
                Response.Redirect("STIReportsView.aspx?reportid=" + report + "&fromdate=" + fromdate + "&todate=" + todate +"&mfileno=" + mfileno);
            }
            if (Request["Report"] == "LettertoDCOSingle")
            {
                int fanofrom = Convert.ToInt32(this.txtFANoFrom.Text);
                //int fanoto = Convert.ToInt32(this.txtFaNoTo.Text);
                string filenos = txtCatCode.Text;
                string report = Request["Report"];
                Response.Redirect("STIReportsView.aspx?reportid=" + report + "&fano=" + fanofrom + "&mfileno=" +filenos);
            }
            if (Request["Report"] == "LettertoDCOMulti")
            {
                DateTime fromdate = this.RadDateTimePicker1.SelectedDate.Value;
                DateTime todate = this.RadDateTimePicker2.SelectedDate.Value;
                int letcode = Convert.ToInt32(this.txtCatCode.Text);
                string filenos = txtCatCode.Text;
                string report = Request["Report"];
                Response.Redirect("STIReportsView.aspx?reportid=" + report + "&fromdate=" + fromdate + "&Todate=" + todate + "&letcode=" +letcode);
            }
            if (Request["Report"] == "TreasuryLetterSingle")
            {
                int fanos = Convert.ToInt32(txtFANoFrom.Text);
                string filenos = txtCatCode.Text;
                string report = Request["Report"];
                Response.Redirect("STIReportsView.aspx?reportid=" + report + "&fanos=" + fanos + "&mfileno=" + filenos);
            }
            if (Request["Report"] == "Treasury12")
            {
                DateTime fromdate = this.RadDateTimePicker1.SelectedDate.Value;
                DateTime todate = this.RadDateTimePicker2.SelectedDate.Value;
                string filenos = txtCatCode.Text;
                string report = Request["Report"];
                Response.Redirect("STIReportsView.aspx?reportid=" + report + "&fromdate=" + fromdate + "&todate=" +todate + "&mfileno=" + filenos);
            }
            if (Request["Report"] == "MADAMLetter2017")
            {
                RadDateTimePicker1.Visible = false;
                RadDateTimePicker2.Visible = false;
                txtCatCode.Visible = false;
                txtCons.Visible = false;
                int fanofrom = Convert.ToInt32(this.txtFANoFrom.Text);
                int fanoto = Convert.ToInt32(this.txtFaNoTo.Text);
                string report = Request["Report"];
                Response.Redirect("STIReportsView.aspx?reportid=" + report + "&fromfano=" + fanofrom + "&tofano=" + fanoto);
            }

            if (Request["Report"] == "MEDICALBOARD")
            {
                RadDateTimePicker1.Visible = false;
                RadDateTimePicker2.Visible = false;
                txtCatCode.Visible = false;
                txtCons.Visible = false;
                int fanofrom = Convert.ToInt32(this.txtFANoFrom.Text);
                int fanoto = Convert.ToInt32(this.txtFaNoTo.Text);
                string report = Request["Report"];
                Response.Redirect("STIReportsView.aspx?reportid=" + report + "&fromfano=" + fanofrom + "&tofano=" + fanoto);
            }
            if (Request["Report"] == "FMT46")
            {
                int fanofrom = Convert.ToInt32(this.txtFANoFrom.Text);
                int fanoto = Convert.ToInt32(this.txtFaNoTo.Text);
                string report = Request["Report"];
                Response.Redirect("STIReportsView.aspx?reportid=" + report + "&fromfano=" + fanofrom + "&tofano=" + fanoto);
            }
            if (Request["Report"] == "FMT47")
            {
                int fanofrom = Convert.ToInt32(this.txtFANoFrom.Text);
                int fanoto = Convert.ToInt32(this.txtFaNoTo.Text);
                string report = Request["Report"];
                Response.Redirect("STIReportsView.aspx?reportid=" + report + "&fromfano=" + fanofrom + "&tofano=" + fanoto);
            }
            if (Request["Report"] == "FMT48")
            {
                int fanofrom = Convert.ToInt32(this.txtFANoFrom.Text);
                int fanoto = Convert.ToInt32(this.txtFaNoTo.Text);
                string report = Request["Report"];
                Response.Redirect("STIReportsView.aspx?reportid=" + report + "&fromfano=" + fanofrom + "&tofano=" + fanoto);
            }
            if (Request["Report"] == "FMT49")
            {
                int fanofrom = Convert.ToInt32(this.txtFANoFrom.Text);
                int fanoto = Convert.ToInt32(this.txtFaNoTo.Text);
                string report = Request["Report"];
                Response.Redirect("STIReportsView.aspx?reportid=" + report + "&fromfano=" + fanofrom + "&tofano=" + fanoto);
            }
        }

        protected void btnStatus_Click(object sender, EventArgs e)
        {
            RadDateTimePicker1.SelectedDate = DateTime.Now;
            RadDateTimePicker2.SelectedDate = DateTime.Now;
            DateTime fromdate = Convert.ToDateTime(this.RadDateTimePicker1.SelectedDate.Value);
            DateTime todate = Convert.ToDateTime(this.RadDateTimePicker2.SelectedDate.Value);
            DGEntities ent = new DGEntities();
            var result = from a in ent.MAINCORRs where a.Printing == true && a.asadate >= fromdate && a.asadate <= todate select a;
            if (result.Count() > 0)
            {
                foreach (var val in result.ToList())
                {
                    val.Printing = false;
                    ent.SaveChanges();
                }
            }

        }
    }
}
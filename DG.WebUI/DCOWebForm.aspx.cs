﻿using DGModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DG.WebUI
{
    public partial class DCOWebForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           
            if (!IsPostBack)
            {
                Cls_Login val = (Cls_Login)Session["login"];
                LoadGrid(val.deptid);
                lblFaNo.Visible = true;
                lblDCName.Text = val.Deptname;

            }

        }
        private void LoadGrid(int deptid)
        {
            DGEntities dgent = new DGEntities();

            var dglist = (from a in dgent.MAINCORRs
                          from d in dgent.DEPTs
                          where a.deptcode == d.deptcode  && a.deptcode == deptid && a.statcode==11
                          orderby a.fano descending
                          select new { a.DGId, a.fano, PATIENTINFO = a.appname + a.address + a.cnic, a.amount, a.chqno, a.chqdate, a.dcremarks, d.deptname }).ToList();

            dgvDG.DataSource = dglist;
            dgvDG.DataBind();
            dglist.Count();
            lblRecords.Text = "(Total Records :" + dglist.Count.ToString()+")";
            Session["dtData"] = dglist;

        }

        protected void dgvDG_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            dgvDG.PageIndex = e.NewPageIndex;
            dgvDG.DataSource = Session["dtData"];
            dgvDG.DataBind();

        }

        protected void dgvDG_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void dgvDG_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {

        }

        protected void lnkBtnSearch_Click(object sender, EventArgs e)
        {
            DGEntities dgent = new DGEntities();


            if (txtFaNo.Text != "")
            {
                Cls_Login val = (Cls_Login)Session["login"];
                int fano = Convert.ToInt32(txtFaNo.Text);
                int deptcode = Convert.ToInt32(val.deptid);
                var fanowise = dgent.SEARCHDCOFANOWISE(fano, deptcode);
                dgvDG.DataSource = fanowise;
                dgvDG.DataBind();
            }
            
        }

        
        protected void dgvDG_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {

            Cls_Login login = (Cls_Login)Session["Login"];
            if (login.Isedit != true)
            {
                LinkButton editbutton = (LinkButton)dgvDG.Rows[e.RowIndex].FindControl("LnkBtnEdit");
                editbutton.Enabled = false;
                return;
            }

            else
            {
                Label dgid = (Label)dgvDG.Rows[e.RowIndex].FindControl("lblDGId");
                int fano = Convert.ToInt32(dgid.Text);
                Response.Redirect("DCOFeedbkForm.aspx?DGId=" + fano);
            }
        }

       

       
    }
}
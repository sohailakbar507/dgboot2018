﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DGModel;
namespace DG.WebUI
{
    public partial class DetailedReportsPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                GetDepartments();
            }

        }

        private void GetDepartments()
        {
            DGEntities dg = new DGEntities();
            var getdept = (from d in dg.DEPTs orderby d.deptcode select d).ToList();
            DDLCode.DataSource = getdept;
            DDLCode.DataValueField = "DeptCode";
            DDLCode.DataTextField = "DeptName";
            DDLCode.SelectedIndex = 0;
            DDLCode.DataBind();
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            DateTime Fromdate = Convert.ToDateTime(DateFrom.DateInput.SelectedDate);
            DateTime Todate = Convert.ToDateTime(DateTo.DateInput.SelectedDate);
            //string mfileno = txtFileNo.Text;
            string rpt = Request["Report"].Replace(" ", string.Empty); 

            if (rpt == "DepartmentWiseDetail")
            {
                Response.Redirect("DetailedReportsPage.aspx?reportid=" + rpt);
            }
            if (rpt == "MPAWiseReport2")
            {
                Response.Redirect("./ReportsPages/ReportMainPage.aspx?reportid=" + rpt);
            }
            if (rpt == "Letter")
            {
                Response.Redirect("./ReportsPages/ReportMainPage.aspx?reportid=" + rpt);
            }
            if (rpt == "GRANTINAID")
            {
                Response.Redirect("./ReportsPages/ReportMainPage.aspx?reportid=" + rpt);
            }
        }
    }
}
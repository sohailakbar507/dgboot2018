﻿using DGModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DG.WebUI
{
    public partial class UpdateDGHealthChequeStatus : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                UploadGrid();
            }

        }

        private void UploadGrid()
        {
            DGEntities dg = new DGEntities();
            int fanos = Convert.ToInt32(Request["fano"]);
            var getfano = (from a in dg.MAINCORRs where a.fano == fanos
                           from d in dg.DEPTs
                           where d.deptcode == a.deptcode
                           orderby a.DGId descending
                           select new

                           {
                               a.DGId,
                               a.fano,
                               a.appname,
                               a.chqno,
                               a.chqdate,
                               a.amount,
                               SancNo = a.SancNo,
                               a.Sancdate,
                               a.AuthorityNumber,
                               a.Authority_Date,
                               a.fyear,
                               d.deptname,
                               a.Expense,
                               a.AmountIncured,
                               a.refunded
                               
                           }
                           ).ToList();
            DGVBill.DataSource = getfano;
            DGVBill.DataBind();
        }
        protected void DGVBill_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
        }
        protected void DGVBill_RowEditing(object sender, GridViewEditEventArgs e)
        {
        }
        protected void DGVBill_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
        }
        protected void DGVBill_RowCommand(object sender, GridViewCommandEventArgs e)
        {
        }
        protected void DGVBill_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            DGEntities dg = new DGEntities();
            foreach (GridViewRow row in DGVBill.Rows)
            {
                TextBox chqno = (TextBox)row.FindControl("txtchqno");
                TextBox chqdate = (TextBox)row.FindControl("txtchqdate");
                TextBox sancno = (TextBox)row.FindControl("txtSancNo");
                Label amount = (Label)row.FindControl("lblAmount");
                TextBox sancdate = (TextBox)row.FindControl("txtSancdate");
                TextBox authno = (TextBox)row.FindControl("txtAuthNo");
                TextBox authdate = (TextBox)row.FindControl("txtAuthdate");
                TextBox expens = (TextBox)row.FindControl("txtexp");
                TextBox refund = (TextBox)row.FindControl("txtrefunded");
                int dgid = Convert.ToInt32(DGVBill.DataKeys[row.RowIndex].Value);
                MAINCORR exp = new MAINCORR();
                MAINCORR update = (from a in dg.MAINCORRs where a.DGId == dgid select a).First();
                update.SancNo = sancno.Text;
                update.Expense = expens.Text==string.Empty ? 0 : Convert.ToInt32(expens.Text);
               // update.Expense = expens.Text == string.Empty ? 0 : Convert.ToInt32(expens.Text);
                int amnt = amount.Text==string.Empty?0:Convert.ToInt32(amount.Text);
                int expenses = expens.Text == string.Empty ? 0 : Convert.ToInt32(expens.Text);
                update.Balance = amnt - expenses;
                update.AuthorityNumber = authno.Text;
                update.refunded = refund.Text == string.Empty ? 0 : Convert.ToInt32(refund.Text);
                update.chqno = chqno.Text;
                
                if (!
                   string.IsNullOrEmpty(chqdate.Text))
                {
                    update.chqdate = Convert.ToDateTime(chqdate.Text);
                }

                if (!
                    string.IsNullOrEmpty(sancdate.Text))
                {
                    update.Sancdate = Convert.ToDateTime(sancdate.Text);
                }
                if (!
                   string.IsNullOrEmpty(authdate.Text))
                {
                    update.Authority_Date = Convert.ToDateTime(authdate.Text);
                }               
                dg.SaveChanges();
                ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "successalert();", true);
            }
        }
        protected void btnBack_Click(object sender, EventArgs e)
        {
            DGEntities dg = new DGEntities();
            int fanos = Convert.ToInt32(Request["fano"]);
            Cls_Login login = (Cls_Login)Session["Login"];
            if(login.Groupid == 2 || login.Groupid==6)
            {
                Response.Redirect("HealthForm.aspx?fano=" + fanos);
            }
            else
            {
                // int fanos = Convert.ToInt32(Request["fano"]);
                Response.Redirect("DGHealthWebForm.aspx?fano=" + fanos);
            }
        }
    }
}
﻿using DGModel;
using Stimulsoft.Report;
using Stimulsoft.Report.Dictionary;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DG.WebUI
{
    public partial class HospitalsStatsPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                this.LoadReport();
            }
        }
        private void LoadReport()
        {
            DGEntities dg = new DGEntities();
            string rptid = Request.QueryString["reportid"];
            object repo = null;
            bool access = true;
            string report_path = "";
            Cls_Login loggedInUser = (Cls_Login)Session["Login"];
            string ConnectionString = ConfigurationManager.ConnectionStrings["Reports"].ConnectionString;
            StiSqlDatabase sqlDB = new StiSqlDatabase();
            StiReport rpt = new StiReport();
            StiWebViewer1.ShowParametersButton = false;
            if (loggedInUser == null)
            {
                Session["LoginMessage"] = "You are not properly logged in.";
                Response.Redirect("~/LoginPage.aspx");
                return;
            }

            if (loggedInUser.Groupid == 15 && loggedInUser.Username == "dms")
            {
                if (rptid == "HospitalsStatsOverall")
                {
                    report_path = Server.MapPath("~/Reports/StatsGangaRam.mrt");
                    rpt.Load(report_path);
                    sqlDB = (StiSqlDatabase)rpt.Dictionary.Databases[0];
                    sqlDB.ConnectionString = ConnectionString;
                    rpt.Compile();

                }
                if (rptid == "HospitalStatsCurrent")
                {
                    report_path = Server.MapPath("~/Reports/StatsGangaRamCurrent.mrt");
                    rpt.Load(report_path);
                    sqlDB = (StiSqlDatabase)rpt.Dictionary.Databases[0];
                    sqlDB.ConnectionString = ConnectionString;
                    rpt.Compile();

                }
                if (rptid == "FinancialPosition")
                {
                    report_path = Server.MapPath("~/Reports/GANGARAMFINANCIALDETAILS.mrt");
                    rpt.Load(report_path);
                    sqlDB = (StiSqlDatabase)rpt.Dictionary.Databases[0];
                    sqlDB.ConnectionString = ConnectionString;
                    rpt.Compile();

                }
                   
            }
            MAINCORR m = new MAINCORR();
            if(loggedInUser.Groupid == 16 && loggedInUser.Username=="Liver")
            {
                if (rptid == "StatsOverall")
                {
                    int hspid = 9;
                    report_path = Server.MapPath("~/Reports/STATSHOSPITALVISIT.mrt");
                    rpt.Load(report_path);
                    sqlDB = (StiSqlDatabase)rpt.Dictionary.Databases[0];
                    sqlDB.ConnectionString = ConnectionString;
                    rpt.Compile();
                    rpt["hospid"] = hspid;

                }
                if (rptid == "DetailedList")
                {
                    int hspid = 9;
                    report_path = Server.MapPath("~/Reports/VisitToHospitals.mrt");
                    rpt.Load(report_path);
                    sqlDB = (StiSqlDatabase)rpt.Dictionary.Databases[0];
                    sqlDB.ConnectionString = ConnectionString;
                    rpt.Compile();
                    rpt["hospid"] = hspid;
                }
            }
            if (loggedInUser.Groupid == 16 && loggedInUser.Username == "Kidney")
            {
                if (rptid == "StatsOverall")
                {
                    int hspid = 8;
                    report_path = Server.MapPath("~/Reports/STATSHOSPITALVISIT.mrt");
                    rpt.Load(report_path);
                    sqlDB = (StiSqlDatabase)rpt.Dictionary.Databases[0];
                    sqlDB.ConnectionString = ConnectionString;
                    rpt.Compile();
                    rpt["hospid"] = hspid;

                }
                if (rptid == "DetailedList")
                {
                    int hspid = 8;
                    report_path = Server.MapPath("~/Reports/VisitToHospitals.mrt");
                    rpt.Load(report_path);
                    sqlDB = (StiSqlDatabase)rpt.Dictionary.Databases[0];
                    sqlDB.ConnectionString = ConnectionString;
                    rpt.Compile();
                    rpt["hospid"] = hspid;
                }
            }

            if (loggedInUser.Groupid == 15 && loggedInUser.Username == "skzstats")
            {
                if (rptid == "HospitalsStatsOverall")
                {
                    report_path = Server.MapPath("~/Reports/StatsShkZayed.mrt");
                    rpt.Load(report_path);
                    sqlDB = (StiSqlDatabase)rpt.Dictionary.Databases[0];
                    sqlDB.ConnectionString = ConnectionString;
                    rpt.Compile();

                }
                if (rptid == "HospitalStatsCurrent")
                {
                    report_path = Server.MapPath("~/Reports/SkzCurrent.mrt");
                    rpt.Load(report_path);
                    sqlDB = (StiSqlDatabase)rpt.Dictionary.Databases[0];
                    sqlDB.ConnectionString = ConnectionString;
                    rpt.Compile();

                }
                if (rptid == "FinancialPosition")
                {
                    report_path = Server.MapPath("~/Reports/SKZFINANCIALDETAILS.mrt");
                    rpt.Load(report_path);
                    sqlDB = (StiSqlDatabase)rpt.Dictionary.Databases[0];
                    sqlDB.ConnectionString = ConnectionString;
                    rpt.Compile();

                }
            }

            if (access == true)
            {
                rpt.Render();
                StiWebViewer1.Report = rpt;

            }
            else
            {
                Session["LoginMessage"] = "You are not authorized for this report. Please contact to Administrator";
                Response.Redirect("../LoginPage.aspx");
            }



        }
    }
}
  





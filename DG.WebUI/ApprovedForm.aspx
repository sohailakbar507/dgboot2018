﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HospitalsMaster.Master" AutoEventWireup="true" CodeBehind="ApprovedForm.aspx.cs" Inherits="DG.WebUI.ApprovedForm" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server">
</asp:ScriptManager>
    <h1 align = "center"> Approved Cases Form </h1>

  <table width="75%" align="center">
              <tr><td align="left">
                 <div class="row">
                   <div class="col-lg-6">
    <div class="input-group">
    <asp:TextBox ID="txtfind" width="200px" Height="34px" runat="server"  CssClass="form-control" placeholder="Search for..."></asp:TextBox>
      <span class="input-group-btn" style="float: left;padding-left: 0px;">
         <asp:LinkButton ID="lnkBtnSearch" runat="server" ToolTip = "Search Button" CssClass="btn btn-danger btn-md" 
                        onclick="lnkBtnSearch_Click" Width="100px">Search</asp:LinkButton>
      </span>
    </div>
  </div>
  </div>  
  </td></tr>
      </table>
     <table width = "75%" align = "center" cellpadding = "0" cellspacing = "0" style = "border:1px dashed white">
      
      <tr>
        
        <td align="left">
            <asp:Label ID="lblFANo" runat="server" Text="FANo" Font-Size="Medium" 
                ForeColor="#000066"></asp:Label></td>
        <td align="left">
            <asp:TextBox ID="txtFano" runat="server" Width = "250px"  CssClass="form-control" 
                BackColor="White" ForeColor="Black"></asp:TextBox> </td>
    
        <td align="left">
            <asp:Label ID="lblAppname" runat="server" Text="Patient Name" Font-Size="Medium" 
                ForeColor="#000066"></asp:Label></td>
        <td align="left">
            <asp:TextBox ID="txtAppname" runat="server" Width="250px" CssClass="form-control" 
                BackColor="White"></asp:TextBox>
        </td>    
    </tr>
    
    <tr>
        <td>
            <asp:Label ID="lblAddress" runat="server" Text="Address" Font-Size="Medium" 
                ForeColor="#000066"></asp:Label></td>
        <td colspan="3">
            <asp:TextBox ID="txtAddress" runat="server" Width = "755px" 
                Font-Names="Arial" BackColor="White" CssClass="form-control"></asp:TextBox>
        </td></tr>
       
        <tr>
             <td align="left">
            <asp:Label ID="lblrefno" runat="server" Text="Reference #" Font-Size="Medium" 
                ForeColor="#000066"></asp:Label></td>
        <td align="left">
            <asp:TextBox ID="txtrefno" runat="server" Width="350px" CssClass="form-control" 
                BackColor="White"></asp:TextBox>
        </td>   
        <td align="left">
            <asp:Label ID="lblDate" runat="server" Text="Date" Font-Size="Medium" 
                ForeColor="#000066"></asp:Label></td>
        <td align="left">
            <telerik:RadDateTimePicker ID="refdate" runat="server" Width="250px"  CssClass="form-control">
            </telerik:RadDateTimePicker>
        </td>
    </tr>
   
    <tr>
        
        <td align="left">
         <asp:Label ID="lblApproved" runat="server" Text="Amount Approved" Font-Size="Medium" 
                ForeColor="#000066"></asp:Label></td>
        <td align="left">
            <asp:TextBox ID="txtApproved" runat="server" Width="250px" CssClass="form-control" 
                BackColor="White"></asp:TextBox>
        </td>
         <td align="left">
        <asp:Label ID="lblIssued" runat="server" Text="Amount Issued" Font-Size="Medium" 
                ForeColor="#000066"></asp:Label></td>
        <td align="left">
            <asp:TextBox ID="txtIssued" runat="server" Width="250px" CssClass="form-control" 
                BackColor="White"></asp:TextBox></td>
    </tr>
         <tr>

             <td align="left">
            <asp:Label ID="lblPayable" runat="server" Text="Amount Payable" Font-Size="Medium" ForeColor="#000066"></asp:Label></td>
        <td align="left">
            <asp:TextBox ID="txtPayable" runat="server" Width = "250px" Font-Names="Arial" BackColor="White" CssClass="form-control"></asp:TextBox>
        </td>

            <td align="left">
            <asp:Label ID="lblfyear" runat="server" Text="Financial Year" Font-Size="Medium" 
                ForeColor="#000066"></asp:Label></td>
        <td align="left">
            <asp:TextBox ID="txtFyear" runat="server" Width = "250px" 
                Font-Names="Arial" BackColor="White" CssClass="form-control"></asp:TextBox>
        </td>
      </tr>
        <tr>    
        <td align="left">
            <asp:Label ID="lblRemarks" runat="server" Text="Remarks" Font-Size="Medium" 
                ForeColor="#000066"></asp:Label></td>
         <td colspan="3" align="left">   
             <asp:TextBox ID="txtRemarks" runat="server" Width = "760px" 
                TextMode="MultiLine" Font-Names="Arial" CssClass="form-control"></asp:TextBox></td>
    </tr>

     <tr>
                    <td align = "center" colspan = "4">
                        <br />
                        
                  <asp:LinkButton ID="lnkbtnSave" Font-Size="Medium" runat="server" ToolTip = "Save" onclick="lnkbtnSave_Click" Height="40px" Width="100px" CssClass="btn btn-danger btn-lg" >Save</asp:LinkButton>
                        <asp:LinkButton ID="lnkbtnCancel" Font-Size="Medium" runat="server" ToolTip = "Cancel" Height="40px" Width="100px" onclick="lnkbtnCancel_Click" CssClass="btn btn-danger btn-lg">Cancel</asp:LinkButton>
                    </td>
                    </tr>
                    <caption>
                        <br />
        </caption>

      </table>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphScripts" runat="server">
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DGMasterPage.Master" AutoEventWireup="true" CodeBehind="MedicalBoard.aspx.cs" Inherits="DG.WebUI.MedicalBoard" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>

<h1 align = "center"> Special Medical Board </h1>
    
<div>
              <table width="75%" align="center">
              <tr><td align="left">
                 <div class="row">
                   <div class="col-lg-6">
    <div class="input-group">
    <asp:TextBox ID="txtfind" width="200px" runat="server"  CssClass="form-control" placeholder="Search for..."></asp:TextBox>
    <asp:Button ID="btnFind" runat="server" ToolTip = "Find Button" CssClass="btn btn-primary btn-md" 
    onclick="btnFind_Click" Text="Find" Width="100px"></asp:Button>
      
        <br />
        <br />
      
        <br />
      
    </div>
  </div>
</div>  
</td></tr>         
      </table>

    <table width = "75%" align = "center" cellpadding = "0" cellspacing = "0" style = "border:1px dashed white">  
    <tr>
        
        <td align="left">
            <asp:Label ID="lblFANo" runat="server" Text="FANo"></asp:Label></td>
        <td align="left" colspan="3">
            <asp:TextBox ID="txtFano" runat="server" Width = "250px" CssClass="form-control"></asp:TextBox>
        </td>
        
    </tr>
    
    <tr>
        <td align="left">
            <asp:Label ID="lblAppname" runat="server" Text="Applicant Name"></asp:Label></td>
        <td align="left">
            <asp:TextBox ID="txtAppname" runat="server" Width="250px" CssClass="form-control"></asp:TextBox>
        </td>
        <td align="left">
            <asp:Label ID="lblAddress" runat="server" Text="Address"></asp:Label></td>
        <td align="left">
            <asp:TextBox ID="txtAddress" runat="server" Width = "350px" 
                TextMode="MultiLine" Font-Names="Arial" CssClass="form-control"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td align="left">
            <asp:Label ID="lblStatus" runat="server" Text="Status"></asp:Label></td>
        
        <td colspan="3" align="left">
            <asp:CheckBoxList ID="ChkStatus" runat="server" Width="265px" Height="80px" CssClass="form-control">
                <asp:ListItem>SMB Status</asp:ListItem>
                <asp:ListItem>RCO Status</asp:ListItem>
                <asp:ListItem>Both</asp:ListItem>
            </asp:CheckBoxList>
        </td>
    </tr>
     <tr>
        <td align="left">
        
            <asp:Label ID="lblSMBDate" runat="server" Text="SMB Date"></asp:Label></td>
         <td align="left">
                <telerik:RadDateTimePicker ID="SMBDate" runat="server" Width="200" CssClass="form-control">
                </telerik:RadDateTimePicker></td>
        <td align="left">
            <asp:Label ID="lblRCODate" runat="server" Text="RCO Date"></asp:Label></td>
            <td align="left">
                <telerik:RadDateTimePicker ID="RCODate" runat="server" Width="200" CssClass="form-control">
                </telerik:RadDateTimePicker></td>
     </tr>

     <tr>
        <td align="left">
            <asp:Label ID="lblRemarks" runat="server" Text="Remarks"></asp:Label></td>
         <td colspan="3" align="left">   
             <asp:TextBox ID="txtRemarks" runat="server" Width = "520px" 
                TextMode="MultiLine" Font-Names="Arial" CssClass="form-control"></asp:TextBox></td>
     </tr>
        
    <tr>
                    <td align = "center" colspan = "4">
                        <br />
                        
                        <asp:Button ID="btnSave" runat="server" Text="Save" Width="100px" CssClass="btn btn-primary btn-md" onclick="btnSave_Click" />
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" Width="100px" CssClass="btn btn-primary btn-md" onclick="btnCancel_Click" 
                            Font-Bold="True" />
                    </td>
                    </tr>

    </table>
</div>


</asp:Content>

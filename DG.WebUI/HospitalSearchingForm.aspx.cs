﻿using DGModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DG.WebUI
{
    public partial class HospitalSearchingForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Login"] == null)
                {
                    Response.Redirect("~/LoginPage.aspx");
                }

                txtfind.Focus();
                UploadCat();
                UploadSentto();
                UploadStatus();
                UploadTreating();
                NoEdit();
                this.ddlRefferedTo.Items.Insert(0, new ListItem("Select Hospital"));
                this.ddlStatus.Items.Insert(0, new ListItem("Select Status"));
                this.DDLSubCat.Items.Insert(0, new ListItem("Select Disease"));
                this.DDLTreating.Items.Insert(0, new ListItem("Select Medical Board Hospital"));
                //if (Request["fano"] != null)
                //{
                //    Cls_Login login = (Cls_Login)Session["Login"];
                //    txtfind.Text = Request["fano"];
                //    GetRecord(login.Username, login.Healthaccessid, Convert.ToInt32(Request["fano"]));
                //}
            }
        }
        private void UploadCat()
        {
            DGEntities dg = new DGEntities();
            var cat = (from b in dg.Subcats select b).ToList();
            DDLSubCat.DataSource = cat;
            DDLSubCat.DataValueField = "subcatid";
            DDLSubCat.DataTextField = "SubCategory";
            DDLSubCat.DataBind();
        }

        private void NoEdit()
        {
            txtFano.Enabled = false;
            txtAppname.Enabled = false;
            txtAddress.Enabled = false;
            txtPageNo.Enabled = false;          
         
        }

        private void Editable()
        {
            txtPageNo.Enabled = true;           

        }
        private void UploadStatus()
        {
            DGEntities dg = new DGEntities();
            var stat = (from b in dg.PatientStatus select b).ToList();
            ddlStatus.DataSource = stat;
            ddlStatus.DataValueField = "PtID";
            ddlStatus.DataTextField = "Description";
            //ddlStatus.SelectedIndex = 0;
            ddlStatus.DataBind();
        }

        private void UploadSentto()
        {
            DGEntities dg = new DGEntities();
            var sentto = (from a in dg.Reffereds select a).ToList();
            ddlRefferedTo.DataSource = sentto;
            ddlRefferedTo.DataValueField = "RefId";
            ddlRefferedTo.DataTextField = "Refto";
            ddlRefferedTo.DataBind();
        }
        private void UploadTreating()
        {
            DGEntities dg = new DGEntities();
            var sentto = (from a in dg.Reffereds select a).ToList();
            DDLTreating.DataSource = sentto;
            DDLTreating.DataValueField = "RefId";
            DDLTreating.DataTextField = "Refto";
            DDLTreating.DataBind();
        }
        protected void btnFind_Click(object sender, EventArgs e)
        {
        }
        private void ClearFields()
        {
            txtPageNo.Text = "";                  
        }
        private void GetRecord(string username, int fano)
        {
            try
            {
                DGEntities dg = new DGEntities();
                int fanos = fano;               
                        var getfno = from h in dg.Healths where h.fanoHealth == fanos select h;
                        var maininfo1 = (from m in dg.MAINCORRs where m.fano == fanos select m);
                        if (maininfo1.Count() > 0)
                        {
                            var maininfo = maininfo1.First();
                            txtFano.Text = Convert.ToInt32(maininfo.fano).ToString();
                            txtAppname.Text = maininfo.appname;
                            txtAddress.Text = maininfo.address;                           
                            Editable();
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "Alert", "alert('Record Not found..');", true);
                            ClearFields();
                        }
                        if (getfno.Count() > 0)
                        {
                            var healinfo = getfno.First();
                            var maininfo = (from m in dg.MAINCORRs where m.fano == healinfo.fanoHealth select m).First();                      
                            txtPageNo.Text = Convert.ToInt32(healinfo.pagenoHealth).ToString();                                                 
                            txtPatientName.Text = maininfo.PatientName;                           
                            ddlRefferedTo.SelectedIndex = ddlRefferedTo.Items.IndexOf(ddlRefferedTo.Items.FindByValue(healinfo.refid.ToString()));
                            DDLTreating.SelectedIndex = DDLTreating.Items.IndexOf(DDLTreating.Items.FindByValue(healinfo.TreatId.ToString()));
                            ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByValue(healinfo.PtStatusID.ToString()));
                            DDLSubCat.SelectedIndex = DDLSubCat.Items.IndexOf(DDLSubCat.Items.FindByValue(maininfo.subcatid.ToString()));                                   

                        }
                        else
                        {
                        }                
            }
            catch (Exception ex)
            {
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
        }
        protected void lnkBtnSearch_Click(object sender, EventArgs e)
        {
            System.Threading.Thread.Sleep(1000);
            Cls_Login login = (Cls_Login)Session["Login"];
            int fanos = Convert.ToInt32(txtfind.Text);
            GetRecord(login.Username, fanos);
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
        }
        protected void lnkbtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("LoginPage.aspx");
        }
        protected void lnkbtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                DGEntities dg = new DGEntities();
                System.Threading.Thread.Sleep(500);
                int fanos = Convert.ToInt32(txtfind.Text);
                var getfano = from a in dg.MAINCORRs where a.fano == fanos select a;
                int hfano = Convert.ToInt32(txtFano.Text);
                var getfno = from h in dg.Healths where h.fanoHealth == hfano select h;
                if (getfano.Count() > 0)
                {
                    if (getfno.Count() > 0)
                    {
                        var h = getfno.First();
                        var m = getfano.First();
                        h.fanoHealth = fanos;
                        m.pageno = Convert.ToInt32(txtPageNo.Text);
                        h.DGIdHealth = m.DGId;
                        //m.refdate = dtLetterDate.DateInput.SelectedDate;
                        h.pagenoHealth = Convert.ToInt32(txtPageNo.Text);
                        if (DDLSubCat.SelectedItem.Value == "Select Disease")
                        {
                            m.subcatid = null;
                        }
                        else
                        {
                            m.subcatid = Convert.ToInt32(DDLSubCat.SelectedItem.Value);
                        }

                        if (ddlRefferedTo.SelectedItem.Value == "Select Hospital")
                        {
                            h.refid = 101;
                        }
                        else
                        {
                            h.refid = Convert.ToInt32(ddlRefferedTo.SelectedItem.Value);
                        }
                        if (DDLTreating.SelectedItem.Value == "Select Medical Board Hospital")
                        {
                            h.TreatId = 101;
                        }
                        else
                        {
                            h.TreatId = Convert.ToInt32(DDLTreating.SelectedItem.Value);
                        }

                        if (ddlStatus.SelectedItem.Value == "Select Status")
                        {
                            h.PtStatusID = 9;
                        }
                        else
                        {
                            h.PtStatusID = Convert.ToInt32(ddlStatus.SelectedItem.Value);
                        }                    
                        m.Expense = h.AmountInc;                        
                        h.UpdatedOn = DateTime.Now;                       
                        h.PatientName = txtPatientName.Text;                      
                        h.HealthRefdate = m.refdate;                       
                        dg.SaveChanges();
                        Cls_Login login = (Cls_Login)Session["Login"];
                        GetRecord(login.Username, fanos);
                        //ClearFields();
                        ScriptManager.RegisterStartupScript(this, GetType(), "Alert", "alert('Record Saved Successfully ..');", true);
                    }
                    else
                    {
                        var m = getfano.First();
                        Health h = new Health();
                        h.fanoHealth = fanos;
                        h.pagenoHealth = txtPageNo.Text == string.Empty ? 0 : Convert.ToInt32(txtPageNo.Text);

                        if (DDLSubCat.SelectedItem.Value == "Select Disease")
                        {
                            m.subcatid = null;
                        }
                        else
                        {
                            m.subcatid = Convert.ToInt32(DDLSubCat.SelectedItem.Value);
                        }
                        if (ddlRefferedTo.SelectedItem.Value == "Select Hospital")
                        {
                            h.refid = 101;
                        }
                        else
                        {
                            h.refid = Convert.ToInt32(ddlRefferedTo.SelectedItem.Value);
                        }
                        if (DDLTreating.SelectedItem.Value == "Select Medical Board Hospital")
                        {
                            h.TreatId = 101;
                        }
                        else
                        {
                            h.TreatId = Convert.ToInt32(DDLTreating.SelectedItem.Value);
                        }

                        if (ddlStatus.SelectedItem.Value == "Select Status")
                        {
                            h.PtStatusID = 9;
                        }
                        else
                        {
                            h.PtStatusID = Convert.ToInt32(ddlStatus.SelectedItem.Value);
                        }                       
                        h.UpdatedOn = DateTime.Now;                     
                        h.PatientName = txtPatientName.Text;                       
                        dg.Healths.Add(h);
                        dg.SaveChanges();
                        Cls_Login login = (Cls_Login)Session["Login"];
                        GetRecord(login.Username, fanos);
                        //ClearFields();
                        ScriptManager.RegisterStartupScript(this, GetType(), "Alert", "alert('Record Saved Successfully ..');", true);
                    }
                }
                else
                {
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Alert", "alert('" + ex.Message.ToString() + "');", true);
            }

        }

       
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HospitalsMaster.Master" AutoEventWireup="true" CodeBehind="dcofeedbkform.aspx.cs" Inherits="DG.WebUI.dcofeedbkform" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
     <style type="text/css">
        .form-control
        {}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    
    <table align="center" width="70%">
    <tr>
         
        <td align="right">
        <h3>Deputy Commissioner - Grant in Aid Feedback Form </h3>
        </td>
       
        <td align="center">
        <img src="Images/punjab.jpg" alt="" />
        </td>
    </tr>
        <tr>
            <td><a href="DCOWebForm.aspx"><i class="fa fa-arrow-circle-left"></i> Go Back</a></td>
        </tr>

    </table>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
            <asp:UpdateProgress runat="server" id="PageUpdateProgress">
            <ProgressTemplate>
            <div id="modal" runat="server" style =" position: fixed;
            z-index: 1000;
            height: 100%;
            width: 100%;
            top: 0;
            background-color: Black;
            filter: alpha(opacity=60);
            opacity: 0.6;
            -moz-opacity: 0.8; ">
                <div id="dvProgress" runat="server" style="position:fixed;  text-align:center; margin: 300px auto; padding: 10px; left:50%; z-index: 1001;" >
            <asp:Image ID="Image2" runat="server"  ImageUrl="Images/imgLoading.gif"  />
        </div> 
        </div>
            </ProgressTemplate>
        </asp:UpdateProgress>

    <table width = "75%" align = "center" cellpadding = "0" cellspacing = "0" style = "border:1px dashed white">
    <tr>
        <td colspan="4" align="left">
            <asp:Label ID="lblheading1" runat="server" Text="Applicant Information" 
                BackColor="#000066" ForeColor="#FFFF66" Font-Size="X-Large"></asp:Label>
        </td>
    </tr>
    <tr>
        
        <td align="left">
            <asp:Label ID="lblFANo" runat="server" Text="FANo" Font-Size="Medium" 
                ForeColor="#000066"></asp:Label></td>
        <td align="left">
            <asp:TextBox ID="txtFano" runat="server" Width = "250px"  CssClass="form-control" 
                BackColor="White" ForeColor="Black"></asp:TextBox> </td>
        <td align="left">
            <asp:Label ID="lblPageNo" runat="server" Text="Computer No" Font-Size="Medium" 
                ForeColor="#000066"></asp:Label></td>
        <td align="left" colspan="3">
            <asp:TextBox ID="txtPageNo" runat="server" Width = "250px" CssClass="form-control" 
                BackColor="White" ForeColor="Black"></asp:TextBox>
        </td>

    </tr>
    
    <tr>
        <td align="left">
            <asp:Label ID="lblAppname" runat="server" Text="Applicant Name" Font-Size="Medium" 
                ForeColor="#000066"></asp:Label></td>
        <td align="left">
            <asp:TextBox ID="txtAppname" runat="server" Width="250px" CssClass="form-control" 
                BackColor="White"></asp:TextBox>
        </td>
        <td align="left">
            <asp:Label ID="lblAddress" runat="server" Text="Address" Font-Size="Medium" 
                ForeColor="#000066"></asp:Label></td>
        <td align="left">
            <asp:TextBox ID="txtAddress" runat="server" Width = "250px" 
                Font-Names="Arial" BackColor="White" CssClass="form-control"></asp:TextBox>
        </td>
    </tr>

    <tr>
        <td align="left">
            <asp:Label ID="lblLetterNo" runat="server" Text="LetterNo" Font-Size="Medium" 
                ForeColor="#000066"></asp:Label></td>
        <td align="left">
            <asp:TextBox ID="txtLetterNo" runat="server" Width="250px" CssClass="form-control" 
                BackColor="White"></asp:TextBox>
        </td>
         <td align="left">
            <asp:Label ID="lblLetDate" runat="server" Text="LetterDate" Font-Size="Medium" 
                ForeColor="#000066"></asp:Label></td>
        
        <td align="left">
            <telerik:RadDateTimePicker ID="dtLetterDate" runat="server" Width="250px"  CssClass="form-control">
            </telerik:RadDateTimePicker>
        </td>
    </tr>
   
    <%-- <%
         if (Session["Login"] != null)
         {
             DGModel.Cls_Login login = (DGModel.Cls_Login)Session["Login"];
             
             DGModel.DGEntities ent = new DGModel.DGEntities();
             var chkaccess = (from a in ent.User_Groups where a.ID == login.Groupid select a).First();

             if (chkaccess.HealthSection1 == true)
             {
                
           %>--%>
       
    
    <tr>
        <td colspan="4" align="left">
            <asp:Label ID="lblAccounts" runat="server" Text="Accounts" 
                BackColor="#000066" ForeColor="#FFFF66" Font-Size="X-Large"></asp:Label>
        </td>
    </tr>
          
    <tr>
        <td align="left">
            <asp:Label ID="lblRecvdchqno" runat="server" Text = "Received ChequeNo" Font-Size="Medium" 
                ForeColor="#000066"></asp:Label></td>
        <td align="left">
            <asp:TextBox ID="txtRecvdChqno" runat="server" Width = "250px" CssClass="form-control"></asp:TextBox>
        </td>
        <td align="left">
            <asp:Label ID="RecvdChqDate" runat="server" Text="Received ChqDate" Font-Size="Medium" 
                ForeColor="#000066"></asp:Label></td>
        
        <td align="left">
            <telerik:RadDateTimePicker ID="rdRecvdChqDate" runat="server" Width="250px" 
                CssClass="form-control">
            </telerik:RadDateTimePicker>
        </td>
                 
    </tr>
      
    <tr>
        <td align="left">
            <asp:Label ID="lblAmount" runat="server"  Font-Size="Medium" 
                ForeColor="#000066" Text="Amount"></asp:Label>
            &nbsp;</td>
        <td align="left">
            <asp:TextBox ID="txtAmount" runat="server" Width = "250px" CssClass="form-control" Enabled="False"></asp:TextBox>
            &nbsp;</td>
        <td align="left">
            <asp:Label ID="lblUpdatedOn" runat="server" Text="Updated On" Font-Size="Medium" 
                ForeColor="#000066"></asp:Label></td>
        
        <td align="left">
            <telerik:RadDateTimePicker ID="rdtUpdatedOn" runat="server" Width="250px" 
                CssClass="form-control">
            </telerik:RadDateTimePicker>
        </td>
        
                 
    </tr>
   <tr>
         <td>
        <asp:Label ID="lblchqstatus" runat="server" Text="ChequeStatus" Font-Size="Medium" 
                ForeColor="#000066"></asp:Label></td>
        
        <td align="left">
            <asp:DropDownList ID="DDLChqStatus" runat="server" Width = "250px" CssClass="form-control">
                <asp:ListItem Value="0">&lt;SELECT&gt;</asp:ListItem>
                <asp:ListItem Value="1">By MPA</asp:ListItem>
                <asp:ListItem Value="2">Applicant - self</asp:ListItem>
                <asp:ListItem Value="3">By Courier</asp:ListItem>
                <asp:ListItem Value="3">Any Other</asp:ListItem>
            </asp:DropDownList>
        </td>
       <td></td>
       <td>
           <asp:TextBox ID="txtfilename" runat="server" Height="30px" Enabled="false"  Width = "250px" BackColor="#FF99FF" ForeColor="#003366"></asp:TextBox>
       </td>

   </tr>             
   <tr>
        <td>
        <asp:Label ID="lblStatus" runat="server" Text="Status" Font-Size="Medium" 
                ForeColor="#000066"></asp:Label></td>
        
        <td align="left">
            <asp:DropDownList ID="ddlStatus" runat="server" Width = "250px" CssClass="form-control">
            </asp:DropDownList>
        </td>
       <td align="left">
            <asp:Label ID="lblUpload" runat="server" Text="Upload Receipt" AllowMultiple="true" Font-Size="Medium" 
                ForeColor="#000066"></asp:Label></td>
       <td>

           <asp:FileUpload ID="FileReceiptUpload" AllowMultiple="true" CssClass="form-control" runat="server" />
       </td>
    </tr>

     <tr>
        <td align="left">
            <asp:Label ID="lblRemarks" runat="server" Text="Remarks" Font-Size="Medium" 
                ForeColor="#000066"></asp:Label></td>
         <td colspan="3" align="left">   
             <asp:TextBox ID="txtRemarks" runat="server" Width = "710px" 
                TextMode="MultiLine" Font-Names="Arial" CssClass="form-control"></asp:TextBox></td>
     </tr>
    <%--<%}
         } %>--%>
           <tr>
                    <td align = "center" colspan = "4">
                        <br />
                        
                  <asp:LinkButton ID="lnkbtnSave" Font-Size="Medium" runat="server" ToolTip = "Save" onclick="lnkbtnSave_Click" Height="40px" Width="100px" CssClass="btn btn-primary btn-lg" >Save</asp:LinkButton>
                        <asp:LinkButton ID="lnkbtnCancel" Font-Size="Medium" runat="server" ToolTip = "Cancel" Height="40px" Width="100px" onclick="lnkbtnCancel_Click" CssClass="btn btn-primary btn-lg">Cancel</asp:LinkButton>
                    </td>
                    </tr>
                    <caption>
                        <br />
        </caption>

    </table>
</div>
<br />
<br />
<br />

    </td>


    </td>
      </ContentTemplate>
     <Triggers>
        <asp:PostBackTrigger ControlID = "lnkbtnSave" />
    </Triggers>
      </asp:UpdatePanel>
     
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DGMasterPage.Master" AutoEventWireup="true" CodeBehind="UpdateWebForm.aspx.cs" Inherits="DG.WebUI.UpdateWebForm" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   <!-- <link href="Styles/bootstrap.css" rel="stylesheet" type="text/css" />-->
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server">
</asp:ScriptManager>
<h1 align = "center"> Update Form </h1>

<div>
    <table width = "60%" align = "center" cellpadding = "0" cellspacing = "0" style = "border:1px dashed white">
           <tr>
                <td><asp:Label ID="lblFileNo" runat="server" Text="File No"></asp:Label></td>
                 <td><asp:TextBox ID="txtFileNo" Width="200px" runat="server" CssClass="form-control"></asp:TextBox> </td>
            
                <td><asp:Label ID="lblLetCode" runat="server" Text="Letter Code"></asp:Label></td>
                <td><asp:TextBox ID="txtLetCode" Width="200px" runat="server" CssClass="form-control"></asp:TextBox></td>
           </tr>

           <tr>
                <td>
                    <asp:Label ID="lblFromChqDate" runat="server" Text="From Date"></asp:Label>
                </td>
                <td>
                    <telerik:RadDateTimePicker ID="FromChqDate" runat="server" CssClass="form-control" Width="200px"
                        Skin="Office2007">
                    <TimeView CellSpacing="-1" runat="server"></TimeView>

                    <TimePopupButton ImageUrl="" HoverImageUrl=""></TimePopupButton>

                    <Calendar UseRowHeadersAsSelectors="False" runat="server" UseColumnHeadersAsSelectors="False" ViewSelectorText="x" 
                            Skin="Office2007"></Calendar>

                    <DateInput DisplayDateFormat="dd-MMM-yyyy" runat="server" DateFormat="dd-MMM-yyyy" LabelWidth="40%"></DateInput>

                    <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                    </telerik:RadDateTimePicker>
                </td>
                      
                <td>
                    <asp:Label ID="lblTochqDate" runat="server" Text="To Date"></asp:Label>
                </td>
                <td>
                    <telerik:RadDateTimePicker ID="ToChqDate" runat="server" Width="200px" CssClass="form-control" 
                        Skin="Office2007">
                    <TimeView ID="TimeView1" CellSpacing="-1" runat="server"></TimeView>

                    <TimePopupButton ImageUrl="" HoverImageUrl=""></TimePopupButton>

                    <Calendar ID="Calendar1" UseRowHeadersAsSelectors="False" runat="server" UseColumnHeadersAsSelectors="False" ViewSelectorText="x" 
                            Skin="Office2007"></Calendar>

                    <DateInput ID="DateInput1" DisplayDateFormat="dd-MMM-yyyy" runat="server" DateFormat="dd-MMM-yyyy" LabelWidth="40%"></DateInput>

                    <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                    </telerik:RadDateTimePicker>
                </td>
           </tr>

           <tr>
            <td colspan="4" align="center">
                <asp:DataPager ID="DataPager1" runat="server"  PagedControlID="DGListView">
                    <Fields>
                        <asp:NextPreviousPagerField ButtonType="Button" ShowFirstPageButton="True" 
                            ShowNextPageButton="False" ShowPreviousPageButton="False" />
                        <asp:NumericPagerField />
                        <asp:NextPreviousPagerField ButtonType="Button" ShowLastPageButton="True" 
                            ShowNextPageButton="False" ShowPreviousPageButton="False" />
                    </Fields>
                </asp:DataPager>
                
    <asp:ListView ID="DGListView" runat="server" 
                    onpagepropertieschanging="DGListView_PagePropertiesChanging" 
                    onselectedindexchanged="DGListView_SelectedIndexChanged">
        <LayoutTemplate>
             <table border="0" cellpadding="1" class="table table-striped table-bordered table-hover">
                
                <th align="left"><asp:LinkButton ID="lnkFaNo" runat="server">FANo</asp:LinkButton></th>
                <th align="left"><asp:LinkButton ID="lnkAppName" runat="server">Applicant Name</asp:LinkButton></th>
                <th align="left"><asp:LinkButton ID="lnkChequeNo" runat="server">Cheque No</asp:LinkButton></th>
                <th align="left"><asp:LinkButton ID="LinkChequeDate" runat="server">Cheque Date</asp:LinkButton></th>
                <th align="left"><asp:LinkButton ID="LinkLetterCode" runat="server">Letter Code</asp:LinkButton></th>
                <th align="left"><asp:LinkButton ID="LinkFileNo" runat="server">File No</asp:LinkButton></th>
                </tr>
                <tr id="itemPlaceholder" runat="server"></tr>
            </table>
    </LayoutTemplate>
    <ItemTemplate>
          <tr>
           <td><asp:Label runat="server" ID="lblFaNo"><%#Eval("FANo") %></asp:Label></td>
           <td><asp:Label runat="server" ID="lblAppName"><%#Eval("AppName")%> </asp:Label></td>
           <td><asp:Label runat="server" ID="lblChqNo"><%#Eval("chqno")%></asp:Label></td>
           <td><asp:Label runat="server" ID="lblChqDate"><%#Eval("chqdate","{0:dd/MM/yyyy}")%></asp:Label></td>
           <td><asp:Label runat="server" ID="lblLetCode"><%#Eval("letcode")%></asp:Label></td>
           <td><asp:Label runat="server" ID="lblFileNo"><%#Eval("FileNo") %></asp:Label></td>
           </tr>
          
    </ItemTemplate>
    </asp:ListView>
            </td>
        </tr>
        <tr>
            <td colspan="4" align="center">
                    <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btn btn-primary btn-lg" Width= "120px"
                    onclick="btnSubmit_Click" />
                    <asp:Button ID="btnCode8" runat="server" Text="Code 8" CssClass="btn btn-primary btn-lg" Width= "120px" onclick="btnCode8_Click"/>
                    <asp:Button ID="btnCode12" runat="server" Text="Code 12" CssClass="btn btn-primary btn-lg" Width= "120px" onclick="btnCode12_Click"/><br /> <br /><br /><br />
                
            </td> 
          </tr>
        
        <tr>
            <td colspan="4" align="center">
                    <asp:Label ID="lblMessage" runat="server" BorderStyle="Solid" Font-Bold="True" 
                        Font-Size="Medium" ForeColor="#960832" BorderColor="#003366" 
                        BorderWidth="4px"></asp:Label>
                

                </td>
          </tr>
        
    </table>
</div>
</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.Entity;
using DGModel;

namespace DG.WebUI
{
    public partial class HealthForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Login"] == null)
                {
                    Response.Redirect("~/LoginPage.aspx");
                }

                txtfind.Focus();
                UploadCat();
                UploadSentto();
                UploadStatus();
                UploadTreating();
                NoEdit();
                this.ddlRefferedTo.Items.Insert(0, new ListItem("Select Hospital"));
                this.ddlStatus.Items.Insert(0, new ListItem("Select Status"));
                this.DDLSubCat.Items.Insert(0, new ListItem("Select Disease"));
                this.DDLTreating.Items.Insert(0, new ListItem("Select Medical Board Hospital"));
                if (Request["fano"] != null)
                {
                    Cls_Login login = (Cls_Login)Session["Login"];
                    txtfind.Text = Request["fano"];
                    GetRecord(login.Username, login.Healthaccessid, Convert.ToInt32(Request["fano"]));
                }
            }
        }
        private void UploadCat()
        {
            DGEntities dg = new DGEntities();
            var cat = (from b in dg.Subcats select b).ToList();
            DDLSubCat.DataSource = cat;
            DDLSubCat.DataValueField = "subcatid";
            DDLSubCat.DataTextField = "SubCategory";
            DDLSubCat.DataBind();
        }

        private void NoEdit()
        {
            txtFano.Enabled = false;
            txtAppname.Enabled = false;
            txtAddress.Enabled = false;
            txtPageNo.Enabled = false;
            txtLetterNo.Enabled = false;
            dtRefferedDate.Enabled = false;
            dtLetterDate.Enabled = false;
            txtBalReturned.Enabled = false;
            rdtUpdatedOn.Enabled = false;
        }

        private void Editable()
        {
            txtPageNo.Enabled = true;
            txtLetterNo.Enabled = true;
            dtRefferedDate.Enabled = true;
            dtLetterDate.Enabled = true;          

        }
        private void UploadStatus()
        {
            DGEntities dg = new DGEntities();
            var stat = (from b in dg.PatientStatus select b).ToList();
            ddlStatus.DataSource = stat;
            ddlStatus.DataValueField = "PtID";
            ddlStatus.DataTextField = "Description";
            //ddlStatus.SelectedIndex = 0;
            ddlStatus.DataBind();
        }

        private void UploadSentto()
        {
            DGEntities dg = new DGEntities();
            var sentto = (from a in dg.Reffereds select a).ToList();
            ddlRefferedTo.DataSource = sentto;
            ddlRefferedTo.DataValueField = "RefId";
            ddlRefferedTo.DataTextField = "Refto";
            ddlRefferedTo.DataBind();                         
        }
        private void UploadTreating()
        {
            DGEntities dg = new DGEntities();
            var sentto = (from a in dg.Reffereds select a).ToList();
            DDLTreating.DataSource = sentto;
            DDLTreating.DataValueField = "RefId";
            DDLTreating.DataTextField = "Refto";
            DDLTreating.DataBind();
        }
        protected void btnFind_Click(object sender, EventArgs e)
        {            
        }

        private void ClearFields()
        {
            txtPageNo.Text = "";
            txtLetterNo.Text = "";
            dtRefferedDate.SelectedDate = null;
            dtLetterDate.SelectedDate = null;
            rdtUpdatedOn.SelectedDate = null;                     
        }
        private void GetRecord(string username,int groupid,int fano)
        {
            
            try
            {
                DGEntities dg = new DGEntities();
                int fanos = fano;

                if (groupid == 3)
                {
                    var gethospitalaccess = from a in dg.Reffereds where a.UserName == username select a;
                    if (gethospitalaccess.Count() > 0)
                    {
                        var hospitalaccess = gethospitalaccess.First();
                        var getfno = from h in dg.Healths where h.fanoHealth == fanos && h.refid == hospitalaccess.RefId select h;
                        if (getfno.Count() > 0)
                        {
                            var healinfo = getfno.First();
                            var maininfo = (from m in dg.MAINCORRs where m.fano == healinfo.fanoHealth select m).First();
                            txtAppname.Text = maininfo.appname;
                            txtAddress.Text = maininfo.address;
                            txtFano.Text = Convert.ToInt32(healinfo.fanoHealth).ToString();
                            txtPageNo.Text = Convert.ToInt32(healinfo.pagenoHealth).ToString();
                            txtLetterNo.Text = healinfo.letterno;
                            txtAmount.Text = Convert.ToInt32(healinfo.AmountInc).ToString();
                            //txtAmount.Text = Convert.ToInt32(maininfo.Expense).ToString();
                            txtSancAmount.Text = Convert.ToInt32(healinfo.HealthAmount).ToString();
                            txtPatientName.Text = maininfo.PatientName;
                            txtMedNumber.Text = healinfo.MedBoardNo;
                            dtLetterDate.SelectedDate = healinfo.LetDate;
                            dtRefferedDate.SelectedDate = healinfo.RefOn;
                            txtOpinion.Text = healinfo.Opinion;
                            txtFDSancNo.Text = healinfo.fdSactionNo;
                            rdtFDSancDate.SelectedDate = healinfo.fdSanctionDate;
                            rdtVouchedDate.SelectedDate = healinfo.VouchedDate;
                            rdtLiquidDate.SelectedDate = healinfo.LiquidationDate;
                            txtAuthorityNo.Text = healinfo.AuthorityNo;
                            rdtAuthorityDate.SelectedDate = healinfo.AuthorityDate;
                            txtFileNumber.Text = healinfo.FileNumber;
                            txtRemarks.Text = healinfo.HealthRemarks;
                            ddlRefferedTo.SelectedIndex = ddlRefferedTo.Items.IndexOf(ddlRefferedTo.Items.FindByValue(healinfo.refid.ToString()));
                            DDLTreating.SelectedIndex = DDLTreating.Items.IndexOf(DDLTreating.Items.FindByValue(healinfo.TreatId.ToString()));
                            ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByValue(healinfo.PtStatusID.ToString()));
                            DDLSubCat.SelectedIndex = DDLSubCat.Items.IndexOf(DDLSubCat.Items.FindByValue(maininfo.subcatid.ToString()));
                            txtLetterNo.Text = healinfo.letterno;
                            rdtUpdatedOn.SelectedDate = healinfo.UpdatedOn;
                            rdtTentativeDate.SelectedDate = healinfo.TentativeDate;
                            rdtActualDate.SelectedDate = healinfo.ActualDate;
                            txtOpinion.Text = healinfo.Opinion;
                            txtRemarks.Text = healinfo.HealthRemarks;
                            txtRegNo.Text = healinfo.HospitalID;
                            txtRecvdChqno.Text = healinfo.RecvdChqno;
                            rdRecvdChqDate.SelectedDate = healinfo.RecvdChqDate;
                            txtBalReturned.Text = Convert.ToInt32(healinfo.BalReturn).ToString();
                            txtBalChqNo.Text = healinfo.BalChqno;
                            rdBalChqDate.SelectedDate = healinfo.BalChqDate;
                            rdtMedicalBoardDate.SelectedDate = healinfo.MedBoardDate;
                            txtRecommendations.Text = healinfo.Recommendations;
                            rdtReportSentOn.SelectedDate = healinfo.ReportSentOn;
                            rdtDateHeldOn.SelectedDate = healinfo.Dateheldon;
                        }
                        else
                        {
                            //string message = "Record Not Found....";
                            ScriptManager.RegisterStartupScript(this, GetType(), "Alert", "alert('Record Not found..');", true);
                            ClearFields();
                        }
                    }
                }
                else
                {
                    if (groupid != 3)
                    {
                       // int fano = Convert.ToInt32(Request["DGId"]);
                        var getfno = from h in dg.Healths where h.fanoHealth == fanos select h;
                        var maininfo1 = (from m in dg.MAINCORRs where m.fano == fanos select m);
                        var filename = from f in dg.tblUploadFiles where f.FaNo == fano select f;
                        var filemedical = from md in dg.tbl_UploadMedboardFile where md.FaNo == fano select md;
                        if (maininfo1.Count() > 0)
                        {
                            var maininfo = maininfo1.First();
                            txtFano.Text = Convert.ToInt32(maininfo.fano).ToString();
                            txtAppname.Text = maininfo.appname;
                            txtAddress.Text = maininfo.address;
                            //txtSancAmount.Text = Convert.ToInt32(maininfo.amount).ToString();
                            Editable();
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "Alert", "alert('Record Not found..');", true);
                            ClearFields();
                        }
                        if (getfno.Count() > 0)
                        {
                            var healinfo = getfno.First();
                            var maininfo = (from m in dg.MAINCORRs where m.fano == healinfo.fanoHealth select m).First();
                            //txtFano.Text = Convert.ToInt32(healinfo.fanoHealth).ToString();
                            txtPageNo.Text = Convert.ToInt32(healinfo.pagenoHealth).ToString();
                            txtLetterNo.Text = healinfo.letterno;
                            txtAmount.Text = Convert.ToInt32(healinfo.AmountInc).ToString();
                           // txtAmount.Text = Convert.ToInt32(maininfo.Expense).ToString();
                            dtLetterDate.SelectedDate = healinfo.LetDate;
                            dtRefferedDate.SelectedDate = healinfo.RefOn;
                            txtOpinion.Text = healinfo.Opinion;
                            txtPatientName.Text = maininfo.PatientName;
                            txtMedNumber.Text = healinfo.MedBoardNo;
                            txtFDSancNo.Text = healinfo.fdSactionNo;
                            txtSancAmount.Text = Convert.ToInt32(healinfo.HealthAmount).ToString();
                            rdtFDSancDate.SelectedDate = healinfo.fdSanctionDate;
                            rdtVouchedDate.SelectedDate = healinfo.VouchedDate;
                            rdtLiquidDate.SelectedDate = healinfo.LiquidationDate;
                            txtAuthorityNo.Text = healinfo.AuthorityNo;
                            rdtAuthorityDate.SelectedDate = healinfo.AuthorityDate;
                            txtFileNumber.Text = healinfo.FileNumber;
                            txtRemarks.Text = healinfo.HealthRemarks;
                            ddlRefferedTo.SelectedIndex = ddlRefferedTo.Items.IndexOf(ddlRefferedTo.Items.FindByValue(healinfo.refid.ToString()));
                            DDLTreating.SelectedIndex = DDLTreating.Items.IndexOf(DDLTreating.Items.FindByValue(healinfo.TreatId.ToString()));
                            ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByValue(healinfo.PtStatusID.ToString()));
                            DDLSubCat.SelectedIndex = DDLSubCat.Items.IndexOf(DDLSubCat.Items.FindByValue(maininfo.subcatid.ToString()));
                            txtLetterNo.Text = healinfo.letterno;
                            rdtUpdatedOn.SelectedDate = healinfo.UpdatedOn;
                            rdtTentativeDate.SelectedDate = healinfo.TentativeDate;
                            rdtActualDate.SelectedDate = healinfo.ActualDate;
                            txtOpinion.Text = healinfo.Opinion;
                            txtRemarks.Text = healinfo.HealthRemarks;
                            txtRegNo.Text = healinfo.HospitalID;
                            txtRecvdChqno.Text = healinfo.RecvdChqno;
                            rdRecvdChqDate.SelectedDate = healinfo.RecvdChqDate;
                            txtBalReturned.Text = Convert.ToInt32(healinfo.BalReturn).ToString();
                            txtBalChqNo.Text = healinfo.BalChqno;
                            rdBalChqDate.SelectedDate = healinfo.BalChqDate;
                            rdtMedicalBoardDate.SelectedDate = healinfo.MedBoardDate;
                            txtRecommendations.Text = healinfo.Recommendations;
                            rdtReportSentOn.SelectedDate = healinfo.ReportSentOn;
                            rdtDateHeldOn.SelectedDate = healinfo.Dateheldon;
                            if (filename.Count() > 0)
                            {
                                var fname = filename.First();
                                txtfilename.Text = fname.FileName;
                               
                            }
                            if (filemedical.Count() >0)
                            {
                                var fnamemed = filemedical.First();
                                lblfileMedical.Text = fnamemed.FileName;

                            }                           

                        }
                        else
                        {

                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
        }
        protected void lnkBtnSearch_Click(object sender, EventArgs e)
        {
            System.Threading.Thread.Sleep(1000);
            Cls_Login login = (Cls_Login)Session["Login"];
            int fanos = Convert.ToInt32(txtfind.Text);
            GetRecord(login.Username, login.Healthaccessid, fanos);
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
        }
        protected void lnkbtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("LoginPage.aspx");
        }
        protected void lnkbtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                DGEntities dg = new DGEntities();
                if (FileReceiptUpload.HasFile)
                {
                    foreach (HttpPostedFile file in FileReceiptUpload.PostedFiles)
                    {

                        file.SaveAs(Server.MapPath("~/UploadFiles/" + txtFano.Text + file.FileName));
                        tblUploadFile file1 = new tblUploadFile();
                        file1.FaNo = Convert.ToInt32(txtFano.Text);
                        file1.FileName = txtFano.Text + file.FileName;
                        file1.FilePath = "UploadFiles/" + txtFano.Text + file.FileName;
                        dg.tblUploadFiles.Add(file1);
                        dg.SaveChanges();
                        txtfilename.Text = file1.FileName;
                    }
                }
                if (FileUploadMedical.HasFile)
                {
                    foreach (HttpPostedFile file in FileUploadMedical.PostedFiles)
                    {

                        file.SaveAs(Server.MapPath("~/MedicalBrdFiles/" + txtFano.Text + file.FileName));
                        tbl_UploadMedboardFile file1 = new tbl_UploadMedboardFile();
                        file1.FaNo = Convert.ToInt32(txtFano.Text);
                        file1.FileName = txtFano.Text + file.FileName;
                        file1.FilePath = "MedicalBrdFiles/" + txtFano.Text + file.FileName;
                        dg.tbl_UploadMedboardFile.Add(file1);
                        dg.SaveChanges();
                        lblfileMedical.Text = file1.FileName;
                    }
                }
                System.Threading.Thread.Sleep(500);
                int fanos = Convert.ToInt32(txtfind.Text);
                var getfano = from a in dg.MAINCORRs where a.fano == fanos select a;
                int hfano = Convert.ToInt32(txtFano.Text);
                var getfno = from h in dg.Healths where h.fanoHealth == hfano select h;
                if (getfano.Count() > 0)
                {
                    if (getfno.Count() > 0)
                    {
                        var h = getfno.First();
                        var m = getfano.First();
                        h.fanoHealth = fanos;
                        m.pageno = Convert.ToInt32(txtPageNo.Text);
                        h.DGIdHealth = m.DGId;
                        //m.refdate = dtLetterDate.DateInput.SelectedDate;
                        h.pagenoHealth = Convert.ToInt32(txtPageNo.Text);
                        if (DDLSubCat.SelectedItem.Value == "Select Disease")
                        {
                            m.subcatid = null;
                        }
                        else
                        {
                            m.subcatid = Convert.ToInt32(DDLSubCat.SelectedItem.Value);
                        }

                        if (ddlRefferedTo.SelectedItem.Value == "Select Hospital")
                        {
                            h.refid = 101;
                        }
                        else
                        {
                            h.refid = Convert.ToInt32(ddlRefferedTo.SelectedItem.Value);
                        }
                        if (DDLTreating.SelectedItem.Value == "Select Medical Board Hospital")
                        {
                            h.TreatId = 101;
                        }
                        else
                        {
                            h.TreatId = Convert.ToInt32(DDLTreating.SelectedItem.Value);
                        }

                        if (ddlStatus.SelectedItem.Value == "Select Status")
                        {
                            h.PtStatusID = 9;
                        }
                        else
                        {
                            h.PtStatusID = Convert.ToInt32(ddlStatus.SelectedItem.Value);
                        }
                        h.AmountInc = txtAmount.Text == string.Empty ? 0 : Convert.ToInt32(txtAmount.Text);
                        m.Expense = h.AmountInc;
                        //h.HealthAmount = txtSancAmount.Text == string.Empty ? 0 : Convert.ToInt32(txtSancAmount.Text);
                        h.HealthAmount = m.amount;
                        h.letterno = txtLetterNo.Text;
                        h.LetDate = dtLetterDate.DateInput.SelectedDate;
                        h.UpdatedOn = DateTime.Now;
                        h.fdSactionNo = txtFDSancNo.Text;
                        h.PatientName = txtPatientName.Text;
                        h.MedBoardNo = txtMedNumber.Text;
                        h.fdSanctionDate = rdtFDSancDate.DateInput.SelectedDate;
                        h.VouchedDate = rdtVouchedDate.DateInput.SelectedDate;
                        h.LiquidationDate = rdtLiquidDate.DateInput.SelectedDate;
                        h.AuthorityNo = txtAuthorityNo.Text;
                        h.AuthorityDate = rdtAuthorityDate.DateInput.SelectedDate;
                        h.FileNumber = txtFileNumber.Text;
                        h.RefOn = dtRefferedDate.DateInput.SelectedDate;
                        h.TentativeDate = rdtTentativeDate.DateInput.SelectedDate;
                        h.ActualDate = rdtActualDate.DateInput.SelectedDate;
                        h.Opinion = txtOpinion.Text;
                        h.HealthRemarks = txtRemarks.Text;
                        h.HealthRefdate = m.refdate;
                        h.HospitalID = txtRegNo.Text;
                        h.RecvdChqno = txtRecvdChqno.Text;
                        h.RecvdChqDate = rdRecvdChqDate.DateInput.SelectedDate;
                        if (string.IsNullOrEmpty(txtSancAmount.Text))
                        {
                            txtSancAmount.Text = "0";
                        }
                        if (string.IsNullOrEmpty(txtAmount.Text))
                        {
                            txtAmount.Text = "0";
                        }

                        {
                            h.BalReturn = Convert.ToInt32(txtSancAmount.Text) - Convert.ToInt32(txtAmount.Text);
                        }
                        //h.BalReturn = txtBalReturned.Text == string.Empty ? 0 : Convert.ToInt32(txtBalReturned.Text);
                        h.BalChqno = txtBalChqNo.Text;
                        h.BalChqDate = rdBalChqDate.DateInput.SelectedDate;
                        h.MedBoardDate = rdtMedicalBoardDate.DateInput.SelectedDate;
                        h.Recommendations = txtRecommendations.Text;
                        h.ReportSentOn = rdtReportSentOn.DateInput.SelectedDate;
                        h.Dateheldon = rdtDateHeldOn.DateInput.SelectedDate;
                        dg.SaveChanges();
                        Cls_Login login = (Cls_Login)Session["Login"];
                        GetRecord(login.Username, login.Groupid, fanos);
                        //ClearFields();
                        ScriptManager.RegisterStartupScript(this, GetType(), "Alert", "alert('Record Saved Successfully ..');", true);
                    }
                    else
                    {
                        var m = getfano.First();
                        Health h = new Health();
                        h.fanoHealth = fanos;
                        h.pagenoHealth = txtPageNo.Text == string.Empty ? 0 : Convert.ToInt32(txtPageNo.Text);

                        if (DDLSubCat.SelectedItem.Value == "Select Disease")
                        {
                            m.subcatid = null;
                        }
                        else
                        {
                            m.subcatid = Convert.ToInt32(DDLSubCat.SelectedItem.Value);
                        }
                        if (ddlRefferedTo.SelectedItem.Value == "Select Hospital")
                        {
                            h.refid = 101;
                        }
                        else
                        {
                            h.refid = Convert.ToInt32(ddlRefferedTo.SelectedItem.Value);
                        }
                        if (DDLTreating.SelectedItem.Value == "Select Medical Board Hospital")
                        {
                            h.TreatId = 101;
                        }
                        else
                        {
                            h.TreatId = Convert.ToInt32(DDLTreating.SelectedItem.Value);
                        }

                        if (ddlStatus.SelectedItem.Value == "Select Status")
                        {
                            h.PtStatusID = 9;
                        }
                        else
                        {
                            h.PtStatusID = Convert.ToInt32(ddlStatus.SelectedItem.Value);
                        }
                        h.letterno = txtLetterNo.Text;
                        h.AmountInc = txtAmount.Text == string.Empty ? 0 : Convert.ToInt32(txtAmount.Text);
                        //m.Expense = h.AmountInc;
                        //h.HealthAmount = txtSancAmount.Text == string.Empty ? 0 : Convert.ToInt32(txtSancAmount.Text);
                        h.HealthAmount = m.amount;
                        h.LetDate = dtLetterDate.DateInput.SelectedDate;
                        h.UpdatedOn = DateTime.Now;
                        //h.UpdatedOn = rdtUpdatedOn.DateInput.SelectedDate;
                        h.fdSactionNo = txtFDSancNo.Text;
                        h.PatientName = txtPatientName.Text;
                        h.MedBoardNo = txtMedNumber.Text;
                        h.fdSanctionDate = rdtFDSancDate.DateInput.SelectedDate;
                        h.VouchedDate = rdtVouchedDate.DateInput.SelectedDate;
                        h.LiquidationDate = rdtLiquidDate.DateInput.SelectedDate;
                        h.AuthorityNo = txtAuthorityNo.Text;
                        h.HealthRefdate = m.refdate;
                        h.AuthorityDate = rdtAuthorityDate.DateInput.SelectedDate;
                        h.FileNumber = txtFileNumber.Text;
                        h.RefOn = dtRefferedDate.DateInput.SelectedDate;
                        h.TentativeDate = rdtTentativeDate.DateInput.SelectedDate;
                        h.ActualDate = rdtActualDate.DateInput.SelectedDate;
                        h.Opinion = txtOpinion.Text;
                        h.HealthRemarks = txtRemarks.Text;
                        h.HospitalID = txtRegNo.Text;
                        h.RecvdChqno = txtRecvdChqno.Text;
                        h.RecvdChqDate = rdRecvdChqDate.DateInput.SelectedDate;
                        if (string.IsNullOrEmpty(txtSancAmount.Text))
                        {
                            txtSancAmount.Text = "0";
                        }
                        if (string.IsNullOrEmpty(txtAmount.Text))
                        {
                            txtAmount.Text = "0";
                        }

                        {
                            h.BalReturn = Convert.ToInt32(txtSancAmount.Text) - Convert.ToInt32(txtAmount.Text);
                        }
                        // h.BalReturn = txtBalReturned.Text == string.Empty ? 0 : Convert.ToInt32(txtBalReturned.Text);
                        h.BalChqno = txtBalChqNo.Text;
                        h.BalChqDate = rdBalChqDate.DateInput.SelectedDate;
                        h.MedBoardDate = rdtMedicalBoardDate.DateInput.SelectedDate;
                        h.Recommendations = txtRecommendations.Text;
                        h.ReportSentOn = rdtReportSentOn.DateInput.SelectedDate;
                        h.Dateheldon = rdtDateHeldOn.DateInput.SelectedDate;
                        dg.Healths.Add(h);
                        dg.SaveChanges();
                        Cls_Login login = (Cls_Login)Session["Login"];
                        GetRecord(login.Username, login.Groupid, fanos);
                        //ClearFields();
                        ScriptManager.RegisterStartupScript(this, GetType(), "Alert", "alert('Record Saved Successfully ..');", true);
                    }
                }
                else
                {
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Alert", "alert('"+ex.Message.ToString()+"');", true);
            }

        }

        protected void btnChqUpdate_Click(object sender, EventArgs e)
        {
           
        }

        protected void btnChqUpdate_Click1(object sender, EventArgs e)
        {
            if (txtFano.Text != "")
            {
                int fano = Convert.ToInt32(txtFano.Text);
                Response.Redirect("UpdateDGHealthChequeStatus.aspx?fano=" + fano);
            }

        }
    }
}
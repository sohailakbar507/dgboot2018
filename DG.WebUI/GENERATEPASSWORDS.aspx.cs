﻿using DGModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DG.WebUI
{
    public partial class GENERATEPASSWORDS : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
               
            }

        }

        private void MakeDCs()
        {
            using (DGEntities db = new DGEntities())
            {
                var allDepts = db.DEPTs.ToList();

                Response.Write("<br /><br /><br /><br /><br />");
                Response.Write("<table border='1'>");
                foreach (DEPT d in allDepts)
                {
                    if (d.deptname.StartsWith("D.C.") && d.deptcode >= 51 && d.deptcode <= 85)
                    {
                        string username = d.deptname.Replace(".", "").Replace(" ", "").ToLower() + "" + d.deptid.ToString();
                        string password = ("dc" + "pass" + (d.deptcode * 11).ToString());
                        string enc_password = Cls_Encrypt.Encrypt(password);
                        DGModel.User newusr = new DGModel.User
                        {
                            Name = username,
                            Password = enc_password,
                            AnalYearReport = false,
                            IsAdd_Record = false,
                            IsDelete_Record = false,
                            IsEdit_Record = true,
                            HealthForm = false,
                            UpdateForm = false,
                            Status = true,
                            Group_ID = 8,

                            DCODeptid = d.deptid
                        };

                        db.Users.Add(newusr);
                        Response.Write("<tr><td>" + username + "</td><td>" + password + "</td></tr>");
                    }
                }
                db.SaveChanges();
                Response.Write("</table>");

                Response.Write("<br /><br /><br /><br /><br />");
            }

        }

        protected void lnkbtnPasswords_Click(object sender, EventArgs e)
        {
            this.MakeDCs();

        }
    }
}
﻿using DGModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DG.WebUI
{
    public partial class PrintingForm : System.Web.UI.Page
    {
        public object SCMSearching { get; private set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["dtprint"] != null)
                {
                    // LoadprintingGrid();
                }
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            DGEntities ent = new DGEntities();
            DateTime fromdate = Convert.ToDateTime(datefrom.DateInput.SelectedDate);
            DateTime todate = Convert.ToDateTime(dateto.DateInput.SelectedDate);
            var printdata = (from a in ent.MAINCORRs
                             where (a.asadate >= fromdate && a.asadate <= todate) && a.letcode==33
                             orderby a.catlet
                             select new { a.DGId, a.fano, a.appname, a.asadate }).ToList();
            PrintDataGridView.DataSource = printdata;
            PrintDataGridView.DataBind();
            Session["dtprint"] = printdata;

        }

        protected void btnPrint_Click(object sender, EventArgs e)
        {
            //if (Request["Report"] == "Noting33")
            //{
            //    //int fanofrom = Convert.ToInt32(this.txtFANoFrom.Text);
            //   // int fanoto = Convert.ToInt32(this.txtFaNoTo.Text);
            //   // DateTime fromdate = Convert.ToDateTime(this.RadDateTimePicker1.SelectedDate.Value);
            //  //  DateTime todate = Convert.ToDateTime(this.RadDateTimePicker2.SelectedDate.Value);
            //    DGEntities ent = new DGEntities();
            //    var result = from a in ent.MAINCORRs where a.Printing == false && a.asadate >= fromdate && a.asadate <= todate select a;
            //    if (result.Count() > 0)
            //    {
            //        foreach (var val in result.ToList())
            //        {
            //            val.Printing = true;
            //            ent.SaveChanges();
            //        }
            //    }
            //    string report = Request["Report"];
            //    Response.Redirect("STIReportsView.aspx?reportid=" + report + "&fromfano=" + fanofrom + "&tofano=" + fanoto + "&fromdate=" + fromdate + "&todate=" + todate);
            //    }

        }

        protected void PrintDataGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            PrintDataGridView.PageIndex = e.NewPageIndex;
            PrintDataGridView.DataSource = Session["dtPrint"];
            PrintDataGridView.DataBind();
            
        }

        protected void PrintDataGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {

        }
    }
}
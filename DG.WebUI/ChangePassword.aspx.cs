﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DGModel;

namespace DG.WebUI
{
    public partial class ChangePassword : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Change_Click(object sender, EventArgs e)
        {
            DGEntities dgent = new DGEntities();
            string username = txtUserName.Text;
            string oldpass = Cls_Encrypt.Encrypt(txtOldPassword.Text);
            string newpass = Cls_Encrypt.Encrypt(txtNewPassword.Text);
            var chkUser = from a in dgent.Users where a.Name == username && a.Password == oldpass select a;

            if (chkUser.Count() > 0)
            {
                var users = chkUser.First();
                users.Password = newpass;
                dgent.SaveChanges();
                Response.Redirect("loginPage.aspx");
            }
            else
            {
                //Messagebox.show("Incorrect Information");
            }

        }
    }
}
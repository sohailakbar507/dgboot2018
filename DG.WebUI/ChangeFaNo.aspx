﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DGMasterPage.Master" AutoEventWireup="true" CodeBehind="ChangeFaNo.aspx.cs" Inherits="DG.WebUI.ChangeFaNo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<h1 align = "center"> Change FaNo </h1>
<table style="border:1px dashed white;border-collapse:collapse;" align="center" width="400">
    
    <tr>
        <td align="center">
            <asp:Label ID="lblOldFaNo" runat="server" Text="Old FaNo"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="txtOldFaNo" runat="server" CssClass="form-control" Width="220px"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td align="center">
            <asp:Label ID="lblNewFaNo" runat="server" Text="New FaNo"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="txtNewFaNo" runat="server" CssClass="form-control" Width="220px"></asp:TextBox>
        </td>
    </tr>
    
    <tr>
        
        <td align = "center" colspan = "2" align="center">
            <br /><br />
            <asp:Button ID="Change"  runat="server" Text="Change FaNo" CssClass="btn btn-primary btn-lg" onclick="Change_Click" />
            <br />
            <br />
            <asp:Label ID="lblMessage" runat="server" BorderStyle="Solid" Font-Bold="True" 
                        Font-Size="Medium" ForeColor="#FFCC00" BorderColor="#003366" 
                        BorderWidth="4px"></asp:Label>
        </td>
    </tr>
    
</table>


</asp:Content>

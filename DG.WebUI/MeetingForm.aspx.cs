﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DGModel;

namespace DG.WebUI
{
    public partial class MeetingForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                if (Session["Login"] == null)
                {
                    Response.Redirect("~/LoginPage.aspx");
                }

                txtfind.Focus();
                NoEdit();                
            }
        }

        private void NoEdit()
        {
            
        }

        protected void lnkBtnSearch_Click(object sender, EventArgs e)
        {
            GetRecord();
        }

        private void GetRecord()
        {
            DGEntities ent = new DGEntities();
            int fanos = Convert.ToInt32(txtfind.Text);
            var getfano = from a in ent.MeetingTabs where a.MeetingFaNo == fanos select a;
            var getfanom = from m in ent.MAINCORRs where m.fano == fanos select m;

            if (getfano.Count() > 0)
            {
                var meetinginfo = getfano.First();
                txtFano.Text = meetinginfo.MeetingFaNo.ToString();
                txtAppname.Text = meetinginfo.MeetingAppName;
                txtAddress.Text = meetinginfo.MeetingAddress;
                txtRefBy.Text = meetinginfo.MeetingRefBy;
                MeetingDate.SelectedDate = meetinginfo.MeetingDate;
                txtCategory.Text = meetinginfo.Category;
                txtAmount.Text = Convert.ToInt32(meetinginfo.Amount).ToString();
                txtProposed.Text = Convert.ToInt32(meetinginfo.Proposal).ToString();
                txtRemarks.Text = meetinginfo.Remarks;
            }

            else
            {
                if (getfanom.Count() > 0)
                {
                    var maininfo = getfanom.First();
                    txtFano.Text = Convert.ToInt32(maininfo.fano).ToString();
                    txtAppname.Text = maininfo.appname;
                    txtAddress.Text = maininfo.address;
                    txtRefBy.Text = maininfo.name;
                    txtAmount.Text = "";
                    txtProposed.Text = "";
                    txtRemarks.Text = "";
                    txtCategory.Text = "";
                }
            }
        }   

        protected void lnkbtnSave_Click(object sender, EventArgs e)
        {
            DGEntities dg = new DGEntities();
            int fanos = Convert.ToInt32(txtfind.Text);
            var getfano = from a in dg.MeetingTabs where a.MeetingFaNo == fanos select a;
            if (getfano.Count() > 0)
            {
                var m = getfano.First();
                m.MeetingFaNo = Convert.ToInt32(txtFano.Text);
                m.MeetingAppName = txtAppname.Text;
                m.MeetingAddress = txtAddress.Text;
                m.MeetingRefBy = txtRefBy.Text;
                m.MeetingDate = MeetingDate.DateInput.SelectedDate;
                m.Category = txtCategory.Text;
                m.Amount = txtAmount.Text == string.Empty ? 0 : Convert.ToInt32(txtAmount.Text);
                m.Proposal = txtProposed.Text == string.Empty ? 0 : Convert.ToInt32(txtProposed.Text);
                m.Remarks = txtRemarks.Text;
                dg.SaveChanges();
            }
            else
            {
                MeetingTab m = new MeetingTab();
                m.MeetingFaNo = Convert.ToInt32(txtFano.Text);
                m.MeetingAppName = txtAppname.Text;
                m.MeetingAddress = txtAddress.Text;
                m.MeetingRefBy = txtRefBy.Text;
                m.MeetingDate = MeetingDate.DateInput.SelectedDate;
                m.Category = txtCategory.Text;
                m.Amount = txtAmount.Text == string.Empty ? 0 : Convert.ToInt32(txtAmount.Text);
                m.Proposal = txtProposed.Text == string.Empty ? 0 : Convert.ToInt32(txtProposed.Text);
                m.Remarks = txtRemarks.Text;
                dg.MeetingTabs.Add(m);
                dg.SaveChanges();
            }


        }

        protected void lnkbtnCancel_Click(object sender, EventArgs e)
        {

        }
    }
}
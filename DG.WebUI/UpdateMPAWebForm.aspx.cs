﻿using DGModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DG.WebUI
{
    public partial class UpdateMPAWebForm : System.Web.UI.Page
    {
        public List<mpamna> mpalist;
        protected void Page_Load(object sender, EventArgs e)
        {
            DGEntities ent = new DGEntities();
            mpalist = (from a in ent.mpamnas select a).ToList();
        }
    }
}
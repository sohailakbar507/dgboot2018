﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DG.WebUI
{
    public partial class MpaReport : System.Web.UI.Page
    {
        private const string ExmpaReport = "Ex MPA Report";
        private object rblReportCategories;
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                List<string> menuOptions = new List<string>();
                menuOptions.Add(ExmpaReport);
            }
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
                string mpaname = txtExmpaName.Text;
                Response.Redirect("STIReportsView.aspx?reportid=ExmpaReport"+ "&mpaname=" + mpaname);
        
        }
    }
}
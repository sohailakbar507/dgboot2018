﻿using DGModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DG.WebUI
{
    public partial class HospitalsUpdateForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                Cls_Login val = (Cls_Login)Session["login"];
                LoadGrid(val.hosptid);
                lblFaNo.Visible = true;
                lblHospitalName.Text = val.Hospitalname;

            }

        }
        private void LoadGrid(int hospid)
        {
            DGEntities dgent = new DGEntities();
            Cls_Login val = (Cls_Login)Session["login"];
            int idhosp = Convert.ToInt32(val.hosptid);
            var dglist = (from h in dgent.Healths
                          from m in dgent.MAINCORRs
                          where h.fanoHealth == m.fano && h.refid == idhosp
                          orderby h.fanoHealth descending
                          select new {m.DGId, h.fanoHealth, PATIENTINFO = m.appname + m.address + m.cnic, h.HealthAmount, h.RecvdChqno, h.RecvdChqDate, m.fyear }).ToList();

            dgvDG.DataSource = dglist;
            dgvDG.DataBind();
            dglist.Count();
            lblRecords.Text = "(Total Records :" + dglist.Count.ToString() + ")";
            Session["dtData"] = dglist;

        }

        protected void dgvDG_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            dgvDG.PageIndex = e.NewPageIndex;
            dgvDG.DataSource = Session["dtData"];
            dgvDG.DataBind();

        }

        protected void dgvDG_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        protected void dgvDG_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {

        }
        protected void lnkBtnSearch_Click(object sender, EventArgs e)
        {
            DGEntities dgent = new DGEntities();


            if (txtFaNo.Text != "")
            {
                Cls_Login val = (Cls_Login)Session["login"];
                int fano = Convert.ToInt32(txtFaNo.Text);
                int hospcode = Convert.ToInt32(val.hosptid);
                var fanowise = dgent.SEARCHHOSPITALFANOWISE(fano, hospcode);
                dgvDG.DataSource = fanowise;
                dgvDG.DataBind();
            }
        }
        protected void dgvDG_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {

            Cls_Login login = (Cls_Login)Session["Login"];
            if (login.Isedit != true)
            {
                LinkButton editbutton = (LinkButton)dgvDG.Rows[e.RowIndex].FindControl("LnkBtnEdit");
                editbutton.Enabled = false;
                return;
            }

            else
            {
                Label dgid = (Label)dgvDG.Rows[e.RowIndex].FindControl("lblFaNo");
                int fano = Convert.ToInt32(dgid.Text);
                Response.Redirect("HospitalsForm.aspx?fanoHealth=" + fano);
               
            }
        }




    }
}
﻿using DGModel;
using Stimulsoft.Report;
using Stimulsoft.Report.Dictionary;
using Stimulsoft.Base;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DG.WebUI
{
    public partial class STIReportsView : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                
                this.LoadReport();
            }

        }
        private void LoadReport()
        {
            BudgetCMOEntities dg = new BudgetCMOEntities();
            string rptid = Request.QueryString["reportid"];
            object repo = null;
            bool access = true;
            string report_path = "";
            Cls_Login loggedInUser = (Cls_Login)Session["Login"];
            string ConnectionString = ConfigurationManager.ConnectionStrings["Reports"].ConnectionString;
            StiSqlDatabase sqlDB = new StiSqlDatabase();
            StiReport rpt = new StiReport();
            StiWebViewer1.ShowParametersButton = false;
            if (loggedInUser == null)
            {
                Session["LoginMessage"] = "You are not properly logged in.";
                Response.Redirect("~/LoginPage.aspx");
                return;
            }

            //if(loggedInUser.Groupid==15)
            //{
            //    report_path = Server.MapPath("~/Reports/StatsGangaRam.mrt");
            //    rpt.Load(report_path);
            //    sqlDB = (StiSqlDatabase)rpt.Dictionary.Databases[0];
            //    sqlDB.ConnectionString = ConnectionString;
            //    rpt.Compile();
            //}
            if (rptid == "FaNoWise")
            {
                int fanofrom = Convert.ToInt32(Request["fromfano"]);
                int fanoto = Convert.ToInt32(Request["tofano"]);
                report_path = Server.MapPath("~/Reports/StatsRpt/DGDetailedListFANOWISE.mrt");
                rpt.Load(report_path);
                sqlDB = (StiSqlDatabase)rpt.Dictionary.Databases[0];
                sqlDB.ConnectionString = ConnectionString;
                rpt.Compile();
                rpt["fano"] = fanofrom;
                rpt["fano2"] = fanoto;
            }
            if (rptid == "DGDetailedList")
            {
                string fromdate = new Cls_Encrypt().GetDate(Convert.ToDateTime(Request["fromdate"]));
                string todate = new Cls_Encrypt().GetDate(Convert.ToDateTime(Request["todate"]));
                report_path = Server.MapPath("~/Reports/StatsRpt/DGDetailedList.mrt");
                rpt.Load(report_path);
                sqlDB = (StiSqlDatabase)rpt.Dictionary.Databases[0];
                sqlDB.ConnectionString = ConnectionString;
                rpt.Compile();
                rpt["mdatefrom"] = fromdate;
                rpt["mdateto"] = todate;
            }
            if (rptid == "DGDetailedListGRANT")
            {
                string fromdate = new Cls_Encrypt().GetDate(Convert.ToDateTime(Request["mfromdate"]));
                string todate = new Cls_Encrypt().GetDate(Convert.ToDateTime(Request["mtodate"]));
                report_path = Server.MapPath("~/Reports/StatsRpt/DGDetailedListGRANT.mrt");
                rpt.Load(report_path);
                sqlDB = (StiSqlDatabase)rpt.Dictionary.Databases[0];
                sqlDB.ConnectionString = ConnectionString;
                rpt.Compile();
                rpt["mfromdate"] = fromdate;
                rpt["mtodate"] = todate;
            }
            if (rptid == "DGHealthApproved")
            {
                report_path = Server.MapPath("~/Reports/StatsRpt/DGHealthPartIwithApprovedAmounts.mrt");
                rpt.Load(report_path);
                sqlDB = (StiSqlDatabase)rpt.Dictionary.Databases[0];
                sqlDB.ConnectionString = ConnectionString;
                rpt.Compile();
            }
            if (rptid == "DGHealthApproved2")
            {
                report_path = Server.MapPath("~/Reports/StatsRpt/DGHealthPartIIwithApprovedAmounts.mrt");
                rpt.Load(report_path);
                sqlDB = (StiSqlDatabase)rpt.Dictionary.Databases[0];
                sqlDB.ConnectionString = ConnectionString;
                rpt.Compile();
            }
            if (rptid == "DGHEALTHCOMPLETESTATS")
            {
                string mfyear = Request["fy"];
                report_path = Server.MapPath("~/Reports/StatsRpt/DGHEALTHCOMPLETESTATS.mrt");
                rpt.Load(report_path);
                sqlDB = (StiSqlDatabase)rpt.Dictionary.Databases[0];
                sqlDB.ConnectionString = ConnectionString;
                rpt.Compile();
                rpt["@fy"] = mfyear;
            }
            if (rptid == "DGHEALTHSTATSTHISTENURE")
            {
                report_path = Server.MapPath("~/Reports/StatsRpt/DGHEALTHCOMPLETESTATSTHISTENURE.mrt");
                rpt.Load(report_path);
                sqlDB = (StiSqlDatabase)rpt.Dictionary.Databases[0];
                sqlDB.ConnectionString = ConnectionString;
                rpt.Compile();
            }
            if (rptid == "DGKhanHEALTHSTATS")
            {
                report_path = Server.MapPath("~/Reports/StatsRpt/DGKHAN_DGHEALTHCOMPLETESTATS.mrt");
                rpt.Load(report_path);
                sqlDB = (StiSqlDatabase)rpt.Dictionary.Databases[0];
                sqlDB.ConnectionString = ConnectionString;
                rpt.Compile();
            }
            if (rptid == "DGKhanSMBDetails")
            {
                report_path = Server.MapPath("~/Reports/StatsRpt/DGKhan_DetailedListGRANT.mrt");
                rpt.Load(report_path);
                sqlDB = (StiSqlDatabase)rpt.Dictionary.Databases[0];
                sqlDB.ConnectionString = ConnectionString;
                rpt.Compile();
            }
            if (rptid == "DGKhanDetailedList")
            {
                string fromdate = new Cls_Encrypt().GetDate(Convert.ToDateTime(Request["mdate1"]));
                string todate = new Cls_Encrypt().GetDate(Convert.ToDateTime(Request["mdate2"]));
                report_path = Server.MapPath("~/Reports/StatsRpt/DGKhan_DetailedList.mrt");
                rpt.Load(report_path);
                sqlDB = (StiSqlDatabase)rpt.Dictionary.Databases[0];
                sqlDB.ConnectionString = ConnectionString;
                rpt.Compile();
                rpt["@mdate1"] = fromdate;
                rpt["@mdate2"] = todate;
            }
            if (rptid == "StatsCategoryWise")
            {
                report_path = Server.MapPath("~/Reports/StatsRpt/StatsCategoryWise.mrt");
                rpt.Load(report_path);
                sqlDB = (StiSqlDatabase)rpt.Dictionary.Databases[0];
                sqlDB.ConnectionString = ConnectionString;
                rpt.Compile();
            }
            if (rptid == "StatsDeptWise")
            {
                report_path = Server.MapPath("~/Reports/StatsRpt/StatsDeptWise.mrt");
                rpt.Load(report_path);
                sqlDB = (StiSqlDatabase)rpt.Dictionary.Databases[0];
                sqlDB.ConnectionString = ConnectionString;
                rpt.Compile();
            }
            if (rptid == "StatsDiseasesWise")
            {
                report_path = Server.MapPath("~/Reports/StatsRpt/StatsDiseasesWise.mrt");
                rpt.Load(report_path);
                sqlDB = (StiSqlDatabase)rpt.Dictionary.Databases[0];
                sqlDB.ConnectionString = ConnectionString;
                rpt.Compile();
            }
            if (rptid == "StatsHospitalWise")
            {
                report_path = Server.MapPath("~/Reports/StatsRpt/StatsHospitalWise.mrt");
                rpt.Load(report_path);
                sqlDB = (StiSqlDatabase)rpt.Dictionary.Databases[0];
                sqlDB.ConnectionString = ConnectionString;
                rpt.Compile();
            }
            if (rptid == "StatsMPAWise")
            {
                report_path = Server.MapPath("~/Reports/StatsRpt/StatsMPAWise.mrt");
                rpt.Load(report_path);
                sqlDB = (StiSqlDatabase)rpt.Dictionary.Databases[0];
                sqlDB.ConnectionString = ConnectionString;
                rpt.Compile();
            }
            if (rptid == "StatsOverall")
            {
                report_path = Server.MapPath("~/Reports/StatsRpt/StatsOverall.mrt");
                rpt.Load(report_path);
                sqlDB = (StiSqlDatabase)rpt.Dictionary.Databases[0];
                sqlDB.ConnectionString = ConnectionString;
                rpt.Compile();
            }
            if (rptid == "DGKhan_StatsMPAWise")
            {
                report_path = Server.MapPath("~/Reports/StatsRpt/DGKhan_StatsMPAWise.mrt");
                rpt.Load(report_path);
                sqlDB = (StiSqlDatabase)rpt.Dictionary.Databases[0];
                sqlDB.ConnectionString = ConnectionString;
                rpt.Compile();
            }
            if (rptid == "DGKhanStatsOverall")
            {
                report_path = Server.MapPath("~/Reports/StatsRpt/DGKhanStatsOverall.mrt");
                rpt.Load(report_path);
                sqlDB = (StiSqlDatabase)rpt.Dictionary.Databases[0];
                sqlDB.ConnectionString = ConnectionString;
                rpt.Compile();
            }
            if (rptid == "Noting32")
            {
                string fanofrom = Request["fromfano"];
                string fanoto = Request["tofano"];
                string fromdate = new Cls_Encrypt().GetDate(Convert.ToDateTime(Request["fromdate"]));
                string todate = new Cls_Encrypt().GetDate(Convert.ToDateTime(Request["todate"]));
                report_path = Server.MapPath("~/Reports/DGNoting32.mrt");
                rpt.Load(report_path);
                sqlDB = (StiSqlDatabase)rpt.Dictionary.Databases[0];
                sqlDB.ConnectionString = ConnectionString;
                rpt.Compile();
                rpt["FromFANo"] = fanofrom;
                rpt["ToFANo"] = fanoto;
                rpt["fromdate"] = fromdate;
                rpt["Todate"] = todate;
            }
            if (rptid == "SingleNoting")
            {
                int fanofrom = Convert.ToInt32(Request["fromfano"]);
                int fanoto = Convert.ToInt32(Request["tofano"]);
                report_path = Server.MapPath("~/Reports/SingleNoting.mrt");
                rpt.Load(report_path);
                sqlDB = (StiSqlDatabase)rpt.Dictionary.Databases[0];
                sqlDB.ConnectionString = ConnectionString;
                rpt.Compile();
                rpt["FromFANo"] = fanofrom;
                rpt["ToFANo"] = fanoto;
            }
            if (rptid == "NotingMultiple")
            {
                string fromdate = new Cls_Encrypt().GetDate(Convert.ToDateTime(Request["fromdate"]));
                string todate = new Cls_Encrypt().GetDate(Convert.ToDateTime(Request["todate"]));
                string mfileno = Request["mfileno"];
                report_path = Server.MapPath("~/Reports/NotingMultiple.mrt");
                rpt.Load(report_path);
                sqlDB = (StiSqlDatabase)rpt.Dictionary.Databases[0];
                sqlDB.ConnectionString = ConnectionString;
                rpt.Compile();
                rpt["FromDate"] = fromdate;
                rpt["ToDate"] = todate;
                rpt["mfileno"] = mfileno;
            }
            if (rptid == "Noting33")
            {
                int fanofrom = Convert.ToInt32(Request["fromfano"]);
                int fanoto = Convert.ToInt32(Request["tofano"]);
                string fromdate = new Cls_Encrypt().GetDate(Convert.ToDateTime(Request["fromdate"]));
                string todate = new Cls_Encrypt().GetDate(Convert.ToDateTime(Request["todate"]));
                report_path = Server.MapPath("~/Reports/DGNoting33.mrt");
                rpt.Load(report_path);
                sqlDB = (StiSqlDatabase)rpt.Dictionary.Databases[0];
                sqlDB.ConnectionString = ConnectionString;
                rpt.Compile();
                rpt["FromFANo"] = fanofrom;
                rpt["ToFANo"] = fanoto;
                rpt["fromdate"] = fromdate;
                rpt["Todate"] = todate;

            }
            if (rptid == "DailyList")
            {
                string todate = new Cls_Encrypt().GetDate(Convert.ToDateTime(Request["todate"]));
                report_path = Server.MapPath("~/Reports/DailyList.mrt");
                rpt.Load(report_path);
                sqlDB = (StiSqlDatabase)rpt.Dictionary.Databases[0];
                sqlDB.ConnectionString = ConnectionString;
                rpt.Compile();
                rpt["refdate"] = todate;
                rpt["pram1"] = Convert.ToDateTime(Request["todate"]).ToShortDateString();
            }
            if (rptid == "MPAWiseReport")
            {
                string catcode = Request["catcode"];
                string cons = Request["cons"];
                report_path = Server.MapPath("~/Reports/STIMPAWiseReport.mrt");
                rpt.Load(report_path);
                sqlDB = (StiSqlDatabase)rpt.Dictionary.Databases[0];
                sqlDB.ConnectionString = ConnectionString;
                rpt.Compile();
                rpt["mcatcode"] = int.Parse(catcode);
                rpt["mcons"] = int.Parse(cons);
            }
            if (rptid == "ExmpaReport")
            {
                string mpaname = Request["mpaname"];
                report_path = Server.MapPath("~/Reports/STIMPAWiseReportBYNAME.mrt");
                rpt.Load(report_path);
                sqlDB = (StiSqlDatabase)rpt.Dictionary.Databases[0];
                sqlDB.ConnectionString = ConnectionString;
                rpt.Compile();
                rpt["mName"] = mpaname;
            }
            if (rptid == "Letter2017")
            {
                int fanofrom = Convert.ToInt32(Request["fromfano"]);
                int fanoto = Convert.ToInt32(Request["tofano"]);
                report_path = Server.MapPath("~/Reports/Letter2017.mrt");
                rpt.Load(report_path);
                sqlDB = (StiSqlDatabase)rpt.Dictionary.Databases[0];
                sqlDB.ConnectionString = ConnectionString;
                rpt.Compile();
                rpt["fromfano"] = fanofrom;
                rpt["ToFANo"] = fanoto;
            }
            if (rptid == "MADAMLetter2017")
            {
                int fanofrom = Convert.ToInt32(Request["fromfano"]);
                int fanoto = Convert.ToInt32(Request["tofano"]);
                report_path = Server.MapPath("~/Reports/MADAMLetter2017.mrt");
                rpt.Load(report_path);
                sqlDB = (StiSqlDatabase)rpt.Dictionary.Databases[0];
                sqlDB.ConnectionString = ConnectionString;
                rpt.Compile();
                rpt["fromfano"] = fanofrom;
                rpt["ToFANo"] = fanoto;
            }
            if (rptid == "FMT46")
            {
                int fanofrom = Convert.ToInt32(Request["fromfano"]);
                int fanoto = Convert.ToInt32(Request["tofano"]);
                report_path = Server.MapPath("~/Reports/rptFMT46.mrt");
                rpt.Load(report_path);
                sqlDB = (StiSqlDatabase)rpt.Dictionary.Databases[0];
                sqlDB.ConnectionString = ConnectionString;
                rpt.Compile();
                rpt["fromfano"] = fanofrom;
                rpt["ToFANo"] = fanoto;
            }
            if (rptid == "FMT47")
            {
                int fanofrom = Convert.ToInt32(Request["fromfano"]);
                int fanoto = Convert.ToInt32(Request["tofano"]);
                report_path = Server.MapPath("~/Reports/rptFMT47.mrt");
                rpt.Load(report_path);
                sqlDB = (StiSqlDatabase)rpt.Dictionary.Databases[0];
                sqlDB.ConnectionString = ConnectionString;
                rpt.Compile();
                rpt["fromfano"] = fanofrom;
                rpt["ToFANo"] = fanoto;
            }
            if (rptid == "FMT48")
            {
                int fanofrom = Convert.ToInt32(Request["fromfano"]);
                int fanoto = Convert.ToInt32(Request["tofano"]);
                report_path = Server.MapPath("~/Reports/rptFMT48.mrt");
                rpt.Load(report_path);
                sqlDB = (StiSqlDatabase)rpt.Dictionary.Databases[0];
                sqlDB.ConnectionString = ConnectionString;
                rpt.Compile();
                rpt["fromfano"] = fanofrom;
                rpt["ToFANo"] = fanoto;
            }
            if (rptid == "FMT49")
            {
                int fanofrom = Convert.ToInt32(Request["fromfano"]);
                int fanoto = Convert.ToInt32(Request["tofano"]);
                report_path = Server.MapPath("~/Reports/rptFMT49.mrt");
                rpt.Load(report_path);
                sqlDB = (StiSqlDatabase)rpt.Dictionary.Databases[0];
                sqlDB.ConnectionString = ConnectionString;
                rpt.Compile();
                rpt["fromfano"] = fanofrom;
                rpt["ToFANo"] = fanoto;
            }
            if (rptid == "MEDICALBOARD")
            {
                int fanofrom = Convert.ToInt32(Request["fromfano"]);
                int fanoto = Convert.ToInt32(Request["tofano"]);
                report_path = Server.MapPath("~/Reports/MEDICALBOARD2017.mrt");
                rpt.Load(report_path);
                sqlDB = (StiSqlDatabase)rpt.Dictionary.Databases[0];
                sqlDB.ConnectionString = ConnectionString;
                rpt.Compile();
                rpt["fromfano"] = fanofrom;
                rpt["ToFANo"] = fanoto;
            }
            if (rptid == "LettertoDCOSingle")
            {
                int fano = Convert.ToInt32(Request["fanos"]);
                //int fanoto = Convert.ToInt32(Request["tofano"]);
                string filenos = Request["mfileno"];
                report_path = Server.MapPath("~/Reports/LettertoDCOSingle.mrt");
                rpt.Load(report_path);
                sqlDB = (StiSqlDatabase)rpt.Dictionary.Databases[0];
                sqlDB.ConnectionString = ConnectionString;
                rpt.Compile();
                rpt["FANo"] = fano;
                //rpt["tofano"] = fanoto;
                rpt["mfileno"] = filenos;

            }
            if (rptid == "LettertoDCOMulti")
            {
                int letcode = Convert.ToInt32(Request["letcode"]);
                string fromdate = new Cls_Encrypt().GetDate(Convert.ToDateTime(Request["fromdate"]));
                string todate = new Cls_Encrypt().GetDate(Convert.ToDateTime(Request["todate"]));
                //int fanoto = Convert.ToInt32(Request["tofano"]);
                string filenos = Request["mfileno"];
                report_path = Server.MapPath("~/Reports/LettertoDCOMultiple.mrt");
                rpt.Load(report_path);
                sqlDB = (StiSqlDatabase)rpt.Dictionary.Databases[0];
                sqlDB.ConnectionString = ConnectionString;
                rpt.Compile();                
                rpt["fromdate"] = fromdate;
                rpt["letcode"] = letcode;
                rpt["Todate"] = todate;

            }
            if(rptid == "TreasuryLetterSingle")
            {
                int fano = Convert.ToInt32(Request["fanos"]);
                string filenos = Request["mfileno"];                
                report_path = Server.MapPath("~/Reports/Letter6Treasury.mrt");
                rpt.Load(report_path);
                sqlDB = (StiSqlDatabase)rpt.Dictionary.Databases[0];
                sqlDB.ConnectionString = ConnectionString;
                rpt.Compile();                
                rpt["FANo"] = fano;
                rpt["mfileno"] = filenos;
            }
            if (rptid == "Treasury12")
            {
                string fromdate = new Cls_Encrypt().GetDate(Convert.ToDateTime(Request["fromdate"]));
                string todate = new Cls_Encrypt().GetDate(Convert.ToDateTime(Request["todate"]));
                string filenos = Request["mfileno"];
                report_path = Server.MapPath("~/Reports/Treasury12.mrt");
                rpt.Load(report_path);
                sqlDB = (StiSqlDatabase)rpt.Dictionary.Databases[0];
                sqlDB.ConnectionString = ConnectionString;
                rpt.Compile();
                rpt["FromDate"] = fromdate;
                rpt["ToDate"] = todate;
                rpt["mfileno"] = filenos;

            }
            if (access == true)
            {
                rpt.Render();
                StiWebViewer1.Report = rpt;

            }
            else
            {
                Session["LoginMessage"] = "You are not authorized for this report. Please contact to Administrator";
                Response.Redirect("../LoginPage.aspx");
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DGModel;

namespace DG.WebUI
{
    public partial class DGSearching : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //LoadGrid();
            }
        }

        private void LoadGrid()
        {
            DGEntities dgent = new DGEntities();

            var dglist = (from a in dgent.MAINCORRs
                          from b in dgent.officers
                          from d in dgent.DEPTs
                          from s in dgent.status
                          where a.offcode == b.OffCode && a.deptcode == d.deptcode && a.statcode == s.StatusCode
                          orderby a.fano
                          select new { a.DGId, a.fano, a.refdate, a.appname, a.address, a.subject, a.cnic, a.chqno, a.chqdate, a.amount, b.Name, d.deptname, s.Description }).ToList();

            DgSearch.DataSource = dglist;
            DgSearch.DataBind();
        }

        protected void DgSearch_PreRender(object sender, EventArgs e)
        {
            LoadGrid();
            if (DgSearch.Rows.Count > 0)
            {
                DgSearch.UseAccessibleHeader = true;
                DgSearch.HeaderRow.TableSection = TableRowSection.TableHeader;
                DgSearch.FooterRow.TableSection = TableRowSection.TableFooter;
            }
        }

        protected void dgvDG_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {

        }

        protected void DgSearch_RowCommand(object sender, GridViewCommandEventArgs e)
        {

        }
    }
}
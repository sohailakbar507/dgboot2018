﻿using DGModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DG.WebUI
{
    public partial class DGHealthFormMultipleHospitals : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Login"] == null)
                {
                    Response.Redirect("~/LoginPage.aspx");
                }

                txtfind.Focus();
                UploadSentto();
                UploadStatus();
                UploadTreating();
                this.ddlRefferedTo.Items.Insert(0, new ListItem("Select Hospital"));
                this.DDLTreating.Items.Insert(0, new ListItem("Select Medical Board Hospital"));
                this.ddlStatus.Items.Insert(0, new ListItem("Select Status"));
                if (Request["fano"] != null)
                {

                    Cls_Login login = (Cls_Login)Session["Login"];
                    txtfind.Text = Request["fano"];
                    GetRecord(login.Username, login.Healthaccessid, Convert.ToInt32(Request["fano"]));
                }
            }
        }
        private void UploadStatus()
        {
            DGEntities dg = new DGEntities();
            var stat = (from b in dg.PatientStatus select b).ToList();
            ddlStatus.DataSource = stat;
            ddlStatus.DataValueField = "PtID";
            ddlStatus.DataTextField = "Description";
            //ddlStatus.SelectedIndex = 0;
            ddlStatus.DataBind();
        }
        private void UploadSentto()
        {
            DGEntities dg = new DGEntities();
            var sentto = (from a in dg.Reffereds orderby a.Refto select a).ToList();
            ddlRefferedTo.DataSource = sentto;
            ddlRefferedTo.DataValueField = "RefId";
            ddlRefferedTo.DataTextField = "Refto";
            ddlRefferedTo.DataBind();
        }
        private void UploadTreating()
        {
            DGEntities dg = new DGEntities();
            var sentto = (from a in dg.Reffereds select a).ToList();
            DDLTreating.DataSource = sentto;
            DDLTreating.DataValueField = "RefId";
            DDLTreating.DataTextField = "Refto";
            DDLTreating.DataBind();
        }
        private void GetRecord(string username, int groupid, int fano)
        {
            try
            {
                DGEntities dg = new DGEntities();
                int fanos = fano;
                var getfno = from h in dg.Healths where h.fanoHealth == fanos select h;
                var maininfo2 = (from m in dg.MAINCORRs where m.fano == fanos select m.DGId).Max();
                var maininfo1 = (from m in dg.MAINCORRs where m.DGId == maininfo2 select m);

                if (maininfo1.Count() > 0)
                {
                    var healinfo = getfno.First();
                    var maininfo = maininfo1.First();
                    txtFano.Text = Convert.ToInt32(maininfo.fano).ToString();
                    txtAppname.Text = maininfo.appname;
                    txtAddress.Text = maininfo.address;
                    txtPatientName.Text = maininfo.PatientName;
                    txtThischqAmount.Text = Convert.ToInt32(maininfo.amount).ToString();
                    txtSancAmount.Text = Convert.ToInt32(healinfo.HealthAmount).ToString();
                    txtBalReturned.Text = Convert.ToInt32(maininfo.Balance).ToString();
                    txtAmount.Text = Convert.ToInt32(maininfo.AmountIncured).ToString();
                    txtAuthorityNo.Text = maininfo.AuthorityNumber;
                    rdtAuthorityDate.SelectedDate = maininfo.Authority_Date;
                    txtFDSancNo.Text = maininfo.SancNo;
                    rdtFDSancDate.SelectedDate = maininfo.Sancdate;
                    txtRefunded.Text = Convert.ToInt32(maininfo.refunded).ToString();
                    txtRecvdChqno.Text = maininfo.chqno;
                    rdRecvdChqDate.SelectedDate = maininfo.chqdate;
                }

                if (getfno.Count() > 0)
                {
                    var healinfo = getfno.First();
                    var maininfo = (from m in dg.MAINCORRs where m.fano == healinfo.fanoHealth select m).First();
                    txtPageNo.Text = Convert.ToInt32(healinfo.pagenoHealth).ToString();
                    txtAmount.Text = Convert.ToInt32(healinfo.AmountInc).ToString();
                    txtPatientName.Text = healinfo.PatientName;
                    txtFDSancNo.Text = healinfo.fdSactionNo;
                    txtRefunded.Text = Convert.ToInt32(healinfo.AmountRefunded).ToString();
                    txtSancAmount.Text = Convert.ToInt32(healinfo.HealthAmount).ToString();
                    rdtFDSancDate.SelectedDate = healinfo.fdSanctionDate;
                    rdtVouchedDate.SelectedDate = healinfo.VouchedDate;
                    rdtLiquidDate.SelectedDate = healinfo.LiquidationDate;
                    txtAuthorityNo.Text = healinfo.AuthorityNo;
                    rdtAuthorityDate.SelectedDate = healinfo.AuthorityDate;
                    txtFileNumber.Text = healinfo.FileNumber;
                    txtRemarks.Text = healinfo.HealthRemarks;
                    ddlRefferedTo.SelectedIndex = ddlRefferedTo.Items.IndexOf(ddlRefferedTo.Items.FindByValue(healinfo.refid.ToString()));
                    DDLTreating.SelectedIndex = DDLTreating.Items.IndexOf(DDLTreating.Items.FindByValue(healinfo.TreatId.ToString()));
                    ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByValue(healinfo.PtStatusID.ToString()));
                    rdtUpdatedOn.SelectedDate = healinfo.UpdatedOn;
                    txtRemarks.Text = healinfo.HealthRemarks;
                    txtRecvdChqno.Text = healinfo.RecvdChqno;
                    rdRecvdChqDate.SelectedDate = healinfo.RecvdChqDate;
                    txtBalReturned.Text = Convert.ToInt32(healinfo.BalReturn).ToString();
                    txtBalChqNo.Text = healinfo.BalChqno;
                    rdBalChqDate.SelectedDate = healinfo.BalChqDate;

                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Alert", "alert('Record Not found..');", true);
                }
            }

            catch (Exception ex)
            { }
        }

        protected void lnkbtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                DGEntities dg = new DGEntities();
                System.Threading.Thread.Sleep(500);
                int fanos = Convert.ToInt32(txtfind.Text);
                var getfano = from a in dg.MAINCORRs where a.fano == fanos select a;
                var getfanomax = (from a in dg.MAINCORRs where a.fano == fanos select a.DGId).Max();
                var m = (from a in dg.MAINCORRs where a.DGId == getfanomax select a).First();
                int hfano = Convert.ToInt32(txtFano.Text);
                var getfno = from h in dg.Healths where h.fanoHealth == hfano select h;
                if (getfano.Count() > 0)
                {
                    if (getfno.Count() > 0)
                    {
                        var h = getfno.First();
                        h.fanoHealth = fanos;
                        if (string.IsNullOrEmpty(txtPageNo.Text))
                        {
                            txtPageNo.Text = "0";
                        }

                        m.pageno = Convert.ToInt32(txtPageNo.Text);
                        h.pagenoHealth = Convert.ToInt32(txtPageNo.Text);

                        if (ddlRefferedTo.SelectedItem.Value == "Select Hospital")
                        {
                            h.refid = null;
                        }
                        else
                        {
                            h.refid = Convert.ToInt32(ddlRefferedTo.SelectedItem.Value);
                        }
                        if (DDLTreating.SelectedItem.Value == "Select Medical Board Hospital")
                        {
                            h.TreatId = 101;
                        }
                        else
                        {
                            h.TreatId = Convert.ToInt32(DDLTreating.SelectedItem.Value);
                        }

                        if (ddlStatus.SelectedItem.Value == "Select Status")
                        {
                            h.PtStatusID = null;
                        }
                        else
                        {
                            h.PtStatusID = Convert.ToInt32(ddlStatus.SelectedItem.Value);
                        }
                        h.AmountInc = txtAmount.Text == string.Empty ? 0 : Convert.ToInt32(txtAmount.Text);
                        m.Expense = h.AmountInc;
                        h.HealthAmount = txtSancAmount.Text == string.Empty ? 0 : Convert.ToInt32(txtSancAmount.Text);
                        h.AmountRefunded = txtRefunded.Text == string.Empty ? 0 : Convert.ToInt32(txtRefunded.Text);
                        h.UpdatedOn = DateTime.Now;
                        h.fdSactionNo = txtFDSancNo.Text;
                        h.PatientName = txtPatientName.Text;
                        h.fdSanctionDate = rdtFDSancDate.DateInput.SelectedDate;
                        h.VouchedDate = rdtVouchedDate.DateInput.SelectedDate;
                        h.LiquidationDate = rdtLiquidDate.DateInput.SelectedDate;
                        h.AuthorityNo = txtAuthorityNo.Text;
                        h.AuthorityDate = rdtAuthorityDate.DateInput.SelectedDate;
                        h.FileNumber = txtFileNumber.Text;
                        h.HealthRemarks = txtRemarks.Text;
                        h.HealthRefdate = m.refdate;
                        h.ThisChqAmount = txtThischqAmount.Text == string.Empty ? 0 : Convert.ToInt32(txtThischqAmount.Text);
                        h.RecvdChqno = txtRecvdChqno.Text;
                        h.RecvdChqDate = rdRecvdChqDate.DateInput.SelectedDate;
                        m.amount = txtThischqAmount.Text == string.Empty ? 0 : Convert.ToInt32(txtThischqAmount.Text);
                        m.AmountIncured = txtAmount.Text == string.Empty ? 0 : Convert.ToInt32(txtAmount.Text);
                        int amnt = txtSancAmount.Text == string.Empty ? 0 : Convert.ToInt32(txtSancAmount.Text);
                        int expenses = txtAmount.Text == string.Empty ? 0 : Convert.ToInt32(txtAmount.Text);
                        m.Balance = amnt - expenses;
                        // m.Balance = Convert.ToInt32(txtSancAmount.Text) - Convert.ToInt32(txtAmount.Text);
                        m.AuthorityNumber = txtAuthorityNo.Text;
                        m.Authority_Date = rdtAuthorityDate.DateInput.SelectedDate;
                        m.SancNo = txtFDSancNo.Text;
                        m.Sancdate = rdtFDSancDate.DateInput.SelectedDate;
                        m.chqno = txtRecvdChqno.Text;
                        m.chqdate = rdRecvdChqDate.SelectedDate;

                        if (string.IsNullOrEmpty(txtSancAmount.Text))
                        {
                            txtSancAmount.Text = "0";
                        }
                        if (string.IsNullOrEmpty(txtAmount.Text))
                        {
                            txtAmount.Text = "0";
                        }

                        {
                            h.BalReturn = Convert.ToInt32(txtThischqAmount.Text) - Convert.ToInt32(txtAmount.Text);
                        }
                        //h.BalReturn = txtBalReturned.Text == string.Empty ? 0 : Convert.ToInt32(txtBalReturned.Text);
                        h.BalChqno = txtBalChqNo.Text;
                        h.BalChqDate = rdBalChqDate.DateInput.SelectedDate;
                        dg.SaveChanges();
                        Cls_Login login = (Cls_Login)Session["Login"];
                        GetRecord(login.Username, login.Groupid, fanos);
                        ScriptManager.RegisterStartupScript(this, GetType(), "Alert", "alert('Record Saved Successfully ..');", true);
                    }
                }
            }

            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Alert", "alert('" + ex.Message.ToString() + "');", true);
            }
        }
        protected void lnkbtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("LoginPage.aspx");
        }
        protected void lnkBtnSearch_Click(object sender, EventArgs e)
        {
            System.Threading.Thread.Sleep(1000);
            Cls_Login login = (Cls_Login)Session["Login"];
            int fanos = Convert.ToInt32(txtfind.Text);
            GetRecord(login.Username, login.Healthaccessid, fanos);
        }
        protected void btnChqUpdate_Click(object sender, EventArgs e)
        {
            if (txtFano.Text != "")
            {
                int fano = Convert.ToInt32(txtfind.Text);
                Response.Redirect("MultipleHospitalsUpdate.aspx?fano=" + fano);
            }
        }

    }
 }

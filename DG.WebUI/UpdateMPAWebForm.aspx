﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DGMain.Master" AutoEventWireup="true" CodeBehind="UpdateMPAWebForm.aspx.cs" Inherits="DG.WebUI.UpdateMPAWebForm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <script type="text/javascript">
    var api_url = "/Api/AddMPA_Api.aspx";
    var mpa_id = 0;
     function MPA_btnAdd_OnClick() {
         mpa_id = 0;
        $("#txtcatcons").val("");
        $("#txtName").val("");
         $("#txtaddress1").val("");
         $("#txtaddress2").val("");
         $("#txtdist").val("");
         }


         function MPA_btnSaveChanges_OnClick() {
             var data = {
                 action: "save",
                 Refid: mpa_id,
                 catcons: $("#txtcatcons").val(),
                 name: $("#txtName").val(),
                 address1: $("#txtaddress1").val(),
                 address2: $("#txtaddress2").val(),
                 district: $("#txtdist").val()
             };
             $.get(api_url, data, function (response) {
                 var r = JSON.parse(response);
                 location.reload();
             })
         }

         function MPA_btnDelete_OnClick(id, name) {

             if (confirm("Are you sure to delete " + name + "?")) {
                 $.get(api_url, { action: "delete", RefId: id }, function (response) {
                     var r = JSON.parse(response);
                     location.reload();
                 });
             }
         }

         function MPA_btnEdit_OnClick(id) {
             $.get(api_url, { action: "get", RefId: id },
                 function (response) {
                     var r = JSON.parse(response);
                 $("#txtcatcons").val(r.CatCons);
                 $("#txtName").val(r.Name);
                 $("#txtaddress1").val(r.Address1);
                 $("#txtaddress2").val(r.Address2);
                 $("#txtdist").val(r.District);
                 mpa_id = id;
             });
         }        

</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="panel panel-primary">
                        <div class="panel-heading">
                            <button type="button" onclick="MPA_btnAdd_OnClick()" data-toggle="modal" data-target=".bs-example-modal-lg" class="btn btn-info">Add New</button>
                        </div>
                        <div class="panel-body">
                             <div class="dataTable_wrapper">

                                <table class="table table-striped table-bordered table-hover" id="Contats-Table">
                                    <thead>
                                        <tr>
                                            <th>Consituency</th>
                                            <th>MPA Name</th>
                                            <th>Address 1</th>
                                            <th>Address 2</th>
                                            <th>District</th>
                                            <th>Edit</th>
                                            <th>Delete</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <%
                                           
                                            foreach (var abc in this.mpalist)
                                            { %>
                                        <tr>
                                            <td><%=abc.CatCons%></td>
                                            <td><%=abc.Name%></td>
                                            <td><%=abc.Address1%></td>
                                            <td><%=abc.Address2%></td>
                                            <td><%=abc.District%></td>
                                            <td>
                                           <%-- <td><span class="input-group-btn">
                                                <button id="selectoff" class="btn btn-default" onclick="FillBilling(<%=abc.partycode %>)" type="button" data-dismiss="modal"><a class="btn btn-social-icon btn-bitbucket"><i class="fa fa-bitbucket"></i></a></button>
                                            </span>--%>
                                               <button type="button" onclick="MPA_btnEdit_OnClick(<%=abc.RefId%>)" data-toggle="modal" data-target=".bs-example-modal-lg" class="btn btn-info btn-circle center-block"><i class="fa fa-edit"></i></button>
                                            </td>
                                            <td>
                                                <%--<a class="btn btn-social-icon btn-bitbucket" onclick="FillBilling(<%=abc.partycode %>)"><i class="fa fa-bitbucket"></i></a>--%>
                                                <button type="button" onclick="MPA_btnDelete_OnClick(<%=abc.RefId%>,'<%=abc.Name %>')" class="btn btn-danger btn-circle center-block"><i class="fa fa-trash-o"></i></button>
                                            </td>
                                        </tr>
                                        <%}
                                        %>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                        <div class="panel-footer">
                            Panel Footer
                        </div>
                    </div>

    <div class="col-md-4" style="z-index:9999;">
            <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title" id="myLargeModalLabel">Update MPAs Information</h4>
                        </div>
                        <div class="modal-body">
                                <div class="form-group m-b-40">
                                    <label for="txtcatcons">Constituency</label>
                                    <input type="text" class="form-control" id="txtcatcons" name="txtcatcons" required><span class="highlight"></span> <span class="bar"></span>
                                </div>
                                <div class="form-group m-b-40">
                                    <label for="txtName">MPA Name</label>
                                    <input type="text" class="form-control" id="txtName" name="txtName" required><span class="highlight"></span> <span class="bar"></span>
                                    
                                </div>
                                <div class="form-group m-b-40">
                                    <label for="txtaddress1">Address 1</label>
                                    <input type="text" class="form-control" id="txtaddress1" name="txtaddress1" required><span class="highlight"></span> <span class="bar"></span>
                                    
                                </div>
                                 <div class="form-group m-b-40">
                                     <label for="txtaddress2">Address 2</label>
                                    <input type="text" class="form-control" id="txtaddress2" name="txtaddress2" required><span class="highlight"></span> <span class="bar"></span>
                                    
                                </div>
                             <div class="form-group m-b-40">
                                    <label for="txtdist">District</label>
                                    <input type="text" class="form-control" id="txtdist" name="txtdist" required><span class="highlight"></span> <span class="bar"></span>
                                    
                                </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" id="btnSaveChanges" class="btn btn-success waves-effect waves-light m-r-10" onclick="MPA_btnSaveChanges_OnClick()">Save Changes</button>
                            <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
        

    </div>
</asp:Content>

<asp:Content ID="Content3" runat="server" contentplaceholderid="CphScript">
     <script type="text/javascript">
         $(document).ready(function () {
             $('#Contats-Table').DataTable();
         });
        </script>
</asp:Content>



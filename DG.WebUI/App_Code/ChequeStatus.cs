﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DG.WebUI.App_Code
{
    public enum ChequeStatus
    {
        NONE = 0,
        BY_MPA = 1,
        CHEQUE_GIVEN_BY_DC = 2,
        BY_MAIL = 3
    }
}
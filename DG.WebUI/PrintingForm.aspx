﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DGMain.Master" AutoEventWireup="true" CodeBehind="PrintingForm.aspx.cs" Inherits="DG.WebUI.PrintingForm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
    </telerik:RadScriptManager>
    <br /><br />
    <div class="container">
  <div class="row">
   <div class="Absolute-Center is-Responsive">
<div class="panel panel-danger">
                        <div class="panel-heading panel-primary">
                        
                            <h5 align = "center"> &nbsp;<asp:Label ID="lblTitle" runat="server" Text="NOTING - PRINTING FORM" ForeColor="blue"></asp:Label>
                            </h5> 
                            
                        </div>
                        <div class =" panel-body">
                        <div class="row" align="center">
                            <div>
                                <br />
                              FROM:  <telerik:RadDatePicker ID="datefrom" runat="server"></telerik:RadDatePicker>
                               TO:   <telerik:RadDatePicker ID="dateto" runat="server"></telerik:RadDatePicker>

                                <br /><br />

                            </div>
             <div  class="dataTable_wrapper">
                 <asp:GridView ID="PrintDataGridView" runat="server" HorizontalAlign="Center" BackColor="White" BorderColor="#CC9966" BorderStyle="None" BorderWidth="1px" CellPadding="4" Height="220px" Width="725px"
                     AutoGenerateColumns="False" CssClass="table table-striped table-bordered table-hover" AllowPaging="True" OnPageIndexChanging="PrintDataGridView_PageIndexChanging" OnRowCommand="PrintDataGridView_RowCommand"
                     >
             <Columns>
            <asp:TemplateField>
                <HeaderTemplate>
				Select All
				<asp:CheckBox ID="chkSelectAll" runat="server" onclick="toggleCheckboxes(this, 'cbRows');"/>
			</HeaderTemplate>
            <ItemTemplate>
                <asp:CheckBox ID = "cbRows" runat = "server" />
            </ItemTemplate>                
            </asp:TemplateField>

            <asp:TemplateField HeaderText="DG_Id" Visible="false">
             <ItemTemplate>
                <asp:Label ID="lbldgId" runat="server" Text='<%#Eval("DGId") %>'></asp:Label>
             </ItemTemplate>
             </asp:TemplateField>
             <asp:TemplateField HeaderText="FANO">
                <ItemTemplate>
                    <asp:Label ID="lblfano" runat="server" Text='<%#Eval("fano")%>'></asp:Label>
                </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Applicant Name">
                <ItemTemplate>
                    <asp:Label ID="lblappname" runat="server" Text='<%#Eval("appname")%>'></asp:Label>
                </ItemTemplate>
                </asp:TemplateField>
             
            <asp:TemplateField HeaderText="Date">
                 <ItemTemplate>
                     <asp:Label ID="lbldate" runat="server" Text='<%#Eval("asadate","{0:dd/MM/yyyy}") %>'></asp:Label>
                  </ItemTemplate>

            </asp:TemplateField>
            
                </Columns>
                        <FooterStyle BackColor="#990000" ForeColor="White" Font-Bold="True" />
                        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                        <PagerSettings FirstPageText=">>" LastPageText="<<" />
                        <PagerStyle HorizontalAlign = "Center" CssClass = "pagination-ys" BackColor="#FFCC66" ForeColor="#333333" />
                     
                     <RowStyle ForeColor="#000066" />
                                          
                     <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#663399" />
                     <SortedAscendingCellStyle BackColor="#FEFCEB" />
                     <SortedAscendingHeaderStyle BackColor="#AF0101" />
                     <SortedDescendingCellStyle BackColor="#F6F0C0" />
                     <SortedDescendingHeaderStyle BackColor="#7E0000" />
                     
                     
                     </asp:GridView>
                 
            </div>
            <asp:Button ID="btnShow" align="center" runat="server" Text="Show Data" CssClass="btn btn-primary btn-danger" onclick="btnSubmit_Click" /> 
            <asp:Button ID="btnPrint" align="center" runat="server" Text="PRINT" CssClass="btn btn-primary btn-danger" OnClick="btnPrint_Click" />
            <br />
            </div></div></div></div></div></div>
    <script type="text/javascript">

        function toggleCheckboxes(ctrl, chk) {
            $('#<%=PrintDataGridView.ClientID %> :checkbox').attr('checked', ctrl.checked);
            <%--$('#<%=PrintDataGridView.ClientID %> :checkbox[id$=' + chk + ']').attr('checked', ctrl.checked);--%>
        }
            </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CphScript" runat="server">
</asp:Content>

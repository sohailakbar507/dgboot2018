﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HospitalsMaster.Master" AutoEventWireup="true" CodeBehind="HospitalsUpdateForm.aspx.cs" Inherits="DG.WebUI.HospitalsUpdateForm" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <style type="text/css">
        .panel {
    border: 0;
}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <div class="container" style="position:center">
  <div class="row">
   <%--<div class="Absolute-Center is-Responsive">--%>
<div class="panel panel-default">
                      <div class="panel-body">
                         
<asp:ScriptManager ID="ScriptManager1" runat="server">
</asp:ScriptManager>
                          <div>
        <table width="100%">
            <tr>
                <td align="center">
                    <asp:Label ID="lblHospName" runat="server" Text="Hospitals - Update Form" Font-Bold="True" Font-Names="Arial Black" Font-Size="X-Large" ForeColor="#006600"></asp:Label>
                    
                </td>
                
                <td align="center">
                    <asp:Label ID="lblHospitalName" runat="server" Font-Bold="True" Font-Names="Arial Black" Font-Size="Large" ForeColor="#FF9900"></asp:Label>
                    <asp:Label ID="lblRecords" runat="server" Font-Names="Arial Black" Font-Size="Large" ForeColor="#000099"></asp:Label>                 
                    <br /><br />
                </td>
            </tr>

        </table></div>
        <div>
    <table align="center" width="60%" cellpadding = "1" cellspacing = "1" border = "0px" style= "border-collapse:collapse">
            <tr>
                <td colspan="2">
                    <asp:Label ID="lblFaNo" runat="server" Text="FaNo."></asp:Label></td>
                <td colspan="2" align="center"><asp:TextBox ID="txtFaNo" runat="server" CssClass="form-control" Width="250px"></asp:TextBox>
                </td>
            </tr>
            </table>
    
    <table  width="100%" cellpadding = "0" cellspacing = "0" border = "0px" style= "border-collapse:collapse">
        <tr>
            <td colspan="4" align="center">
            <br />
                  
    </td>
</tr>
         <tr>
                <td colspan="4" align="center">
                    <asp:LinkButton ID="lnkBtnSearch" runat="server" ToolTip = "Search Button" onclick="lnkBtnSearch_Click"><img src="Images/Zoom-icon.png" alt = ""/> </asp:LinkButton>
                </td>
            </tr>
        <tr>
            <td><div class="dataTable_wrapper">
            <asp:GridView ID="dgvDG" runat="server" AllowPaging="True" PageSize="20"
            onpageindexchanging="dgvDG_PageIndexChanging" 
            onselectedindexchanged="dgvDG_SelectedIndexChanged" 
            AutoGenerateColumns="False" onrowcancelingedit="dgvDG_RowCancelingEdit" 
            onrowupdating="dgvDG_RowUpdating" DataKeyNames="DGId" 
            CssClass="table table-striped table-bordered table-hover" CellPadding="4" BackColor="White" BorderColor="#CC9966" BorderStyle="None" BorderWidth="1px">
           
               <Columns>
                   
                   <asp:TemplateField>
                        <ItemTemplate>
                            <asp:CheckBox ID = "cbRows" runat = "server" />
                        </ItemTemplate>
                   </asp:TemplateField>
                   
                   <asp:TemplateField HeaderText="DGId" Visible="False">
                   <ItemTemplate>
                       <asp:Label ID="lblDGId" runat="server" Text= '<%#Eval("DGId")%>'></asp:Label>
                   </ItemTemplate>
                   
                   </asp:TemplateField>
                   
                   
                   <asp:TemplateField HeaderText="FANo.">
                   <ItemTemplate>
                       <asp:Label ID="lblFaNo" runat="server" Text='<%#Eval("fanoHealth")%>'></asp:Label>
                   </ItemTemplate>
                   </asp:TemplateField>
                   <asp:TemplateField HeaderText="Applicant Name">
                   <ItemTemplate>
                       <asp:Label ID="lblAppname" runat="server" Text='<%#Eval("PATIENTINFO")%>'></asp:Label>
                   </ItemTemplate>
                   <ControlStyle Width="350px" />
                   <ItemStyle Width="350px" />
                   </asp:TemplateField> 
                   <asp:TemplateField HeaderText="Amount">
                       <ItemTemplate>
                           <asp:Label ID="lblAmount" runat="server" Text='<%#Eval("HealthAmount")%>'></asp:Label>
                       </ItemTemplate>
                   </asp:TemplateField>
                   <asp:TemplateField HeaderText="Cheque No.">
                       <ItemTemplate>
                           <asp:Label ID="lblchqno" runat="server" Text='<%#Eval("RecvdChqno")%>'></asp:Label>
                       </ItemTemplate>
                        </asp:TemplateField>
                       <asp:TemplateField HeaderText="Cheque Date">
                       <ItemTemplate>
                           <asp:Label ID="lblchqdate" runat="server" Text='<%#Eval("RecvdChqDate","{0:dd/MM/yyyy}") %>'></asp:Label>
                       </ItemTemplate>
                   </asp:TemplateField>
                   
                   <asp:TemplateField HeaderText="Financial Year">
                       <ItemTemplate>
                           <asp:Label ID="lblfyear" runat="server" Text='<%#Eval("fyear")%>'></asp:Label>
                       </ItemTemplate>
                   </asp:TemplateField>
                                     
                   
                   <asp:TemplateField HeaderText="Edit">
                   <ItemTemplate>
                       <asp:LinkButton ID="LnkBtnEdit" CommandName = "update" runat="server"><img src = "Images/Text-Edit-icon.png" alt = "" /> </asp:LinkButton>
                   </ItemTemplate>
                   </asp:TemplateField>
                   
                   
               </Columns>
              
                <FooterStyle BackColor="#FFFFCC" ForeColor="#330099" />
                <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="#FFFFCC" />
              
                 <PagerSettings FirstPageText=">>" LastPageText="<<" />
                <PagerStyle HorizontalAlign = "Center" CssClass = "pagination-ys" BackColor="#FFFFCC" ForeColor="#330099" />

               
                <RowStyle BackColor="White" ForeColor="#330099" />
                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#663399" />
                <SortedAscendingCellStyle BackColor="#FEFCEB" />
                <SortedAscendingHeaderStyle BackColor="#AF0101" />
                <SortedDescendingCellStyle BackColor="#F6F0C0" />
                <SortedDescendingHeaderStyle BackColor="#7E0000" />

               
        </asp:GridView>
        </div>
        </td>
      </tr>

    </table>
    </div></div></div></div>
</div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphScripts" runat="server">
</asp:Content>

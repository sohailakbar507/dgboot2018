﻿using DGModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DG.WebUI
{
    public partial class LoginPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
            }
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            DGEntities dgent = new DGEntities();
            string name = txtusername.Value;
            string pass = Cls_Encrypt.Encrypt(txtPass.Value);
            //string pass1 = Cls_Encrypt.Decrypt(txtPass.Value);
            var CheckUser = from a in dgent.Users where a.Name == name && a.Password == pass select a;


            if (CheckUser.Count() > 0)
            {
                var users = CheckUser.First();
                Cls_Login login = new Cls_Login();
                login.Userid = Convert.ToInt32(users.ID);
                login.Username = users.Name;
                var getdeptname = from a in dgent.DEPTs where a.deptcode == users.DCODeptid select a;
                var gethospname = from h in dgent.Reffereds where h.RefId == users.HospId select h;
                if (getdeptname.Count() > 0)
                {
                    var depname = getdeptname.First();
                    login.Deptname = depname.deptname;
                    //login.Deptname = users.Dept.deptname;

                }
                if (gethospname.Count() > 0)
                {
                    var hname = gethospname.First();
                    login.Hospitalname = hname.Refto;
                }

                login.deptid = Convert.ToInt32(users.DCODeptid);
                login.hosptid = Convert.ToInt32(users.HospId);
                login.Groupid = Convert.ToInt32(users.Group_ID);
                login.Isadd = Convert.ToBoolean(users.IsAdd_Record);
                login.Isedit = Convert.ToBoolean(users.IsEdit_Record);
                login.Isdel = Convert.ToBoolean(users.IsDelete_Record);
                login.IsUpdateForm = Convert.ToBoolean(users.UpdateForm);
                var gethealthaccessid = from a in dgent.User_Groups where a.ID == users.Group_ID select a;
                if (gethealthaccessid.Count() > 0)
                {
                    var getaccessid = gethealthaccessid.First();
                    login.Healthaccessid = Convert.ToInt32(getaccessid.HealthAccessid);
                }
                //login.IsAnalYearReport = Convert.ToBoolean(users.AnalYearReport);
                Session["Login"] = login;
                if (login.Healthaccessid == 3)
                {
                    Response.Redirect("HealthForm.aspx");
                }

                if (login.Healthaccessid == 5)
                {
                    Response.Redirect("HealthForm.aspx");
                }

                if (login.Groupid == 8)
                {
                    Response.Redirect("DCOWebForm.aspx");
                }
                if (login.Groupid == 9 && login.Userid==1806)
                {
                    Response.Redirect("DGHealthFormMultipleHospitals.aspx");
                }

                if (login.Groupid == 9)
                {
                    Response.Redirect("DGHealthWebForm.aspx");
                }
                if (login.Groupid == 10)
                {
                    Response.Redirect("DataTransferWebForm.aspx");
                }
                if (login.Groupid == 11)
                {
                    Response.Redirect("HospitalsUpdateForm.aspx");
                }
                if (login.Groupid == 12)
                {
                    Response.Redirect("DgStats.aspx");
                }
                if (login.Groupid == 13)
                {
                    Response.Redirect("DgKhanAnalysis.aspx");
                }
                if (login.Groupid == 14)
                {
                    Response.Redirect("DgHealthPage.aspx");
                }
                if (login.Groupid == 15)
                {
                    Response.Redirect("HospitalsPage.aspx");
                }
                if(login.Groupid ==16)
                {
                    Response.Redirect("HospitalVisitForm.aspx");
                }
                Response.Redirect("MainPage.aspx");

            }
            
        }

        protected void btnChangePasswordForm_Click(object sender, EventArgs e)
        {
            //Response.Redirect("ChangePassword.aspx");

        }

    }
}
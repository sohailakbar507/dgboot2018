﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HospitalsMaster.Master" AutoEventWireup="true" CodeBehind="UpdateApproved.aspx.cs" Inherits="DG.WebUI.UpdateApproved" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            $('.form-control').keyup(function (e) {
                //console.log(e.keyCode);
                if (e.keyCode == 13) {
                    var $this = $(this),
                        index = $this.closest('td').index();
                    $this.closest('tr').next().find('td').eq(index).find('.form-control').focus();
                    //$this.closest('tr').next().find('td').eq(index).find('.form-control').val("");
                    e.preventDefault();
                }
            })
       });

       function successalert() {
           swal({
               title: 'Success!',
               text: 'Record Saved Successfully.......',
               type: 'success'
           });
       }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <br /><br />

    <div class="container" style="position:center; width: 95%;">
  <div class="row">
<div class="panel panel-primary">
                        <div class="panel-heading">
                        
                                                    
    <h3 class="panel-title">Update Approved Form</h3>

                        </div>
                        <div class="panel-body">
    
    
        <div class="row">
</div>
                            <div>

                                <asp:Label ID="lblfano" runat="server" Text="FA #"></asp:Label>
                                <asp:TextBox ID="txtFano" runat="server"></asp:TextBox>
                                <asp:Button ID="btnFind" runat="server" Text="FIND" OnClick="btnFind_Click" Width="109px" />
                            </div>
                <asp:GridView ID="DGVApproved" runat="server" CssClass="table" AutoGenerateColumns= "False" CellPadding="0" 
                    HorizontalAlign ="Center"
                    onrowediting="DGVApproved_RowEditing" Width="100%" 
                    onpageindexchanging="DGVApproved_PageIndexChanging" 
                    onrowupdating="DGVApproved_RowUpdating" DataKeyNames="DGId" 
                     GridLines="None" OnRowCommand="DGVApproved_RowCommand" OnRowDeleting="DGVApproved_RowDeleting" Font-Size= "12px">

               <Columns>
              
                <asp:TemplateField HeaderText="DGID" Visible="true">
                <ItemTemplate>
                    <asp:Label ID="lblDGId" runat="server" Text='<%#Eval("DGId") %>'></asp:Label>
                </ItemTemplate>
                </asp:TemplateField>
              <asp:TemplateField HeaderText="FaNo.">
              <ItemTemplate>
                 <asp:Label ID="lblfano" runat="server" CssClass="form-control input-sm" Text='<%#Eval("fano") %>'></asp:Label>
              </ItemTemplate> <ControlStyle Width="70px" Font-Size="X-Small" ForeColor="#CC0000" /><HeaderStyle Font-Size="Smaller" />
                        
              </asp:TemplateField>    
                    <asp:TemplateField HeaderText="Applciant Name">
              <ItemTemplate>
                 <asp:Label ID="lblappname" runat="server" CssClass="form-control input-sm" Text='<%#Eval("appname") %>'></asp:Label>
              </ItemTemplate> <ControlStyle Width="70px" Font-Size="X-Small" ForeColor="#CC0000" /><HeaderStyle Font-Size="Smaller" />
                        
              </asp:TemplateField>   
              
              <asp:TemplateField HeaderText="Amount">
              <ItemTemplate>
                 <asp:Label ID="lblAmount" CssClass="form-control input-sm" runat="server" Text='<%#Eval("amount") %>'></asp:Label>
              </ItemTemplate>
                  <ControlStyle Width="70px" ForeColor="#CC0000" /><HeaderStyle Font-Size="Smaller" />
                  <HeaderStyle HorizontalAlign="Center" Height="40px" VerticalAlign="Middle" />
                  <ItemStyle HorizontalAlign="Center" />
              </asp:TemplateField>
               <asp:TemplateField HeaderText="Approved">
              <ItemTemplate>
                 <asp:Label ID="lblApproved" CssClass="form-control input-sm" runat="server" Text='<%#Eval("ApprovedAmount") %>'></asp:Label>
              </ItemTemplate>
                  <ControlStyle Width="70px" ForeColor="#CC0000" /><HeaderStyle Font-Size="Smaller" />
                  <HeaderStyle HorizontalAlign="Center" Height="40px" VerticalAlign="Middle" />
                  <ItemStyle HorizontalAlign="Center" />
              </asp:TemplateField>
                    <asp:TemplateField HeaderText="Fyear">
              <ItemTemplate>
                 <asp:Label ID="lblfyear" runat="server" CssClass="form-control input-sm" Text='<%#Eval("fyear") %>'></asp:Label>
              </ItemTemplate> <ControlStyle Width="70px" Font-Size="X-Small" ForeColor="#CC0000" /><HeaderStyle Font-Size="Smaller" />
                        
              </asp:TemplateField>

                   </Columns>
                     </asp:GridView>            
         <br /><br />       
    </div>
    <div class="modal-footer">
        <asp:Button ID="btnSave" class="btn btn-danger btn-lg" runat="server" Text="Save Changes"  
                   onclick="btnSave_Click" />                   
     </div>
</div></div><%--</div>--%></div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphScripts" runat="server">
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DGMasterPage.Master" AutoEventWireup="true" CodeBehind="UserRegistration.aspx.cs" Inherits="DGWebUI.WebUi.UserRegistration" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Styles/default.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div align = "center" = "Account">
<table align="center" width="600" cellpadding="1" cellspacing="0" border="1">
<tr>

<td align = "center">
<h1 class = "heading"> Sign up for New Account </h1>

    <asp:CreateUserWizard ID="CreateUserWizard1" runat="server" BackColor="#999966" 
        BorderColor="#003300" BorderStyle="Solid">
        <CreateUserButtonStyle BackColor="#003300" ForeColor="#CCCCCC" />
        <TextBoxStyle Height="25px" Width="250px" />
        <WizardSteps>
            <asp:CreateUserWizardStep ID="CreateUserWizardStep1" runat="server">
            </asp:CreateUserWizardStep>
            <asp:CompleteWizardStep ID="CompleteWizardStep1" runat="server">
            </asp:CompleteWizardStep>
        </WizardSteps>
    </asp:CreateUserWizard>
</td>
</tr>
</table>
</div>
</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DG.WebUI
{
    public partial class DGReportsNew : System.Web.UI.Page
    {
        private const string CompleteList = "COMPLTE LIST";
        private const string CompleteStats = "COMPLETE STATS";
    
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
                {
                List<string> items = new List<string>();
                items.Add(CompleteList);
                items.Add(CompleteStats);
                rblReportCategories.DataSource = items;
                rblReportCategories.DataBind();

                }
          

        }
        protected void rblReportCategories_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (((System.Web.UI.WebControls.RadioButtonList)sender).SelectedValue)
            {
                case "COMPLTE LIST":
                    Response.Redirect("ViewSTIReports.aspx?reportid=PayBudgetReport&fyear=" + rdtFromdate);
                    break;
                case "COMPLETE STATS":
                    Response.Redirect("ViewSTIReports.aspx?reportid=PayBudget&fyear=" + rdtFromdate + "&cbno=" );
                    break;

            }

        }
    }
}
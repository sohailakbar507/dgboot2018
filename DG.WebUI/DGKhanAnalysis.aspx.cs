﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DG.WebUI
{
    public partial class DGKhanAnalysis : System.Web.UI.Page
    {
        private const string DGKhanStatsOverall = "D.G Khan Stats Overall";
        private const string DGKhanStatsMPAWise = "D.G Khan Stats MPA Wise";
        private const string DGKhanDetailedList = "D.G Khan Detailed List";
        private const string DGKhanSMBDetails = "D.G Khan SMB Details";
        private const string DGKhanHEALTHSTATS = "DG KHAN DGHEALTH STATS";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Login"] == null)
                {
                    Session["LoginMessage"] = "You are not properly logged in.";
                    Response.Redirect("~/LoginPage.aspx");
                }
                List<string> items = new List<string>();
                items.Add(DGKhanStatsOverall);
                items.Add(DGKhanStatsMPAWise);
                items.Add(DGKhanDetailedList);
                items.Add(DGKhanSMBDetails);
                items.Add(DGKhanHEALTHSTATS);
                rblReportCategories.DataSource = items;
                rblReportCategories.DataBind();
                RadDatePicker1.SelectedDate = DateTime.Now;
                RadDatePicker2.SelectedDate = DateTime.Now;
            }

        }
        protected void rblReportCategories_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (((System.Web.UI.WebControls.RadioButtonList)sender).SelectedValue)
            {
                case "D.G Khan Stats MPA Wise":
                    Response.Redirect("STIReportsView.aspx?reportid=DGKhan_StatsMPAWise");
                    break;
                case "D.G Khan Stats Overall":
                    Response.Redirect("STIReportsView.aspx?reportid=DGKhanStatsOverall");
                    break;
                case "D.G Khan Detailed List":
                    Response.Redirect("STIReportsView.aspx?reportid=DGKhanDetailedList&mdate1=" + RadDatePicker1.DateInput.SelectedDate + "&mdate2=" + RadDatePicker2.DateInput.SelectedDate);
                    break;
                case "D.G Khan SMB Details":
                    Response.Redirect("STIReportsView.aspx?reportid=DGKhanSMBDetails");
                    break;

                case "DG KHAN DGHEALTH STATS":
                    Response.Redirect("STIReportsView.aspx?reportid=DGKhanHEALTHSTATS");
                    break;

            }
        }
    }
}
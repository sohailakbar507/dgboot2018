﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DGModel;
using Microsoft.Reporting.WebForms;

namespace DG.WebUI.ReportsPages
{
    public partial class ReportMainPage : System.Web.UI.Page
    {
        public object ConnectionString { get; private set; }

        protected void Page_Load(object sender, EventArgs e)
        {
                     
            if (!IsPostBack)
            {                
               LoadReport();
                
            }
        }

        private void LoadReport()
        {
            DGEntities dg = new DGEntities();
            string rptid = Request.QueryString["reportid"];
            object repo = null;
            bool access = true;
            string report_path = "";
            Cls_Login loggedInUser = (Cls_Login)Session["Login"];
            var getrptid = (from a in dg.ReportsTables where a.ReportName == rptid select a).First();
            var getrptacess = from a in dg.ReportAccesses where a.UserId == loggedInUser.Userid && a.ReportId == getrptid.ReportId  select a;

            if (getrptacess.Count() > 0)
            {
                var accessssssss = getrptacess.First();
                if (accessssssss.Access == true)
                {
                    access = true;
                }
                else
                {
                    access = false;
                }
            }
            else
            {
                
            }

            if (loggedInUser == null)
            {
                Session["LoginMessage"] = "You are not properly logged in.";
                Response.Redirect("../LoginPage.aspx");
                return;
            }

            if (rptid == "MpaWise")             //1
            {
                repo = dg.dgrepo();
                report_path = Server.MapPath("~/Reports/fampa.rdlc");
            }

            if (rptid == "MPAWiseReport2")      //2
            {
                repo = dg.MPAREPORT2();
                report_path = Server.MapPath("~/Reports/fampaf.rdlc");
            }

            if (rptid == "Letter")              //3
            {
                int fromfano = Convert.ToInt32(Request.QueryString["FromFANo"], null);
                int Tofano = Convert.ToInt32(Request.QueryString["ToFANo"], null);
                repo = dg.LETTER(fromfano, Tofano);
                report_path = Server.MapPath("~/Reports/letter1.rdlc");
            }

            if (rptid == "MadamLetter")              //3
            {
                int fromfano = Convert.ToInt32(Request.QueryString["FromFANo"], null);
                int Tofano = Convert.ToInt32(Request.QueryString["ToFANo"], null);
                repo = dg.LETTER(fromfano, Tofano);
                report_path = Server.MapPath("~/Reports/letter1.rdlc");
            }
            if (rptid == "GRANTINAID")          //4
            {
                int fromfano = Convert.ToInt32(Request.QueryString["FromFANo"], null);
                int Tofano = Convert.ToInt32(Request.QueryString["ToFANo"], null);
                repo = dg.GRANTINAID(fromfano, Tofano);
                report_path = Server.MapPath("~/Reports/letter4GrantinAid.rdlc");
            }

            if (rptid == "Letter6treasury")     //5
            {
                int fano = Convert.ToInt32(Request.QueryString["FANos"], null);
                string mfileno = Request.QueryString["mfileno"];
                repo = dg.TREASURY11(fano, mfileno);
                report_path = Server.MapPath("~/Reports/Letter6treasury.rdlc");
            }

            if (rptid == "ForHospital")         //7
            {
                int fromfano = Convert.ToInt32(Request.QueryString["FromFANo"], null);
                int Tofano = Convert.ToInt32(Request.QueryString["ToFANo"], null);
                repo = dg.HOSPITAL(fromfano, Tofano);
                report_path = Server.MapPath("~/Reports/letter3Hospital.rdlc");
            }

            if (rptid == "Letter12")            //8
            {
                int fromfano = Convert.ToInt32(Request.QueryString["FromFANo"], null);
                int Tofano = Convert.ToInt32(Request.QueryString["ToFANo"], null);
                repo = dg.LETTER12(fromfano, Tofano);
                report_path = Server.MapPath("~/Reports/letter12.rdlc");
            }

            if (rptid == "LetterforTreasury")   //9   
            {
                int fromfano = Convert.ToInt32(Request.QueryString["FromFANo"], null);
                int Tofano = Convert.ToInt32(Request.QueryString["ToFANo"], null);
                DateTime startDate = Convert.ToDateTime(Request["StartDate"]);
                DateTime endDate = Convert.ToDateTime(Request["EndDate"]);
                string mfileno = Request.QueryString["mfileno"];
                repo = dg.LETTER2TREASURY(fromfano, Tofano, mfileno, startDate, endDate);
                report_path = Server.MapPath("~/Reports/Lettertotreasury.rdlc");
            }

           
            if (rptid == "Note7")               //12
            {
                int fromfano = Convert.ToInt32(Request.QueryString["FromFANo"], null);
                int Tofano = Convert.ToInt32(Request.QueryString["ToFANo"], null);
                repo = dg.NOTE7(fromfano, Tofano);
                report_path = Server.MapPath("~/Reports/Note7.rdlc");
            }

            if (rptid == "Note7all")            //13 
            {
                DateTime startDate = Convert.ToDateTime(Request["StartDate"]);
                DateTime endDate = Convert.ToDateTime(Request["EndDate"]);
                repo = dg.MULTIPLENOTINGREVAL(startDate, endDate);
                report_path = Server.MapPath("~/Reports/Note7all.rdlc");
            }

            if (rptid == "RevalAdviceSingle")   //14
            {
                int fromfano = Convert.ToInt32(Request.QueryString["FromFANo"], null);
                repo = dg.REVALADVICE(fromfano);
                report_path = Server.MapPath("~/Reports/RevalAdviceSingle.rdlc");
            }

            if (rptid == "RevalAdviceMulti")    //15
            {
                DateTime startDate = Convert.ToDateTime(Request["StartDate"]);
                DateTime endDate = Convert.ToDateTime(Request["EndDate"]);
                repo = dg.REVALADVICEMULTI(startDate, endDate);
                report_path = Server.MapPath("~/Reports/RevalAdviceMulti.rdlc");
            }

            if (rptid == "Note17toASA")         //16
            {
                int fromfano = Convert.ToInt32(Request.QueryString["FromFANo"], null);
                int Tofano = Convert.ToInt32(Request.QueryString["ToFANo"], null);
                DateTime startDate = Convert.ToDateTime(Request["StartDate"]);
                DateTime endDate = Convert.ToDateTime(Request["EndDate"]);
                repo = dg.NOTE17TOASA(fromfano, Tofano, startDate, endDate);
                report_path = Server.MapPath("~/Reports/note17toasa.rdlc");
            }

            if (rptid == "Note18toASA")         //17
            {
                int fromfano = Convert.ToInt32(Request.QueryString["FromFANo"], null);
                int Tofano = Convert.ToInt32(Request.QueryString["ToFANo"], null);
                DateTime startDate = Convert.ToDateTime(Request["StartDate"]);
                DateTime endDate = Convert.ToDateTime(Request["EndDate"]);
                repo = dg.NOTE18TOASA(fromfano, Tofano, startDate, endDate);
                report_path = Server.MapPath("~/Reports/Note18toasa.rdlc");
            }

            if (rptid == "Note19toASA")         //18
            {
                DateTime startDate = Convert.ToDateTime(Request["StartDate"]);
                DateTime endDate = Convert.ToDateTime(Request["EndDate"]);
                int fromfano = Convert.ToInt32(Request.QueryString["FromFANo"], null);
                int Tofano = Convert.ToInt32(Request.QueryString["ToFANo"], null);
                repo = dg.NOTE19TOASA(fromfano, Tofano, startDate, endDate);
                report_path = Server.MapPath("~/Reports/Note19toasa.rdlc");
            }

            if (rptid == "Note21toASA")         //19
            {
                int fromfano = Convert.ToInt32(Request.QueryString["FromFANo"], null);
                int Tofano = Convert.ToInt32(Request.QueryString["ToFANo"], null);
                repo = dg.NOTE21TOASA(fromfano, Tofano);
                report_path = Server.MapPath("~/Reports/Note21toasa.rdlc");
            }

            if (rptid == "NotetoSCM12")         //20
            {
                int fromfano = Convert.ToInt32(Request.QueryString["FromFANo"], null);
                int Tofano = Convert.ToInt32(Request.QueryString["ToFANo"], null);
                repo = dg.NOTETOSCM12(fromfano, Tofano);
                report_path = Server.MapPath("~/Reports/Notetoscmbyasa12.rdlc");
            }

            if (rptid == "DGReport")            //21
            {
                repo = dg.DGReport();
                report_path = Server.MapPath("~/Reports/Report1DG.rdlc");
            }

            if (rptid == "YearWise")            //22
            {
                repo = dg.FYEAR();
                report_path = Server.MapPath("~/Reports/Analyear.rdlc");
            }

            if (rptid == "YearWiseDG")          //23
            {
                repo = dg.YEARWISEDG();
                report_path = Server.MapPath("~/Reports/YearWiseDG.rdlc");
            }

            if (rptid == "LettertoDCO")         //24
            {
                DateTime startDate = Convert.ToDateTime(Request["StartDate"]);
                DateTime endDate = Convert.ToDateTime(Request["EndDate"]);
                int letcode = Convert.ToInt32(Request.QueryString["LetCode"], null);
                //repo = dg.DCOLETTER(startDate, endDate, letcode);
                report_path = Server.MapPath("~/Reports/LetterDCO.rdlc");
            }

            if (rptid == "LetterDCOSingle")     //25
            {
                int fano = Convert.ToInt32(Request.QueryString["FANo"], null);
                int mfileno = Convert.ToInt32(Request.QueryString["mFileNo"], null);
                repo = dg.DCOLETTERSINGLE15(fano, mfileno);
                report_path = Server.MapPath("~/Reports/letterdcoSingle.rdlc");
            }

            if (rptid == "BaitLetter")          //26
            {
                
                DateTime startDate = Convert.ToDateTime(Request["StartDate"]);
                DateTime endDate = Convert.ToDateTime(Request["EndDate"]);
                repo = dg.BAIT(startDate, endDate);
                report_path = Server.MapPath("~/Reports/baitletter1.rdlc");
            }

            if (rptid == "DGAnalysis1")         //27
            {
                DateTime startDate = Convert.ToDateTime(Request["StartDate"]);
                DateTime endDate = Convert.ToDateTime(Request["EndDate"]);
                repo = dg.DGANALYSIS(startDate, endDate);
                report_path = Server.MapPath("~/Reports/DGAnalysis1.rdlc");
            }

            if (rptid == "TimeLimitCase")       //28
            {
                repo = dg.TARGETCASES();
                report_path = Server.MapPath("~/Reports/TimeLimitCase.rdlc");
            }
            if (rptid == "TargetCasesComplete")   //29
            {
                repo = dg.TARGETCASESCOMPLETE();
                report_path = Server.MapPath("~/Reports/TimeLimitCaseComplete.rdlc");
            }

            if (rptid == "DGAnalysis")        //30
            {
                //DateTime startDate = Convert.ToDateTime(Request["StartDate"]);
                //DateTime endDate = Convert.ToDateTime(Request["EndDate"]);
                //access = loggedInUser.IsAnalYearReport;
                repo = dg.DG_ANALYEAR();
                report_path = Server.MapPath("~/Reports/DG_Analyear1.rdlc");
            }

            if (rptid == "DGAnalysis2")         //31
            {
                repo = dg.DG_ANALYEAR2();
                report_path = Server.MapPath("~/Reports/DG_Analyear.rdlc");
            }
            if (rptid == "MPAWISEREPO")
            {
                int catcode = Convert.ToInt32(Request.QueryString["CatCode"], null);
                int cons = Convert.ToInt32(Request.QueryString["Cons"], null);
                repo = dg.MPAREPO(catcode, cons);
                report_path = Server.MapPath("~/Reports/MPAWISEREPO.RDLC");
            }
            
            if (rptid == "SenttoHospital")
            {
                string hosp = (Request.QueryString["Hosp"]);
                repo = dg.Senttohosp(hosp);
                report_path = Server.MapPath("~/Reports/SenttoHospital.rdlc");

            }
            if (rptid == "MedicalBoard")
            {
                //string medical = (Request.QueryString["Medical"]);
                int fromfano = Convert.ToInt32(Request.QueryString["FromFANo"], null);
                int Tofano = Convert.ToInt32(Request.QueryString["ToFANo"], null);
                repo = dg.MedicalBoard(fromfano, Tofano);
                report_path = Server.MapPath("~/Reports/MedicalBoard.rdlc");

            }

            if (rptid == "MeetingReport")
            {
                DateTime startDate = Convert.ToDateTime(Request["StartDate"]);
                //string medical = (Request.QueryString["Medical"]);
                repo = dg.Meeting(startDate).ToList();
                report_path = Server.MapPath("~/Reports/MeetingReportFinal.rdlc");

            }


            if (rptid == "Receipt")
            {
                int fromfano = Convert.ToInt32(Request.QueryString["FromFANo"], null);
                repo = dg.RECEIPT(fromfano);
                report_path = Server.MapPath("~/Reports/ReceiptNew.rdlc");
            }
            if (rptid == "ChqReceipt")
            {
                DateTime startDate = Convert.ToDateTime(Request["StartDate"]);
                DateTime endDate = Convert.ToDateTime(Request["EndDate"]);
                repo = (dg.RECEIPTCHQDATEWISE(startDate, endDate)).ToList();
                report_path = Server.MapPath("~/Reports/ChqReceipt.rdlc");
            }
            
            if (access == true)
            {
                ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
                ReportViewer1.LocalReport.ReportPath = report_path;
                ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DataSet1", repo));
                ReportViewer1.LocalReport.Refresh();
            }
            else
            {
                Session["LoginMessage"] = "You are not authorized for this report. Please contact to Administrator";
                Response.Redirect("../LoginPage.aspx");
            }
        }

    }
}
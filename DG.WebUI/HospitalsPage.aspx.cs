﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DG.WebUI
{
    public partial class HospitalsPage : System.Web.UI.Page
    {
        private const string HospitalsStatsOverall = "STATS OVERALL WITH DETAIL";
        private const string HospitalStatsCurrent = "STATS THIS TENURE WITH DETAIL";
        private const string FinancialPosition = "FINANCIAL POSITION";
        private const string SearchForm = "SEARCHING FORM";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                RadDatePicker1.Enabled = false;
                RadDatePicker2.Enabled = false;
                if (Session["Login"] == null)
                {
                    Session["LoginMessage"] = "You are Not Properly Logged in:  ";
                    Response.Redirect("~/LoginPage.aspx");
                }
                List<string> items = new List<string>();
                items.Add(HospitalsStatsOverall);
                items.Add(HospitalStatsCurrent);
                items.Add(FinancialPosition);
                items.Add(SearchForm);
                rblReportCategories.DataSource = items;
                rblReportCategories.DataBind();
                RadDatePicker1.SelectedDate = DateTime.Now;
                RadDatePicker2.SelectedDate = DateTime.Now;
            }

        }

        protected void rblReportCategories_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch(((System.Web.UI.WebControls.RadioButtonList)sender).SelectedValue)
            {
                case "STATS OVERALL WITH DETAIL":
                    Response.Redirect("HospitalsStatsPage.aspx?reportid=HospitalsStatsOverall");
                break;

                case "STATS THIS TENURE WITH DETAIL":
                    Response.Redirect("HospitalsStatsPage.aspx?reportid=HospitalStatsCurrent");
                break;
                case "FINANCIAL POSITION":
                    Response.Redirect("HospitalsStatsPage.aspx?reportid=FinancialPosition");
                break;
                case "SEARCHING FORM":
                    Response.Redirect("HospitalSearchingForm.aspx");
                    break;

            }
        }
    }
}
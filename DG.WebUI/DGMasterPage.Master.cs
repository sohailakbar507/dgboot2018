﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DGModel;

namespace DG.WebUI
{
    public partial class DGMasterPage : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["login"] != null)
            {
                Cls_Login val = (Cls_Login)Session["login"];
                //lblLogin.Text ="Welcome Mr."+ val.Username;
                if (val.Groupid == 8)
                {
                    Response.Redirect("DCOWebForm.aspx");
                }
            }
            else
            {
                Response.Redirect("loginPage.aspx");
            }
        }

        protected void btnLogout_Click1(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("loginPage.aspx");
        }

        protected void lbllogout_Click(object sender, EventArgs e)
        {
            Session["Login"] = null;
            Response.Redirect("loginPage.aspx");
        }
    }
}
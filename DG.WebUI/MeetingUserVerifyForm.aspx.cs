﻿using DGModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DG.WebUI
{
    public partial class MeetingUserVerifyForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            //http://localhost:1443/meetinguserverifyform.aspx?user=CMPUNJAB\asfs&type=officer
            verfyLoginUser(Request["user"],Request["type"]);
        }


        private void verfyLoginUser(string user, string type)
        {
            cmsadbEntities ent = new cmsadbEntities();
            if (user == null)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Alert", "alert('You are not authorized here ..');", true);
                Response.Redirect("/dgpublish/LoginPage.aspx");
            }
            

            if (type != "officer" && type != "district")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Alert", "alert('You are not authorized here ..');", true);
                Response.Redirect("/LoginPage.aspx");
            }
            else
            {
                Cls_Login login = new Cls_Login();
                login.OfficerName = user;
                login.Type = type;
                Session["officer"] = login;
                Response.Redirect("/dgpublish/Dev_MeetingUpdateForm.aspx");
            }


        }
    }
}
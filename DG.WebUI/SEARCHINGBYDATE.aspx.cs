﻿using DGModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DG.WebUI
{
    public partial class SEARCHINGBYDATE1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           
            if(!IsPostBack)
            {
                LoadGrid();
            }

        }
        private void LoadGrid()
        {
            DGEntities dgent = new DGEntities();

            var dglist = (from a in dgent.MAINCORRs
                          from b in dgent.officers
                          from d in dgent.DEPTs
                          from s in dgent.status
                          where a.offcode == b.OffCode && a.deptcode == d.deptcode && a.statcode == s.StatusCode
                          orderby a.fano
                          select new { a.DGId, a.fano, a.refdate, a.appname, a.address, a.subject, a.cnic, a.chqno, a.chqdate, a.amount, b.Name, d.deptname, s.Description }).ToList();

            DgSearchBYDATE.DataSource = dglist;
            DgSearchBYDATE.DataBind();
        }     
        protected void DgSearch_RowCommand(object sender, GridViewCommandEventArgs e)
        {
        }
        protected void DgSearch_PreRender(object sender, EventArgs e)
        {
            LoadGrid();
            if (DgSearchBYDATE.Rows.Count > 0)
            {
                DgSearchBYDATE.UseAccessibleHeader = true;
                DgSearchBYDATE.HeaderRow.TableSection = TableRowSection.TableHeader;
                DgSearchBYDATE.FooterRow.TableSection = TableRowSection.TableFooter;
            }
        }
        protected void DgSearch_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {

        }

        protected void lnkBtnSearch_Click(object sender, EventArgs e)
        {

        }
    }
}
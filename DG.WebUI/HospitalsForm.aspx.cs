﻿using DGModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DG.WebUI
{
    public partial class HospitalsForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Login"] == null)
                {
                    Response.Redirect("~/LoginPage.aspx");
                }
                string username = null;
                int groupid = 0;
                GetRecord(username,groupid);
                UploadCat();
                UploadSentto();
                UploadStatus();
                NoEdit();
                this.ddlRefferedTo.Items.Insert(0, new ListItem("Select Hospital"));
                this.ddlStatus.Items.Insert(0, new ListItem("Select Status"));
                this.DDLSubCat.Items.Insert(0, new ListItem("Select Disease"));
            }
        }
        private void UploadCat()
        {
            DGEntities dg = new DGEntities();
            var cat = (from b in dg.Subcats select b).ToList();
            DDLSubCat.DataSource = cat;
            DDLSubCat.DataValueField = "subcatid";
            DDLSubCat.DataTextField = "SubCategory";
            DDLSubCat.DataBind();
        }

        private void NoEdit()
        {
            txtFano.Enabled = false;
            txtAppname.Enabled = false;
            txtAddress.Enabled = false;
            txtPageNo.Enabled = false;
            txtLetterNo.Enabled = false;
            dtRefferedDate.Enabled = false;
            dtLetterDate.Enabled = false;
            txtBalReturned.Enabled = false;
            rdtUpdatedOn.Enabled = false;
        }

        private void Editable()
        {
            txtPageNo.Enabled = true;
            txtLetterNo.Enabled = true;
            dtRefferedDate.Enabled = true;
            dtLetterDate.Enabled = true;

        }
        private void UploadStatus()
        {
            DGEntities dg = new DGEntities();
            var stat = (from b in dg.PatientStatus select b).ToList();
            ddlStatus.DataSource = stat;
            ddlStatus.DataValueField = "PtID";
            ddlStatus.DataTextField = "Description";
            //ddlStatus.SelectedIndex = 0;
            ddlStatus.DataBind();
        }

        private void UploadSentto()
        {
            DGEntities dg = new DGEntities();
            var sentto = (from a in dg.Reffereds select a).ToList();
            ddlRefferedTo.DataSource = sentto;
            ddlRefferedTo.DataValueField = "RefId";
            ddlRefferedTo.DataTextField = "Refto";
            ddlRefferedTo.DataBind();
        }

        protected void btnFind_Click(object sender, EventArgs e)
        {
        }

        private void ClearFields()
        {
            txtPageNo.Text = "";
            txtLetterNo.Text = "";
            dtRefferedDate.SelectedDate = null;
            dtLetterDate.SelectedDate = null;
            rdtUpdatedOn.SelectedDate = null;

        }

        private void GetRecord(string username, int groupid)
        {
            try
            {
                DGEntities dg = new DGEntities();
                int fanos = Convert.ToInt32(Request["fanoHealth"]);
                // int fanos = fano;
                var filename = from f in dg.tblUploadFiles where f.FaNo == fanos select f;
                var getfno = from h in dg.Healths where h.fanoHealth == fanos select h;
                var maininfo2 = (from m in dg.MAINCORRs where m.fano == fanos select m.DGId).Max();
                var maininfo1 = (from m in dg.MAINCORRs where m.DGId == maininfo2 select m);
                if (maininfo1.Count() > 0)
                {
                    var healinfo = getfno.First();
                    var maininfo = maininfo1.First();
                    txtFano.Text = Convert.ToInt32(maininfo.fano).ToString();
                    txtAppname.Text = maininfo.appname;
                    txtAddress.Text = maininfo.address;
                    txtPatientName.Text = maininfo.PatientName;
                    ddlRefferedTo.SelectedIndex = ddlRefferedTo.Items.IndexOf(ddlRefferedTo.Items.FindByValue(healinfo.refid.ToString()));
                    DDLSubCat.SelectedIndex = DDLSubCat.Items.IndexOf(DDLSubCat.Items.FindByValue(maininfo.subcatid.ToString()));
                  //  // txtThischqAmount.Text = Convert.ToInt32(maininfo.amount).ToString();
                  //  txtSancAmount.Text = Convert.ToInt32(healinfo.HealthAmount).ToString();
                  //  txtBalReturned.Text = Convert.ToInt32(maininfo.Balance).ToString();
                  //  txtAmount.Text = Convert.ToInt32(maininfo.AmountIncured).ToString();
                  //  txtAuthorityNo.Text = maininfo.AuthorityNumber;
                  //  rdtAuthorityDate.SelectedDate = maininfo.Authority_Date;
                  //  txtFDSancNo.Text = maininfo.SancNo;
                  //  rdtFDSancDate.SelectedDate = maininfo.Sancdate;
                  ////  txtRefunded.Text = Convert.ToInt32(maininfo.refunded).ToString();
                  //  txtRecvdChqno.Text = maininfo.chqno;
                  //  rdRecvdChqDate.SelectedDate = maininfo.chqdate;
                }
                if (getfno.Count() > 0)
                {
                    var healinfo = getfno.First();
                    var maininfo = (from m in dg.MAINCORRs where m.fano == healinfo.fanoHealth select m).First();
                    txtPageNo.Text = Convert.ToInt32(healinfo.pagenoHealth).ToString();                   
                    txtPatientName.Text = maininfo.PatientName;
                    txtFDSancNo.Text = healinfo.fdSactionNo;
                    rdtFDSancDate.SelectedDate = healinfo.fdSanctionDate;
                    txtAuthorityNo.Text = healinfo.AuthorityNo;
                    rdtAuthorityDate.SelectedDate = healinfo.AuthorityDate;
                    txtSancAmount.Text = Convert.ToInt32(healinfo.HealthAmount).ToString();
                    txtFileNumber.Text = healinfo.FileNumber;
                    txtRecvdChqno.Text = healinfo.RecvdChqno;
                    rdRecvdChqDate.SelectedDate = healinfo.RecvdChqDate;
                    txtAmount.Text = Convert.ToInt32(healinfo.AmountInc).ToString();
                    rdtUpdatedOn.SelectedDate = healinfo.UpdatedOn;
                    txtBalReturned.Text = Convert.ToInt32(healinfo.BalReturn).ToString();                    
                    txtBalChqNo.Text = healinfo.BalChqno;
                    rdBalChqDate.SelectedDate = healinfo.BalChqDate;
                    rdtVouchedDate.SelectedDate = healinfo.VouchedDate;
                    rdtLiquidDate.SelectedDate = healinfo.LiquidationDate;
                    txtRemarks.Text = healinfo.HealthRemarks;                   
                    ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByValue(healinfo.PtStatusID.ToString()));
                    if(filename.Count()>1)
                    {
                        var fname = filename.First();
                        txtfilename.Text = fname.FileName;
                    }
                    

                }
                else
                    {
                        //string message = "Record Not Found....";
                        ScriptManager.RegisterStartupScript(this, GetType(), "Alert", "alert('Record Not found..');", true);
                        ClearFields();
                    }
               
            }
            catch (Exception ex) { }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
        }
        protected void lnkBtnSearch_Click(object sender, EventArgs e)
        {
            System.Threading.Thread.Sleep(1000);
            Cls_Login login = (Cls_Login)Session["Login"];
            GetRecord(login.Username, login.Healthaccessid);
        }

        protected void lnkbtnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("HospitalsUpdateForm.aspx");

        }

        protected void lnkbtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("LoginPage.aspx");

        }

        protected void lnkbtnSave_Click(object sender, EventArgs e)
        {
            
                DGEntities dg = new DGEntities();
                if (FileReceiptUpload.HasFile)
                {
                    foreach (HttpPostedFile file in FileReceiptUpload.PostedFiles)
                    {

                        file.SaveAs(Server.MapPath(@"~/UploadFiles/" + txtFano.Text + file.FileName));
                        tblUploadFile file1 = new tblUploadFile();
                        file1.FaNo = Convert.ToInt32(txtFano.Text);
                        file1.FileName = txtFano.Text + file.FileName;
                        file1.FilePath = "UploadFiles/" + txtFano.Text + file.FileName;
                        dg.tblUploadFiles.Add(file1);
                        dg.SaveChanges();
                        txtfilename.Text = file1.FileName;
                    }
                }

            try
            {               
                System.Threading.Thread.Sleep(500);
                int fanos = Convert.ToInt32(txtFano.Text);
                var getfano = from a in dg.MAINCORRs where a.fano == fanos select a;
                var getfanomax = (from a in dg.MAINCORRs where a.fano == fanos select a.DGId).Max();
                var m = (from a in dg.MAINCORRs where a.DGId == getfanomax select a).First();
                int hfano = Convert.ToInt32(txtFano.Text);
                var getfno = from h in dg.Healths where h.fanoHealth == hfano select h;
                if (getfano.Count() > 0)
                {
                    if (getfno.Count() > 0)
                    {
                        var h = getfno.First();
                        h.fanoHealth = fanos;
                        if (string.IsNullOrEmpty(txtPageNo.Text))
                        {
                            txtPageNo.Text = "0";
                        }

                        m.pageno = Convert.ToInt32(txtPageNo.Text);
                        h.pagenoHealth = Convert.ToInt32(txtPageNo.Text);

                        if (ddlRefferedTo.SelectedItem.Value == "Select Hospital")
                        {
                            h.refid = null;
                        }
                        else
                        {
                            h.refid = Convert.ToInt32(ddlRefferedTo.SelectedItem.Value);
                        }

                        if (ddlStatus.SelectedItem.Value == "Select Status")
                        {
                            h.PtStatusID = null;
                        }
                        else
                        {
                            h.PtStatusID = Convert.ToInt32(ddlStatus.SelectedItem.Value);
                        }
                        h.AmountInc = txtAmount.Text == string.Empty ? 0 : Convert.ToInt32(txtAmount.Text);
                        h.HealthAmount = txtSancAmount.Text == string.Empty ? 0 : Convert.ToInt32(txtSancAmount.Text);
                       // h.AmountRefunded = txtRefunded.Text == string.Empty ? 0 : Convert.ToInt32(txtRefunded.Text);
                        h.UpdatedOn = DateTime.Now;
                        h.fdSactionNo = txtFDSancNo.Text;
                        h.PatientName = txtPatientName.Text;
                        h.fdSanctionDate = rdtFDSancDate.DateInput.SelectedDate;
                        h.VouchedDate = rdtVouchedDate.DateInput.SelectedDate;
                        h.LiquidationDate = rdtLiquidDate.DateInput.SelectedDate;
                        h.AuthorityNo = txtAuthorityNo.Text;
                        h.AuthorityDate = rdtAuthorityDate.DateInput.SelectedDate;
                        h.FileNumber = txtFileNumber.Text;
                        h.HealthRemarks = txtRemarks.Text;
                        h.HealthRefdate = m.refdate;
                      //  h.ThisChqAmount = txtThischqAmount.Text == string.Empty ? 0 : Convert.ToInt32(txtThischqAmount.Text);
                        h.RecvdChqno = txtRecvdChqno.Text;
                        h.RecvdChqDate = rdRecvdChqDate.DateInput.SelectedDate;
                      //  m.amount = txtThischqAmount.Text == string.Empty ? 0 : Convert.ToInt32(txtThischqAmount.Text);
                        m.AmountIncured = txtAmount.Text == string.Empty ? 0 : Convert.ToInt32(txtAmount.Text);
                        int amnt = txtSancAmount.Text == string.Empty ? 0 : Convert.ToInt32(txtSancAmount.Text);
                        int expenses = txtAmount.Text == string.Empty ? 0 : Convert.ToInt32(txtAmount.Text);
                        m.Balance = amnt - expenses;
                        // m.Balance = Convert.ToInt32(txtSancAmount.Text) - Convert.ToInt32(txtAmount.Text);
                        m.AuthorityNumber = txtAuthorityNo.Text;
                        m.Authority_Date = rdtAuthorityDate.DateInput.SelectedDate;
                        m.SancNo = txtFDSancNo.Text;
                        m.Sancdate = rdtFDSancDate.DateInput.SelectedDate;
                        m.chqno = txtRecvdChqno.Text;
                        m.chqdate = rdRecvdChqDate.SelectedDate;

                        if (string.IsNullOrEmpty(txtSancAmount.Text))
                        {
                            txtSancAmount.Text = "0";
                        }
                        if (string.IsNullOrEmpty(txtAmount.Text))
                        {
                            txtAmount.Text = "0";
                        }

                        {
                           // h.BalReturn = Convert.ToInt32(txtThischqAmount.Text) - Convert.ToInt32(txtAmount.Text);
                        }
                        //h.BalReturn = txtBalReturned.Text == string.Empty ? 0 : Convert.ToInt32(txtBalReturned.Text);
                        h.BalChqno = txtBalChqNo.Text;
                        h.BalChqDate = rdBalChqDate.DateInput.SelectedDate;
                        dg.SaveChanges();
                        Cls_Login login = (Cls_Login)Session["Login"];
                        GetRecord(login.Username, login.Groupid);
                        ScriptManager.RegisterStartupScript(this, GetType(), "Alert", "alert('Record Saved Successfully ..');", true);
                    }
                }
            }

            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Alert", "alert('" + ex.Message.ToString() + "');", true);
            }
        }
        protected void btnChqUpdate_Click(object sender, EventArgs e)
        {
            if (txtFano.Text != "")
            {
                int fano = Convert.ToInt32(txtFano.Text);
                Response.Redirect("UpdateDGHealthChequeStatus.aspx?fano=" + fano);
            }
        }
      
    }
}

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DGMain.Master" AutoEventWireup="true" CodeBehind="DGReportsNew.aspx.cs" Inherits="DG.WebUI.DGReportsNew" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
     <div style=" position:relative;left:200px; top:50px">
<table>
    <tr>
        <td>
            
        </td>
    </tr>
</table></div>
    <div style="width: 100%; height: 100%;">
    <div style="background-color: #f7f7f9; border: 1px solid #e1e1e8; width:100%; height: 700px; position:relative; left: 0px;top: 70px;">
        
        <div class="row">
        
    <div class="col-md-12" style="width:650px;">
                <div class="row">
            <div class="col-md-12">
                <h1> Budget Reports </h1>
            </div>
        </div>
               <div class="row">
                     <div class="col-md-12">
                         <asp:Label ID="lblFromDate" runat="server" Text="From Date : "></asp:Label>
                    <telerik:RadDateTimePicker ID="rdtFromdate" runat="server" Culture="en-US" CssClass="form-control">
                    <TimeView CellSpacing="-1">
                    </TimeView>
                    <TimePopupButton HoverImageUrl="" ImageUrl="" />
                    <Calendar EnableWeekends="True" runat="server" FastNavigationNextText="&amp;lt;&amp;lt;" UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False">
                    </Calendar>
                    <DateInput DateFormat="dd-MM-yyyy" DisplayDateFormat="dd-MM-yyyy" runat="server" LabelWidth="40%">
                        <EmptyMessageStyle Resize="None" />
                        <ReadOnlyStyle Resize="None" />
                        <FocusedStyle Resize="None" />
                        <DisabledStyle Resize="None" />
                        <InvalidStyle Resize="None" />
                        <HoveredStyle Resize="None" />
                        <EnabledStyle Resize="None" />
                    </DateInput>
                    <DatePopupButton HoverImageUrl="" ImageUrl="" />
                </telerik:RadDateTimePicker>
                    </div>
                </div>
                 <div class="row">
                     <div class="col-md-12">
                         <asp:Label ID="lblToDate" Width="75px" runat="server" Text="To Date : "></asp:Label>
                        <telerik:RadDateTimePicker ID="rdtTodate" runat="server" Culture="en-US" CssClass="form-control">
                    <TimeView CellSpacing="-1">
                    </TimeView>
                    <TimePopupButton HoverImageUrl="" ImageUrl="" />
                    <Calendar EnableWeekends="True" runat="server" FastNavigationNextText="&amp;lt;&amp;lt;" UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False">
                    </Calendar>
                    <DateInput DateFormat="dd-MM-yyyy" DisplayDateFormat="dd-MM-yyyy" runat="server" LabelWidth="40%">
                        <EmptyMessageStyle Resize="None" />
                        <ReadOnlyStyle Resize="None" />
                        <FocusedStyle Resize="None" />
                        <DisabledStyle Resize="None" />
                        <InvalidStyle Resize="None" />
                        <HoveredStyle Resize="None" />
                        <EnabledStyle Resize="None" />
                    </DateInput>
                    <DatePopupButton HoverImageUrl="" ImageUrl="" />
                </telerik:RadDateTimePicker>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <asp:Label ID="lblError" runat="server" or="Red"></asp:Label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <asp:RadioButtonList ID="rblReportCategories" runat="server" 
                            CssClass="myTable table table-hover" 
                            onselectedindexchanged="rblReportCategories_SelectedIndexChanged" 
                            AutoPostBack="True">
                        </asp:RadioButtonList>
                    </div>
                </div>
            </div>
            </div>
                </div>
            </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CphScript" runat="server">
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HospitalsMaster.Master" AutoEventWireup="true" CodeBehind="HospitalSearchingForm.aspx.cs" Inherits="DG.WebUI.HospitalSearchingForm" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <table align="center" width="70%">
    <tr>
        <td align="right">
        <h1></h1>
        </td>
        <td align="center">
        <img src="Images/Hospital1.png" alt="" />
        </td>
    </tr>
    </table>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
            <asp:UpdateProgress runat="server" id="PageUpdateProgress">
            <ProgressTemplate>
            <div id="modal" runat="server" style =" position: fixed;
            z-index: 1000;
            height: 100%;
            width: 100%;
            top: 0;
            background-color: Black;
            filter: alpha(opacity=60);
            opacity: 0.6;
            -moz-opacity: 0.8; ">
                <div id="dvProgress" runat="server" style="position:fixed;  text-align:center; margin: 300px auto; padding: 10px; left:50%; z-index: 1001;" >
            <asp:Image ID="Image2" runat="server"  ImageUrl="Images/imgLoading.gif"  />
        </div> 
        </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
              <table width="75%" align="center">
              <tr><td align="left">
                 <div class="row">
                   <div class="col-lg-6">
    <div class="input-group">
    <asp:TextBox ID="txtfind" width="200px" Height="34px" runat="server"  CssClass="form-control" placeholder="Search for..."></asp:TextBox>
      <span class="input-group-btn" style="float: left;padding-left: 0px;">
         <asp:LinkButton ID="lnkBtnSearch" runat="server" ToolTip = "Search Button" CssClass="btn btn-danger btn-md" 
         onclick="lnkBtnSearch_Click" Width="100px">Search</asp:LinkButton>
      </span>
    </div>
  </div>
</div>  
</td></tr>         
      </table>     
    <table width = "75%" align = "center" cellpadding = "0" cellspacing = "0" style = "border:1px dashed white">
    <tr>
        <td colspan="4" align="left">
            <script type="text/javascript" language="javascript">
                Sys.Application.add_load(jScript);
            </script>
            <asp:Label ID="lblheading1" runat="server" Text="Patient Information" 
                BackColor="#000066" ForeColor="#FFFF66" Font-Size="X-Large"></asp:Label>
        </td>
    </tr>
    <tr>        
        <td align="left">
            <asp:Label ID="lblFANo" runat="server" Text="FANo" Font-Size="Medium" 
                ForeColor="#000066"></asp:Label></td>
        <td align="left">
            <asp:TextBox ID="txtFano" runat="server" Width = "250px"  CssClass="form-control" 
                BackColor="White" ForeColor="Black"></asp:TextBox> </td>
        <td align="left">
            <asp:Label ID="lblPageNo" runat="server" Text="Computer No" Font-Size="Medium" 
                ForeColor="#000066"></asp:Label></td>
        <td align="left" colspan="3">
            <asp:TextBox ID="txtPageNo" runat="server" Width = "250px" CssClass="form-control" 
                BackColor="White" ForeColor="Black"></asp:TextBox>
        </td>
    </tr>    
    <tr>
        <td align="left">
            <asp:Label ID="lblAppname" runat="server" Text="Applicant Name" Font-Size="Medium" ForeColor="#000066"></asp:Label></td>
        <td align="left">
            <asp:TextBox ID="txtAppname" runat="server" Width="250px" CssClass="form-control" BackColor="White"></asp:TextBox>
         </td>
         <td>
            <asp:Label ID="lblPatientName" runat="server" Text="Patient Name" Font-Size="Medium" ForeColor="#000066"></asp:Label></td>
        
        <td>
        <asp:TextBox ID="txtPatientName" runat="server" Width="250px" CssClass="form-control" BackColor="White"></asp:TextBox>
        </td>
    </tr>
       <tr>
        
        <td align="left">
            <asp:Label ID="lblAddress" runat="server" Text="Address" Font-Size="Medium" ForeColor="#000066"></asp:Label></td>
        <td align="left" colspan="3">
            <asp:TextBox ID="txtAddress" runat="server" Width = "710px" TextMode="MultiLine" Font-Names="Arial" BackColor="White" CssClass="form-control"></asp:TextBox>
        </td>
    </tr>
<%--    <%
        if (Session["Login"] != null)
        {
            DGModel.Cls_Login login = (DGModel.Cls_Login)Session["Login"];

            DGModel.DGEntities ent = new DGModel.DGEntities();
            var chkaccess = (from a in ent.User_Groups where a.ID == login.Groupid select a).First();

            if (chkaccess.HealthSection1 == true)
            {

           %>  --%>     
     
    <tr>
        <td align="left">
            <asp:Label ID="lblRefto" runat="server" Text = "Referred To Hospital" Font-Size="Medium" ForeColor="#000066"></asp:Label></td>
        <td align="left">
            <asp:DropDownList  CssClass="form-control" ID="ddlRefferedTo" runat="server" Width = "250px" BackColor="#6699FF" > </asp:DropDownList>
        </td>
         <td align="left">
            <asp:Label ID="lblDisease" runat="server" Text = "Disease" Font-Size="Medium" ForeColor="#000066"></asp:Label></td>
        <td align="left">
            <asp:DropDownList ID="DDLSubCat" runat="server" CssClass="form-control" Width="250px"> </asp:DropDownList>
        </td>              
    </tr>
    
     <tr>
          <td align="left">
            <asp:Label ID="lblMedicalBoard" runat="server" Text = "Medical Board Constituted At" Font-Size="Medium" ForeColor="#000066"></asp:Label></td>
        <td align="left">
            <asp:DropDownList  CssClass="form-control" ID="DDLTreating" runat="server" Width = "250px" BackColor="#009933" ForeColor="#FFFF99" >
            </asp:DropDownList>
        </td>
     
        <td>
        <asp:Label ID="lblStatus" runat="server" Text="Status" Font-Size="Medium" ForeColor="#000066"></asp:Label></td>
        
        <td align="left">
            <asp:DropDownList ID="ddlStatus" runat="server" Width = "250px" CssClass="form-control" BackColor="#FF7575"></asp:DropDownList>
        </td>
    </tr>
        <%--    <%}               
         } %>--%>
           <tr>
                    <td align = "center" colspan = "4"> <br />
                        
                  <asp:LinkButton ID="lnkbtnSave" Font-Size="Medium" runat="server" ToolTip = "Save" onclick="lnkbtnSave_Click" Height="40px" Width="100px" CssClass="btn btn-danger btn-lg" >Save</asp:LinkButton>
                  <asp:LinkButton ID="lnkbtnCancel" Font-Size="Medium" runat="server" ToolTip = "Cancel" Height="40px" Width="100px" onclick="lnkbtnCancel_Click" CssClass="btn btn-danger btn-lg">Cancel</asp:LinkButton>
                    </td>
                    </tr>
                    <caption>
                        <br />
        </caption>

    </table>
</div>
<br />
<br />
<br />

    </td>


    </td>
      </ContentTemplate>     
      </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphScripts" runat="server">
</asp:Content>

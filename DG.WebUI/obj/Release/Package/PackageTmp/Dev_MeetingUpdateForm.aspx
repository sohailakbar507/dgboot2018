﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HospitalsMaster.Master" AutoEventWireup="true" CodeBehind="Dev_MeetingUpdateForm.aspx.cs" Inherits="DG.WebUI.Dev_MeetingUpdateForm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
     <style type="text/css">
        .form-control
        {}
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:ScriptManager ID="ScriptManager" runat="server">
    </asp:ScriptManager>
    <br /> <br />  <br /> <br /> 
    <table align="center" width="70%">
  
    <tr>
         
        <td align="right">
        <h3>DCC MEETINGS UPDATE FORM </h3>
        </td>
       
        <td align="center">
        <img src="Images/Meeting3.png" alt="" />
        </td>
    </tr>
    </table>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
            <asp:UpdateProgress runat="server" id="PageUpdateProgress">
            <ProgressTemplate>
            <div id="modal" runat="server" style =" position: fixed;
            z-index: 1000;
            height: 100%;
            width: 100%;
            top: 0;
            background-color: Black;
            filter: alpha(opacity=60);
            opacity: 0.6;
            -moz-opacity: 0.8; ">
                <div id="dvProgress" runat="server" style="position:fixed;  text-align:center; margin: 300px auto; padding: 10px; left:50%; z-index: 1001;" >
            <asp:Image ID="Image2" runat="server"  ImageUrl="Images/imgLoading.gif"  />
        </div> 
        </div>
            </ProgressTemplate>
        </asp:UpdateProgress>

    <table width = "75%" align = "center" cellpadding = "0" cellspacing = "0" style = "border:1px dashed white">
    <tr>
        
        <td align="left">
            <asp:Label ID="Label1" runat="server" Text="District" Font-Size="Medium"
                ForeColor="#000066"></asp:Label></td>
         <td>
             <asp:DropDownList ID="ddldistricts" runat="server"></asp:DropDownList>
       </td>
    </tr>
        <tr>
        
        <td align="left">
            <asp:Label ID="lblMeetingId" runat="server" Text="Meeting Id" Font-Size="Medium"
                ForeColor="#000066"></asp:Label></td>
         <td>
           <asp:TextBox ID="txtMeetingId" runat="server" CssClass="form-control"></asp:TextBox>
       </td>
    </tr>
 
    <tr>
         <td align="left">
            <asp:Label ID="lblMeetingDate" runat="server" Text="Meeting Date" Font-Size="Medium" 
                ForeColor="#000066"></asp:Label></td>
        
        <td align="left">
            <telerik:RadDateTimePicker ID="dtMeetingDate" runat="server" Width="250px"  CssClass="form-control">
            </telerik:RadDateTimePicker>
        </td>
         <%--<td align="left">
            <asp:Label ID="lblUpdatedOn" runat="server" Text="Updated On" Font-Size="Medium" 
                ForeColor="#000066"></asp:Label></td>
        
        <td align="left">
            <telerik:RadDateTimePicker ID="rdtUpdatedOn" runat="server" Width="250px" 
                CssClass="form-control">
            </telerik:RadDateTimePicker>
        </td> --%>                      
    </tr>
            
   <tr>        

       <td align="left">
            <asp:Label ID="lblUpload" runat="server" Text="Upload File" AllowMultiple="true" Font-Size="Medium" 
                ForeColor="#000066"></asp:Label></td>
       <td>

           <asp:FileUpload ID="FileReceiptUpload" AllowMultiple="true" CssClass="form-control" runat="server" />
       </td>
    </tr>
       <tr>
           <td></td>
       <td>
           <asp:TextBox ID="txtfilename" runat="server" Height="50px" CssClass="form-control" Enabled="false"  Width = "350px" BackColor="#F6AD59" ForeColor="#CCFFFF"></asp:TextBox>
       </td>
   </tr> 

           <tr>
                    <td align = "center" colspan = "4">
                        <br />
                        
                  <asp:LinkButton ID="lnkbtnSave" Font-Size="Medium" runat="server" ToolTip = "Save" onclick="lnkbtnSave_Click" Height="40px" Width="100px" CssClass="btn btn-primary btn-lg" >Save</asp:LinkButton>
                        <asp:LinkButton ID="lnkbtnCancel" Font-Size="Medium" runat="server" ToolTip = "Cancel" Height="40px" Width="100px" onclick="lnkbtnCancel_Click" CssClass="btn btn-primary btn-lg">Cancel</asp:LinkButton>
                    </td>
                    </tr>
                    <caption>
                        <br />
        </caption>

    </table>
</div>
<br />
<br />
<br />

    </td>


    </td>
      </ContentTemplate>
     <Triggers>
        <asp:PostBackTrigger ControlID = "lnkbtnSave" />
    </Triggers>
      </asp:UpdatePanel>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphScripts" runat="server">
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DGMasterPage.Master" AutoEventWireup="true" CodeBehind="UpdateDepartment.aspx.cs" Inherits="DG.WebUI.UpdateDepartment" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="Departmentjs"></div>
</asp:Content>
<asp:Content ID="Content3" runat="server" contentplaceholderid="cphScripts">
    <script type="text/javascript">
        
        
            $(function() {

                $("#Departmentjs").jsGrid({
                    height: "auto",
                    width: "100%",
                    filtering: false,
                    editing: true,
                    inserting: true,
                    sorting: true,
                    paging: true,
                    autoload: true,
                    pageSize: 20,
                    pageButtonCount: 5,
                    deleteConfirm: "Do you really want to delete the client?",
                    controller: {
                        loadData: function () {
                            var d = $.Deferred();

                            $.ajax({
                                url: "/Api/DeptApi.aspx?action=get",
                                dataType: "json"
                            }).done(function (response) {
                                console.log(response);
                                d.resolve(response);
                            });

                            return d.promise();
                        },

                        insertItem: function (insertingClient) {
                            console.log(insertingClient);
                            $.post("/Api/DeptApi.aspx?action=save&depcode=0", insertingClient, function (response) {
                                var r = JSON.parse(response);
                                location.reload();
                            })
                    },

                        updateItem: function (updatingClient) {
                            console.log(updatingClient);
                            $.post("/Api/DeptApi.aspx?action=save", updatingClient, function (response) {
                                var r = JSON.parse(response);
                                location.reload();
                            })
                    },

                    deleteItem: function (deletingClient) {
                        $.post("/Api/DeptApi.aspx?action=delete&deptcode="+deletingClient.deptcode, function (response) {
                            var r = JSON.parse(response);
                            location.reload();
                        })
                    }
                    },
                    fields: [
                        { name: "deptcode",title:"Dept Code",type: "label", width: 20 },
                        { name: "deptname",title:"Dept Name", type: "text", width: 200 },
                        { name: "contact",title:"Contact", type: "text", width: 50 },
                        { name: "email",title:"Email", type: "text", width: 150 },
                        { type: "control" }
                    ]
                });

        });
    </script>
</asp:Content>


﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HospitalsMaster.Master" AutoEventWireup="true" CodeBehind="HealthFormShow.aspx.cs" Inherits="DG.WebUI.HealthFormShow" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<style type="text/css">
        .form-control
        {}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <table align="center" width="70%">
    <tr>
        <td align="right">
        <h1></h1>
        </td>
        <td align="center">
        <img src="Images/Hospital1.png" alt="" />
        </td>
    </tr>
    </table>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
            <asp:UpdateProgress runat="server" id="PageUpdateProgress">
            <ProgressTemplate>
            <div id="modal" runat="server" style =" position: fixed;
            z-index: 1000;
            height: 100%;
            width: 100%;
            top: 0;
            background-color: Black;
            filter: alpha(opacity=60);
            opacity: 0.6;
            -moz-opacity: 0.8; ">
                <div id="dvProgress" runat="server" style="position:fixed;  text-align:center; margin: 300px auto; padding: 10px; left:50%; z-index: 1001;" >
            <asp:Image ID="Image2" runat="server"  ImageUrl="Images/imgLoading.gif"  />
        </div> 
        </div>
            </ProgressTemplate>
        </asp:UpdateProgress>

   <!-- <div style="height: 60px">
        <div style="display: inline-block; left: 260px; position:absolute">
       -->
 
              <!--  <asp:Panel ID = "findpanel" runat="server" DefaultButton="lnkBtnSearch">  -->
              <table width="75%" align="center">
              <tr><td align="left">
                 <div class="row">
                   <div class="col-lg-6">
    <div class="input-group">
    <asp:TextBox ID="txtfind" width="200px" Height="34px" runat="server"  CssClass="form-control" placeholder="Search for..."></asp:TextBox>
      <span class="input-group-btn" style="float: left;padding-left: 0px;">
         <asp:LinkButton ID="lnkBtnSearch" runat="server" ToolTip = "Search Button" CssClass="btn btn-primary btn-md" 
                        onclick="lnkBtnSearch_Click" Width="100px">Search</asp:LinkButton>
      </span>
    </div>
  </div>
</div>  
</td></tr>         
      </table> 
        <!--   </asp:Panel> -->
           
     <!--   </div>
    </div>-->
    
    <table width = "75%" align = "center" cellpadding = "0" cellspacing = "0" style = "border:1px dashed white">
    <tr>
        <td colspan="4" align="left">
            <asp:Label ID="lblheading1" runat="server" Text="Patient Information" 
                BackColor="#000066" ForeColor="#FFFF66" Font-Size="X-Large"></asp:Label>
        </td>
    </tr>
    <tr>
        
        <td align="left">
            <asp:Label ID="lblFANo" runat="server" Text="FANo" Font-Size="Medium" 
                ForeColor="#000066"></asp:Label></td>
        <td align="left">
            <asp:TextBox ID="txtFano" runat="server" Width = "250px"  CssClass="form-control" 
                BackColor="White" ForeColor="Black"></asp:TextBox> </td>
        <td align="left">
            <asp:Label ID="lblPageNo" runat="server" Text="Computer No" Font-Size="Medium" 
                ForeColor="#000066"></asp:Label></td>
        <td align="left" colspan="3">
            <asp:TextBox ID="txtPageNo" runat="server" Width = "250px" CssClass="form-control" 
                BackColor="White" ForeColor="Black"></asp:TextBox>
        </td>

    </tr>
    
    <tr>
        <td align="left">
            <asp:Label ID="lblAppname" runat="server" Text="Applicant Name" Font-Size="Medium" 
                ForeColor="#000066"></asp:Label></td>
        <td align="left">
            <asp:TextBox ID="txtAppname" runat="server" Width="250px" CssClass="form-control" 
                BackColor="White"></asp:TextBox>
        </td>
        <td align="left">
            <asp:Label ID="lblAddress" runat="server" Text="Address" Font-Size="Medium" 
                ForeColor="#000066"></asp:Label></td>
        <td align="left">
            <asp:TextBox ID="txtAddress" runat="server" Width = "250px" 
                Font-Names="Arial" BackColor="White" CssClass="form-control"></asp:TextBox>
        </td>
    </tr>

    <tr>
        <td align="left">
            <asp:Label ID="lblLetterNo" runat="server" Text="LetterNo" Font-Size="Medium" 
                ForeColor="#000066"></asp:Label></td>
        <td align="left">
            <asp:TextBox ID="txtLetterNo" runat="server" Width="250px" CssClass="form-control" 
                BackColor="White"></asp:TextBox>
        </td>
         <td align="left">
            <asp:Label ID="lblLetDate" runat="server" Text="LetterDate" Font-Size="Medium" 
                ForeColor="#000066"></asp:Label></td>
        
        <td align="left">
            <telerik:RadDateTimePicker ID="dtLetterDate" runat="server" Width="250px"  CssClass="form-control">
            </telerik:RadDateTimePicker>
        </td>
    </tr>
    <tr>
        <td align="left">
            <asp:Label ID="lblRefto" runat="server" Text = "Reffered To" Font-Size="Medium" 
                ForeColor="#000066"></asp:Label></td>
        <td align="left">
            <asp:DropDownList  CssClass="form-control" ID="ddlRefferedTo" runat="server" Width = "250px" >
            </asp:DropDownList>
        </td>
        <td align="left">
            <asp:Label ID="lblRefOn" runat="server" Text="Reffered Date" Font-Size="Medium" 
                ForeColor="#000066"></asp:Label></td>
        
        <td align="left">
            <telerik:RadDateTimePicker ID="dtRefferedDate" runat="server" Width="250px" CssClass="form-control">
            </telerik:RadDateTimePicker>
                
                
        </td>
                 
    </tr>
    <tr>
        <td>
            <asp:Label ID="lblPatientName" runat="server" Text="Patient Name" Font-Size="Medium" 
                ForeColor="#000066"></asp:Label></td>
        </td>
        <td>
        <asp:TextBox ID="txtPatientName" runat="server" Width="250px" CssClass="form-control" 
        BackColor="White"></asp:TextBox>
        </td>
    </tr>


     <%
         if (Session["Login"] != null)
         {
             DGModel.Cls_Login login = (DGModel.Cls_Login)Session["Login"];
             
             DGModel.DGEntities ent = new DGModel.DGEntities();
             var chkaccess = (from a in ent.User_Groups where a.ID == login.Groupid select a).First();

             if (chkaccess.HealthSection1 == true)
             {
                
           %>
       
    <tr>
        <td colspan="4" align="left">
            <asp:Label ID="lblMedBoard" runat="server" Text="Medical Board" 
                BackColor="#000066" ForeColor="#FFFF66" Font-Size="X-Large"></asp:Label>
        </td>
    </tr>
    <tr>
        <td align="left">
            <asp:Label ID="lblMedBoardDate" runat="server" Text="Medical Board Constituted On" Font-Size="Medium" 
                ForeColor="#000066"></asp:Label></td>
        <td align="left">
                <telerik:RadDateTimePicker ID="rdtMedicalBoardDate" runat="server" Width="250px" 
                CssClass="form-control">
            </telerik:RadDateTimePicker></td>
    
    
        <td align="left">
            <asp:Label ID="heldDate" runat="server" Text="Dated Held On" Font-Size="Medium" 
                ForeColor="#000066"></asp:Label></td>
        <td align="left">
                <telerik:RadDateTimePicker ID="rdtDateHeldOn" runat="server" Width="250px" 
                CssClass="form-control">
            </telerik:RadDateTimePicker></td> </tr>

    <tr>    
        <td align="left">
            <asp:Label ID="lblRecommendations" runat="server" Text="Recommendations" Font-Size="Medium" 
                ForeColor="#000066"></asp:Label></td>
         <td colspan="3" align="left">   
             <asp:TextBox ID="txtRecommendations" runat="server" Width = "710px" 
                TextMode="MultiLine" Font-Names="Arial" CssClass="form-control"></asp:TextBox></td>
    </tr>

    <tr>
        <td align="left">
            <asp:Label ID="lblReportSentOn" runat="server" Text="Report Sent On" Font-Size="Medium" 
                ForeColor="#000066"></asp:Label></td>
        <td align="left">
                <telerik:RadDateTimePicker ID="rdtReportSentOn" runat="server" Width="250px" 
                CssClass="form-control">
            </telerik:RadDateTimePicker></td> </tr>
            <%}
             if (chkaccess.HealthSection2 == true)
             { 
              %>
            <tr>
        <td colspan="4" align="left">
            <asp:Label ID="lblOprStatus" runat="server" Text="Operation Status" 
                BackColor="#000066" ForeColor="#FFFF66" Font-Size="X-Large"></asp:Label>
        </td>
    </tr>
    <tr>
        <td align="left">
            <asp:Label ID="lblRegNo" runat="server" Text="Hospital Reg.No" Font-Size="Medium" 
                ForeColor="#000066"></asp:Label></td>
        <td align="left">
            <asp:TextBox ID="txtRegNo" runat="server" Width = "250px" CssClass="form-control"></asp:TextBox> </td>
        

    </tr>
    <tr>
        <td align="left">
            <asp:Label ID="lblTentativeDate" runat="server" Text="Tentative Date" Font-Size="Medium" 
                ForeColor="#000066"></asp:Label></td>
        <td align="left">
                <telerik:RadDateTimePicker ID="rdtTentativeDate" runat="server" Width="250px" 
                CssClass="form-control">
            </telerik:RadDateTimePicker>
        
        </td>
        <td align="left">
            <asp:Label ID="lblActualDate" runat="server" Text="Actual Date" Font-Size="Medium" 
                ForeColor="#000066"></asp:Label></td>
        <td align="left">
                <telerik:RadDateTimePicker ID="rdtActualDate" runat="server" Width="250px" 
                CssClass="form-control">
            </telerik:RadDateTimePicker>
        
        </td>
        </tr>
        <tr>
        
        <td align="left">
            <asp:Label ID="lblOpinion" runat="server" Text="Opinion / FollowUp" Font-Size="Medium" 
                ForeColor="#000066"></asp:Label></td>
         <td colspan="3" align="left">   
             <asp:TextBox ID="txtOpinion" runat="server" Width = "710px" 
                TextMode="MultiLine" Font-Names="Arial" CssClass="form-control"></asp:TextBox></td>
        </tr>
    <%}

             if (chkaccess.HealthSection3 == true)
             { %>
    
    <tr>
        <td colspan="4" align="left">
            <asp:Label ID="lblAccounts" runat="server" Text="Accounts" 
                BackColor="#000066" ForeColor="#FFFF66" Font-Size="X-Large"></asp:Label>
        </td>
    </tr>
    <tr>
        <td align="left">
            <asp:Label ID="fdSancNo" runat="server" Text = "FD Sanction Number" Font-Size="Medium" 
                ForeColor="#000066"></asp:Label></td>
        <td align="left">
            <asp:TextBox ID="txtFDSancNo" runat="server" Width = "250px" CssClass="form-control"></asp:TextBox>
        </td>
        <td align="left">
            <asp:Label ID="lblfdSancDate" runat="server" Text="FD Sanction Date" Font-Size="Medium" 
                ForeColor="#000066"></asp:Label></td>
        
        <td align="left">
            <telerik:RadDateTimePicker ID="rdtFDSancDate" runat="server" Width="250px" 
                CssClass="form-control">
            </telerik:RadDateTimePicker>

    </tr>


    <tr>
        <td align="left">
            <asp:Label ID="lblAuthorityNo" runat="server" Text = "Authority Number" Font-Size="Medium" 
                ForeColor="#000066"></asp:Label></td>
        <td align="left">
            <asp:TextBox ID="txtAuthorityNo" runat="server" Width = "250px" CssClass="form-control"></asp:TextBox>
        </td>
        <td align="left">
            <asp:Label ID="lblAuthorityDate" runat="server" Text="Authority Date" Font-Size="Medium" 
                ForeColor="#000066"></asp:Label></td>
        
        <td align="left">
            <telerik:RadDateTimePicker ID="rdtAuthorityDate" runat="server" Width="250px" 
                CssClass="form-control">
            </telerik:RadDateTimePicker>

    </tr>




    <tr>
        <td align="left">
            <asp:Label ID="lblSancAmount" runat="server" Text = "Amount Sanctioned" Font-Size="Medium" 
                ForeColor="#000066"></asp:Label></td>
        <td align="left">
            <asp:TextBox ID="txtSancAmount" runat="server" Width = "250px" CssClass="form-control"></asp:TextBox>
        </td>

        <td align="left">
            <asp:Label ID="lblFileNo" runat="server" Text="File Number" Font-Size="Medium" 
                ForeColor="#000066"></asp:Label></td>
        <td align="left">
            <asp:TextBox ID="txtFileNumber" runat="server" Width = "250px" CssClass="form-control"></asp:TextBox> </td>
            

    </tr>
    <tr>
        <td align="left">
            <asp:Label ID="lblRecvdchqno" runat="server" Text = "Received ChequeNo" Font-Size="Medium" 
                ForeColor="#000066"></asp:Label></td>
        <td align="left">
            <asp:TextBox ID="txtRecvdChqno" runat="server" Width = "250px" CssClass="form-control"></asp:TextBox>
        </td>
        <td align="left">
            <asp:Label ID="RecvdChqDate" runat="server" Text="Received ChqDate" Font-Size="Medium" 
                ForeColor="#000066"></asp:Label></td>
        
        <td align="left">
            <telerik:RadDateTimePicker ID="rdRecvdChqDate" runat="server" Width="250px" 
                CssClass="form-control">
            </telerik:RadDateTimePicker>
        </td>
        
               
    </tr>
   
    <tr>
        <td align="left">
            <asp:Label ID="lblExpences" runat="server" Text = "Expenditure" Font-Size="Medium" 
                ForeColor="#000066"></asp:Label></td>
        <td align="left">
            <asp:TextBox ID="txtAmount" runat="server" Width = "250px" CssClass="form-control"></asp:TextBox>
        </td>
        <td align="left">
            <asp:Label ID="lblUpdatedOn" runat="server" Text="Updated On" Font-Size="Medium" 
                ForeColor="#000066"></asp:Label></td>
        
        <td align="left">
            <telerik:RadDateTimePicker ID="rdtUpdatedOn" runat="server" Width="250px" 
                CssClass="form-control">
            </telerik:RadDateTimePicker>
        </td>
        
                 
    </tr>

    
    
    
    
    <tr>
        <td align="left">
            <asp:Label ID="lblBalReturn" runat="server" Text = "Balance" Font-Size="Medium" 
                ForeColor="#000066"></asp:Label></td>
        <td align="left">
            <asp:TextBox ID="txtBalReturned" runat="server" Width = "250px" CssClass="form-control" 
            Font-Size="Medium" ForeColor="#000066"></asp:TextBox>
        </td>
        </tr>

        <tr>
        <td align="left">
            <asp:Label ID="lblBalChqNo" runat="server" Text = "Deposited Through ChallanNo" Font-Size="Medium" 
                ForeColor="#000066"></asp:Label></td>
        <td align="left">
            <asp:TextBox ID="txtBalChqNo" runat="server" Width = "250px" CssClass="form-control"></asp:TextBox>
        </td>
        <td align="left">
            <asp:Label ID="lblBalChqDate" runat="server" Text="Challan Date" Font-Size="Medium" 
                ForeColor="#000066"></asp:Label></td>
        
        <td align="left">
            <telerik:RadDateTimePicker ID="rdBalChqDate" runat="server" Width="250px" 
                CssClass="form-control">
            </telerik:RadDateTimePicker>
        </td>
                    
    </tr>

    <tr>
        <td>
            <asp:Label ID="lblVouchedDate" runat="server" Text="Vouched Date" Font-Size="Medium" 
                ForeColor="#000066"></asp:Label></td>
        </td>
        <td align="left">
            <telerik:RadDateTimePicker ID="rdtVouchedDate" runat="server" Width="250px" 
                CssClass="form-control">
            </telerik:RadDateTimePicker>
        </td>
        <td align="left">
        <asp:Label ID="lblLiquidDate" runat="server" Text="Liquidation Date" Font-Size="Medium" 
                ForeColor="#000066"></asp:Label></td>
        <td align="left">
            <telerik:RadDateTimePicker ID="rdtLiquidDate" runat="server" Width="250px" 
                CssClass="form-control">
            </telerik:RadDateTimePicker>
        </td>
    </tr>



    <tr>
        <td>
        <asp:Label ID="lblStatus" runat="server" Text="Status" Font-Size="Medium" 
                ForeColor="#000066"></asp:Label></td>
        
        <td align="left">
            <asp:DropDownList ID="ddlStatus" runat="server" Width = "250px" CssClass="form-control">
            </asp:DropDownList>
        </td>
    </tr>

     <tr>
        <td align="left">
            <asp:Label ID="lblRemarks" runat="server" Text="Remarks" Font-Size="Medium" 
                ForeColor="#000066"></asp:Label></td>
         <td colspan="3" align="left">   
             <asp:TextBox ID="txtRemarks" runat="server" Width = "710px" 
                TextMode="MultiLine" Font-Names="Arial" CssClass="form-control"></asp:TextBox></td>
     </tr>
    <%}
         } %>
           <tr>
                    <td align = "center" colspan = "4">
                        <br />
                        
                  <asp:LinkButton ID="lnkbtnSave" Font-Size="Medium" runat="server" ToolTip = "Save" onclick="lnkbtnSave_Click" Height="40px" Width="100px" CssClass="btn btn-primary btn-lg" >Save</asp:LinkButton>
                        <asp:LinkButton ID="lnkbtnCancel" Font-Size="Medium" runat="server" ToolTip = "Cancel" Height="40px" Width="100px" onclick="lnkbtnCancel_Click" CssClass="btn btn-primary btn-lg">Cancel</asp:LinkButton>
                    </td>
                    </tr>
                    <caption>
                        <br />
        </caption>

    </table>
</div>
<br />
<br />
<br />

    </td>
    </td>
      </ContentTemplate>     
      </asp:UpdatePanel>
    
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HospitalsMaster.Master" AutoEventWireup="true" CodeBehind="MeetingForm.aspx.cs" Inherits="DG.WebUI.MeetingForm" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server">
</asp:ScriptManager>

<h1 align = "center"> Meeting Form </h1>

  <table width="75%" align="center">
              <tr><td align="left">
                 <div class="row">
                   <div class="col-lg-6">
    <div class="input-group">
    <asp:TextBox ID="txtfind" width="200px" Height="34px" runat="server"  CssClass="form-control" placeholder="Search for..."></asp:TextBox>
      <span class="input-group-btn" style="float: left;padding-left: 0px;">
         <asp:LinkButton ID="lnkBtnSearch" runat="server" ToolTip = "Search Button" CssClass="btn btn-primary btn-md" 
                        onclick="lnkBtnSearch_Click" Width="100px">Search</asp:LinkButton>
      </span>
    </div>
  </div>
  </div>  
  </td></tr>
           
      </table>
      
      <table width = "75%" align = "center" cellpadding = "0" cellspacing = "0" style = "border:1px dashed white">
      
      <tr>
        
        <td align="left">
            <asp:Label ID="lblFANo" runat="server" Text="FANo" Font-Size="Medium" 
                ForeColor="#000066"></asp:Label></td>
        <td align="left">
            <asp:TextBox ID="txtFano" runat="server" Width = "250px"  CssClass="form-control" 
                BackColor="White" ForeColor="Black"></asp:TextBox> </td>
    
        <td align="left">
            <asp:Label ID="lblAppname" runat="server" Text="Applicant Name" Font-Size="Medium" 
                ForeColor="#000066"></asp:Label></td>
        <td align="left">
            <asp:TextBox ID="txtAppname" runat="server" Width="250px" CssClass="form-control" 
                BackColor="White"></asp:TextBox>
        </td>    
    </tr>
    
    <tr>
        <td align="left">
            <asp:Label ID="lblAddress" runat="server" Text="Address" Font-Size="Medium" 
                ForeColor="#000066"></asp:Label></td>
        <td align="left">
            <asp:TextBox ID="txtAddress" runat="server" Width = "250px" 
                Font-Names="Arial" BackColor="White" CssClass="form-control"></asp:TextBox>
        </td>
        <td align="left">
            <asp:Label ID="lblRefby" runat="server" Text="Recommended By" Font-Size="Medium" 
                ForeColor="#000066"></asp:Label></td>
        <td align="left">
            <asp:TextBox ID="txtRefBy" runat="server" Width="250px" CssClass="form-control" 
                BackColor="White"></asp:TextBox>
        </td></tr>
        <tr>
        <td align="left">
            <asp:Label ID="lblMeetingDate" runat="server" Text="Meeting Date" Font-Size="Medium" 
                ForeColor="#000066"></asp:Label></td>
        <td align="left">
            <telerik:RadDateTimePicker ID="MeetingDate" runat="server" Width="250px"  CssClass="form-control">
            </telerik:RadDateTimePicker>
        </td>
    </tr>
    <tr>    
        <td align="left">
            <asp:Label ID="lblCategory" runat="server" Text="Category" Font-Size="Medium" 
                ForeColor="#000066"></asp:Label></td>
         <td colspan="3" align="left">   
             <asp:TextBox ID="txtCategory" runat="server" Width = "760px" 
                TextMode="MultiLine" Font-Names="Arial" CssClass="form-control"></asp:TextBox></td>
    </tr>
    <tr>
        <td align="left">
            <asp:Label ID="lblAmount" runat="server" Text="Amount Received" Font-Size="Medium" 
                ForeColor="#000066"></asp:Label></td>
        <td align="left">
            <asp:TextBox ID="txtAmount" runat="server" Width = "250px" 
                Font-Names="Arial" BackColor="White" CssClass="form-control"></asp:TextBox>
        </td>
        <td align="left">
            <asp:Label ID="lblProposal" runat="server" Text="Proposed Amount" Font-Size="Medium" 
                ForeColor="#000066"></asp:Label></td>
        <td align="left">
            <asp:TextBox ID="txtProposed" runat="server" Width="250px" CssClass="form-control" 
                BackColor="White"></asp:TextBox>
        </td></tr>
        <tr>    
        <td align="left">
            <asp:Label ID="lblRemarks" runat="server" Text="Remarks" Font-Size="Medium" 
                ForeColor="#000066"></asp:Label></td>
         <td colspan="3" align="left">   
             <asp:TextBox ID="txtRemarks" runat="server" Width = "760px" 
                TextMode="MultiLine" Font-Names="Arial" CssClass="form-control"></asp:TextBox></td>
    </tr>

     <tr>
                    <td align = "center" colspan = "4">
                        <br />
                        
                  <asp:LinkButton ID="lnkbtnSave" Font-Size="Medium" runat="server" ToolTip = "Save" onclick="lnkbtnSave_Click" Height="40px" Width="100px" CssClass="btn btn-primary btn-lg" >Save</asp:LinkButton>
                        <asp:LinkButton ID="lnkbtnCancel" Font-Size="Medium" runat="server" ToolTip = "Cancel" Height="40px" Width="100px" onclick="lnkbtnCancel_Click" CssClass="btn btn-primary btn-lg">Cancel</asp:LinkButton>
                    </td>
                    </tr>
                    <caption>
                        <br />
        </caption>

      </table>


</asp:Content>

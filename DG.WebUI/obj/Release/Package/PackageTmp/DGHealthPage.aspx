﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DGMain.Master" AutoEventWireup="true" CodeBehind="DGHealthPage.aspx.cs" Inherits="DG.WebUI.DGHealthPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
    <div class="col-lg-4" style="background-color: #d4e157; border: 1px solid #e1e1e8; width:100%; height: 750px; position:relative; left: 0px;top: 70px;">
        
        <div class="row">
        
    <div class="col-md-12" >
                <div class="row">
            <div class="col-md-12 col-md-offset-4">
                <h1><b>DG Health Analysis</b></h1>
            </div></div> 
        <div class="row">
                     <div class="col-md-12">
                         <div class="input-group">
                             <span class="input-group-addon">Fyear</span>
                         <asp:TextBox ID="txtfyear" CssClass="form-control" runat="server" Width="190px"></asp:TextBox>
                    </div></div>
                </div>
        
               <div class="row">
                     <div class="col-md-12">
                         <div class="input-group">
                             <span class="input-group-addon">From Date</span>
                         <telerik:RadDatePicker CssClass="form-control" ID="RadDatePicker1" runat="server"></telerik:RadDatePicker>
                    </div></div>
                </div>
                 <div class="row">
                     <div class="col-md-12">
                         <div class="input-group">
                             <span class="input-group-addon">To &nbsp&nbsp&nbsp&nbsp Date</span>
                         <telerik:RadDatePicker CssClass="form-control" ID="RadDatePicker2" runat="server"></telerik:RadDatePicker>
                             </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-12">
                        <asp:Label ID="lblError" runat="server" or="Red"></asp:Label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <asp:RadioButtonList Font-Size="X-Large" Font-Bold="true" ID="rblReportCategories" runat="server" 
                            CssClass="myTable table table-hover" 
                            onselectedindexchanged="rblReportCategories_SelectedIndexChanged" 
                            AutoPostBack="True">
                        </asp:RadioButtonList>
                    </div>
                </div>
            </div>
            </div>
                </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CphScript" runat="server">
</asp:Content>

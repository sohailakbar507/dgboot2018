﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HospitalsMaster.Master" AutoEventWireup="true" CodeBehind="UpdateDGHealthChequeStatus.aspx.cs" Inherits="DG.WebUI.UpdateDGHealthChequeStatus" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            $('.form-control').keyup(function (e) {
                //console.log(e.keyCode);
                if (e.keyCode == 13) {
                    var $this = $(this),
                        index = $this.closest('td').index();
                    $this.closest('tr').next().find('td').eq(index).find('.form-control').focus();
                    //$this.closest('tr').next().find('td').eq(index).find('.form-control').val("");
                    e.preventDefault();
                }
            })
       });

       function successalert() {
           swal({
               title: 'Success!',
               text: 'Record Saved Successfully.......',
               type: 'success'
           });
       }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

     <br /><br />

    <div class="container" style="position:center; width: 95%;">
  <div class="row">
<div class="panel panel-primary">
                        <div class="panel-heading">
                        
                                                    
    <h3 class="panel-title">DG Health - Cheque Update Form</h3>

                        </div>
                        <div class="panel-body">
    
    
        <div class="row">
</div>
                <asp:GridView ID="DGVBill" runat="server" CssClass="table" AutoGenerateColumns= "False" CellPadding="0" 
                    HorizontalAlign ="Center"
                    onrowediting="DGVBill_RowEditing" Width="100%" 
                    onpageindexchanging="DGVBill_PageIndexChanging" 
                    onrowupdating="DGVBill_RowUpdating" DataKeyNames="DGId" 
                     GridLines="None" OnRowCommand="DGVBill_RowCommand" OnRowDeleting="DGVBill_RowDeleting" Font-Size= "12px">
                    
          <Columns>
              
                <asp:TemplateField HeaderText="DGID" Visible="false">
                <ItemTemplate>
                    <asp:Label ID="lblDGId" runat="server" Text='<%#Eval("DGId") %>'></asp:Label>
                </ItemTemplate>
                </asp:TemplateField>
              <asp:TemplateField HeaderText="FaNo.">
              <ItemTemplate>
                 <asp:Label ID="lblfano" runat="server" CssClass="form-control input-sm" Text='<%#Eval("fano") %>'></asp:Label>
              </ItemTemplate> <ControlStyle Width="70px" Font-Size="X-Small" ForeColor="#CC0000" /><HeaderStyle Font-Size="Smaller" />
                        
              </asp:TemplateField>    
               <asp:TemplateField HeaderText="Fyear">
              <ItemTemplate>
                 <asp:Label ID="lblfyear" runat="server" CssClass="form-control input-sm" Text='<%#Eval("fyear") %>'></asp:Label>
              </ItemTemplate> <ControlStyle Width="70px" Font-Size="X-Small" ForeColor="#CC0000" /><HeaderStyle Font-Size="Smaller" />
                        
              </asp:TemplateField> 
               <asp:TemplateField HeaderText="Department">
              <ItemTemplate>
                 <asp:Label ID="lbldeptname" runat="server" CssClass="form-control input-sm" Text='<%#Eval("deptname") %>'></asp:Label>
              </ItemTemplate> <ControlStyle Width="150px" Font-Size="X-Small" ForeColor="#CC0000" /><HeaderStyle Font-Size="Smaller" />
                        
                   <ItemStyle Font-Size="X-Small" />
                        
              </asp:TemplateField> 
              <asp:TemplateField HeaderText="Cheque No.">
              <ItemTemplate>
                  <asp:TextBox ID="txtchqno" CssClass="form-control input-sm" runat="server" Text='<%#Eval("chqno") %>'></asp:TextBox>
                                
              </ItemTemplate> <ControlStyle Width="80px" /><HeaderStyle Font-Size="Smaller" />
                        
                </asp:TemplateField>
               <asp:TemplateField HeaderText="Cheque Date">
              <ItemTemplate>
                 <asp:TextBox ID="txtchqdate" runat="server" CssClass="form-control input-sm" Text='<%#Eval("chqdate","{0:dd/MM/yyyy}") %>'></asp:TextBox>
              </ItemTemplate>
                  <ControlStyle Width="80px" /><HeaderStyle Font-Size="Smaller" />
                  <HeaderStyle HorizontalAlign="Center" Height="40px" VerticalAlign="Middle" />
                  <ItemStyle HorizontalAlign="Center" />
              </asp:TemplateField>
                                      
              <asp:TemplateField HeaderText="Amount">
              <ItemTemplate>
                 <asp:Label ID="lblAmount" CssClass="form-control input-sm" runat="server" Text='<%#Eval("amount") %>'></asp:Label>
              </ItemTemplate>
                  <ControlStyle Width="70px" ForeColor="#CC0000" /><HeaderStyle Font-Size="Smaller" />
                  <HeaderStyle HorizontalAlign="Center" Height="40px" VerticalAlign="Middle" />
                  <ItemStyle HorizontalAlign="Center" />
              </asp:TemplateField>
               <asp:TemplateField HeaderText="Expenditure">
              <ItemTemplate>
                 <asp:TextBox ID="txtexp" CssClass="form-control input-sm" runat="server" Text='<%#Eval("Expense") %>'></asp:TextBox>
              </ItemTemplate>
                  <ControlStyle Width="80px" /><HeaderStyle Font-Size="Smaller" />
                  <HeaderStyle HorizontalAlign="Center" Height="40px" VerticalAlign="Middle" />
                  <ItemStyle HorizontalAlign="Center" />
              </asp:TemplateField>
              <asp:TemplateField HeaderText="Sanction No">
              <ItemTemplate>
                 <asp:TextBox ID="txtSancNo" CssClass="form-control input-sm" runat="server" Text='<%#Eval("SancNo") %>'></asp:TextBox>
              </ItemTemplate>
                  <ControlStyle Width="75px" /><HeaderStyle Font-Size="Smaller" />
                  <HeaderStyle HorizontalAlign="Center" Height="40px" VerticalAlign="Middle" />
                  <ItemStyle HorizontalAlign="Center" />
              </asp:TemplateField>
                <asp:TemplateField HeaderText="Sanction Date">
              <ItemTemplate>
                 <asp:TextBox ID="txtSancdate" runat="server" CssClass="form-control input-sm" Text='<%#Eval("Sancdate","{0:dd/MM/yyyy}") %>'></asp:TextBox>
              </ItemTemplate>
                  <ControlStyle Width="75px" /><HeaderStyle Font-Size="Smaller" />
                  <HeaderStyle HorizontalAlign="Center" Height="40px" VerticalAlign="Middle" />
                  <ItemStyle HorizontalAlign="Center" />
              </asp:TemplateField>

              <asp:TemplateField HeaderText="Authority No">
              <ItemTemplate>
                 <asp:TextBox ID="txtAuthNo" CssClass="form-control input-sm" runat="server" Text='<%#Eval("AuthorityNumber") %>'></asp:TextBox>
              </ItemTemplate>
                  <ControlStyle Width="75px" /><HeaderStyle Font-Size="Smaller" />
                  <HeaderStyle HorizontalAlign="Center" Height="40px" VerticalAlign="Middle" />
                  <ItemStyle HorizontalAlign="Center" />
              </asp:TemplateField>
              
               <asp:TemplateField HeaderText="Authority Date">
              <ItemTemplate>
                 <asp:TextBox ID="txtAuthdate" runat="server" CssClass="form-control input-sm" Text='<%#Eval("Authority_Date","{0:dd/MM/yyyy}") %>'></asp:TextBox>
              </ItemTemplate>
                  <ControlStyle Width="75px" /><HeaderStyle Font-Size="Smaller" />
                  <HeaderStyle HorizontalAlign="Center" Height="40px" VerticalAlign="Middle" />
                  <ItemStyle HorizontalAlign="Center" />
              </asp:TemplateField>  
                <asp:TemplateField HeaderText="Amount Refunded">
              <ItemTemplate>
                 <asp:TextBox ID="txtrefunded" CssClass="form-control input-sm" runat="server" Text='<%#Eval("refunded") %>'></asp:TextBox>
              </ItemTemplate>
                  <ControlStyle Width="80px" /><HeaderStyle Font-Size="Smaller" />
                  <HeaderStyle HorizontalAlign="Center" Height="40px" VerticalAlign="Middle" />
                  <ItemStyle HorizontalAlign="Center" />
              </asp:TemplateField>
              </Columns>
                </asp:GridView>            
         <br /><br />       
    </div>
    <div class="modal-footer">
        <div class="row">
            <div class="col-md-2 col-md-offset-4">
                <asp:Button ID="btnBack" class="btn btn-primary btn-lg" runat="server" Text="Go Back" OnClick="btnBack_Click"  
                   
            />
            </div>
            <div class="col-md-2">
                <asp:Button ID="btnSave" class="btn btn-primary btn-lg" runat="server" Text="Save Changes"  
                   onclick="btnSave_Click" />  
            </div>
        </div>
        
                         
     </div>
</div></div><%--</div>--%></div>
</asp:Content>

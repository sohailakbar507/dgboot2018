USE [DGPM]
GO
/****** Object:  Table [dbo].[tblUploadFiles]    Script Date: 7/23/2017 10:44:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblUploadFiles](
	[H_fano] [int] IDENTITY(1,1) NOT NULL,
	[FaNo] [int] NULL,
	[FilePath] [nvarchar](max) NULL,
	[FileName] [nvarchar](50) NULL,
 CONSTRAINT [PK_tblUploadFiles_1] PRIMARY KEY CLUSTERED 
(
	[H_fano] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DGMasterPage.Master" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="DGWebUI.WebUi.UserLogin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Styles/default.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="Login">
        <br />
<table align="center" width="600" cellpadding="0" cellspacing="0" border="0">
<tr>
    <td>
        <asp:Login ID="Login1" runat="server" BackColor="#ED8A27" BorderColor="#006600" 
            BorderStyle="Solid" BorderWidth="10px" Font-Names="Verdana" Font-Size="10pt" 
            Height="221px" Width="506px" TitleText="CHIEF MINISTER SECRETARIAT PUNJAB"
            LoginButtonText="Sign In" CreateUserText="Don't have and account "  
            CreateUserUrl="~/Admin/UserRegistration.aspx"
            PasswordRecoveryText="Forgot your password ?" 
            PasswordRecoveryUrl="~/Admin/ForgotPassword.aspx" Font-Bold="False" 
            
            VisibleWhenLoggedIn="False" DestinationPageUrl="~/NewForm.aspx">
            <CheckBoxStyle HorizontalAlign="Center" />
            <HyperLinkStyle BackColor="#ED8A27" ForeColor="#003300" Height="150px" 
                HorizontalAlign="Center" />
            <FailureTextStyle ForeColor="#FFFFCC" />
            <LoginButtonStyle BackColor="#003300"  Font-Bold="False" ForeColor="Yellow" 
                Width="100px" />
                
            <TextBoxStyle Height="20px" Width="250px" ForeColor="Black" 
                BorderStyle="Groove" />
            <TitleTextStyle BackColor="#006600" Font-Bold="True" ForeColor="#FFFFCC" 
                Height="80px" BorderColor="#006600" Font-Names="Arial Black" 
                Font-Overline="False" Font-Size="Large" />
            
            </asp:Login>
    

    </td>
    </tr>
    
<tr>
    <td>
    </td>
        
</tr>
<tr>
<td>
    &nbsp;</td>

</tr>
</table>
    
</div>
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DGMasterPage.Master" AutoEventWireup="true" CodeBehind="FDUpdateForm.aspx.cs" Inherits="DG.WebUI.FDUpdateForm" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   

</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
  </asp:ScriptManager>

 <h1 align = "center"> FD Search Form </h1>    
       
        <table align="center"  width="60%" cellpadding = "" cellspacing = "0" border = "0" style= "border-collapse:collapse">
        
        
        <tr>
            <td>
              <asp:Label ID="lblFaNo" runat="server" Text="FaNo."></asp:Label></td>
              <td>
              <asp:TextBox ID="txtFaNo" runat="server" CssClass="form-control"  Width="500px"></asp:TextBox>
                  <br />
            </td>
             
         </tr>
         <tr>
         
                <td colspan="2" align="center">
                    <asp:LinkButton ID="lnkBtnSearch" runat="server" CssClass="btn btn-primary btn-lg" ToolTip = "Search Button" onclick="lnkBtnSearch_Click"><img src="Images/Zoom-icon.png" alt = ""/> </asp:LinkButton>
                </td> 
            </tr>
             
             
        </table>
        <br /><br />
        <table align="center"  width="100%" cellpadding = "1" cellspacing = "0" border = "0px" style= "border-collapse:collapse">
         <tr>
         <td align="center"><div class="dataTable_wrapper">
            <asp:GridView ID="DGVFDUpdate" runat="server" AutoGenerateColumns="False" 
                 PageSize="10" 
                 onselectedindexchanged="DGVFDUpdate_SelectedIndexChanged" onrowcancelingedit = "DVGFDUpdate_RowCancelingEdit"
                 onrowupdate = "DVGFDUpdate_RowUpdate" 
                 onpageindexchanging="DGVFDUpdate_PageIndexChanging" 
                 onrowupdating="DGVFDUpdate_RowUpdating" 
                 CssClass="table table-striped table-bordered table-hover">
                 <Columns>
                    <asp:TemplateField HeaderText="DGId" Visible="False">
                    <ItemTemplate>
                    <asp:Label ID="lblDGId" runat="server" Text='<%#Eval("DGId")%>'></asp:Label>
                    </ItemTemplate>
                    </asp:TemplateField>
                    
                    <asp:TemplateField HeaderText = "FaNo.">
                        <ItemTemplate>
                            <asp:Label ID="lblFaNo" runat="server" Text='<%#Eval("fano")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText = "Applicant Name">
                        <ItemTemplate>
                            <asp:Label ID="lblappname" runat="server" Text='<%#Eval("appname")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                   <asp:TemplateField HeaderText = "Amount">
                        <ItemTemplate>
                            <asp:Label ID="lblAmount" runat="server" Text='<%#Eval("amount")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                   
                    <asp:TemplateField HeaderText = "Amount Incurred">
                        <ItemTemplate>
                            <asp:Label ID="lblAmountIncurred" runat="server" Text='<%#Eval("AmountIncured")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText = "Balance">
                        <ItemTemplate>
                            <asp:Label ID="lblBalance" runat="server" Text='<%#Eval("Balance")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                    <asp:TemplateField HeaderText="FD Refdate">
                   <ItemTemplate>
                       <asp:Label ID="lblFDRefDate" runat="server" Text='<%#Eval("fdrefdate","{0:dd/MM/yyyy}") %>'></asp:Label>
                   </ItemTemplate>
                       <ItemStyle Width="70px" />
                   </asp:TemplateField>
                    <asp:TemplateField HeaderText = "FD RefNo.">
                        <ItemTemplate>
                            <asp:Label ID="lblFDRefNo" runat="server" Text='<%#Eval("fdrefno")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                                 
                    <asp:TemplateField HeaderText = "Sent To">
                        <ItemTemplate>
                            <asp:Label ID="lblSentTo" runat="server" Text='<%#Eval("sentto")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    

                    <asp:TemplateField HeaderText="Edit">
                        <ItemTemplate>
                       <asp:LinkButton ID="LnkBtnEdit" CommandName = "update" runat="server"><img src = "Images/Text-Edit-icon.png" alt = "" /> </asp:LinkButton>
                   </ItemTemplate>
                    </asp:TemplateField>
                    

            </Columns>
                    <PagerStyle HorizontalAlign = "right" CssClass = "pagination-ys" />
            </asp:GridView>
            </div> </td></tr>
            
</table>
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HospitalsMaster.Master" AutoEventWireup="true" CodeBehind="MultipleHospitalsUpdate.aspx.cs" Inherits="DG.WebUI.MultipleHospitalsUpdate" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <script type="text/javascript">
        $(document).ready(function () {
            $('.form-control').keyup(function (e) {
                //console.log(e.keyCode);
                if (e.keyCode == 13) {
                    var $this = $(this),
                        index = $this.closest('td').index();
                    $this.closest('tr').next().find('td').eq(index).find('.form-control').focus();
                    //$this.closest('tr').next().find('td').eq(index).find('.form-control').val("");
                    e.preventDefault();
                }
            })
       });

       function successalert() {
           swal({
               title: 'Success!',
               text: 'Record Saved Successfully.......',
               type: 'success'
           });
       }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <br /><br />

    <div class="container" style="position:center; width: 95%;">
  <div class="row">
      <div class="col-md-10 col-md-offset-2">
<div class="panel panel-primary">
                        <div class="panel-heading">
                        
                                                    
    <h3 class="panel-title">DG Health - Multiple Activity Update Form</h3>

                        </div>
                        <div class="panel-body">
    
    
        <div class="row">
</div>
                <asp:GridView ID="DGVHospitals" runat="server" CssClass="table table-responsive table-hover" AutoGenerateColumns= "False" CellPadding="0" 
                    HorizontalAlign ="Center" Font-Size= "12px" BackColor="White" 
                    BorderColor="#CC9966" BorderStyle="None" BorderWidth="1px" OnRowDataBound="DGVHospitals_RowDataBound">
                    
          <Columns>
              
                <asp:TemplateField HeaderText="DGID" Visible="false">
                <ItemTemplate>
                    <asp:Label ID="lblDGId" runat="server" Text='<%#Eval("DGId") %>'></asp:Label>
                </ItemTemplate>
                </asp:TemplateField>
              <asp:TemplateField HeaderText="FaNo.">
              <ItemTemplate>
                 <asp:Label ID="lblfano" runat="server" CssClass="form-control input-sm" Text='<%#Eval("fano") %>'></asp:Label>
              </ItemTemplate> <ControlStyle Width="70px" Font-Size="X-Small" ForeColor="#CC0000" /><HeaderStyle Font-Size="Smaller" />
                        
              </asp:TemplateField>    
               <asp:TemplateField HeaderText="Fyear">
              <ItemTemplate>
                 <asp:Label ID="lblfyear" runat="server" CssClass="form-control input-sm" Text='<%#Eval("fyear") %>'></asp:Label>
              </ItemTemplate> <ControlStyle Width="70px" Font-Size="X-Small" ForeColor="#CC0000" /><HeaderStyle Font-Size="Smaller" />
                        
              </asp:TemplateField> 
              <asp:TemplateField HeaderText="Cheque No.">
              <ItemTemplate>
                  <asp:TextBox ID="txtchqno" CssClass="form-control input-sm" runat="server" Text='<%#Eval("chqno") %>'></asp:TextBox>
                                
              </ItemTemplate> <ControlStyle Width="80px" /><HeaderStyle Font-Size="Smaller" />
                        
                </asp:TemplateField>
               <asp:TemplateField HeaderText="Cheque Date">
              <ItemTemplate>
                 <asp:TextBox ID="txtchqdate" runat="server" CssClass="form-control input-sm" Text='<%#Eval("chqdate","{0:dd/MM/yyyy}") %>'></asp:TextBox>
              </ItemTemplate>
                  <ControlStyle Width="80px" /><HeaderStyle Font-Size="Smaller" />
                  <HeaderStyle HorizontalAlign="Center" Height="40px" VerticalAlign="Middle" />
                  <ItemStyle HorizontalAlign="Center" />
              </asp:TemplateField>
                                      
              <asp:TemplateField HeaderText="Amount">
              <ItemTemplate>
                 <asp:Label ID="lblAmount" CssClass="form-control input-sm" runat="server" Text='<%#Eval("amount") %>'></asp:Label>
              </ItemTemplate>
                  <ControlStyle Width="70px" ForeColor="#CC0000" /><HeaderStyle Font-Size="Smaller" />
                  <HeaderStyle HorizontalAlign="Center" Height="40px" VerticalAlign="Middle" />
                  <ItemStyle HorizontalAlign="Center" />
              </asp:TemplateField>
               <asp:TemplateField HeaderText="Expenditure">
              <ItemTemplate>
                 <asp:TextBox ID="txtexp" CssClass="form-control input-sm" runat="server" Text='<%#Eval("Expense") %>'></asp:TextBox>
              </ItemTemplate>
                  <ControlStyle Width="80px" /><HeaderStyle Font-Size="Smaller" />
                  <HeaderStyle HorizontalAlign="Center" Height="40px" VerticalAlign="Middle" />
                  <ItemStyle HorizontalAlign="Center" />
              </asp:TemplateField>
                <asp:TemplateField ItemStyle-Width="250px" HeaderStyle-Width="250px" HeaderText ="Referred To: ">
                     <ItemTemplate>
                 <asp:Label ID="lblRefId" runat="server" Visible="false" Text='<%#Eval("senttoid") %>'></asp:Label>
                <asp:DropDownList ID="DDLRef" Width="250px" runat="server" CssClass="form-control">
                </asp:DropDownList>
                   </ItemTemplate>
                    <ControlStyle /><HeaderStyle Font-Size="Smaller" />
                  <HeaderStyle HorizontalAlign="Center" Height="40px" VerticalAlign="Middle" />
                  <ItemStyle HorizontalAlign="Center" />

                </asp:TemplateField>
              </Columns>
                    <FooterStyle BackColor="#FFFFCC" ForeColor="#330099" />
                    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="#FFFFCC" />
                    <PagerStyle BackColor="#FFFFCC" ForeColor="#330099" HorizontalAlign="Center" />
                    <RowStyle BackColor="White" ForeColor="#330099" />
                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#663399" />
                    <SortedAscendingCellStyle BackColor="#FEFCEB" />
                    <SortedAscendingHeaderStyle BackColor="#AF0101" />
                    <SortedDescendingCellStyle BackColor="#F6F0C0" />
                    <SortedDescendingHeaderStyle BackColor="#7E0000" />
                </asp:GridView>            
         <br /><br />       
    </div>
    <div class="modal-footer">
        <asp:Button ID="btnBack" class="btn btn-primary btn-danger" runat="server" Text="Go Back" OnClick="btnBack_Click"  
                   
            />
        <asp:Button ID="btnSave" class="btn btn-primary btn-danger" runat="server" Text="Save Changes"  
                   onclick="btnSave_Click" />                   
     </div>
</div>

   </div></div></div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphScripts" runat="server">
</asp:Content>

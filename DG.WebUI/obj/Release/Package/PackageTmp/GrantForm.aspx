﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DGMasterPage.Master" AutoEventWireup="true" CodeBehind="GrantForm.aspx.cs" Inherits="DG.WebUI.GrantForm" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   <%-- <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>--%>

<h1 align = "center"> Grant Form </h1>
    
<div>
    <div style="height: 60px">
        <div style="display: inline-block; left: 260px; position:absolute">
                <asp:Panel ID = "findpanel" runat="server" DefaultButton="btnFind">              
               <asp:TextBox ID="txtfind" runat="server" 
               Width="220px" Height="40px"></asp:TextBox>
                <asp:Button ID="btnFind" runat="server" Text="Find"  
                   onclick="btnFind_Click" 
                    Width="125px" Font-Size="Medium" CssClass="btn btn-primary btn-lg"  />
            </asp:Panel>
        </div>
    </div>
    <table width = "65%" align = "center" cellpadding = "0" cellspacing = "0" style = "border:1px dashed white">
    <tr>
        <td align="left">
            <asp:Label ID="lblPageNo" runat="server" Text="PageNo"></asp:Label> </td>
         <td align="left">
            <asp:TextBox ID="txtPageNo" runat="server" Width="250px" CssClass="form-control"></asp:TextBox>
        </td>
        <td align="left">
            <asp:Label ID="lblFANo" runat="server" Text="FANo"></asp:Label></td>
        <td align="left">
            <asp:TextBox ID="txtFano" runat="server" Width = "270px" CssClass="form-control"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td align="left">
            <asp:Label ID="lblGrant" runat="server" Text="Grant"></asp:Label></td>
        <td align="left">
            <asp:TextBox ID="txtGrant" runat="server" Width="250px" CssClass="form-control"></asp:TextBox>
        </td>
        <td align="left">
            <asp:Label ID="lbldept" runat="server" Text="Department"></asp:Label>
        </td>
        <td align="left">
            <asp:DropDownList ID="ddlDept" Width="270px" CssClass="form-control" runat="server"></asp:DropDownList>
                  
        </td>
    </tr>
    <tr>
        <td align="left">
            <asp:Label ID="lblAppname" runat="server" Text="Applicant Name"></asp:Label></td>
        <td align="left">
            <asp:TextBox ID="txtAppname" runat="server" Width="250px" CssClass="form-control"></asp:TextBox>
        </td>
        <td align="left">
            <asp:Label ID="lblAddress" runat="server" Text="Address"></asp:Label></td>
        <td align="left">
            <asp:TextBox ID="txtAddress" runat="server" Width = "270px" 
                TextMode="MultiLine" Font-Names="Arial" CssClass="form-control"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td align="left">
            <asp:Label ID="lblSubject" runat="server" Text="Subject"></asp:Label></td>
            <td align="left" colspan = "3">
             <asp:TextBox ID="txtSubject" runat="server" Width="700px" CssClass="form-control" TextMode="MultiLine" 
                    Font-Names="Arial"></asp:TextBox>
        </td>
        </tr>
        <tr>
        <td align="left">
            <asp:Label ID="lblRefno" runat="server" Text="RefNo"></asp:Label></td>
        <td align="left">
            <asp:TextBox ID="txtRefno" runat="server" CssClass="form-control" Width = "250px"></asp:TextBox>
        </td>
        <td align="left">
            <asp:Label ID="lblRefDate" runat="server" Text="RefDate"></asp:Label>
        </td>
        <td align="left">
            <telerik:RadDateTimePicker ID="RefDate" runat="server" 
                Skin="Office2007" CssClass="form-control" Width="270px">
            <TimeView ID="TimeView1" CellSpacing="-1" runat="server"></TimeView>

            <TimePopupButton ImageUrl="" HoverImageUrl=""></TimePopupButton>

            <Calendar ID="Calendar1" UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" 
                    ViewSelectorText="x" Skin="Office2007" runat="server"></Calendar>

            <DateInput ID="DateInput1" DisplayDateFormat="dd-MMM-yy" DateFormat="dd-MMM-yy" LabelWidth="40%" 
                    runat="server" Height="23px"></DateInput>

            <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
            </telerik:RadDateTimePicker>
        </td>
    </tr>
    <tr>
        <td align="left">
            <asp:Label ID="lblFDRefno" runat="server" Text="FD Ref No"></asp:Label></td>
        <td align="left">
            <asp:TextBox ID="txtFDRefNo" runat="server" CssClass="form-control" Width="250px"></asp:TextBox>
        </td>
        <td align="left">
            <asp:Label ID="lblFDRefDate" runat="server" Text="FD RefDate"></asp:Label></td>
        <td align="left">
            <telerik:RadDateTimePicker ID="FDRefDate" runat="server" 
                Skin="Office2007" Width="270px" CssClass="form-control"> 
            <TimeView CellSpacing="-1" runat="server"></TimeView>

            <TimePopupButton ImageUrl="" HoverImageUrl=""></TimePopupButton>

            <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" 
                    ViewSelectorText="x" Skin="Office2007" runat="server"></Calendar>

            <DateInput DisplayDateFormat="dd-MMM-yy" DateFormat="dd-MMM-yy" LabelWidth="40%" runat="server"></DateInput>

            <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
            </telerik:RadDateTimePicker>
        </td>
    </tr>
     <tr>
        <td align="left">
            <asp:Label ID="lblCat" runat="server" Text="Category"></asp:Label></td>
        <td align="left">
            <asp:TextBox ID="txtCategory" runat="server" Width = "250px" CssClass="form-control"></asp:TextBox>
        </td>
        <td align="left">
            <asp:Label ID="lblStatCode" runat="server" Text="Status"></asp:Label></td>
        <td align="left">
            <asp:DropDownList ID="ddlStatus" Width="270px" CssClass="form-control" runat="server"></asp:DropDownList>
        </td>
    </tr>
        
        <tr>
        <td align="left">
            <asp:Label ID="lblRemarks" runat="server" Text="Remarks"></asp:Label></td>
        <td align="left"colspan =  "3">
            <asp:TextBox ID="txtRemarks" runat="server" Width = "700px" 
                TextMode="MultiLine" CssClass="form-control" Font-Names="Arial"></asp:TextBox> </td>
        </tr>
        
    <tr>
        <td align="left">
            <asp:Label ID="lblFupdate" runat="server" Text="FupDate"></asp:Label></td>
        <td align="left">
            <telerik:RadDateTimePicker ID="FupDate" runat="server" 
                Skin="Office2007" CssClass="form-control" Width="250px">
            <TimeView CellSpacing="-1" runat="server"></TimeView>

            <TimePopupButton ImageUrl="" HoverImageUrl=""></TimePopupButton>

            <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" 
                    ViewSelectorText="x" Skin="Office2007" runat="server"></Calendar>

            <DateInput DisplayDateFormat="dd-MMM-yy" DateFormat="dd-MMM-yy" LabelWidth="40%" 
                    runat="server"></DateInput>

            <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
            </telerik:RadDateTimePicker>
        </td>
        <td align="left">
            <asp:Label ID="lblTotal" runat="server" Text="Total"></asp:Label></td>
        <td align="left">
            <asp:TextBox ID="txtTotal" runat="server" CssClass="form-control" Width = "270px"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td align="left">
            <asp:Label ID="lblAmount" runat="server" Text="Amount"></asp:Label></td>
        <td align="left">
            <asp:TextBox ID="txtAmount" runat="server" CssClass="form-control" Width="250px"></asp:TextBox>
        </td>
        <td align="left">
            <asp:Label ID="lblSentto" runat="server" Text="Sent To"></asp:Label></td>
        <td align="left">
            <asp:TextBox ID="txtSentto" CssClass="form-control" runat="server" Width = "270px"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td align="left">
            <asp:Label ID="lblfYear" runat="server" Text="Finanical Year"></asp:Label></td>
        
        <td align="left">
            <asp:TextBox ID="txtfYear" runat="server" CssClass="form-control" Width = "250px"></asp:TextBox>
        </td>
        
        <td align="left">
            <asp:Label ID="lblIncurred" runat="server" Text="Amount Incurred"></asp:Label></td>
        
        <td align="left">
            <asp:TextBox ID="txtIncurred" runat="server" Width = "270px" CssClass="form-control"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td align="left">
            <asp:Label ID="lblBalance" runat="server" Text="Balance"></asp:Label></td>
        
        <td align="left">
            <asp:TextBox ID="txtBalance" runat="server" Width = "250px" CssClass="form-control"></asp:TextBox>
        </td>
        
            <td align="left">
            <asp:Label ID="lblCountry" runat="server" Text="Country"></asp:Label> </td>
        
        <td align="left">
        
            <asp:DropDownList ID="ddlCountry" runat="server" CssClass="form-control" Width="250px">
            </asp:DropDownList>
        </td>
    </tr>

    <tr>
        <td align="left">
            <asp:Label ID="lblSubcat" runat="server" Text="Patient Category"></asp:Label> </td>
        
        <td align="left">
        
            <asp:DropDownList ID="DDLSubCat" runat="server" CssClass="form-control" Width="250px">
            </asp:DropDownList>
        
        </td>
        <td align="left">
            <asp:Label ID="lblHospital" runat="server" Text="Hospital"></asp:Label> </td>
        
        <td align="left">
        
            <asp:DropDownList ID="DDLSentto" runat="server" Width="270px" CssClass="form-control">
            </asp:DropDownList>
        
        </td>
    </tr>

    <tr>
        <td align="left">
            <asp:Label ID="lblRemRemarks" runat="server" Text="Rem_Remarks"></asp:Label> </td>
        <td colspan = "3" align="left">
            <asp:TextBox ID="txtRemRemarks" runat="server" Width="700px" 
                TextMode="MultiLine" Font-Names="Arial" CssClass="form-control"></asp:TextBox>
        </td>
        
    </tr>
    <tr>
                    <td align = "center" colspan = "4">
                        <br />
                        <asp:Button ID="btnEdit" runat="server" Text="Edit" Font-Size="Medium" CssClass="btn btn-primary btn-lg" Width = "100px"   onclick="btnEdit_Click" />
                        <asp:Button ID="btnSave" runat="server" Text="Save"  Font-Size="Medium"  CssClass="btn btn-primary btn-lg" Width = "100px"   onclick="btnSave_Click" />
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel"  Font-Size="Medium"  CssClass="btn btn-primary btn-lg" Width = "100px" onclick="btnCancel_Click" />
                    </td>
                    </tr>

    </table>
</div>
</asp:Content>

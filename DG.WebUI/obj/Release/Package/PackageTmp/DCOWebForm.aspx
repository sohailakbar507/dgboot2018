﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HospitalsMaster.Master" AutoEventWireup="true" CodeBehind="DCOWebForm.aspx.cs" Inherits="DG.WebUI.DCOWebForm" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
        <style type="text/css">
        .panel {
    border: 0;
}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
  <div class="container" style="position:center">
  <div class="row">
   <%--<div class="Absolute-Center is-Responsive">--%>
<div class="panel panel-default">
                      <div class="panel-body">
                         
<asp:ScriptManager ID="ScriptManager1" runat="server">
</asp:ScriptManager>
<div>
        <table width="100%">
            <tr>
                <td>
                     <h1> &nbsp;D.C Update Form </h1>
                </td>
                
                <td align="right">
                    <asp:Label ID="lblDCName" runat="server" Font-Bold="True" Font-Names="Arial Black" Font-Size="X-Large"></asp:Label>
                    <asp:Label ID="lblRecords" runat="server" Font-Names="Arial Black" Font-Size="Large"></asp:Label>                 
                </td>
            </tr>



        </table>

</div>

<%--<div>
        <table width="60%" cellpadding = "1" cellspacing = "0" border = "0px" style= "border-collapse:collapse">
        <tr> 
            <td>
                
                <asp:LinkButton ID="lnkBtnDelete" runat="server" CommandName = "" 
                    ToolTip = "Delete a Record" onclick="lnkBtnDelete_Click"><img src="Images/Misc-Delete-Database-icon.png" border = "" alt=""/></asp:LinkButton>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <asp:LinkButton ID="lnkBtnFirst" CommandName="" runat="server" 
                    onclick="lnkBtnFirst_Click" ToolTip = "Goto First Page">  <img src="Images/Actions-go-first-icon.png" border = "" alt = ""/></asp:LinkButton>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <asp:LinkButton ID="lnkBtnLast" runat="server" onclick="lnkBtnLast_Click" ToolTip= "Goto Last Page"><img src="Images/Actions-go-last-icon.png" border = "" alt = ""/></asp:LinkButton>
                
            </td></tr></table>
       </div>--%>
<div>
    <table align="center" width="60%" cellpadding = "1" cellspacing = "1" border = "0px" style= "border-collapse:collapse">
            <tr>
                <td colspan="2">
                    <asp:Label ID="lblFaNo" runat="server" Text="FaNo."></asp:Label></td>
                <td colspan="2" align="center"><asp:TextBox ID="txtFaNo" runat="server" CssClass="form-control" Width="250px"></asp:TextBox>
                </td>
            </tr>
            </table>
    
    <table  width="100%" cellpadding = "0" cellspacing = "0" border = "0px" style= "border-collapse:collapse">
        <tr>
            <td colspan="4" align="center">
            <br />
            <%--<asp:RadioButtonList ID="RadioButtonList1" Width="850px" runat="server" 
            RepeatDirection="Horizontal" AutoPostBack="True" 
            onselectedindexchanged="RadioButtonList1_SelectedIndexChanged" >
            
            <asp:ListItem Value = "rdoFaNowise" Selected = "True">FaNo Wise</asp:ListItem>
            
        </asp:RadioButtonList>--%>
       
    </td>
</tr>
        <tr>
                <td colspan="4" align="center">
                    <asp:LinkButton ID="lnkBtnSearch" runat="server" ToolTip = "Search Button" onclick="lnkBtnSearch_Click"><img src="Images/Zoom-icon.png" alt = ""/> </asp:LinkButton>
                </td>
            </tr>
        <tr>
            <td><div class="dataTable_wrapper">
            <asp:GridView ID="dgvDG" runat="server" AllowPaging="True" PageSize="20"
            onpageindexchanging="dgvDG_PageIndexChanging" 
            onselectedindexchanged="dgvDG_SelectedIndexChanged" 
            AutoGenerateColumns="False" onrowcancelingedit="dgvDG_RowCancelingEdit" 
            onrowupdating="dgvDG_RowUpdating" DataKeyNames="DGId" 
            CssClass="table table-striped table-bordered table-hover">
           
               <Columns>
                   
                   <asp:TemplateField>
                        <ItemTemplate>
                            <asp:CheckBox ID = "cbRows" runat = "server" />
                        </ItemTemplate>
                   </asp:TemplateField>
                   
                   <asp:TemplateField HeaderText="DGId" Visible="false">
                   <ItemTemplate>
                       <asp:Label ID="lblDGId" runat="server" Text= '<%#Eval("DGId")%>'></asp:Label>
                   </ItemTemplate>
                   
                   </asp:TemplateField>
                   
                   
                   <asp:TemplateField HeaderText="FANo.">
                   <ItemTemplate>
                       <asp:Label ID="lblFaNo" runat="server" Text='<%#Eval("fano")%>'></asp:Label>
                   </ItemTemplate>
                   </asp:TemplateField>
                   <asp:TemplateField HeaderText="Applicant Name">
                   <ItemTemplate>
                       <asp:Label ID="lblAppname" runat="server" Text='<%#Eval("PATIENTINFO")%>'></asp:Label>
                   </ItemTemplate>
                   <ControlStyle Width="350px" />
                   <ItemStyle Width="350px" />
                   </asp:TemplateField> 
                   <asp:TemplateField HeaderText="Amount">
                       <ItemTemplate>
                           <asp:Label ID="lblAmount" runat="server" Text='<%#Eval("amount")%>'></asp:Label>
                       </ItemTemplate>
                   </asp:TemplateField>
                   <asp:TemplateField HeaderText="Cheque No.">
                       <ItemTemplate>
                           <asp:Label ID="lblchqno" runat="server" Text='<%#Eval("chqno")%>'></asp:Label>
                       </ItemTemplate>
                        </asp:TemplateField>
                       <asp:TemplateField HeaderText="Cheque Date">
                       <ItemTemplate>
                           <asp:Label ID="lblchqdate" runat="server" Text='<%#Eval("chqdate")%>'></asp:Label>
                       </ItemTemplate>
                   </asp:TemplateField>
                   
                   <asp:TemplateField HeaderText="Remarks">
                       <ItemTemplate>
                           <asp:Label ID="lblremarks" runat="server" Text='<%#Eval("dcremarks")%>'></asp:Label>
                       </ItemTemplate>
                   </asp:TemplateField>
                                     
                   
                   <asp:TemplateField HeaderText="Edit">
                   <ItemTemplate>
                       <asp:LinkButton ID="LnkBtnEdit" CommandName = "update" runat="server"><img src = "Images/Text-Edit-icon.png" alt = "" /> </asp:LinkButton>
                   </ItemTemplate>
                   </asp:TemplateField>
                   
                   
               </Columns>
              
                 <PagerSettings FirstPageText=">>" LastPageText="<<" />
                <PagerStyle HorizontalAlign = "right" CssClass = "pagination-ys" />

               
        </asp:GridView>
        </div>
        </td>
      </tr>

    </table>
    </div></div></div></div>
</div>
   
</asp:Content>

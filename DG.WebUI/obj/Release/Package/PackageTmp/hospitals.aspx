﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DGMain.Master" AutoEventWireup="true" CodeBehind="hospitals.aspx.cs" Inherits="DG.WebUI.hospitals" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<link rel="Stylesheet" href="styles/bootstrap.css" />
<script src="js/bootstrap.js" type="text/jscript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
 <table width = "70%" class="table" align = "center" cellpadding = "0" cellspacing = "0" style = "border:1px dashed white">
    <tr>
        
        <td align="left">
            <asp:Label ID="lblFANo" runat="server" Text="FANo"></asp:Label></td>
        <td align="left">
            <asp:TextBox ID="txtFano" runat="server" Width = "250px" Height="25px" 
                BackColor="#D1D1BA"></asp:TextBox> </td>
        <td align="left">
            <asp:Label ID="lblPageNo" runat="server" Text="PageNo"></asp:Label></td>
        <td align="left" colspan="3">
            <asp:TextBox ID="txtPageNo" runat="server" Width = "250px" Height="25px" 
                BackColor="#D1D1BA"></asp:TextBox>
        </td>

    </tr>
    <tr>
        <td align="left">
            <asp:Label ID="lblRegNo" runat="server" Text="Hospital Reg.No"></asp:Label></td>
        <td align="left">
            <asp:TextBox ID="txtRegNo" runat="server" Width = "250px" Height="25px" 
                BackColor="#D1D1BA"></asp:TextBox> </td>
    </tr>
    
    <tr>
        <td align="left">
            <asp:Label ID="lblAppname" runat="server" Text="Applicant Name"></asp:Label></td>
        <td align="left">
            <asp:TextBox ID="txtAppname" runat="server" Width="250px" Height="25px" 
                BackColor="#D1D1BA"></asp:TextBox>
        </td>
        <td align="left">
            <asp:Label ID="lblAddress" runat="server" Text="Address"></asp:Label></td>
        <td align="left">
            <asp:TextBox ID="txtAddress" runat="server" Width = "350px" 
                TextMode="MultiLine" Font-Names="Arial" BackColor="#D1D1BA" Height="31px"></asp:TextBox>
        </td>
    </tr>

    <tr>
        <td align="left">
            <asp:Label ID="lblLetterNo" runat="server" Text="LetterNo"></asp:Label></td>
        <td align="left">
            <asp:TextBox ID="txtLetterNo" runat="server" Width="250px" Height="25px" 
                BackColor="#D1D1BA"></asp:TextBox>
        </td>
         <td align="left">
            <asp:Label ID="lblLetDate" runat="server" Text="LetterDate"></asp:Label></td>
        
        <td align="left">
            <telerik:RadDateTimePicker ID="dtLetterDate" runat="server" Width="250px" Height="30px">
            </telerik:RadDateTimePicker>
        </td>
    </tr>
    <tr>
        <td align="left">
            <asp:Label ID="lblRefto" runat="server" Text = "Reffered To"></asp:Label></td>
        <td align="left">
            <asp:DropDownList ID="ddlRefferedTo" runat="server" Width = "250px" Height="25px">
            </asp:DropDownList>
        </td>
        <td align="left">
            <asp:Label ID="lblRefOn" runat="server" Text="Reffered Date"></asp:Label></td>
        
        <td align="left">
            <telerik:RadDateTimePicker ID="dtRefferedDate" runat="server" Width="250px" Height="30px">
            </telerik:RadDateTimePicker>
        </td>       
                 
    </tr>
    <tr>
        <td align="left">
            <asp:Label ID="lblExpences" runat="server" Text = "Expences / Amount Incurred"></asp:Label></td>
        <td align="left">
            <asp:TextBox ID="txtAmount" runat="server" Width = "250px" Height="25px" 
                BackColor="#E6AABC"></asp:TextBox>
        </td>
        <td align="left">
            <asp:Label ID="lblUpdatedOn" runat="server" Text="Updated On"></asp:Label></td>
        
        <td align="left">
            <telerik:RadDateTimePicker ID="rdtUpdatedOn" runat="server" Width="250px" 
                Height="30px" BackColor="#E6AABC">
            </telerik:RadDateTimePicker>
        </td>       
                 
    </tr>

    <tr>
        <td align="left">
            <asp:Label ID="lblStatus" runat="server" Text = "Status / Stage"></asp:Label></td>
        <td align="left">
            <asp:DropDownList ID="ddlStatus" runat="server" Width = "250px" Height="25px" 
                BackColor="#E6AABC">
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td align="left">
            <asp:Label ID="lblTentativeDate" runat="server" Text="Tentative Date"></asp:Label></td>
        <td align="left">
                <telerik:RadDateTimePicker ID="rdtTentativeDate" runat="server" Width="250px" 
                Height="30px" BackColor="#E6AABC">
            </telerik:RadDateTimePicker>
        
        </td>
        <td align="left">
            <asp:Label ID="lblActualDate" runat="server" Text="Actual Date"></asp:Label></td>
        <td align="left">
                <telerik:RadDateTimePicker ID="rdtActualDate" runat="server" Width="250px" 
                Height="30px" BackColor="#E6AABC">
            </telerik:RadDateTimePicker>
        
        </td>



    </tr>
    <tr>
        <td align="left">
            <asp:Label ID="lblRecvdchqno" runat="server" Text = "Received ChqNo"></asp:Label></td>
        <td align="left">
            <asp:TextBox ID="txtRecvdChqno" runat="server" Width = "250px" Height="25px" 
                BackColor="#E6AABC"></asp:TextBox>
        </td>
        <td align="left">
            <asp:Label ID="RecvdChqDate" runat="server" Text="Received ChqDate"></asp:Label></td>
        
        <td align="left">
            <telerik:RadDateTimePicker ID="rdRecvdChqDate" runat="server" Width="250px" 
                Height="30px" BackColor="#E6AABC">
            </telerik:RadDateTimePicker>
        </td>
        
                 
    </tr>

    <tr>
        <td align="left">
            <asp:Label ID="lblBalReturn" runat="server" Text = "Balance Returned"></asp:Label></td>
        <td align="left">
            <asp:TextBox ID="txtBalReturned" runat="server" Width = "250px" Height="25px" 
                BackColor="#E6AABC"></asp:TextBox>
        </td>
        </tr>

        <tr>
        <td align="left">
            <asp:Label ID="lblBalChqNo" runat="server" Text = " Balance ChqNo"></asp:Label></td>
        <td align="left">
            <asp:TextBox ID="txtBalChqNo" runat="server" Width = "250px" Height="25px" 
                BackColor="#E6AABC"></asp:TextBox>
        </td>
        <td align="left">
            <asp:Label ID="lblBalChqDate" runat="server" Text="Balance ChqDate"></asp:Label></td>
        
        <td align="left">
            <telerik:RadDateTimePicker ID="rdBalChqDate" runat="server" Width="250px" 
                Height="30px" BackColor="#E6AABC">
            </telerik:RadDateTimePicker>
        </td>
        
                 
    </tr>

    <tr>
        
        <td align="left">
            <asp:Label ID="lblOpinion" runat="server" Text="Opinion"></asp:Label></td>
         <td colspan="3" align="left">   
             <asp:TextBox ID="txtOpinion" runat="server" Width = "520px" 
                TextMode="MultiLine" Font-Names="Arial" BackColor="#D1D1BA" Height="50px"></asp:TextBox></td>
    </tr>


     <tr>
        <td align="left">
            <asp:Label ID="lblRemarks" runat="server" Text="Remarks"></asp:Label></td>
         <td colspan="3" align="left">   
             <asp:TextBox ID="txtRemarks" runat="server" Width = "520px" 
                TextMode="MultiLine" Font-Names="Arial" BackColor="#D1D1BA" Height="50px"></asp:TextBox></td>
     </tr>
        
    <tr>
                    <td align = "center" colspan = "4">
                        <br />
                        
                        <asp:Button ID="btnSave" runat="server" CssClass="btn btn-primary btn-lg" Text="Save"  
                            Height="30px" Width="85px" onclick="btnSave_Click" 
                            Font-Bold="True" />
                        <asp:Button ID="btnCancel" runat="server" CssClass="btn btn-primary btn-lg" Text="Cancel" 
                            Height="30px" Width="85px" onclick="btnCancel_Click" 
                            Font-Bold="True" />
                    </td>
                    </tr>

    </table>
</asp:Content>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DataTransferWebForm.aspx.cs" Inherits="DG.WebUI.DataTransferWebForm" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Data Transfer</title>
     <link  href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
</head>
<body>

    <form id="form1" runat="server">
        <div class="container">
             <br />
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                     <div class="panel panel-primary">
                    <div class="panel-heading text-center">
                        <h4>Export Data </h4>
                    </div>
                    <div class =" panel-body">
                        <div class="row">
                        <div class="col-md-6">
                             FaNo From
                      <asp:TextBox ID="txtfanofrom" CssClass="form-control input-sm" runat="server"></asp:TextBox>
                        </div>
                        <div class="col-md-6">
                             FaNo To:
                      <asp:TextBox CssClass="form-control input-sm" ID="txtfanoto" runat="server"></asp:TextBox>
                        </div>
                            </div>
                        <br />
                         <div class="row">
                        <div  class="col-md-6">
                            <asp:LinkButton ID="btnExcel" CssClass="btn-primary btn-sm pull-right" runat="server" OnClick="btnExcel_Click">Create Excel File</asp:LinkButton>
                        </div>
                        <div class="col-md-6">
                            <asp:LinkButton ID="btnFoxFile" CssClass="btn-primary btn-sm pull-left" runat="server" >Create Foxpro File</asp:LinkButton>
                        </div>
                             </div>
                    </div>
                </div>
                </div>
            </div>
               
            </div>
    </form>
     <script type="text/javascript" src="bower_components/jquery/dist/jquery.min.js"></script>
    <script type="text/javascript" src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
</body>
</html>

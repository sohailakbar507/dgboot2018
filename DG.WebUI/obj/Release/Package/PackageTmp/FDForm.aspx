﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DGMasterPage.Master" AutoEventWireup="true" CodeBehind="FDForm.aspx.cs" Inherits="DG.WebUI.FDForm" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<asp:ScriptManager ID="ScriptManager1" runat="server">
</asp:ScriptManager>


<h1 align = "center"> FD Update Form </h1>
<div>
    <table align = "center" width="70%"  cellpadding = "0" cellspacing = "0" style = "border:1px dashed white"  bgcolor="#CC8099">
        
         <tr>
                        <td>
                            <asp:Label ID="lblFaNo" runat="server" Text="FaNo" Width= "100px"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtFaNo" runat="server" Width = "200px" CssClass="form-control" ReadOnly="True"></asp:TextBox>
                        </td>
                        <td>
                            <asp:Label ID="lblAppName" runat="server" Text="Applicant Name"></asp:Label>
                        </td>
                        <td align = "left">
                            <asp:TextBox ID="txtAppName" runat="server" Width = "200px" CssClass="form-control"></asp:TextBox>
                        </td></tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblAmount" runat="server" Text="Amount"></asp:Label>
                            </td>
                            <td align = "left">
                            <asp:TextBox ID="txtamount" runat="server" Width = "200px" CssClass="form-control"></asp:TextBox>
                            </td>
                            <td>
                                <asp:Label ID="lblAmountIncurred" runat="server" Text="Amount Incurred"></asp:Label>
                            </td>
                            <td align = "left">
                            <asp:TextBox ID="txtAmountIncurred" runat="server" Width = "200px" CssClass="form-control"></asp:TextBox>
                            </td>


                        </tr>

                        <tr>
                            <td>
                                <asp:Label ID="lblfdrefno" runat="server" Text="FD Ref No"></asp:Label>
                            </td>
                            <td align = "left">
                            <asp:TextBox ID="txtfdrefno" runat="server" Width = "200px" CssClass="form-control"></asp:TextBox>
                            </td>
                            <td>
                                <asp:Label ID="lblfdrefdate" runat="server" Text="FD Refdate"></asp:Label>
                            </td>
                            <td align = "left">
                            
                                <telerik:RadDateTimePicker ID="rdFDDate" runat="server" Skin="Forest">
                                <TimeView CellSpacing="-1" runat = "server"></TimeView>

                                <TimePopupButton ImageUrl="" HoverImageUrl=""></TimePopupButton>

                                <Calendar UseRowHeadersAsSelectors="False" runat= "server" UseColumnHeadersAsSelectors="False" ViewSelectorText="x" Skin="Forest"></Calendar>

                                <DateInput DisplayDateFormat="dd-MMM-yy" DateFormat="dd-MMM-yy" LabelWidth="40%" runat="server"></DateInput>

                                <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                                </telerik:RadDateTimePicker>
                            </td>

                        
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblBalance" runat="server" Text="Balance"></asp:Label>
                            </td>
                            <td align = "left">
                            <asp:TextBox ID="txtbalance" runat="server" Width = "200px" CssClass="form-control"></asp:TextBox>
                            </td>


                            <td>
                                <asp:Label ID="lblSentto" runat="server" Text="Sent To"></asp:Label>
                            </td>
                            <td align = "left">
                            <asp:TextBox ID="txtSentto" runat="server" Width = "200px" CssClass="form-control"></asp:TextBox>
                            </td>
                            


                        </tr>
                        
                        <tr>
                             <td align = "center" colspan = "4">
                        <br />
                        <asp:Button ID="btnEdit" runat="server" Text="Edit" Width="100px" 
                                    CssClass="btn btn-primary btn-lg" onclick="btnEdit_Click" />
                        <asp:Button ID="btnSave" runat="server" Text="Save" Width="100px" 
                                     onclick="btnSave_Click" CssClass="btn btn-primary btn-lg" />
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn btn-primary btn-lg"
                                 Width="100px" onclick="btnCancel_Click" />
                            </td>

                        </tr>


                   
</table>
</div>


</asp:Content>

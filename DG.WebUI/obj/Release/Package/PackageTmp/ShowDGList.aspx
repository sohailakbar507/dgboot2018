﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DGMasterPage.Master" AutoEventWireup="true" CodeBehind="ShowDGList.aspx.cs" Inherits="DG.WebUI.ShowDGList" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>All DG Data</h1>
                
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div>
        <table cellpadding = "1" cellspacing = "0" border = "0px" style= "border-collapse:collapse">
        <tr> 
            <td>
                
                <asp:LinkButton ID="lnkBtnDelete" runat="server" CommandName = "" 
                    ToolTip = "Delete a Record" onclick="lnkBtnDelete_Click"><img src="Images/Misc-Delete-Database-icon.png" border = "" alt=""/></asp:LinkButton>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <asp:LinkButton ID="lnkBtnFirst" CommandName="" runat="server" 
                    onclick="lnkBtnFirst_Click" ToolTip = "Goto First Page">  <img src="Images/Actions-go-first-icon.png" border = "" alt = ""/></asp:LinkButton>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <asp:LinkButton ID="lnkBtnLast" runat="server" onclick="lnkBtnLast_Click" ToolTip= "Goto Last Page"><img src="Images/Actions-go-last-icon.png" border = "" alt = ""/></asp:LinkButton>
                
            </td></tr></table>
       </div>
    <div>
          <table width = "60%" align = "center" cellpadding = "0" cellspacing = "0" style = "border:1px dashed white">
           <tr>
                <td align="center"><asp:Label ID="lblFaNo" runat="server" Text="FaNo."></asp:Label></td>
                <td align="left"><asp:TextBox ID="txtFaNo" Width="200px" runat="server" CssClass="form-control"></asp:TextBox> </td>
                     
           </tr>
                    
        <tr>
                <td align = "center" valign = "top">
                    <asp:Label ID="lblbyName" runat="server" Text="Enter your Text here:"></asp:Label>
                    <asp:TextBox ID="txtbyName" runat="server" CssClass="form-control" Width="215px"></asp:TextBox>
                    
                &nbsp;&nbsp;
                 
                    </td> 
                          
        </tr>
        <tr>
            <td align = "center" valign = "top">
                <asp:Label ID="lblname" runat="server" Text="Name"></asp:Label> 
                <asp:TextBox ID="txtName" runat="server" Width = "200px" CssClass="form-control"></asp:TextBox>
                <asp:Label ID="lblAddress" runat="server" Text="Address"></asp:Label> 
                <asp:TextBox ID="txtAddress" runat="server" Width = "250px" CssClass="form-control"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="center">
            <asp:RadioButtonList ID="RadioButtonList1" runat="server" 
            RepeatDirection="Horizontal" AutoPostBack="True" 
            onselectedindexchanged="RadioButtonList1_SelectedIndexChanged" 
            Font-Size="Small" ForeColor="#663300">
            <asp:ListItem Value = "rdoFaNowise" Selected = "True">FaNo Wise</asp:ListItem>
            <asp:ListItem Value = "rdoSearchbyName">Searching by Name</asp:ListItem>
            <asp:ListItem Value = "rdochqwise">Cheque Wise</asp:ListItem>
            <asp:ListItem Value = "rdocnicwise">CNIC # Wise</asp:ListItem>
            <asp:ListItem Value = "rdoNameandAddress">by Name & Address</asp:ListItem>
            

        </asp:RadioButtonList>
    </td>
</tr>
            <tr>
                <td align="center">
                    <asp:LinkButton ID="lnkBtnSearch" runat="server" ToolTip = "Search Button" onclick="lnkBtnSearch_Click"><img src="Images/Zoom-icon.png" alt = ""/> </asp:LinkButton>
                </td>
            </tr>
          
           
         <tr>
            <td><div class="dataTable_wrapper">
           <asp:GridView ID="dgvDG" runat="server" AllowPaging="True" 
            onpageindexchanging="dgvDG_PageIndexChanging" 
            onselectedindexchanged="dgvDG_SelectedIndexChanged" 
            AutoGenerateColumns="False" onrowcancelingedit="dgvDG_RowCancelingEdit" 
                    onrowupdating="dgvDG_RowUpdating" DataKeyNames="DGId" 
                    CssClass="table table-striped table-bordered table-hover">
           
               <Columns>
                   
                   <asp:TemplateField>
                        <ItemTemplate>
                            <asp:CheckBox ID = "cbRows" runat = "server" />
                        </ItemTemplate>
                   </asp:TemplateField>
                   
                   <asp:TemplateField HeaderText="DGId" Visible="False">
                   <ItemTemplate>
                       <asp:Label ID="lblDGId" runat="server" Text= '<%#Eval("DGId")%>'></asp:Label>
                   </ItemTemplate>
                   
                   </asp:TemplateField>
                   
                   
                   <asp:TemplateField HeaderText="FANo.">
                   <ItemTemplate>
                       <asp:Label ID="lblFaNo" runat="server" Text='<%#Eval("fano")%>'></asp:Label>
                   </ItemTemplate>
                   </asp:TemplateField>
                   
                   <asp:TemplateField HeaderText="Refdate">
                   <ItemTemplate>
                       <asp:Label ID="lblRefdate" runat="server" Text='<%#Eval("refdate","{0:dd/MM/yyyy}") %>'></asp:Label>
                   </ItemTemplate>
                       
                       <ControlStyle Width="80px" />
                       
                   </asp:TemplateField>
                   
                   <asp:TemplateField HeaderText="Applicant Name">
                   <ItemTemplate>
                       <asp:Label ID="lblAppname" runat="server" Text='<%#Eval("appname")%>'></asp:Label>
                   </ItemTemplate>
                   
                       <ControlStyle Width="120px" />
                   
                   </asp:TemplateField>
                   
                   <asp:TemplateField HeaderText="Address">
                   <ItemTemplate>
                       <asp:Label ID = "lblAddress" runat="server" Text='<%#Eval("address")%>'></asp:Label>
                       
                   </ItemTemplate>
                       <ControlStyle Width="200px" />
                   </asp:TemplateField>
                   
                   <asp:TemplateField HeaderText="CNIC">
                       <ItemTemplate>
                           <asp:Label ID="lblCNIC" runat="server" Text='<%#Eval("CNIC")%>'></asp:Label>
                       </ItemTemplate>
                   </asp:TemplateField>
                   
                   
                   <asp:TemplateField HeaderText="Cheque No.">
                       <ItemTemplate>
                           <asp:Label ID="lblChequeno" runat="server" Text='<%#Eval("chqno")%>'></asp:Label>
                       </ItemTemplate>
                   </asp:TemplateField>

                   <asp:TemplateField HeaderText="Cheque Date">
                   <ItemTemplate>
                       <asp:Label ID="lblChqDate" runat="server" Text='<%#Eval("chqdate","{0:dd/MM/yyyy}") %>'></asp:Label>
                   </ItemTemplate>
                   </asp:TemplateField>

                   
                   <asp:TemplateField HeaderText="Amount">
                   
                     <ItemTemplate>
                         <asp:Label ID="lblAmount" runat="server" Text='<%#Eval("Amount")%>'></asp:Label>
                       </ItemTemplate>
                   
                   
                   </asp:TemplateField>

                   
                   <asp:TemplateField HeaderText="Department Name">
                   <ItemTemplate>
                       <asp:Label ID="lblDeptName" runat="server" Text='<%#Eval("deptname") %>'></asp:Label>
                   </ItemTemplate>
                   <ItemStyle HorizontalAlign = "Center" />
                   </asp:TemplateField>
                   
                                  
                   <asp:TemplateField HeaderText="Status">
                   <ItemTemplate>
            
            
                       <asp:Label ID="lblStatus" runat="server" Text='<%#Eval("Description") %>'></asp:Label>
                   </ItemTemplate>
                   <ItemStyle HorizontalAlign = "Center" />
                   </asp:TemplateField>
                   
                   <asp:TemplateField HeaderText="Edit">
                   <ItemTemplate>
                       <asp:LinkButton ID="LnkBtnEdit" CommandName = "update" runat="server"><img src = "Images/Text-Edit-icon.png" alt = "" /> </asp:LinkButton>
                   </ItemTemplate>
                   </asp:TemplateField>
                   
                   
               </Columns>
                <PagerStyle HorizontalAlign = "right" CssClass = "pagination-ys" />

        </asp:GridView>
        </div>
        </td>
      </tr>
      
</table> </div>
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DGMain.Master" AutoEventWireup="true" CodeBehind="STIReportPage.aspx.cs" Inherits="DG.WebUI.STIReportPage" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
    
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Sohail Akbar">
    <link  href="styles/default.css" rel="stylesheet" />
     <!-- Bootstrap Core CSS -->
    <link  href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />

    <!-- MetisMenu CSS -->
    <link  href="bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet" />

    <!-- Timeline CSS -->
    <link  href="dist/css/timeline.css" rel="stylesheet" />
    <!-- Custom CSS -->
    <link href="dist/css/sb-admin-2.css" rel="stylesheet" />

    <!-- DataTables CSS -->
    <link href="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet" />

    <!-- DataTables Responsive CSS -->
    <link href="../bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet" />

    <!-- Custom Fonts -->
    <link  href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1 align = "center"> 
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
    </h1>
<h1 align = "center"> Reports Page</h1>
     <p> &nbsp;</p>
<asp:Panel ID = "ReportPanel" runat="server" DefaultButton="btnSubmit"> 
<table style="border:1px dashed white;border-collapse:collapse;" align="center" width="700" cellpadding="2" cellspacing="1">
    <tr>
        <td colspan = "4" align = "left"> 
            <br />
            <br />
        </td>
    </tr>

    <tr>
        <td align ="left" width="100px">
            <asp:Label ID="lblFanoFrom" runat="server" Text="FA No. From:"></asp:Label> </td>
        <td align = "left">
            
            <asp:TextBox ID="txtFANoFrom" runat="server" CssClass="form-control"></asp:TextBox> 
            
        </td>
            <td align="center" width="100px">
            <asp:Label ID="lblFanoTo" runat="server" Text=" To:"></asp:Label> </td>
         <td align="left">
            <asp:TextBox ID="txtFaNoTo" runat="server" CssClass="form-control"></asp:TextBox></td>

    </tr>
    <tr>
            <td align = "left">
                <asp:Label ID="lblDateFrom" runat="server" Text="Date From:" ></asp:Label></td>
            <td align = "left">    
                <telerik:RadDateTimePicker ID="RadDateTimePicker1" runat="server" Culture="en-US" CssClass="form-control">
                    <TimeView CellSpacing="-1">
                    </TimeView>
                    <TimePopupButton HoverImageUrl="" ImageUrl="" />
                    <Calendar EnableWeekends="True" runat="server" FastNavigationNextText="&amp;lt;&amp;lt;" UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False">
                    </Calendar>
                    <DateInput DateFormat="dd-MM-yyyy" DisplayDateFormat="dd-MM-yyyy" runat="server" LabelWidth="40%">
                        <EmptyMessageStyle Resize="None" />
                        <ReadOnlyStyle Resize="None" />
                        <FocusedStyle Resize="None" />
                        <DisabledStyle Resize="None" />
                        <InvalidStyle Resize="None" />
                        <HoveredStyle Resize="None" />
                        <EnabledStyle Resize="None" />
                    </DateInput>
                    <DatePopupButton HoverImageUrl="" ImageUrl="" />
                </telerik:RadDateTimePicker>
            </td>
            <td align="Center" width="100px">
                <asp:Label ID="lblDateTo" runat="server" Text=" To:" ></asp:Label> </td>
            <td align = "left">
                <telerik:RadDateTimePicker ID="RadDateTimePicker2" runat="server" Culture="en-US" CssClass="form-control">
                    <TimeView CellSpacing="-1">
                    </TimeView>
                    <TimePopupButton HoverImageUrl="" ImageUrl="" />
                    <Calendar runat="server" EnableWeekends="True" FastNavigationNextText="&amp;lt;&amp;lt;" UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False">
                    </Calendar>
                    <DateInput DateFormat="dd-MM-yyyy" DisplayDateFormat="dd-MM-yyyy" runat="server" LabelWidth="40%">
                        <EmptyMessageStyle Resize="None" />
                        <ReadOnlyStyle Resize="None" />
                        <FocusedStyle Resize="None" />
                        <DisabledStyle Resize="None" />
                        <InvalidStyle Resize="None" />
                        <HoveredStyle Resize="None" />
                        <EnabledStyle Resize="None" />
                    </DateInput>
                    <DatePopupButton HoverImageUrl="" ImageUrl="" />
                </telerik:RadDateTimePicker>
                
            </td>
    </tr>
        <tr>
        <td align ="left" width="100px">
            <asp:Label ID="lblCatCode" runat="server" Text="CatCode:"></asp:Label> </td>
        <td align = "left">
            
            <asp:TextBox ID="txtCatCode" runat="server" CssClass="form-control"></asp:TextBox> 
            
        </td>
            <td align="center" width="100px">
            <asp:Label ID="lblCons" runat="server" Text=" Constituency"></asp:Label> </td>
         <td align="left">
            <asp:TextBox ID="txtCons" runat="server" CssClass="form-control"></asp:TextBox></td>

    </tr>
        <tr>
            <td colspan = "4" align = "center">
                <br /><br />
                <asp:Button ID="btnSubmit" Width="200px" runat="server" Text="SUBMIT" CssClass="btn btn-primary btn-lg" onclick="btnSubmit_Click" />
                <%--<asp:Button ID="btnStatus" runat="server" Text="Change Status" CssClass="btn btn-primary btn-lg" OnClick="btnStatus_Click" />--%>
            </td>
        </tr>
        

</table>
<br /></asp:Panel>
    <!-- jQuery -->
    <script type="text/javascript" src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script type="text/javascript" src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script type="text/javascript" src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script type="text/javascript" src="../bower_components/raphael/raphael-min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script type="text/javascript" src="../dist/js/sb-admin-2.js"></script>

    <script type="text/javascript" src="../bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" src="JS/Menu-Collapse.js"></script>

   <!--<script>
       $(document).ready(function () {
       $('#ContentPlaceHolder1_DgSearch').DataTable({
       "responsive": true

       });
       $("#menu-toggle").click(function () {
       $("#side-menu").hide();
       });
       });

    </script>-->
</asp:Content>

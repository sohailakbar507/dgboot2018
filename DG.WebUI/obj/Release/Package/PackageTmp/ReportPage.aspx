﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DGMasterPage.Master" AutoEventWireup="true" CodeBehind="ReportPage.aspx.cs" Inherits="DG.WebUI.ReportPage" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1 align = "center"> 
      <%--  <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>--%>
    </h1>
<h1 align = "center"> Report Page </h1>
     <p> &nbsp;</p>
<asp:Panel ID = "ReportPanel" runat="server" DefaultButton="btnSubmit"> 
<table style="border:1px dashed white;border-collapse:collapse;" align="center" width="600" cellpadding="2" cellspacing="1">
    <tr>
        <td colspan = "4" align = "left"> 
            <br />
            <br />
        </td>
    </tr>

    <tr>
        <td align = "left">
            <asp:Label ID="lblFanoFrom" runat="server" Text="FA No. From:"></asp:Label> </td>
        <td align = "left">
            
            <asp:TextBox ID="txtFANoFrom" runat="server" CssClass="form-control"></asp:TextBox> 
            
        </td>
            <td align="left">
            <asp:Label ID="lblFanoTo" runat="server" Text=" To:"></asp:Label> </td>
         <td align="left">
            <asp:TextBox ID="txtFaNoTo" runat="server" CssClass="form-control"></asp:TextBox></td>

    </tr>
    <tr>
            <td align = "left">
                <asp:Label ID="lblDateFrom" runat="server" Text="Date From:"></asp:Label></td>
            <td align = "left">    
                <telerik:RadDateTimePicker ID="DateFrom" CssClass="form-control" Runat="server" 
                    Skin="Office2007">
                <TimeView CellSpacing="-1" runat = "server"></TimeView>

                <TimePopupButton ImageUrl="" HoverImageUrl=""></TimePopupButton>

                <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x" 
                        skin="Office2007" runat="server"></Calendar>

                <DateInput DisplayDateFormat="dd-MMM-yy" DateFormat="dd-MMM-yy" LabelWidth="40%" runat="server"></DateInput>

                <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                </telerik:RadDateTimePicker>
            </td>
            <td align="left">
                <asp:Label ID="lblDateTo" runat="server" Text=" To:"></asp:Label> </td>
            <td align = "left">
                <telerik:RadDateTimePicker ID="DateTo" CssClass="form-control" Runat="server" 
                    Skin="Office2007">
                <TimeView CellSpacing="-1" runat = "server"></TimeView>

                <TimePopupButton ImageUrl="" HoverImageUrl=""></TimePopupButton>

                <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x" 
                        skin="Office2007" runat="server"></Calendar>

                <DateInput DisplayDateFormat="dd-MMM-yy" DateFormat="dd-MMM-yy" LabelWidth="40%" runat="server"></DateInput>

                <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                </telerik:RadDateTimePicker>
            </td>
    </tr>
        <tr>
            <td align="left">
                <asp:Label ID="lblFileNo" runat="server" Text="File No"></asp:Label>
            </td>
            <td align="left">
                <asp:TextBox ID="txtFileNo" runat="server" CssClass="form-control"></asp:TextBox>
            </td>
            <td align="left">
                <asp:Label ID="lblLetCode" runat="server" Text="Letter Code"></asp:Label>
            </td>
            <td align="left">
                <asp:TextBox ID="txtLetCode" runat="server" CssClass="form-control"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="left">
                <asp:Label ID="lblCatCode" runat="server" Text="CatCode"></asp:Label>
            </td>
            <td align="left">
                <asp:TextBox ID="txtCatCode" runat="server" CssClass="form-control"></asp:TextBox>
            </td>
            <td align="left">
                <asp:Label ID="lblCons" runat="server" Text="Cons:"></asp:Label>
            </td>
            <td align="left">
                <asp:TextBox ID="txtCons" runat="server" CssClass="form-control"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan = "4" align = "center">
                <br /><br />
                <asp:Button ID="btnSubmit" runat="server" Text="SUBMIT" Width="150px" CssClass="btn btn-primary btn-lg" onclick="btnSubmit_Click" />
            </td>
        </tr>
        

</table>
<br /></asp:Panel>
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DGMasterPage.Master" AutoEventWireup="true" CodeBehind="UpdateForm.aspx.cs" Inherits="DG.WebUI.UpdateForm" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="Styles/ListViewSheet.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <tr>
        <td>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
 

    <div align = "center">
    <table align = "center" cellpadding = "1" cellspacing = "0" border = "1" style= "border-collapse:collapse">
        <tr>
            <td align="left">
                <asp:Label ID="lblFileNo" runat="server" Text="File No"></asp:Label>
               <asp:TextBox ID="txtFileNo" runat="server"></asp:TextBox>
            
                <asp:Label ID="lblLetCode" runat="server" Text="Letter Code"></asp:Label>
                <asp:TextBox ID="txtLetCode" runat="server"></asp:TextBox>
                <br />
            </td>
        </tr>
        <tr>
            <td align="left">
                <asp:Label ID="lblFromChqDate" runat="server" Text="From Date"></asp:Label>
                <telerik:RadDateTimePicker ID="FromChqDate" runat="server" Skin="Office2007">
                <TimeView CellSpacing="-1" runat="server"></TimeView>

                <TimePopupButton ImageUrl="" HoverImageUrl=""></TimePopupButton>

                <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x" 
                        Skin="Office2007" runat="server"></Calendar>

                <DateInput DisplayDateFormat="dd-MMM-yy" DateFormat="dd-MMM-yy" LabelWidth="40%" runat="server"></DateInput>

                <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                </telerik:RadDateTimePicker>
                
                

                <asp:Label ID="lblTochqDate" runat="server" Text="To Date"></asp:Label>
                <telerik:RadDateTimePicker ID="ToChqDate" runat="server" 
                    Skin="Office2007">
                <TimeView CellSpacing="-1" runat="server"></TimeView>

                <TimePopupButton ImageUrl="" HoverImageUrl=""></TimePopupButton>

                <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x" 
                        Skin="Office2007" runat="server"></Calendar>

                <DateInput DisplayDateFormat="dd-MMM-yy" DateFormat="dd-MMM-yy" LabelWidth="40%" runat="server"></DateInput>

                <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                </telerik:RadDateTimePicker>
                <br />
            </td>

        </tr>
        <tr>
            <td>
                <asp:DataPager ID="DataPager1" runat="server" PagedControlID="DGListView">
                    <Fields>
                        <asp:NextPreviousPagerField ButtonType="Button" ShowFirstPageButton="True" 
                            ShowNextPageButton="False" ShowPreviousPageButton="False" />
                        <asp:NumericPagerField />
                        <asp:NextPreviousPagerField ButtonType="Button" ShowLastPageButton="True" 
                            ShowNextPageButton="False" ShowPreviousPageButton="False" />
                    </Fields>
                </asp:DataPager>
    <asp:ListView ID="DGListView" runat="server" 
                    onpagepropertieschanging="DGListView_PagePropertiesChanging" 
                    onselectedindexchanged="DGListView_SelectedIndexChanged">
        <LayoutTemplate>
             <table border="0" cellpadding="1" class="TableHeader">
                
                <th align="left"><asp:LinkButton ID="lnkFaNo" runat="server">FANo</asp:LinkButton></th>
                <th align="left"><asp:LinkButton ID="lnkAppName" runat="server">Applicant Name</asp:LinkButton></th>
                <th align="left"><asp:LinkButton ID="lnkChequeNo" runat="server">Cheque No</asp:LinkButton></th>
                <th align="left"><asp:LinkButton ID="LinkChequeDate" runat="server">Cheque Date</asp:LinkButton></th>
                <th align="left"><asp:LinkButton ID="LinkLetterCode" runat="server">Letter Code</asp:LinkButton></th>
                <th align="left"><asp:LinkButton ID="LinkFileNo" runat="server">File No</asp:LinkButton></th>
                <th></th>
                </tr>
                <tr id="itemPlaceholder" runat="server"></tr>
            </table>
</LayoutTemplate>
    <ItemTemplate>
          <tr>
           <td><asp:Label runat="server" ID="lblFaNo"><%#Eval("FANo") %></asp:Label></td>
           <td><asp:Label runat="server" ID="lblAppName"><%#Eval("AppName")%> </asp:Label></td>
           <td><asp:Label runat="server" ID="lblChqNo"><%#Eval("chqno")%></asp:Label></td>
           <td><asp:Label runat="server" ID="lblChqDate"><%#Eval("chqdate","{0:dd/MM/yyyy}")%></asp:Label></td>
           <td><asp:Label runat="server" ID="lblLetCode"><%#Eval("letcode")%></asp:Label></td>
           <td><asp:Label runat="server" ID="lblFileNo"><%#Eval("FileNo") %></asp:Label></td>
           <td></td>
          </tr>
          
    </ItemTemplate>
    </asp:ListView>
            </td>
        </tr>
        <tr>
            <td>
                    <asp:Button ID="btnSubmit" runat="server" Text="Submit" 
                    onclick="btnSubmit_Click" />
                    <asp:Button ID="btnCode8" runat="server" Text="Code 8" onclick="btnCode8_Click"/>
                   <asp:Button ID="btnCode12" runat="server" Text="Code 12" onclick="btnCode12_Click"/><br /> <br /><br /><br />
                
            </td> 
          </tr>

          
    </table>
    </div>

    
    <div style="height: 40px">
        <div style="display: inline-block; left: 500px; position:absolute">
        <table>
        <tr>
            <td>
                    <asp:Label ID="lblMessage" runat="server" BorderStyle="Solid" Font-Bold="True" 
                        Font-Size="Medium" ForeColor="#FFCC00" BorderColor="#003366" 
                        BorderWidth="4px"></asp:Label>
                

                </td>
          </tr>
          </table>
          </div>
          </div>
        </td>
    </tr>
</asp:Content>

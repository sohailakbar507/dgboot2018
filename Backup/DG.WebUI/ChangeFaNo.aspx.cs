﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DGModel;

namespace DG.WebUI
{
    public partial class ChangeFaNo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                lblMessage.Visible = false;
            }

        }

        protected void Change_Click(object sender, EventArgs e)
        {
            DGEntities1 DG = new DGEntities1();

            int newfano = Convert.ToInt32(txtNewFaNo.Text);
            int oldfano = Convert.ToInt32(txtOldFaNo.Text);
            //var result = DG.ChangeFaNo(12028,40000);
            var result = DG.ChangeFaNo(oldfano, newfano);
            lblMessage.Visible = true;
            lblMessage.Text = "*** FaNo Changed ***";

            

        }
    }
}
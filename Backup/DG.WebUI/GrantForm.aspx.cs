﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DGModel;

namespace DG.WebUI
{
    public partial class GrantForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {


                UploadCountry();
                UploadHospital();
                UploadCat();
                this.ddlCountry.Items.Insert(0, new ListItem("Select Country"));
                this.DDLSentto.Items.Insert(0, new ListItem("Select Hospital"));
                this.DDLSubCat.Items.Insert(0, new ListItem("Select Disease"));
                
                txtfind.Focus();

                if (Request["DGID"] != null)
                {

                }
                if (Request["DGID"] == null)
                {

                }
            }
        }

        private void UploadCountry()
        {
            DGEntities1 dg = new DGEntities1();
            var cntry = (from c in dg.DGCountries select c);
            ddlCountry.DataSource = cntry;
            ddlCountry.DataValueField = "CountryId";
            ddlCountry.DataTextField = "CountryName";
            ddlCountry.DataBind();
        }

        private void UploadCat()
        {
            DGEntities1 dg = new DGEntities1();
            var cat = (from b in dg.Subcats select b);
            DDLSubCat.DataSource = cat;
            DDLSubCat.DataValueField = "subcatid";
            DDLSubCat.DataTextField = "SubCategory";
            DDLSubCat.DataBind();
        }

        private void UploadHospital()
        {
            DGEntities1 dg = new DGEntities1();
            var hosp = (from h in dg.Reffereds select h);
            DDLSentto.DataSource = hosp;
            DDLSentto.DataValueField = "RefId";
            DDLSentto.DataTextField = "Refto";
            DDLSentto.DataBind();
        }        
        
        protected void btnFind_Click(object sender, EventArgs e)
        {
            DGEntities1 dg = new DGEntities1();
            int fanos = Convert.ToInt32(txtfind.Text);
            var getfano = from a in dg.MAINCORRs where a.fano == fanos select a;
            if (getfano.Count() > 0)
            {
                var gt = getfano.First();
                txtPageNo.Text = Convert.ToInt32(gt.pageno).ToString();
                txtFano.Text = Convert.ToInt32(fanos).ToString();
                txtGrant.Text = gt.cataid;
                txtDeptCode.Text = Convert.ToInt32(gt.deptcode).ToString();
                txtAppname.Text = gt.appname;
                txtAddress.Text = gt.address;
                txtSubject.Text = gt.subject;
                txtRefno.Text = gt.refno;
                RefDate.SelectedDate = gt.refdate;
                txtFDRefNo.Text = gt.fdrefno;
                FDRefDate.SelectedDate = gt.fdrefdate;
                txtCategory.Text = Convert.ToInt32(gt.catcode).ToString();
                txtStatCode.Text = Convert.ToInt32(gt.statcode).ToString();
                txtRemarks.Text = gt.remarks;
                //FupDate.DateInput.DisplayText = gt.fupdate.ToString();
                FupDate.SelectedDate = gt.fupdate;
                txtTotal.Text = Convert.ToInt32(gt.total).ToString();
                txtAmount.Text = Convert.ToInt32(gt.amount).ToString();
                txtIncurred.Text = Convert.ToInt32(gt.AmountIncured).ToString();
                gt.Balance = gt.amount - gt.AmountIncured;
                ddlCountry.SelectedIndex = ddlCountry.Items.IndexOf(ddlCountry.Items.FindByValue(gt.Country_Id.ToString()));
                DDLSentto.SelectedIndex = DDLSentto.Items.IndexOf(DDLSentto.Items.FindByValue(gt.senttoid.ToString()));
                DDLSubCat.SelectedIndex = DDLSubCat.Items.IndexOf(DDLSubCat.Items.FindByValue(gt.subcatid.ToString()));
                txtBalance.Text = Convert.ToInt32(gt.Balance).ToString();
                txtSentto.Text = gt.sentto;
                txtfYear.Text = gt.fyear;
                txtRemRemarks.Text = gt.remremarks;
                DisabledFields();
                txtfind.Enabled = false;
                btnFind.Enabled = false;
                
            }

        }

        private void DisabledFields()
        {
            txtPageNo.Enabled = false;
            txtFano.Enabled = false;
            txtGrant.Enabled = false;
            txtDeptCode.Enabled = false;
            txtAppname.Enabled = false;
            txtAddress.Enabled = false;
            txtSubject.Enabled = false;
            txtRefno.Enabled = false;
            RefDate.Enabled = false;
            txtFDRefNo.Enabled = false;
            FDRefDate.Enabled = false;
            txtCategory.Enabled = false;
            txtStatCode.Enabled = false;
            txtRemarks.Enabled = false;
            FupDate.Enabled = false;
            txtTotal.Enabled = false;
            txtAmount.Enabled = false;
            txtSentto.Enabled = false;
            txtfYear.Enabled = false;
            txtRemRemarks.Enabled = false;
            txtBalance.Enabled = false;
            txtIncurred.Enabled = false;
            
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            EnabledFields();
            txtPageNo.Focus();   
            btnEdit.Visible = false;
            
            
        }

        private void EnabledFields()
        {
            txtPageNo.Enabled = true;
            txtFano.Enabled = true;
            txtGrant.Enabled = true;
            txtDeptCode.Enabled = true;
            txtAppname.Enabled = true;
            txtAddress.Enabled = true;
            txtSubject.Enabled = true;
            txtRefno.Enabled = true;
            RefDate.Enabled= true;
            txtFDRefNo.Enabled = true;
            FDRefDate.Enabled  = true;
            txtCategory.Enabled = true;
            txtStatCode.Enabled = true;
            txtRemarks.Enabled = true;
            FupDate.Enabled = true;
            txtTotal.Enabled = true;
            txtAmount.Enabled = true;
            txtfYear.Enabled = true;
            txtSentto.Enabled = true;
            txtRemRemarks.Enabled = true;
            txtIncurred.Enabled = true;
            txtBalance.Enabled = true;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                DGEntities1 dg = new DGEntities1();
                int fanos = Convert.ToInt32(txtfind.Text);
                int fanoh = Convert.ToInt32(txtfind.Text);
                var getfano = from a in dg.MAINCORRs where a.fano == fanos select a;
                if (getfano.Count() > 0)
                {
                    var m = getfano.First();
                    m.pageno = Convert.ToInt32(txtPageNo.Text);
                    m.fano = Convert.ToInt32(txtFano.Text);
                    m.cataid = txtGrant.Text;
                    m.deptcode = Convert.ToInt32(txtDeptCode.Text);
                    m.appname = txtAppname.Text;
                    m.address = txtAddress.Text;
                    m.subject = txtSubject.Text;
                    m.refno = txtRefno.Text;
                    if (ddlCountry.SelectedItem.Value == "Select Country")
                    {
                        m.Country_Id = null;
                    }
                    else
                    {
                        m.Country_Id = Convert.ToInt32(ddlCountry.SelectedItem.Value);
                    }
                    if (DDLSubCat.SelectedItem.Value == "Select Disease")
                    {
                        m.subcatid = null;
                    }
                    else
                    {
                        m.subcatid = Convert.ToInt32(DDLSubCat.SelectedItem.Value);
                    }

                    
                    if (DDLSentto.SelectedItem.Value == "Select Hospital")
                    {
                        m.senttoid = null;
                    }
                    else
                    {
                        m.senttoid = Convert.ToInt32(DDLSentto.SelectedItem.Value);
                    }
                    m.refdate = RefDate.DateInput.SelectedDate;
                    m.fdrefno = txtFDRefNo.Text;
                    m.fdrefdate = FDRefDate.DateInput.SelectedDate;
                    m.catcode = txtCategory.Text == string.Empty ? 0 : Convert.ToInt32(txtCategory.Text);
                    if (string.IsNullOrEmpty(txtStatCode.Text))
                    {
                        m.statcode = null;
                    }
                    m.remarks = txtRemarks.Text;
                    m.fupdate = FupDate.DateInput.SelectedDate;
                    m.total = txtTotal.Text == string.Empty ? 0 : Convert.ToInt32(txtTotal.Text);
                    m.amount = txtAmount.Text == string.Empty ? 0 : Convert.ToInt32(txtAmount.Text);
                    m.AmountIncured = txtIncurred.Text == string.Empty ? 0 : Convert.ToInt32(txtIncurred.Text);
                    m.Balance = txtBalance.Text == string.Empty ? 0 : Convert.ToInt32(txtBalance.Text);
                    m.fyear = txtfYear.Text;
                    m.remremarks = txtRemRemarks.Text;
                    //Saving some info from grantform to health table
                    if (DDLSentto.SelectedItem.Value == "Select Hospital")
                    {
                       // m.senttoid = null;
                    }
                    else
                    {
                        int fano = Convert.ToInt32(txtFano.Text);
                        var checkfanoexist = from a in dg.Healths where a.fanoHealth == fano select a;
                        if (checkfanoexist.Count() > 0)
                        {

                        }
                        else
                        {
                            Health h = new Health();
                            h.fanoHealth = fano;
                            h.pagenoHealth = Convert.ToInt32(txtPageNo.Text);
                            h.refid = Convert.ToInt32(DDLSentto.SelectedItem.Value);
                            dg.Healths.AddObject(h);
                        }
                    }
                   

                    dg.SaveChanges();
                }

                DisabledFields();
                btnEdit.Visible = true;
            }

            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Alert", "alert('" + ex.Message.ToString() + "');", true);
            }
        }





        protected void btnCancel_Click(object sender, EventArgs e)
        {

        }



        public object cat { get; set; }
    }
}
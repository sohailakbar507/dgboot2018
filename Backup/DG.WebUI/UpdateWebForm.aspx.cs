﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DGModel;

namespace DG.WebUI
{
    public partial class UpdateWebForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Cls_Login loggedInUser = (Cls_Login)Session["Login"];

            if (loggedInUser == null)
            {
                Session["LoginMessage"] = "You are not properly logged in.";
                Response.Redirect("LoginPage.aspx");
            }
            if (loggedInUser.IsUpdateForm == false)
            {
                Session["LoginMessage"] = "You are not authorized for this page. Please contact to Administrator";
                Response.Redirect("LoginPage.aspx");
            }

            if (!IsPostBack)
            {
                lblMessage.Visible = false;
                LoadListView();
            }
        }

            private void LoadListView()
        {
            DGEntities1 dg = new DGEntities1();
            var dglist = from a in dg.MAINCORRs  orderby a.chqno select new {a.fano,a.appname,a.chqno,a.chqdate,a.letcode,a.fileno};
            DGListView.DataSource = dglist;
            DGListView.DataBind();
           Session["dgData"] = dglist;
        }

        protected void DGListView_PagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
        {
            DataPager1.SetPageProperties(e.StartRowIndex, e.MaximumRows, false);
            LoadViewSession();

        }

        private void LoadViewSession()
        {
            DGListView.DataSource = Session["dgData"];
            DGListView.DataBind();
          
            
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            DGEntities1 dg = new DGEntities1();
            int letcode = Convert.ToInt32(txtLetCode.Text);
            int fileno = Convert.ToInt32(txtFileNo.Text);
            DateTime Fromdate = Convert.ToDateTime(FromChqDate.DateInput.SelectedDate);
            DateTime Todate = Convert.ToDateTime(ToChqDate.DateInput.SelectedDate);

            var getdata = dg.DGdata(letcode, fileno, Fromdate, Todate);
            Session["dgData"] = getdata;
            DGListView.DataSource = getdata;
            DGListView.DataBind();
        }

        protected void btnCode8_Click(object sender, EventArgs e)
        {
            DGEntities1 dg = new DGEntities1();
            int letcode = Convert.ToInt32(txtLetCode.Text);
            int fileno = Convert.ToInt32(txtFileNo.Text);
            DateTime Fromdate = Convert.ToDateTime(FromChqDate.DateInput.SelectedDate);
            DateTime Todate = Convert.ToDateTime(ToChqDate.DateInput.SelectedDate);

            var getdata = dg.Code8(letcode, fileno, Fromdate, Todate);
            DGListView.DataBind();

            lblMessage.Visible = true;
            lblMessage.Text = "Letter Code Replaced with LetCode 8";
        }

        protected void btnCode12_Click(object sender, EventArgs e)
        {
            DGEntities1 dg = new DGEntities1();
            int letcode = Convert.ToInt32(txtLetCode.Text);
            int fileno = Convert.ToInt32(txtFileNo.Text);
            DateTime Fromdate = Convert.ToDateTime(FromChqDate.DateInput.SelectedDate);
            DateTime Todate = Convert.ToDateTime(ToChqDate.DateInput.SelectedDate);

            var getdata = dg.Code12(letcode, fileno, Fromdate, Todate);
            DGListView.DataBind();
            lblMessage.Visible = true;
            lblMessage.Text = "Letter Code Replaced with LetCode 12";
        }

        protected void DGListView_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        
    }
}
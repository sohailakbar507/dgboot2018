﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DGModel;

namespace DG.WebUI
{
    public partial class FDForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                EditRecordFD();
                DisabledFields();
                btnSave.Visible = false;
            }
        }

        private void DisabledFields()
        {
            txtFaNo.Enabled = false;
            txtAppName.Enabled = false;
            txtamount.Enabled = false;
            txtbalance.Enabled = false;
            txtAmountIncurred.Enabled = false;
            txtSentto.Enabled = false;
            rdFDDate.Enabled = false;
            txtfdrefno.Enabled = false;
            
        }

        private void EditRecordFD()
        {
            DGEntities1 DGE = new DGEntities1();
            int dgid = Convert.ToInt32(Request["DGID"]);
            var fd = from a in DGE.MAINCORRs
                     where a.DGId == dgid
                     select new
                     {
                         a.fano,
                         a.amount,
                         a.Balance,
                         a.AmountIncured,
                         a.appname,
                         a.fdrefdate,
                         a.fdrefno
                     };
            if (fd.Count() > 0)
            {
                var abc = fd.First();

                txtFaNo.Text = Convert.ToInt32(abc.fano).ToString();
                txtAppName.Text = abc.appname;
                txtamount.Text = Convert.ToInt32(abc.amount).ToString();
                txtAmountIncurred.Text = Convert.ToInt32(abc.AmountIncured).ToString();
                txtbalance.Text = Convert.ToInt32(abc.Balance).ToString();
                txtfdrefno.Text = abc.fdrefno;
                rdFDDate.SelectedDate = abc.fdrefdate;
                
            }




        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            int dgid = Convert.ToInt32(Request["DGID"]);
            DGEntities1 dg = new DGEntities1();
            MAINCORR M = new MAINCORR();
            //int dgid = Convert.ToInt32(Request["DGID"]);
            M = (from a in dg.MAINCORRs where a.DGId == dgid select a).First();
            M.appname = txtAppName.Text;
            M.amount = Convert.ToInt32(txtamount.Text);
            M.AmountIncured = Convert.ToInt32(txtAmountIncurred.Text);
            M.Balance = Convert.ToInt32(txtbalance.Text);
            M.sentto = txtSentto.Text;
            M.fdrefno = txtfdrefno.Text;
            M.fdrefdate = rdFDDate.DateInput.SelectedDate;
            //dg.MAINCORRs.AddObject(M);
            dg.SaveChanges();
            Response.Redirect("FDUpdateForm.aspx");
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            txtAppName.Focus();
            FieldsEnabled();
            btnSave.Visible = true;
            btnEdit.Visible = false;
        }

        private void FieldsEnabled()
        {
            txtAppName.Enabled = true;
            txtamount.Enabled = true;
            txtAmountIncurred.Enabled = true;
            txtbalance.Enabled = true;
            txtfdrefno.Enabled = true;
            rdFDDate.Enabled = true;
            txtSentto.Enabled = true;

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("FDUpdateForm.aspx");
        }

       /* protected void txtAmountIncurred_TextChanged(object sender, EventArgs e)
        {
            int amount;
            int amountincurred;
            int bal;
            amount = Convert.ToInt32(txtamount.Text);
            amountincurred = Convert.ToInt32(txtAmountIncurred.Text);
            bal = amount-amountincurred;
            txtbalance.Text = Convert.ToInt32(bal).ToString();

        } */

        
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DGModel;

namespace DG.WebUI
{
    public partial class MedicalBoard : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtfind.Focus();
            }

        }

        protected void btnFind_Click(object sender, EventArgs e)
        {
            DGEntities1 dg = new DGEntities1();
            int fanos = Convert.ToInt32(txtfind.Text);
            var getfano = from a in dg.MAINCORRs where a.fano == fanos select a;
            if (getfano.Count() > 0)
            {
                var gt = getfano.First();
                txtFano.Text = Convert.ToInt32(fanos).ToString();
                txtAppname.Text = gt.appname;
                txtAddress.Text = gt.address;
                this.ChkStatus.Items[0].Selected = Convert.ToBoolean(gt.SMBStatus);
                this.ChkStatus.Items[1].Selected = Convert.ToBoolean(gt.RCOStatus);
                SMBDate.SelectedDate = gt.SMBDate;
                RCODate.SelectedDate = gt.RCODate;
                txtRemarks.Text = gt.SMBRemarks;
            }
            //var health = from a in dg.health where a.fano == fanos select 
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
           

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            DGEntities1 dg = new DGEntities1();
            int fanos = Convert.ToInt32(txtfind.Text);
            var getfano = from a in dg.MAINCORRs where a.fano == fanos select a;
            if (getfano.Count() > 0)
            {
                var m = getfano.First();
                m.fano = Convert.ToInt32(txtFano.Text);
                m.appname = txtAppname.Text;
                m.address = txtAddress.Text;
                m.SMBStatus = this.ChkStatus.Items[0].Selected;
                m.RCOStatus = this.ChkStatus.Items[1].Selected;
                m.SMBDate = SMBDate.DateInput.SelectedDate;
                m.RCODate = RCODate.DateInput.SelectedDate;
                m.SMBRemarks = txtRemarks.Text;
                m.letcode = 30;
                m.subjcode = 26;
                dg.SaveChanges();
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {

        }
    }
}
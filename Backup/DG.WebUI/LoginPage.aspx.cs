﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DGModel;

namespace DG.WebUI
{
    public partial class LoginPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
            
            if (!IsPostBack)
            {
                
               // if (Session["LoginMessage"] != null)
                   // this.lblMessage.Text = Session["LoginMessage"].ToString();
              //  else
                //    this.lblMessage.Text = "";
            }

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
                    
                DGEntities1 dgent = new DGEntities1();

                string name = txtusername.Value;
                string pass = Cls_Encrypt.Encrypt(txtPass.Value);

                var CheckUser = from a in dgent.Users where a.Name == name && a.Password == pass select a;


                if (CheckUser.Count() > 0)
                {
                    var users = CheckUser.First();
                    Cls_Login login = new Cls_Login();
                    login.Userid = Convert.ToInt32(users.ID);
                    login.Username = users.Name;
                    login.Groupid = Convert.ToInt32(users.Group_ID);
                    login.Isadd = Convert.ToBoolean(users.IsAdd_Record);
                    login.Isedit = Convert.ToBoolean(users.IsEdit_Record);
                    login.Isdel = Convert.ToBoolean(users.IsDelete_Record);
                    login.IsUpdateForm = Convert.ToBoolean(users.UpdateForm);
                    var gethealthaccessid = from a in dgent.User_Groups where a.ID == users.Group_ID select a;
                    if (gethealthaccessid.Count() > 0)
                    {
                        var getaccessid = gethealthaccessid.First();
                        login.Healthaccessid = Convert.ToInt32(getaccessid.HealthAccessid);
                    }
                    //login.IsAnalYearReport = Convert.ToBoolean(users.AnalYearReport);
                    Session["Login"] = login;
                    if (login.Healthaccessid == 3)
                    {
                        Response.Redirect("HealthForm.aspx");
                    }

                    if (login.Healthaccessid == 5)
                    {
                        Response.Redirect("HealthForm.aspx");
                    }

                    Response.Redirect("MainPage.aspx");

                }
                
            
        }

        protected void btnChangePasswordForm_Click(object sender, EventArgs e)
        {
            //Response.Redirect("ChangePassword.aspx");

        }
                
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DGMasterPage.Master" AutoEventWireup="true" CodeBehind="DGForm.aspx.cs" Inherits="DG.WebUI.DGForm" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<h1 align="center">DG Main Form</h1>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    
    <script language="javascript">


        function OnFiguresChange() {
            var obj1 = document.getElementById('<%=txtAmount.ClientID %>');
            var obj2 = document.getElementById('<%=txtAmtinWords.ClientID %>');
            obj2.value = ConvertToWords(obj1.value);
            //alert("test");
        }
        function GetWords(figure) {
            var singles = new Array();
            singles[0] = "";
            singles[1] = "One";
            singles[2] = "Two";
            singles[3] = "Three";
            singles[4] = "Four";
            singles[5] = "Five";
            singles[6] = "Six";
            singles[7] = "Seven";
            singles[8] = "Eight";
            singles[9] = "Nine";
            singles[10] = "Ten";
            singles[11] = "Eleven";
            singles[12] = "Twelve";
            singles[13] = "Thirteen";
            singles[14] = "Fourteen";
            singles[15] = "Fifteen";
            singles[16] = "Sixteen";
            singles[17] = "Seventeen";
            singles[18] = "Eighteen";
            singles[19] = "Nineteen";
            singles[20] = "Twenty";

            var tens = new Array();
            tens[2] = "Twenty";
            tens[3] = "Thrity";
            tens[4] = "Forty";
            tens[5] = "Fifty";
            tens[6] = "Sixty";
            tens[7] = "Seventy";
            tens[8] = "Eighty";
            tens[9] = "Ninty";

            if (figure <= 20) { return singles[figure]; }
            else { return tens[Math.floor(figure / 10)] + " " + singles[figure % 10]; }
        }
        function ConvertToWords(amount) {
            var units = new Array();
            units[0] = "Hundred";
            units[1] = "Thousand";
            units[2] = "Lacs";
            units[3] = "Crore";
            var returnValue = "";


            var intTens = amount % 100;
            amount /= 100;
            amount = Math.floor(amount);

            var inHunderds = amount % 10;
            amount /= 10;
            amount = Math.floor(amount);

            var inThousands = amount % 100;
            amount /= 100;
            amount = Math.floor(amount);

            var inLacs = amount % 100;
            amount /= 100;
            amount = Math.floor(amount);

            var inCrores = amount;

            if (inCrores > 0) {
                returnValue += " " + GetWords(inCrores) + " " + units[3];
            }

            if (inLacs > 0) {
                returnValue += " " + GetWords(inLacs) + " " + units[2];
            }

            if (inThousands > 0) {
                returnValue += " " + GetWords(inThousands) + " " + units[1];
            }

            if (inHunderds > 0) {
                returnValue += " " + GetWords(inHunderds) + " " + units[0];
            }

            if (intTens > 0) {
                if (returnValue != "") {
                    returnValue += " and";
                }
                returnValue += " " + GetWords(intTens);
            }

            return returnValue;
        }
    </script>    

<div class="row">
<div class="col-lg-12">
                    
                </div>
                </div>
<div>
    <table align = "center"  cellpadding = "0" cellspacing = "0" style = "border:1px dashed white"  bgcolor="#CC8099">
        
        <tr>
            <td colspan = "2" align = "center" height = "30">
                Constituency
            </td>
            <td align = "center">Name</td>
        </tr>
        <tr>
            <td> <asp:TextBox ID="txtCatcode" runat="server" CssClass="form-control" 
                    Width = "100px" ToolTip="CatCode"></asp:TextBox> </td>
            <td> <asp:TextBox ID="txtCons" runat="server" CssClass="form-control" 
                    Width = "100px" AutoPostBack="True" 
                    ontextchanged="txtCons_TextChanged" ToolTip="Cons"></asp:TextBox> </td>
            <td> <asp:TextBox ID="txtName" runat="server" CssClass="form-control" Width = "250px"></asp:TextBox> </td>
        </tr>
        </table>
        
              <br />

                 <table align = "center" width= "80%" cellpadding = "0" cellspacing = "0" style = "border:1px dashed white">
                 
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblFaNo" runat="server" Text="FaNo"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtFaNo" runat="server" Width = "200px" CssClass="form-control" ReadOnly="True"></asp:TextBox>
                        </td>
                        <td align="left">
                            <asp:Label ID="lblAppName" runat="server" Text="Applicant Name"></asp:Label>
                        </td>
                        <td align = "left">
                            <asp:TextBox ID="txtAppName" runat="server" Width = "200px" CssClass="form-control"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblAddress" runat="server" Text="Address"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtAddress" runat="server" Width = "350px" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                            </td>
                        <td align="left">
                            <asp:Label ID="lblContact" runat="server" Text="Contact"></asp:Label>
                        </td>
                        <td align = "left">
                            <asp:TextBox ID="txtContact" runat="server" Width = "200px" CssClass="form-control" Enabled="False"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblDistrict" runat="server" Text="District"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtDistrict" runat="server" Width = "200px" CssClass="form-control"></asp:TextBox>
                        </td>
                        <td align="left">
                            <asp:Label ID="lblType" runat="server" Text="Type"></asp:Label>
                        </td>
                        <td align = "left">
                            <asp:DropDownList ID="ddlType" runat="server" Width = "200px" CssClass="form-control">
                            </asp:DropDownList>
                            
                        </td>
                    </tr>
                      <tr>
                        <td align="left">
                            <asp:Label ID="lblDepartment" runat="server" Text="Department"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:DropDownList ID="ddlDepartment" runat="server" Width = "200px" CssClass="form-control">
                            </asp:DropDownList>
                        </td>
                        <td align="left">
                            <asp:Label ID="lblActionBy" runat="server" Text="Action By"></asp:Label>
                        </td>
                        <td align = "left">
                            <asp:TextBox ID="txtActionyBy" runat="server" Width = "200px" CssClass="form-control"></asp:TextBox>
                            
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblSentOn" runat="server" Text="Sent On"></asp:Label>
                        </td>
                        <td align="left">
                            <telerik:RadDateTimePicker ID="SentOn" Runat="server" CssClass="form-control" 
                                Skin="Office2007" Width="202px">
                        <TimeView CellSpacing="-1" runat = "server"></TimeView>

                        <TimePopupButton ImageUrl="" HoverImageUrl=""></TimePopupButton>

                        <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x" 
                                    Skin="Office2007" runat = "server"></Calendar>

                        <DateInput DisplayDateFormat="dd-MMM-yy" DateFormat="dd-MMM-yy" LabelWidth="40%" 
                                    Height="16px" runat="server"></DateInput>

                        <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                            </telerik:RadDateTimePicker>
                        </td>
                        <td align="left">
                            <asp:Label ID="lblFileNo" runat="server" Text="File No."></asp:Label>
                        </td>
                        <td align = "left">
                            <asp:TextBox ID="txtFileNo" runat="server" Width = "200px" CssClass="form-control"></asp:TextBox>
                            
                        </td>
        
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblCpno" runat="server" Text="CP No."></asp:Label>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtCPNo" runat="server" Width = "200px" CssClass="form-control"></asp:TextBox>
                        </td>
                        <td align="left">
                            <asp:Label ID="lblAmount" runat="server" Text="Amount"></asp:Label>
                        </td>
                        <td align = "left">
                            <asp:TextBox ID="txtAmount" runat="server" Width = "200px" 
                                CssClass="form-control" ToolTip="Amount"></asp:TextBox>
                            
                            <asp:TextBox ID="txtAmtinWords" runat="server" Width="200px" 
                                CssClass="form-control" ToolTip="Amount in Words"></asp:TextBox>
                            
                        </td>

                    </tr>

                    <tr>
                        <td align="left">
                            <asp:Label ID="lblChqNo" runat="server" Text="Cheque No."></asp:Label>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtChqno" runat="server" Width = "200px" CssClass="form-control"></asp:TextBox>
                        </td>
                        <td align="left">
                            <asp:Label ID="lblChqdate" runat="server" Text="Cheque Date"></asp:Label>
                        </td>
                        <td align = "left">
                            <telerik:RadDateTimePicker ID="Chqdate" Runat="server" Skin="Office2007" CssClass="form-control" 
                                Width="200px">
                        <TimeView CellSpacing="-1" runat="server"></TimeView>

                        <TimePopupButton ImageUrl="" HoverImageUrl=""></TimePopupButton>

                        <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x" 
                                    Skin="Office2007" runat="server"></Calendar>

                        <DateInput DisplayDateFormat="dd-MMM-yy" DateFormat="dd-MMM-yy" LabelWidth="40%" 
                                    Height="16px" runat="server"></DateInput>

                        <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                            </telerik:RadDateTimePicker>
                            
                        </td>
                        
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblCNIC" runat="server" Text="CNIC"></asp:Label>
                        </td>        
                        <td align="left">
                            <asp:TextBox ID="txtCnic" runat="server" Width = "200px" CssClass="form-control"></asp:TextBox>
                        </td>
                        
                            <td align="left">
                            <asp:Label ID="lblStatus" runat="server" Text="Status"></asp:Label>
                            </td>        
                        
                        <td align = "left">
                            <asp:DropDownList ID="ddlStatus" runat="server" Width = "200px" CssClass="form-control">
                            </asp:DropDownList>
                            
                        </td>
                        
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblIssueDate" runat="server" Text="Issue Date" ></asp:Label>
                        </td>
                        <td align="left">
                            <telerik:RadDateTimePicker ID="IssueDate" Runat="server" CssClass="form-control" 
                                Skin="Office2007" Width="202px">
                            <TimeView CellSpacing="-1" runat="server"></TimeView>

                        <TimePopupButton ImageUrl="" HoverImageUrl=""></TimePopupButton>

                        <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x" 
                                    Skin="Office2007" runat="server"></Calendar>

                        <DateInput DisplayDateFormat="dd-MMM-yy" DateFormat="dd-MMM-yy" LabelWidth="40%"  
                                    Height="16px" runat="server"></DateInput>

                        <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                            </telerik:RadDateTimePicker>
                        </td>
                        <td align="left">
                            <asp:Label ID="lblLetterNo" runat="server" Text="Letter No."></asp:Label>
                        </td>
                        <td align = "left">
                            <asp:TextBox ID="txtLetterNo" runat="server" Width = "200px" CssClass="form-control"></asp:TextBox>
                        </td>

                    </tr>
                    <tr>
                       <td align="left">
                           <asp:Label ID="lblSCMDate" runat="server" Text="SCM Date"></asp:Label>
                       </td>
                       <td align="left">
                           <telerik:RadDateTimePicker ID="scmdate" Runat="server" CssClass="form-control" 
                               Skin="Office2007" Width="200px">
                    <TimeView CellSpacing="-1" runat="server"></TimeView>

                    <TimePopupButton ImageUrl="" HoverImageUrl="" runat="server"></TimePopupButton>

                    <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x" 
                                   Skin="Office2007" runat="server"></Calendar>

                    <DateInput DisplayDateFormat="dd-MMM-yy" DateFormat="dd-MMM-yy" LabelWidth="40%" Height="23px" runat="server"></DateInput>

                    <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                           </telerik:RadDateTimePicker>
                       </td> 
                        <td align="left">
                            <asp:Label ID="lblOldFileNo" runat="server" Text="Old File No."></asp:Label>
                        </td>
                        <td align = "left">
                            <asp:TextBox ID="txtOldFileNo" runat="server" Width = "200px" CssClass="form-control"></asp:TextBox>
                            
                        </td>
                    </tr>

                    <tr>
                        
                        <td align="left">
                            <asp:Label ID="lblOldChqDate" runat="server" Text="Old Cheque Date"></asp:Label>
                        </td>
                        <td align="left">
                            <telerik:RadDateTimePicker ID="OldChqDate" Runat="server" CssClass="form-control" 
                                Skin="Office2007" Width="200px">
                        <TimeView CellSpacing="-1" runat="server"></TimeView>

                        <TimePopupButton ImageUrl="" HoverImageUrl=""></TimePopupButton>

                        <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x" 
                                    Skin="Office2007" runat="server"></Calendar>

                        <DateInput DisplayDateFormat="dd-MMM-yy" DateFormat="dd-MMM-yy" LabelWidth="40%"  
                                    Height="21px" runat="server"></DateInput>

                        <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                            </telerik:RadDateTimePicker>
                        </td>
                        <td align="left">
                            <asp:Label ID="lblOldchqNumber" runat="server" Text="Old Cheque #"></asp:Label>
                        </td>
                        <td align = "left">
                            <asp:TextBox ID="txtOldChqNumber" runat="server" Width = "200px" CssClass="form-control"></asp:TextBox>
                        </td>
                        
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblRemarks" runat="server" Text="Remarks"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtRemarks" runat="server" Width = "200px" CssClass="form-control"></asp:TextBox>
                        </td>
                    

                        <td align="left">
                            <asp:Label ID="lblChqRemarks" runat="server" Text="Cheque Remarks"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtChqRemarks" runat="server" Width = "200px" CssClass="form-control"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblFyear" runat="server" Text="Financial Year"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtFyear" runat="server" Width = "200px" CssClass="form-control"></asp:TextBox>
                        </td>
                    </tr>
                    <tr> <td colspan = "4"> <h2 align = "center"> Follow Up </h2>
                         <br />
                         </td></tr> &nbsp;
                    
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblTreasuryDate" runat="server" Text="Treasury Date"></asp:Label>
                        </td>
                        <td align="left">
                            <telerik:RadDateTimePicker ID="TreasuryDate" Runat="server" CssClass="form-control" 
                                Skin="Office2007" Width="200px">
                            <TimeView CellSpacing="-1" runat="server"></TimeView>

                            <TimePopupButton ImageUrl="" HoverImageUrl=""></TimePopupButton>

                            <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x" 
                                    Skin="Office2007" runat="server"></Calendar>

                            <DateInput DisplayDateFormat="dd-MMM-yy" DateFormat="dd-MMM-yy" LabelWidth="40%" Height="19px" runat="server"></DateInput>

                            <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                            </telerik:RadDateTimePicker>
                        </td>
                        <td align="left">
                            <asp:Label ID="lblBankDate" runat="server" Text="Bank Date"></asp:Label>
                        </td>
                        <td align = "left">
                            <telerik:RadDateTimePicker ID="BankDate" Runat="server" CssClass="form-control" 
                                style="margin-left: 0px" Width="200px" Skin="Office2007">
                            <TimeView CellSpacing="-1" runat="server"></TimeView>

                            <TimePopupButton ImageUrl="" HoverImageUrl=""></TimePopupButton>

                            <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" 
                                    ViewSelectorText="x" runat="server" Skin="Office2007"></Calendar>

                            <DateInput DisplayDateFormat="dd-MMM-yy" DateFormat="dd-MMM-yy" LabelWidth="40%" Height="16px" runat="server"></DateInput>

                            <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                            </telerik:RadDateTimePicker>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblOldAcknDate" runat="server" Text="OldAknDate"></asp:Label>
                        </td>
                        <td align="left">
                            <telerik:RadDateTimePicker ID="OldAknDate" Runat="server" CssClass="form-control" 
                                Skin="Office2007" Width="200px">
                            <TimeView CellSpacing="-1" runat= "server"></TimeView>

                            <TimePopupButton ImageUrl="" HoverImageUrl=""></TimePopupButton>

                            <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x" 
                                    Skin="Office2007" runat="server"></Calendar>

                            <DateInput DisplayDateFormat="dd-MMM-yy" DateFormat="dd-MMM-yy" LabelWidth="40%" Height="24px" runat="server"></DateInput>

                            <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                            </telerik:RadDateTimePicker>
                        </td>
                        <td align="left">
                            <asp:Label ID="lblNewAcknDate" runat="server" Text="NewAknDate"></asp:Label>
                        </td>
                        <td align = "left">
                            <telerik:RadDateTimePicker ID="NewAknDate" Runat="server" CssClass="form-control"
                                Skin="Office2007" Width = "200px">
                        <TimeView CellSpacing="-1" runat="server"></TimeView>

                        <TimePopupButton ImageUrl="" HoverImageUrl=""></TimePopupButton>

                        <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x" 
                                    Skin="Office2007" runat="server"></Calendar>

                        <DateInput DisplayDateFormat="dd-MMM-yy" DateFormat="dd-MMM-yy" LabelWidth="40%" runat="server"></DateInput>

                        <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                            </telerik:RadDateTimePicker>
                        </td>
                        
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblFinalRemarks" runat="server" Text="Final Remarks"></asp:Label>
                        </td>
                        <td colspan = "4">
                            <asp:TextBox ID="txtFinalRemarks" runat="server" CssClass="form-control" Width = "900px" 
                             TextMode="MultiLine"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                    <td align = "center" colspan = "4">
                        <br />
                        <asp:Button ID="btnAdd" runat="server" Text="Add" CssClass="btn btn-primary btn-lg" Width="100px" onclick="btnAdd_Click"/>
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-primary btn-lg" Width="100px" onclick="btnSave_Click" 
                            ValidationGroup="SaveRecord" />
                        <asp:Button ID="btnEdit" runat="server" Text="Edit" CssClass="btn btn-primary btn-lg" Width="100px" onclick="btnEdit_Click"/>
                        <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-primary btn-lg" Width="100px" onclick="btnSearch_Click"/>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn btn-primary btn-lg" Width="100px" onclick="btnCancel_Click" />
                    </td>
                    </tr>

                    

        </table>

</div>                
</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.Entity;
using DGModel;

namespace DG.WebUI
{
    public partial class HealthForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["Login"] == null)
                {
                    Response.Redirect("~/LoginPage.aspx");
                }

                txtfind.Focus();
                UploadSentto();
                UploadStatus();
                NoEdit();
                this.ddlRefferedTo.Items.Insert(0, new ListItem("Select Hospital"));
                this.ddlStatus.Items.Insert(0, new ListItem("Select Status"));
                
           }

        }

        private void NoEdit()
        {
            txtFano.Enabled = false;
            txtAppname.Enabled = false;
            txtAddress.Enabled = false;
            txtPageNo.Enabled = false;
            txtLetterNo.Enabled = false;
            ddlRefferedTo.Enabled = false;
            dtRefferedDate.Enabled = false;
            dtLetterDate.Enabled = false;
            txtBalReturned.Enabled = false;
            rdtUpdatedOn.Enabled = false;
        }

        private void Editable()
        {
            txtPageNo.Enabled = true;
            txtLetterNo.Enabled = true;
            ddlRefferedTo.Enabled = true;
            dtRefferedDate.Enabled = true;
            dtLetterDate.Enabled = true;
            

        }

        private void UploadStatus()
        {
            DGEntities1 dg = new DGEntities1();
            var stat = (from b in dg.PatientStatus select b);
            ddlStatus.DataSource = stat;
            ddlStatus.DataValueField = "PtID";
            ddlStatus.DataTextField = "Description";
            //ddlStatus.SelectedIndex = 0;
            ddlStatus.DataBind();
        }

        private void UploadSentto()
        {
            DGEntities1 dg = new DGEntities1();
            var sentto = (from a in dg.Reffereds select a);
            ddlRefferedTo.DataSource = sentto;
            ddlRefferedTo.DataValueField = "RefId";
            ddlRefferedTo.DataTextField = "Refto";
            ddlRefferedTo.DataBind(); 
                        
        }
        
        protected void btnFind_Click(object sender, EventArgs e)
        {
            
            
        }

        private void ClearFields()
        {
            txtPageNo.Text = "";
            txtLetterNo.Text = "";
            dtRefferedDate.SelectedDate = null;
            dtLetterDate.SelectedDate = null;
            rdtUpdatedOn.SelectedDate = null;
                     
        }

        private void GetRecord(string username,int groupid)
        {
            try
            {
                DGEntities1 dg = new DGEntities1();
                int fanos = Convert.ToInt32(txtfind.Text);

                if (groupid == 3)
                {
                    var gethospitalaccess = from a in dg.Reffereds where a.UserName == username select a;
                    if (gethospitalaccess.Count() > 0)
                    {
                        var hospitalaccess = gethospitalaccess.First();
                        var getfno = from h in dg.Healths where h.fanoHealth == fanos && h.refid == hospitalaccess.RefId select h;
                        if (getfno.Count() > 0)
                        {

                            var healinfo = getfno.First();
                            var maininfo = (from m in dg.MAINCORRs where m.fano == healinfo.fanoHealth select m).First();
                            txtAppname.Text = maininfo.appname;
                            txtAddress.Text = maininfo.address;
                            txtFano.Text = Convert.ToInt32(healinfo.fanoHealth).ToString();
                            txtPageNo.Text = Convert.ToInt32(healinfo.pagenoHealth).ToString();
                            txtLetterNo.Text = healinfo.letterno;
                            txtAmount.Text = Convert.ToInt32(healinfo.AmountInc).ToString();
                            txtSancAmount.Text = Convert.ToInt32(maininfo.amount).ToString();
                            txtPatientName.Text = healinfo.PatientName;
                            dtLetterDate.SelectedDate = healinfo.LetDate;
                            dtRefferedDate.SelectedDate = healinfo.RefOn;
                            txtOpinion.Text = healinfo.Opinion;
                            txtFDSancNo.Text = healinfo.fdSactionNo;
                            rdtFDSancDate.SelectedDate = healinfo.fdSanctionDate;
                            rdtVouchedDate.SelectedDate = healinfo.VouchedDate;
                            rdtLiquidDate.SelectedDate = healinfo.LiquidationDate;
                            txtAuthorityNo.Text = healinfo.AuthorityNo;
                            rdtAuthorityDate.SelectedDate = healinfo.AuthorityDate;
                            txtFileNumber.Text = healinfo.FileNumber;
                            txtRemarks.Text = healinfo.HealthRemarks;
                            ddlRefferedTo.SelectedIndex = ddlRefferedTo.Items.IndexOf(ddlRefferedTo.Items.FindByValue(healinfo.refid.ToString()));
                            ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByValue(healinfo.PtStatusID.ToString()));
                            txtLetterNo.Text = healinfo.letterno;
                            rdtUpdatedOn.SelectedDate = healinfo.UpdatedOn;
                            rdtTentativeDate.SelectedDate = healinfo.TentativeDate;
                            rdtActualDate.SelectedDate = healinfo.ActualDate;
                            txtOpinion.Text = healinfo.Opinion;
                            txtRemarks.Text = healinfo.HealthRemarks;
                            txtRegNo.Text = healinfo.HospitalID;
                            txtRecvdChqno.Text = healinfo.RecvdChqno;
                            rdRecvdChqDate.SelectedDate = healinfo.RecvdChqDate;
                            txtBalReturned.Text = Convert.ToInt32(healinfo.BalReturn).ToString();
                            txtBalChqNo.Text = healinfo.BalChqno;
                            rdBalChqDate.SelectedDate = healinfo.BalChqDate;
                            rdtMedicalBoardDate.SelectedDate = healinfo.MedBoardDate;
                            txtRecommendations.Text = healinfo.Recommendations;
                            rdtReportSentOn.SelectedDate = healinfo.ReportSentOn;
                            rdtDateHeldOn.SelectedDate = healinfo.Dateheldon;
                        }

                        else
                        {
                            //string message = "Record Not Found....";
                            ScriptManager.RegisterStartupScript(this, GetType(), "Alert", "alert('Record Not found..');", true);
                            ClearFields();
                        }
                    }
                }
                else
                {
                    if (groupid != 3)
                    {


                        var getfno = from h in dg.Healths where h.fanoHealth == fanos select h;
                        var maininfo1 = (from m in dg.MAINCORRs where m.fano == fanos select m);
                        if (maininfo1.Count() > 0)
                        {
                            var maininfo = maininfo1.First();
                            txtFano.Text = Convert.ToInt32(maininfo.fano).ToString();
                            txtAppname.Text = maininfo.appname;
                            txtAddress.Text = maininfo.address;
                            //txtSancAmount.Text = Convert.ToInt32(maininfo.amount).ToString();
                            Editable();
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "Alert", "alert('Record Not found..');", true);
                            ClearFields();
                        }
                        if (getfno.Count() > 0)
                        {

                            var healinfo = getfno.First();
                            //txtFano.Text = Convert.ToInt32(healinfo.fanoHealth).ToString();
                            txtPageNo.Text = Convert.ToInt32(healinfo.pagenoHealth).ToString();
                            txtLetterNo.Text = healinfo.letterno;
                            txtAmount.Text = Convert.ToInt32(healinfo.AmountInc).ToString();
                            dtLetterDate.SelectedDate = healinfo.LetDate;
                            dtRefferedDate.SelectedDate = healinfo.RefOn;
                            txtOpinion.Text = healinfo.Opinion;
                            txtPatientName.Text = healinfo.PatientName;
                            txtFDSancNo.Text = healinfo.fdSactionNo;
                            txtSancAmount.Text = Convert.ToInt32(healinfo.HealthAmount).ToString();
                            rdtFDSancDate.SelectedDate = healinfo.fdSanctionDate;
                            rdtVouchedDate.SelectedDate = healinfo.VouchedDate;
                            rdtLiquidDate.SelectedDate = healinfo.LiquidationDate;
                            txtAuthorityNo.Text = healinfo.AuthorityNo;
                            rdtAuthorityDate.SelectedDate = healinfo.AuthorityDate;
                            txtFileNumber.Text = healinfo.FileNumber;
                            txtRemarks.Text = healinfo.HealthRemarks;
                            ddlRefferedTo.SelectedIndex = ddlRefferedTo.Items.IndexOf(ddlRefferedTo.Items.FindByValue(healinfo.refid.ToString()));
                            ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByValue(healinfo.PtStatusID.ToString()));
                            txtLetterNo.Text = healinfo.letterno;
                            rdtUpdatedOn.SelectedDate = healinfo.UpdatedOn;
                            rdtTentativeDate.SelectedDate = healinfo.TentativeDate;
                            rdtActualDate.SelectedDate = healinfo.ActualDate;
                            txtOpinion.Text = healinfo.Opinion;
                            txtRemarks.Text = healinfo.HealthRemarks;
                            txtRegNo.Text = healinfo.HospitalID;
                            txtRecvdChqno.Text = healinfo.RecvdChqno;
                            rdRecvdChqDate.SelectedDate = healinfo.RecvdChqDate;
                            txtBalReturned.Text = Convert.ToInt32(healinfo.BalReturn).ToString();
                            txtBalChqNo.Text = healinfo.BalChqno;
                            rdBalChqDate.SelectedDate = healinfo.BalChqDate;
                            rdtMedicalBoardDate.SelectedDate = healinfo.MedBoardDate;
                            txtRecommendations.Text = healinfo.Recommendations;
                            rdtReportSentOn.SelectedDate = healinfo.ReportSentOn;
                            rdtDateHeldOn.SelectedDate = healinfo.Dateheldon;
                            
                        }
                        else
                        {


                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            


        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {

        }

        protected void lnkBtnSearch_Click(object sender, EventArgs e)
        {
            System.Threading.Thread.Sleep(1000);
            Cls_Login login = (Cls_Login)Session["Login"];
            GetRecord(login.Username, login.Healthaccessid);

        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
        }

        protected void lnkbtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("LoginPage.aspx");

        }

        protected void lnkbtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                DGEntities1 dg = new DGEntities1();
                System.Threading.Thread.Sleep(500);
                int fanos = Convert.ToInt32(txtfind.Text);
                var getfano = from a in dg.MAINCORRs where a.fano == fanos select a;
                int hfano = Convert.ToInt32(txtFano.Text);
                var getfno = from h in dg.Healths where h.fanoHealth == hfano select h;
                if (getfano.Count() > 0)
                {
                    if (getfno.Count() > 0)
                    {
                        var h = getfno.First();
                        var m = getfano.First();
                        h.fanoHealth = fanos;
                        m.pageno = Convert.ToInt32(txtPageNo.Text);
                        m.refdate = dtLetterDate.DateInput.SelectedDate;
                        h.pagenoHealth = Convert.ToInt32(txtPageNo.Text);
                        if (ddlRefferedTo.SelectedItem.Value == "Select Hospital")
                        {
                            h.refid = null;
                        }
                        else
                        {
                            h.refid = Convert.ToInt32(ddlRefferedTo.SelectedItem.Value);
                        }

                        if (ddlStatus.SelectedItem.Value == "Select Status")
                        {
                            h.PtStatusID = null;
                        }
                        else
                        {
                            h.PtStatusID = Convert.ToInt32(ddlStatus.SelectedItem.Value);
                        }
                        h.AmountInc = txtAmount.Text == string.Empty ? 0 : Convert.ToInt32(txtAmount.Text);
                        h.HealthAmount = txtSancAmount.Text == string.Empty ? 0 : Convert.ToInt32(txtSancAmount.Text);
                        h.letterno = txtLetterNo.Text;
                        h.LetDate = dtLetterDate.DateInput.SelectedDate;
                        h.UpdatedOn = DateTime.Now;
                        //h.UpdatedOn = rdtUpdatedOn.DateInput.SelectedDate;
                        h.fdSactionNo = txtFDSancNo.Text;
                        h.PatientName = txtPatientName.Text;
                        h.fdSanctionDate = rdtFDSancDate.DateInput.SelectedDate;
                        h.VouchedDate = rdtVouchedDate.DateInput.SelectedDate;
                        h.LiquidationDate = rdtLiquidDate.DateInput.SelectedDate;
                        h.AuthorityNo = txtAuthorityNo.Text;
                        h.AuthorityDate = rdtAuthorityDate.DateInput.SelectedDate;
                        h.FileNumber = txtFileNumber.Text;
                        h.RefOn = dtRefferedDate.DateInput.SelectedDate;
                        h.TentativeDate = rdtTentativeDate.DateInput.SelectedDate;
                        h.ActualDate = rdtActualDate.DateInput.SelectedDate;
                        h.Opinion = txtOpinion.Text;
                        h.HealthRemarks = txtRemarks.Text;
                        h.HospitalID = txtRegNo.Text;
                        h.RecvdChqno = txtRecvdChqno.Text;
                        h.RecvdChqDate = rdRecvdChqDate.DateInput.SelectedDate;
                        if (string.IsNullOrEmpty(txtSancAmount.Text))
                        {
                            txtSancAmount.Text = "0";
                        }
                        if (string.IsNullOrEmpty(txtAmount.Text))
                        {
                            txtAmount.Text = "0";
                        }

                        {
                            h.BalReturn = Convert.ToInt32(txtSancAmount.Text) - Convert.ToInt32(txtAmount.Text);
                        }
                        //h.BalReturn = txtBalReturned.Text == string.Empty ? 0 : Convert.ToInt32(txtBalReturned.Text);
                        h.BalChqno = txtBalChqNo.Text;
                        h.BalChqDate = rdBalChqDate.DateInput.SelectedDate;
                        h.MedBoardDate = rdtMedicalBoardDate.DateInput.SelectedDate;
                        h.Recommendations = txtRecommendations.Text;
                        h.ReportSentOn = rdtReportSentOn.DateInput.SelectedDate;
                        h.Dateheldon = rdtDateHeldOn.DateInput.SelectedDate;
                        dg.SaveChanges();
                        Cls_Login login = (Cls_Login)Session["Login"];
                        GetRecord(login.Username, login.Groupid);
                        //ClearFields();
                        ScriptManager.RegisterStartupScript(this, GetType(), "Alert", "alert('Record Saved Successfully ..');", true);
                    }
                    else
                    {
                        var m = getfano.First();
                        Health h = new Health();
                        h.fanoHealth = fanos;
                        h.pagenoHealth = txtPageNo.Text == string.Empty ? 0 : Convert.ToInt32(txtPageNo.Text);
                        if (ddlRefferedTo.SelectedItem.Value == "Select Hospital")
                        {
                            h.refid = null;
                        }
                        else
                        {
                            h.refid = Convert.ToInt32(ddlRefferedTo.SelectedItem.Value);
                        }

                        if (ddlStatus.SelectedItem.Value == "Select Status")
                        {
                            h.PtStatusID = null;
                        }
                        else
                        {
                            h.PtStatusID = Convert.ToInt32(ddlStatus.SelectedItem.Value);
                        }
                        h.letterno = txtLetterNo.Text;
                        h.AmountInc = txtAmount.Text == string.Empty ? 0 : Convert.ToInt32(txtAmount.Text);
                        h.HealthAmount = txtSancAmount.Text == string.Empty ? 0 : Convert.ToInt32(txtSancAmount.Text);
                        h.LetDate = dtLetterDate.DateInput.SelectedDate;
                        h.UpdatedOn = DateTime.Now;
                        //h.UpdatedOn = rdtUpdatedOn.DateInput.SelectedDate;
                        h.fdSactionNo = txtFDSancNo.Text;
                        h.PatientName = txtPatientName.Text;
                        h.fdSanctionDate = rdtFDSancDate.DateInput.SelectedDate;
                        h.VouchedDate = rdtVouchedDate.DateInput.SelectedDate;
                        h.LiquidationDate = rdtLiquidDate.DateInput.SelectedDate;
                        h.AuthorityNo = txtAuthorityNo.Text;
                        h.AuthorityDate = rdtAuthorityDate.DateInput.SelectedDate;
                        h.FileNumber = txtFileNumber.Text;
                        h.RefOn = dtRefferedDate.DateInput.SelectedDate;
                        h.TentativeDate = rdtTentativeDate.DateInput.SelectedDate;
                        h.ActualDate = rdtActualDate.DateInput.SelectedDate;
                        h.Opinion = txtOpinion.Text;
                        h.HealthRemarks = txtRemarks.Text;
                        h.HospitalID = txtRegNo.Text;
                        h.RecvdChqno = txtRecvdChqno.Text;
                        h.RecvdChqDate = rdRecvdChqDate.DateInput.SelectedDate;
                        if (string.IsNullOrEmpty(txtSancAmount.Text))
                        {
                            txtSancAmount.Text = "0";
                        }
                        if (string.IsNullOrEmpty(txtAmount.Text))
                        {
                            txtAmount.Text = "0";
                        }

                        {
                            h.BalReturn = Convert.ToInt32(txtSancAmount.Text) - Convert.ToInt32(txtAmount.Text);
                        }
                        // h.BalReturn = txtBalReturned.Text == string.Empty ? 0 : Convert.ToInt32(txtBalReturned.Text);
                        h.BalChqno = txtBalChqNo.Text;
                        h.BalChqDate = rdBalChqDate.DateInput.SelectedDate;
                        h.MedBoardDate = rdtMedicalBoardDate.DateInput.SelectedDate;
                        h.Recommendations = txtRecommendations.Text;
                        h.ReportSentOn = rdtReportSentOn.DateInput.SelectedDate;
                        h.Dateheldon = rdtDateHeldOn.DateInput.SelectedDate;
                        dg.Healths.AddObject(h);
                        dg.SaveChanges();
                        Cls_Login login = (Cls_Login)Session["Login"];
                        GetRecord(login.Username, login.Groupid);
                        //ClearFields();
                        ScriptManager.RegisterStartupScript(this, GetType(), "Alert", "alert('Record Saved Successfully ..');", true);
                    }
                }
                else
                {



                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Alert", "alert('"+ex.Message.ToString()+"');", true);
            }

        }

        

    }
}
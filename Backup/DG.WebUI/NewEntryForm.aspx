﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DGMasterPage.Master" AutoEventWireup="true" CodeBehind="NewEntryForm.aspx.cs" Inherits="DG.WebUI.NewEntryForm" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<!--<link href="Styles/bootstrap.css" rel="stylesheet" type="text/css"/> -->
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
 <script language="javascript">

 
     function OnFiguresChange() {
         var obj1 = document.getElementById('<%=txtAmount.ClientID %>');
         var obj2 = document.getElementById('<%=txtAmountinWords.ClientID %>');
         obj2.value = ConvertToWords(obj1.value);
         //alert("test");
     }
     function GetWords(figure) {
         var singles = new Array();
         singles[0] = "";
         singles[1] = "One";
         singles[2] = "Two";
         singles[3] = "Three";
         singles[4] = "Four";
         singles[5] = "Five";
         singles[6] = "Six";
         singles[7] = "Seven";
         singles[8] = "Eight";
         singles[9] = "Nine";
         singles[10] = "Ten";
         singles[11] = "Eleven";
         singles[12] = "Twelve";
         singles[13] = "Thirteen";
         singles[14] = "Fourteen";
         singles[15] = "Fifteen";
         singles[16] = "Sixteen";
         singles[17] = "Seventeen";
         singles[18] = "Eighteen";
         singles[19] = "Nineteen";
         singles[20] = "Twenty";

         var tens = new Array();
         tens[2] = "Twenty";
         tens[3] = "Thrity";
         tens[4] = "Forty";
         tens[5] = "Fifty";
         tens[6] = "Sixty";
         tens[7] = "Seventy";
         tens[8] = "Eighty";
         tens[9] = "Ninty";

         if (figure <= 20) { return singles[figure]; }
         else { return tens[Math.floor(figure / 10)] + " " + singles[figure % 10]; }
     }
     function ConvertToWords(amount) {
         var units = new Array();
         units[0] = "Hundred";
         units[1] = "Thousand";
         units[2] = "Lacs";
         units[3] = "Crore";
         var returnValue = "";


         var intTens = amount % 100;
         amount /= 100;
         amount = Math.floor(amount);

         var inHunderds = amount % 10;
         amount /= 10;
         amount = Math.floor(amount);

         var inThousands = amount % 100;
         amount /= 100;
         amount = Math.floor(amount);

         var inLacs = amount % 100;
         amount /= 100;
         amount = Math.floor(amount);

         var inCrores = amount;

         if (inCrores > 0) {
             returnValue += " " + GetWords(inCrores) + " " + units[3];
         }

         if (inLacs > 0) {
             returnValue += " " + GetWords(inLacs) + " " + units[2];
         }

         if (inThousands > 0) {
             returnValue += " " + GetWords(inThousands) + " " + units[1];
         }

         if (inHunderds > 0) {
             returnValue += " " + GetWords(inHunderds) + " " + units[0];
         }

         if (intTens > 0) {
             if (returnValue != "") {
                 returnValue += " and";
             }
             returnValue += " " + GetWords(intTens);
         }

         return returnValue;
     }
    </script>    
<h1 align = "center"> New Entry Form </h1>
<div>
    <table  align = "center" cellpadding = "0" cellspacing = "0" style = "border:1px dashed white">
               
        <tr>
            <td colspan="3" align="center">
           <!-- <asp:Panel ID="pnlFind" runat="server" DefaultButton="btnFind">-->
              <div class="row">
                   <div class="col-lg-6">
    <div class="input-group">
                <asp:TextBox ID="txtFind" Width="150" Height="34px" CssClass="form-control" runat="server"></asp:TextBox>
                <span class="input-group-btn" style="float: left;padding-left: 0px;">
                <asp:Button ID="btnFind" runat="server" Text="Find" CssClass="btn btn-primary btn-md" onclick="btnFind_Click" />
                </span>
              <!--  </asp:Panel> -->
                 </div></div></div>
            </td>
        </tr>
       
        
        <tr>
            <td colspan = "2" align = "center" height = "30">
                Constituency
           </td>
            <td align = "center">Name</td>
        </tr>
        <tr>
            
            <td> <asp:TextBox ID="txtCatcode" CssClass="form-control" runat="server" 
                    Width = "100px" ToolTip="CatCode"></asp:TextBox> </td>
            <td> <asp:TextBox ID="txtCons" CssClass="form-control" runat="server" 
                    Width = "100px" AutoPostBack="True" 
                    ontextchanged="txtCons_TextChanged" ToolTip="Cons"></asp:TextBox> </td>
            <td> <asp:TextBox ID="txtName" CssClass="form-control" runat="server" 
                    Width = "250px" ToolTip="MPA/MNA Name"></asp:TextBox> </td>
        </tr>
        </table>

              <br />
                 <table width = "80%" align = "center" cellpadding = "0" cellspacing = "0" style = "border:1px dashed white">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblFaNo" runat="server" Text="FaNo" ForeColor="Black" Font-Size="Medium"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtFaNo" runat="server" Width = "200px" CssClass="form-control" ReadOnly="True"></asp:TextBox>
                        </td>
                        <td align="left"> 
                            <asp:Label ID="lblDate" runat="server" Text="Date" ForeColor="Black" 
                                Font-Size="Medium"></asp:Label></td>
                        <td align = "left">
                            <telerik:RadDateTimePicker ID="Date1" Runat="server" CssClass="form-control" 
                                Skin="Office2007" Width="204px">
                        <TimeView CellSpacing="-1" runat = "server"></TimeView>

                        <TimePopupButton ImageUrl="" HoverImageUrl=""></TimePopupButton>

                        <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x" 
                                    skin="Office2007" runat="server"></Calendar>

                        <DateInput DisplayDateFormat="dd-MMM-yy" DateFormat="dd-MMM-yy" LabelWidth="40%" height="25px" runat="server"></DateInput>

                        <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                            </telerik:RadDateTimePicker>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblAppName" runat="server" Text="Appname" ForeColor="Black" 
                                Font-Size="Medium"></asp:Label></td>
                        <td align="left">
                            <asp:TextBox ID="txtAppName" runat="server" CssClass="form-control"
                                Width="200px"></asp:TextBox>
                        </td>
                        <td align="left">

                            <asp:Label ID="lblContact" runat="server" Text="Contact" ForeColor="Black" Font-Size="Medium"></asp:Label>
                        </td>
                        <td align = "left">
                        
                            <asp:TextBox ID="txtContact" runat="server" Width = "200px" CssClass="form-control"></asp:TextBox>
                            
                        </td>
                    </tr>
                    <tr>
                    <td align="left">
                        <asp:Label ID="lblAddress" runat="server" Text="Address" ForeColor="Black" Font-Size="Medium"></asp:Label>
                    </td>
                           <td align="left">
                           
                               <asp:TextBox ID="txtAddress" runat="server" Width="350px" CssClass="form-control" 
                                   TextMode="MultiLine"></asp:TextBox>
                                  
                            </td>
                   <td align = "left">
                                <asp:Label ID="lblRefNo" runat="server" Text="Reference No" ForeColor="Black" Font-Size="Medium"></asp:Label>
                   </td>
                            
                            <td align="left">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                
                                <asp:TextBox ID="txtRef" runat="server" Width="350px" CssClass="form-control"></asp:TextBox>
                                 </ContentTemplate>
                            </asp:UpdatePanel>
                                
                                 
                            </td>
                            
                    </tr>
                    
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblDistrict" runat="server" Text="District" ForeColor="Black" Font-Size="Medium"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtDistrict" runat="server" Width = "200px" CssClass="form-control"></asp:TextBox>
                        </td>
                        <td align="left">
                            <asp:Label ID="lblType" runat="server" Text="Type" ForeColor="Black" Font-Size="Medium"></asp:Label>
                        </td>
                        <td align = "left">
                            <asp:DropDownList ID="ddlType" runat="server" CssClass="form-control" Width = "200px">
                            </asp:DropDownList>
                            <asp:TextBox ID="txtSubject" runat="server" Width="350px" 
                                CssClass="form-control"></asp:TextBox>
                        </td>
                           
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblCNIC" runat="server" Text="CNIC" ForeColor="Black" Font-Size="Medium"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtCNIC" runat="server" Width="200px" CssClass="form-control"></asp:TextBox>
                            
                            </td>

                            <td align="left">
                            <asp:Label ID="lblLetterNo" runat="server" Text="Letter No." ForeColor="Black" Font-Size="Medium"></asp:Label>
                        </td>
                        <td align = "left">
                            <asp:TextBox ID="txtLetterNo" runat="server" Width = "200px" CssClass="form-control"></asp:TextBox>
                        </td>

                    </tr>
                    
                      <tr>
                        <td align="left">
                            <asp:Label ID="lblDepartment" runat="server" Text="Department" ForeColor="Black" Font-Size="Medium"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:DropDownList ID="ddlDepartment" runat="server" Width = "200px" CssClass="form-control">
                            </asp:DropDownList>
                        </td>
                        
                        <td align="left">
                            <asp:Label ID="lblSentOn" runat="server" Text="Sent On" ForeColor="Black" Font-Size="Medium"></asp:Label>
                        </td>
                        <td align="left">
                            <telerik:RadDateTimePicker ID="SentOn" Runat="server" CssClass="form-control" 
                                Skin="Office2007" Width="200px">
                            <TimeView ID="TimeView1" CellSpacing="-1" runat="server"></TimeView>

                            <TimePopupButton ImageUrl="" HoverImageUrl=""></TimePopupButton>

                            <Calendar ID="Calendar1" UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x" 
                                    Skin="Office2007" runat="server"></Calendar>

                            <DateInput ID="DateInput1" DisplayDateFormat="dd-MMM-yy" DateFormat="dd-MMM-yy" LabelWidth="40%" Height="25px" runat="server"></DateInput>

                            <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                            </telerik:RadDateTimePicker>
                        </td>
                    </tr>
                    <tr>
                        
                        <td align="left">
                            <asp:Label ID="lblTargetDate" runat="server" Text="Target Date" ForeColor="Black" Font-Size="Medium"></asp:Label>
                        </td>
                        <td align = "left" class="style2">
                            <telerik:RadDateTimePicker ID="TargetDate" runat="server" CssClass="form-control" 
                                Skin="Office2007" Width="206px">
                            <TimeView CellSpacing="-1" runat="server"></TimeView>

                            <TimePopupButton ImageUrl="" HoverImageUrl=""></TimePopupButton>

                            <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x" 
                                    Skin="Office2007" runat="server"></Calendar>

                            <DateInput DisplayDateFormat="dd-MMM-yy" DateFormat="dd-MMM-yy" LabelWidth="40%" Height="22px" runat="server"></DateInput>

                            <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                            </telerik:RadDateTimePicker>
                            
                        </td>
                        <td align="left">
                            <asp:Label ID="lblMode" runat="server" Text="Mode of F.Aid (D,G,F)" ForeColor="Black" Font-Size="Medium"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtMode" runat="server" Width="200px" CssClass="form-control"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            PC1: 
                            
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtPC1" runat="server" Width="350px" CssClass="form-control"></asp:TextBox>
                        </td>
                        <td align="left">
                            PC2: 
                            
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtPC2" runat="server" Width="350px" CssClass="form-control"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            PC3: 
                            
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtPC3" runat="server" Width="350px" CssClass="form-control"></asp:TextBox>
                        </td>
                        <td align="left">
                            PC4: 
                            
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtPC4" runat="server" Width="350px" CssClass="form-control"></asp:TextBox>
                        </td>
                    </tr>

                    <tr>
                        <td align="left">
                            <asp:Label ID="lblReplyNo" runat="server" Text="Reply #" ForeColor="Black" Font-Size="Medium"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtReplyNo" runat="server" Width = "200px" CssClass="form-control"></asp:TextBox>
                        </td>
                        <td align="left">
                            <asp:Label ID="lblReplyDate" runat="server" Text="Reply Date" ForeColor="Black" Font-Size="Medium"></asp:Label>
                        </td>
                        <td align = "left">
                            <telerik:RadDateTimePicker ID="ReplyDate" runat="server" 
                                Skin="Office2007" CssClass="form-control" Width="200px">
                        <TimeView CellSpacing="-1" runat="server"></TimeView>

                        <TimePopupButton ImageUrl="" HoverImageUrl="" runat="server"></TimePopupButton>

                        <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x" 
                                    Skin="Office2007" runat="server"></Calendar>

                        <DateInput DisplayDateFormat="dd-MMM-yy" DateFormat="dd-MMM-yy" LabelWidth="40%" 
                                    runat="server" Height="23px"></DateInput>

                        <DatePopupButton ImageUrl="" HoverImageUrl="" runat="server"></DatePopupButton>
                            </telerik:RadDateTimePicker>
                            
                        </td>

                    </tr>
                                      
                    <tr>
                            <td align="left">
                                <asp:Label ID="lblStatus" runat="server" Text="Status" ForeColor="Black" Font-Size="Medium"></asp:Label>
                            </td>        
                        
                        <td align = "left" class="style2">
                            <asp:DropDownList ID="ddlStatus" runat="server" CssClass="form-control" 
                                Width = "200px" AutoPostBack="True" 
                                onselectedindexchanged="ddlStatus_SelectedIndexChanged">
                            </asp:DropDownList>
                            
                        </td>
                        <td align="left">
                            <asp:Label ID="lblRecommendation" runat="server" Text="Recommendation of DCO" ForeColor="Black" Font-Size="Medium"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtRecommendation" runat="server" Width="200px" CssClass="form-control"></asp:TextBox>
                        </td>
                        
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblMeeting" runat="server" Text="Meeting" ForeColor="Black" Font-Size="Medium"></asp:Label>
                            
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtMeeting" runat="server" Width="200px" CssClass="form-control"></asp:TextBox>
                        </td>
                        <td align="left">
                            <asp:Label ID="lblAmount" runat="server" Text="Amount" ForeColor="Black" Font-Size="Medium"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtAmount" runat="server" Width="200px" CssClass="form-control"></asp:TextBox> 
                            <asp:TextBox ID="txtAmountinWords" runat="server" Width="350px" CssClass="form-control"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblCMOrders" runat="server" Text="CM Orders" ForeColor="Black" Font-Size="Medium"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtCMOrders" runat="server" CssClass="form-control" TextMode="MultiLine" 
                                Width="360px"></asp:TextBox>
                        </td>
                    
                    
                        <td align="left">
                           <asp:Label ID="lblRemarks" runat="server" Text="Remarks" ForeColor="Black" Font-Size="Medium"></asp:Label>
                        </td>
                        <td align = "left">
                            <asp:TextBox ID="txtRemarks" runat="server" Width = "350px" CssClass="form-control"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblRefferedTo" runat="server" Text="Ref.to Committee On" ForeColor="Black" Font-Size="Medium"></asp:Label>
                        </td>
                        <td align="left">
                            <telerik:RadDateTimePicker ID="ReftoCommDate" runat="server" CssClass="form-control" 
                                Width="200px">
                            <TimeView CellSpacing="-1" runat="server"></TimeView>

                            <TimePopupButton ImageUrl="" HoverImageUrl=""></TimePopupButton>

                            <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x" runat="server"></Calendar>

                            <DateInput DisplayDateFormat="dd-MMM-yy" DateFormat="dd-MMM-yy" LabelWidth="40%" Height="25px" runat="server"></DateInput>

                            <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                            </telerik:RadDateTimePicker>
                        </td>
                            <td align="left">
                                <asp:Label ID="lblMeetingDate" runat="server" Text="Meeting Date" ForeColor="Black" Font-Size="Medium"></asp:Label>
                            </td>
                        <td align="left">
                            <telerik:RadDateTimePicker ID="MeetingDate" runat="server" CssClass="form-control" 
                                Skin="Office2007" Width="200px">
                            <TimeView CellSpacing="-1" runat="server"></TimeView>

                            <TimePopupButton ImageUrl="" HoverImageUrl=""></TimePopupButton>

                            <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x" 
                                    Skin="Office2007" runat="server"></Calendar>

                            <DateInput DisplayDateFormat="dd-MMM-yy" DateFormat="dd-MMM-yy" LabelWidth="40%" Height="25px" runat="server"></DateInput>

                            <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                            </telerik:RadDateTimePicker>
                        </td>
                    </tr>
                   
                    <tr>
                    <td align = "center" colspan = "4">
                        <br />
                        <asp:Button ID="btnAdd" runat="server" Text="Add" Font-Size="Medium" CssClass="btn btn-primary btn-lg" Width="85px" onclick="btnAdd_Click" />
                        <asp:Button ID="btnEdit" runat="server" Text="Edit" Font-Size="Medium" CssClass="btn btn-primary btn-lg" Width="85px" onclick="btnEdit_Click" />
                        <asp:Button ID="btnSave" runat="server" Text="Save" Font-Size="Medium" CssClass="btn btn-primary btn-lg" Width="85px" onclick="btnSave_Click" />
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" Font-Size="Medium" CssClass="btn btn-primary btn-lg" Width="85px" onclick="btnCancel_Click" />
                    </td>
                    </tr>

                    

        </table>

</div>              
</asp:Content>

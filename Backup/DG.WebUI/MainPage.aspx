﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DGMasterPage.Master" AutoEventWireup="true" CodeBehind="MainPage.aspx.cs" Inherits="DG.WebUI.MainPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<div>
    <!--<table align = "center" width="1000px">
       
        <tr>
            <td>
       <br /> <br /> <br />     
                <img src="Images/SSharif.png" alt="" />
            </td>
        
        </tr>
        
    </table>-->

     <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner" role="listbox">
        <div class="item active">
          <img class="first-slide" src="Images/SSBig.jpg" alt="First slide" />
          <div class="container">
            <div class="carousel-caption">
              <h1>Chief Minister Punjab</h1>
              Discretionary Grant Program - <p> Chief Minister Punjab, Mian Muhammad Shahbaz Sharif loves to help the poor and needy people. A big amount is fixed in the budget for Discretionary Grant. On daily basis a lot of needy people come to his office, he himself and his staff manage to help all the poor people.</p>
              <p><a class="btn btn-lg btn-primary" href="LoginPage.aspx" role="button"></a></p>
            </div>
          </div>
        </div>
        <div class="item">
          <img class="second-slide" src="Images/map-of-punjab-pakistan.jpg" alt="Second slide">
          <div class="container">
            <div class="carousel-caption">
              <h1>Discretionary Grant</h1>
              <h3> From all over the Punjab poor and needy people come to Chief Minister's Office at 8-Club Road, Lahore.</h3>
              <p><a class="btn btn-lg btn-primary" href="#" role="button"></a></p>
                
            </div>
          </div>
        </div>
        <div class="item">
          <img class="third-slide" src="Images/SSharif.png" alt="Third slide">
          <div class="container">
            <div class="carousel-caption">
              <h1>We love Pakistan.</h1>
              <p>We all should love with our country.</p>
              <p><a class="btn btn-lg btn-primary" href="#" role="button"></a></p>
            </div>
          </div>
        </div>
      </div>
      <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div><!-- /.carousel -->


</div>


</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DGMasterPage.Master" AutoEventWireup="true" CodeBehind="DgFormDirector.aspx.cs" Inherits="DG.WebUI.DgFormDirector" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<!--<link href="Styles/bootstrap.css" rel="stylesheet" type="text/css"/>-->
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>

<h1 align = "center"> Director Office </h1>
<div>
    <table  align = "center" cellpadding = "0" cellspacing = "0" style = "border:1px dashed white"  bgcolor="#F0E68C">
               
        <tr>
            <td colspan="3" align="center">
           <!-- <asp:Panel ID="pnlFind" runat="server" DefaultButton="btnFind">-->
              <div class="row">
                   <div class="col-lg-6">
    <div class="input-group">
                <asp:TextBox ID="txtFind" Width="150" Height="34px" CssClass="form-control" runat="server"></asp:TextBox>
                <span class="input-group-btn" style="float: left;padding-left: 0px;">
                <asp:Button ID="btnFind" runat="server" Text="Find" CssClass="btn btn-primary btn-md" onclick="btnFind_Click" />
                </span>
              <!--  </asp:Panel> -->
                 </div></div></div>
            </td>
        </tr>
       
        
        <tr>
            <td colspan = "2" align = "center" height = "30">
                Constituency
           </td>
            <td align = "center">Name</td>
        </tr>
        <tr>
            
            <td> <asp:TextBox ID="txtCatcode" CssClass="form-control" runat="server" 
                    Width = "100px" ToolTip="CatCode"></asp:TextBox> </td>
            <td> <asp:TextBox ID="txtCons" CssClass="form-control" runat="server" 
                    Width = "100px" AutoPostBack="True" 
                    ontextchanged="txtCons_TextChanged" ToolTip="Cons"></asp:TextBox> </td>
            <td> <asp:TextBox ID="txtName" CssClass="form-control" runat="server" 
                    Width = "250px" ToolTip="MPA/MNA Name"></asp:TextBox> </td>
        </tr>
        </table>
              <br />
                 <table width = "80%" align = "center" cellpadding = "0" cellspacing = "0" style = "border:1px dashed white">
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblFaNo" runat="server" Text="FaNo"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtFaNo" runat="server" Width = "200px" CssClass = "form-control" ReadOnly="True"></asp:TextBox>
                        </td>
                        <td align="left"> <asp:Label ID="lblDate" runat="server" Text="Date"></asp:Label></td>
                        <td align = "left">
                            <telerik:RadDateTimePicker ID="date" Runat="server" CssClass = "form-control" 
                                Width="200px" Skin="Office2007">
                             <TimeView CellSpacing="-1" runat = "server"></TimeView>

                            <TimePopupButton ImageUrl="" HoverImageUrl=""></TimePopupButton>

                            <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" 
                                    ViewSelectorText="x" runat="server" skin="Office2007"></Calendar>

                            <DateInput DisplayDateFormat="dd-MMM-yy" DateFormat="dd-MMM-yy" LabelWidth="40%" height="24px" runat="server"></DateInput>

                            <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                            </telerik:RadDateTimePicker>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblAppName" runat="server" Text="Appname"></asp:Label></td>
                        <td align="left">
                            <asp:TextBox ID="txtAppName" runat="server" 
                                Width="200px" CssClass = "form-control"></asp:TextBox>
                        </td>
                        <td align="left">

                            <asp:Label ID="lblContact" runat="server" Text="Contact"></asp:Label>
                        </td>
                        <td align = "left">
                        
                            <asp:TextBox ID="txtContact" runat="server" CssClass = "form-control" Width = "200px"></asp:TextBox>
                            
                        </td>
                    </tr>
                    <tr>
                    <td align="left">
                        <asp:Label ID="lblAddress" runat="server" Text="Address"></asp:Label>
                    </td>
                           <td align="left">
                           
                               <asp:TextBox ID="txtAddress" runat="server" Width="350px" CssClass = "form-control" 
                                   TextMode="MultiLine"></asp:TextBox>
                                  
                            </td>
                            <td align="left">
                                <asp:Label ID="lblRefNo" runat="server" Text="Reference No"></asp:Label>
                            </td>
                            
                            <td align="left">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                
                                <asp:TextBox ID="txtRef" runat="server" CssClass = "form-control" Width="300px"></asp:TextBox>
                                 </ContentTemplate>
                            </asp:UpdatePanel>
                                
                                 
                            </td>
                            
                    </tr>
                    
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblDistrict" runat="server" Text="District"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtDistrict" runat="server" Width = "200px" CssClass = "form-control"></asp:TextBox>
                        </td>
                        <td align="left">
                            <asp:Label ID="lblType" runat="server" Text="Type"></asp:Label>
                        </td>
                        <td align = "left">
                            <asp:DropDownList ID="ddlType" runat="server" Width = "200px" 
                                CssClass = "form-control">
                            </asp:DropDownList>
                            <asp:TextBox ID="txtSubject" runat="server" Width="315px" 
                                CssClass = "form-control"></asp:TextBox>
                        </td>
                                                   
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblSubCat" runat="server" Text="Sub Category"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtSubCat" runat="server" Width="200px" CssClass = "form-control"></asp:TextBox>
                        </td>
                        <td align="left">
                            <asp:Label ID="lblDept" runat="server" Text="Department"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:DropDownList ID="ddlDepartment" runat="server" Width = "200px" CssClass = "form-control">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left"> <asp:Label ID="lblSentOn" runat="server" Text="SentOn"></asp:Label></td>
                        <td align = "left">
                            <telerik:RadDateTimePicker ID="SentOn" Runat="server" CssClass = "form-control" 
                                Skin="Office2007" Width="200px">
                            <TimeView CellSpacing="-1" runat="server"></TimeView>

                            <TimePopupButton ImageUrl="" HoverImageUrl=""></TimePopupButton>

                            <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x" 
                                    skin="Office2007" runat="server"></Calendar>

                            <DateInput DisplayDateFormat="dd-MMM-yy" DateFormat="dd-MMM-yy" LabelWidth="40%" height="23px" runat="server"></DateInput>

                        <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                            </telerik:RadDateTimePicker>
                        </td>

                        <td align="left"> <asp:Label ID="lblTargetDate" runat="server" Text="Target Date"></asp:Label></td>
                        <td align = "left">
                            <telerik:RadDateTimePicker ID="TargetDate" Runat="server" 
                                Skin="Office2007" Width="200px" CssClass = "form-control">
                            <TimeView CellSpacing="-1" runat = "server"></TimeView>

                            <TimePopupButton ImageUrl="" HoverImageUrl=""></TimePopupButton>

                            <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x" 
                                    skin="Office2007" runat="server"></Calendar>

                            <DateInput DisplayDateFormat="dd-MMM-yy" DateFormat="dd-MMM-yy" LabelWidth="40%" runat="server"></DateInput>

                            <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                            </telerik:RadDateTimePicker>
                        </td>

                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblReplyNo" runat="server" Text="Reply No."></asp:Label>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtReplyno" runat="server" Width="200px" CssClass = "form-control"></asp:TextBox>
                        </td>

                        <td align="left">
                            <asp:Label ID="lblReplyDate" runat="server" Text="Reply Date"></asp:Label>
                        </td>
                        <td align="left">
                            
                            <telerik:RadDateTimePicker ID="ReplyDate" Runat="server" CssClass = "form-control" 
                                Skin="Office2007" Width="200px">
                            <TimeView CellSpacing="-1" runat="server"></TimeView>

                            <TimePopupButton ImageUrl="" HoverImageUrl=""></TimePopupButton>

                            <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x" 
                                    skin="Office2007" runat="server"></Calendar>

                            <DateInput DisplayDateFormat="dd-MMM-yy" DateFormat="dd-MMM-yy" LabelWidth="40%" height="23px" runat="server"></DateInput>

                            <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                            </telerik:RadDateTimePicker>
                            
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblDCO" runat="server" Text="Recommendation of DCO"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtDCO" runat="server" Width="200px" CssClass = "form-control"></asp:TextBox>
                        </td>
                        <td align="left">
                            <asp:Label ID="lblIncome" runat="server" Text="Monthly Income"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtIncome" runat="server" Width="200px" CssClass = "form-control"></asp:TextBox>
                        </td>

                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="LblLetterno" runat="server" Text="Letter No."></asp:Label>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtLetCode" runat="server" Width="200px" CssClass = "form-control"></asp:TextBox>
                        </td>
                        <td align="left">
                            <asp:Label ID="lblStatus" runat="server" Text="Status"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:DropDownList ID="ddlStatus" runat="server" Width = "200px"  CssClass = "form-control">
                            </asp:DropDownList>
                        </td>
                        
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblASADate" runat="server" Text="AS(A) Date"></asp:Label>
                        </td>
                        <td align="left">
                            <telerik:RadDateTimePicker ID="ASADate" runat="server" 
                                Skin="Office2007" Width="200px" CssClass = "form-control">
                            <TimeView CellSpacing="-1" runat="server"></TimeView>

                            <TimePopupButton ImageUrl="" HoverImageUrl=""></TimePopupButton>

                            <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x" 
                                    skin="Office2007" runat="server"></Calendar>

                            <DateInput DisplayDateFormat="dd-MMM-yy" DateFormat="dd-MMM-yy" LabelWidth="40%" runat="server"></DateInput>

                            <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                            </telerik:RadDateTimePicker>
                        </td>
                        <td align="left">
                            <asp:Label ID="lblCNIC" runat="server" Text="CNIC"></asp:Label>
                            <br />
                            <asp:Label ID="lblFamNo" runat="server" Text="Family #"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtCNIC" runat="server" Width = "200px"  CssClass = "form-control"></asp:TextBox>
                            <asp:TextBox ID="txtFamNo" runat="server" Width= "200px"   CssClass = "form-control"></asp:TextBox>
                        </td>
                        
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblAmount" runat="server" Text="Amount"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtAmount" runat="server" Width = "200px" CssClass = "form-control"></asp:TextBox>
                        </td>
                        <td align="left">
                            <asp:Label ID="lblAmtinWords" runat="server" Text="Amount in Words"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtAmtinWords" runat="server" Width = "400px" CssClass = "form-control"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblSCMDate" runat="server" Text="SCM Date"></asp:Label>
                        </td>
                        <td align="left">
                            <telerik:RadDateTimePicker ID="SCMDate" runat="server" 
                                Skin="Office2007" Width="200px" CssClass = "form-control">
                            <TimeView CellSpacing="-1" runat="server"></TimeView>

                            <TimePopupButton ImageUrl="" HoverImageUrl=""></TimePopupButton>

                            <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x" 
                                    skin="Office2007" runat="server"></Calendar>

                            <DateInput DisplayDateFormat="dd-MMM-yy" DateFormat="dd-MMM-yy" LabelWidth="40%" runat="server"></DateInput>

                            <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                            </telerik:RadDateTimePicker>
                        </td>
                        <td align="left">
                            <asp:Label ID="lblSCMFile" runat="server" Text="SCM FileNo"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtSCMFileNo" runat="server" Width = "200px" CssClass = "form-control"></asp:TextBox>
                        </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <asp:Label ID="lblBaitDate" runat="server" Text="Bait Date"></asp:Label>
                            </td>
                            <td align="left">
                                <telerik:RadDateTimePicker ID="BaitDate" runat="server" 
                                    Skin="Office2007" Width="200px" CssClass = "form-control">
                                <TimeView CellSpacing="-1" runat="server"></TimeView>

                                <TimePopupButton ImageUrl="" HoverImageUrl=""></TimePopupButton>

                                <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x" 
                                        skin="Office2007" runat="server"></Calendar>

                                <DateInput DisplayDateFormat="dd-MMM-yy" DateFormat="dd-MMM-yy" LabelWidth="40%" runat="server"></DateInput>

                                <DatePopupButton ImageUrl="" HoverImageUrl="" runat="server"></DatePopupButton>
                                </telerik:RadDateTimePicker>
                            </td>
                            <td align="left">
                                <asp:Label ID="lblRemarks" runat="server" Text="Remarks"></asp:Label>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtRemarks" runat="server" Width = "400px" CssClass = "form-control" 
                                    TextMode="MultiLine"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <asp:Label ID="lblfYear" runat="server" Text="Financial Year"></asp:Label>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtfYear" runat="server" Width = "200px" CssClass = "form-control"></asp:TextBox>
                            </td>
                        </tr>
                           <tr>
                    <td align = "center" colspan = "4">
                        <br />
                        <asp:Button ID="btnAdd" runat="server" Text="Add" Width="85px" onclick="btnAdd_Click" Font-Size="Medium" CssClass="btn btn-primary btn-lg" />
                        <asp:Button ID="btnEdit" runat="server" Text="Edit" Width="85px" onclick="btnEdit_Click" Font-Size="Medium" CssClass="btn btn-primary btn-lg" />
                        <asp:Button ID="btnSave" runat="server" Text="Save" Width="85px" onclick="btnSave_Click" Font-Size="Medium" CssClass="btn btn-primary btn-lg"/>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" Width="85px" onclick="btnCancel_Click" Font-Size="Medium" CssClass="btn btn-primary btn-lg" />
                    </td>
                    </tr>

        </table>
</div>
</asp:Content>

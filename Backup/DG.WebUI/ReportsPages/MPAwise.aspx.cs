﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DGModel;
using Microsoft.Reporting.WebForms;

namespace DG.WebUI.ReportsPages
{
    public partial class MPAwise : System.Web.UI.Page
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadReport();

            }

            
        }

        private void LoadReport()
        {

                DGEntities1 dg = new DGEntities1();
                var repo = dg.dgrepo();
                ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports/fampa.rdlc");
                ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DataSet1", repo));
                ReportViewer1.LocalReport.Refresh();

            
                ////string proc = "dgrepo()";
                //string reponame = "~/Reports/fampa.rdlc";
                ////string repoproc = "dg.dgrepo()";
                //var repo = dg.dgrepo();
                //ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
                //ReportViewer1.LocalReport.ReportPath = Server.MapPath(reponame);
                //ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DataSet1", repo));
                //ReportViewer1.LocalReport.Refresh();
        }

            

        }

        //public System.Collections.IEnumerable repo { get; set; }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DGModel;

namespace DG.WebUI
{
    public partial class NewEntryForm : System.Web.UI.Page
    {
        int refid;
        protected void Page_Load(object sender, EventArgs e)
        {
            this.txtAmount.Attributes.Add("onkeyup", "OnFiguresChange()");
            if (!IsPostBack)
            {
                btnFind.Enabled = false;
                txtFind.Enabled = false;
                DisableFields();
                btnEdit.Visible = true;
                btnSave.Visible = false;
                GetDepartments();
                GetSubject();
                GetStatus();
                if (Request["DGID"] != null)
                {
                    //EditRecord();
                }
                if (Request["DGID"] == null)
                {
                    GetFaNo();
                    
                }
                DisableFields();    


            }
            btnSave.Attributes.Add("onclick", "return confirm('Do you want to Save this record[Yes / No]?');");
            
        }

        

        private void DisableFields()
        {
            txtCons.Enabled = false;
            txtCatcode.Enabled = false;
            txtName.Enabled = false;
            txtFaNo.Enabled = false;
            Date1.Enabled = false;
            txtAppName.Enabled = false;
            txtContact.Enabled = false;
            txtAddress.Enabled = false;
            txtCNIC.Enabled = false;
            txtDistrict.Enabled = false;
            txtRef.Enabled = false;
            ddlType.Enabled = false;
            txtSubject.Enabled = false;
            txtLetterNo.Enabled = false;
            ddlDepartment.Enabled = false;
            ddlStatus.Enabled = false;
            TargetDate.Enabled = false;
            SentOn.Enabled = false;
            txtMode.Enabled = false;
            ReplyDate.Enabled = false;
            txtPC1.Enabled = false;
            txtPC2.Enabled = false;
            txtPC3.Enabled = false;
            txtPC4.Enabled = false;
            txtReplyNo.Enabled = false;
            txtMeeting.Enabled = false;
            MeetingDate.Enabled = false;
            txtRecommendation.Enabled = false;
            txtCMOrders.Enabled = false;
            txtRemarks.Enabled = false;
            txtAmount.Enabled = false;
            txtAmountinWords.Enabled = false;
            ReftoCommDate.Enabled = false;

        }

        private void EnabledFields()
        {
            txtCons.Enabled = true;
            txtCatcode.Enabled = true;
            txtName.Enabled = true;
            txtFaNo.Enabled = true;
            Date1.Enabled = true;
            txtAppName.Enabled = true;
            txtContact.Enabled = true;
            txtAddress.Enabled = true;
            txtCNIC.Enabled = true;
            txtDistrict.Enabled = true;
            txtRef.Enabled = true;
            ddlType.Enabled = true;
            txtSubject.Enabled = true;
            txtLetterNo.Enabled = true;
            ddlDepartment.Enabled = true;
            ddlStatus.Enabled = true;
            TargetDate.Enabled = true;
            SentOn.Enabled = true;
            txtMode.Enabled = true;
            ReplyDate.Enabled = true;
            txtPC1.Enabled = true;
            txtPC2.Enabled = true;
            txtPC3.Enabled = true;
            txtPC4.Enabled = true;
            txtReplyNo.Enabled = true;
            txtMeeting.Enabled = true;
            MeetingDate.Enabled = true;
            txtRecommendation.Enabled = true;
            txtCMOrders.Enabled = true;
            txtRemarks.Enabled = true;
            txtAmount.Enabled = true;
            txtAmountinWords.Enabled = true;
            ReftoCommDate.Enabled = true;


        }


        private void GetFaNo()
        {
            DGEntities1 dg = new DGEntities1();
            int fano;
            var getfano = from a in dg.MAINCORRs select a.fano;
            if (getfano.Count() > 0)
            {
                var fansso = getfano.Max() + 1;
                fano = Convert.ToInt32(fansso);

            }
            else
            {
                fano = 1;
            }
            txtFaNo.Text = fano.ToString();
                        
        }

        private void GetStatus()
        {
            DGEntities1 dg = new DGEntities1();
            var stat = (from st in dg.status select st);
            ddlStatus.DataSource = stat;
            ddlStatus.DataValueField = "StatusCode";
            ddlStatus.DataTextField = "Description";
            ddlStatus.SelectedIndex = 0;
            ddlStatus.DataBind();

        }

        private void GetSubject()
        {
            DGEntities1 dg = new DGEntities1();
            var subj = (from s in dg.subjects select s);
            ddlType.DataSource = subj;
            ddlType.DataValueField = "SubjectCode";
            ddlType.DataTextField = "Description";
            ddlType.SelectedIndex = 0;
            ddlType.DataBind();
        }

        private void GetDepartments()
        {
            DGEntities1 dg = new DGEntities1();
            var dept = (from d in dg.DEPTs select d);
            ddlDepartment.DataSource = dept;
            ddlDepartment.DataValueField = "DeptCode";
            ddlDepartment.DataTextField = "DeptName";
            ddlDepartment.SelectedIndex = 0;
            ddlDepartment.DataBind();


        }

        protected void txtCons_TextChanged(object sender, EventArgs e)
        {

            int catcodes = Convert.ToInt32(txtCatcode.Text);
            int consss = Convert.ToInt32(txtCons.Text);
            DGEntities1 ent = new DGEntities1();
            var getmnampa = from a in ent.mpamnas where a.CatCode == catcodes && a.Cons == consss select a;
                if (getmnampa.Count() > 0)
                {
                    var mna = getmnampa.First();
                    txtName.Text = mna.Name;
                    refid = mna.RefId;
                }
                string ref_text = "DIR/FA/CMO/16/";

                switch (this.txtCatcode.Text)
                {
                    case "1":
                        ref_text += "AB";
                        break;
                    case "2":
                        ref_text += "AA";
                        break;
                    case "3":
                        ref_text += "SN";
                        break;
                    case "4":
                        ref_text += "OT";
                        break;
                }
                ref_text += "-" + txtCons.Text + "/" + "C" + "/" + "(" + txtFaNo.Text + ")";
                //txtRef.Text = "Dir/AS(A)/CMS/09/" + txtCatcode.Text+"-"+txtCons.Text +"/"+"C"+"/"+ "("+txtFaNo.Text+")";
                txtRef.Text = ref_text;
}
        
        protected void btnSave_Click(object sender, EventArgs e)
        {
            
            DGEntities1 dg = new DGEntities1();
            if (Cls_Encrypt.fanos != 1)
            {
                MAINCORR M = new MAINCORR();
                mpamna m = new mpamna();
                M.catcode = txtCatcode.Text == string.Empty ? 0 : Convert.ToInt32(txtCatcode.Text);
                M.cons = txtCons.Text == string.Empty ? 0 : Convert.ToInt32(txtCons.Text);
                M.fano = txtFaNo.Text == string.Empty ? 0 : Convert.ToInt32(txtFaNo.Text);
                M.name = txtName.Text;
                M.refdate = Date1.DateInput.SelectedDate;
                M.appname = txtAppName.Text;
                M.address = txtAddress.Text;
                //M.RefId = Convert.ToInt32(txtCons.Text); do the same thing here...
                int catcodes = Convert.ToInt32(txtCatcode.Text==""?"0":this.txtCatcode.Text);
                int consss = Convert.ToInt32(txtCons.Text == ""?"0":this.txtCons.Text);
                var getmnampa = from a in dg.mpamnas where a.CatCode == catcodes && a.Cons == consss select a;
                if (getmnampa.Count() > 0)
                {
                    var mna = getmnampa.First();
                    M.RefId = mna.RefId;
                    M.CATCONSMAIN = mna.CatCons;
                }
                M.refno = txtRef.Text;
                M.contactno = txtContact.Text;
                M.district = txtDistrict.Text;
                M.subjcode = Convert.ToInt32(ddlType.SelectedItem.Value);
                M.subject = txtSubject.Text;
                M.cnic = txtCNIC.Text;
                M.letcode = txtLetterNo.Text == string.Empty ? 0 : Convert.ToInt32(txtLetterNo.Text);
                M.deptcode = Convert.ToInt32(ddlDepartment.SelectedItem.Value);
                M.comdate = SentOn.DateInput.SelectedDate;
                M.targetdate = TargetDate.DateInput.SelectedDate;
                M.cataid = txtMode.Text;
                M.pc1 = txtPC1.Text;
                M.pc2 = txtPC2.Text;
                M.pc3 = txtPC3.Text;
                M.pc4 = txtPC4.Text;
                M.replno = txtReplyNo.Text;
                M.repldate = ReplyDate.DateInput.SelectedDate;
                M.statcode = Convert.ToInt32(ddlStatus.SelectedItem.Value);
                M.subdept = txtRecommendation.Text;
                M.set = txtMeeting.Text;
                M.amount = txtAmount.Text == string.Empty ? 0 : Convert.ToInt32(txtAmount.Text);
                M.amtinwords = txtAmountinWords.Text;
                M.cmorders = txtCMOrders.Text;
                M.remarks = txtRemarks.Text;
                M.refcomdate = ReftoCommDate.DateInput.SelectedDate;
                M.meetindate = MeetingDate.DateInput.SelectedDate;
                //deptcode,statcode,offcode,subjcode refid
                if (M.letcode == 0) M.letcode = null;
                if (M.deptcode == 0) M.deptcode = null;
                if (M.statcode == 0) M.statcode = null;
                if (M.offcode == 0) M.offcode = null;
                if (M.subjcode == 0) M.subjcode = null;
                if (M.RefId == 0) M.RefId = null;
                dg.MAINCORRs.AddObject(M);
                dg.SaveChanges();
                //txtAddress.Text = "";
                //txtAppName.Text = "";
                //txtContact.Text = "";
                DisableFields();
                
                
            }
            else
            {
                int dgid = Convert.ToInt32(txtFind.Text);
                MAINCORR M = new MAINCORR();
                M = (from a in dg.MAINCORRs where a.fano == dgid select a).First();
                M.catcode = txtCatcode.Text == string.Empty ? 0 : Convert.ToInt32(txtCatcode.Text);
                M.cons = txtCons.Text == string.Empty ? 0 : Convert.ToInt32(txtCons.Text);
                if ((M.cons == 46 || M.cons == 47 || M.cons == 62 || M.cons == 68) && M.catcode == 4)
                {
                    M.name = txtName.Text;
                }
                M.fano = txtFaNo.Text == string.Empty ? 0 : Convert.ToInt32(txtFaNo.Text);
                M.refdate = Date1.DateInput.SelectedDate;
                M.appname = txtAppName.Text;
                M.address = txtAddress.Text;
                M.refno = txtRef.Text;
                M.contactno = txtContact.Text;
                M.district = txtDistrict.Text;
                M.subjcode = Convert.ToInt32(ddlType.SelectedItem.Value);
                M.subject = txtSubject.Text;
                M.cnic = txtCNIC.Text;
                M.letcode = Convert.ToInt32(txtLetterNo.Text);
                M.deptcode = Convert.ToInt32(ddlDepartment.SelectedItem.Value);
                M.comdate = SentOn.DateInput.SelectedDate;
                M.targetdate = TargetDate.DateInput.SelectedDate;
                M.cataid = txtMode.Text;
                M.pc1 = txtPC1.Text;
                M.pc2 = txtPC2.Text;
                M.pc3 = txtPC3.Text;
                M.pc4 = txtPC4.Text;
                M.replno = txtReplyNo.Text;
                M.repldate = ReplyDate.DateInput.SelectedDate;
                M.statcode = Convert.ToInt32(ddlStatus.SelectedItem.Value);
                M.subdept = txtRecommendation.Text;
                M.set = txtMeeting.Text;
                M.amount = txtAmount.Text == string.Empty ? 0 : Convert.ToInt32(txtAmount.Text);
                M.amtinwords = txtAmountinWords.Text;
                M.cmorders = txtCMOrders.Text;
                M.remarks = txtRemarks.Text;
                M.refcomdate = ReftoCommDate.DateInput.SelectedDate;
                M.meetindate = MeetingDate.DateInput.SelectedDate;
                dg.SaveChanges();
                //Response.Redirect("ShowDGList.aspx");
            }
                btnAdd.Visible = true;
                ClearField();
                btnSave.Visible = false;
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("MainPage.aspx");
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
           Cls_Encrypt.fanos = 1;
            btnforEdit();
                        
            //Response.Redirect("ShowDGList.aspx");
        }

        private void btnforEdit()
        {
            txtFind.Focus();
            txtFind.Enabled = true;
            btnEdit.Visible = false;
            btnSave.Visible = true;
            btnCancel.Visible = true;
            btnAdd.Visible = false;
            btnFind.Enabled = true;
            EnabledFields();
        }

        

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            Cls_Encrypt.fanos = 0;
            EnabledFields();
            btnSave.Visible = true;
            btnAdd.Visible = false;
            txtCatcode.Focus();
            ClearField();
            GetFaNo();
        }

        private void ClearField()
        {
            txtCatcode.Text = "";
            txtCons.Text = "";
            txtName.Text = "";
            //txtFaNo.Text = "";
            txtAppName.Text = "";
            txtAddress.Text = "";
            txtContact.Text = "";
            txtDistrict.Text = "";
            txtRef.Text = "";
            txtSubject.Text = "";
            txtCNIC.Text = "";
            txtLetterNo.Text = "";
            txtMode.Text = "";
            txtPC1.Text = "";
            txtPC2.Text = "";
            txtPC3.Text = "";
            txtPC4.Text = "";
            txtReplyNo.Text = "";
            txtMeeting.Text = "";
            txtRecommendation.Text = "";
            txtAmount.Text = "";
            txtCMOrders.Text = "";
            txtRemarks.Text = "";
           
        }

        protected void btnFind_Click(object sender, EventArgs e)
        {
            DGEntities1 dg = new DGEntities1();
            int fano1 = Convert.ToInt32(txtFind.Text);
            var getfano = from a in dg.MAINCORRs where a.fano == fano1 select a;
            if (getfano.Count() > 0)
            {
                var bc = getfano.First();
                txtFaNo.Text = Convert.ToInt32(fano1).ToString();
                Date1.DateInput.DisplayText = bc.refdate.ToString();
                txtAppName.Text = bc.appname;
                txtContact.Text = bc.contactno;
                txtAddress.Text = bc.address;
                txtCons.Text = bc.cons.ToString();
                txtCatcode.Text = bc.catcode.ToString();
                txtName.Text = bc.name;
                txtRef.Text = bc.refno;
                txtDistrict.Text = bc.district;
                ddlType.SelectedIndex = ddlType.Items.IndexOf(ddlType.Items.FindByValue(bc.subjcode.ToString()));
                txtSubject.Text = bc.subject;
                txtCNIC.Text = bc.cnic;
                txtLetterNo.Text = bc.letcode.ToString();
                ddlDepartment.SelectedIndex = ddlDepartment.Items.IndexOf(ddlDepartment.Items.FindByValue(bc.deptcode.ToString()));
                SentOn.DateInput.DisplayText = bc.comdate.ToString();
                TargetDate.DateInput.DisplayText = bc.targetdate.ToString();
                txtMode.Text = bc.cataid;
                txtPC1.Text = bc.pc1;
                txtPC2.Text = bc.pc2;
                txtPC3.Text = bc.pc3;
                txtPC4.Text = bc.pc4;
                txtReplyNo.Text = bc.replno;
                ReplyDate.DateInput.DisplayText = bc.repldate.ToString();
                ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByValue(bc.statcode.ToString()));
                txtRecommendation.Text = bc.subdept;
                txtMeeting.Text = bc.set;
                txtAmount.Text = bc.amount.ToString();
                txtCMOrders.Text = bc.cmorders;
                txtRemarks.Text = bc.remarks;
                ReftoCommDate.DateInput.DisplayText = bc.refcomdate.ToString();
                MeetingDate.DateInput.DisplayText = bc.meetindate.ToString();
                
            }
        }

        protected void ddlStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int stcode = Convert.ToInt32(ddlStatus.SelectedItem.Value);
                if (stcode == 33)
                {
                    int catcodes = Convert.ToInt32(txtCatcode.Text);
                    int consss = Convert.ToInt32(txtCons.Text);
                    DGEntities1 ent = new DGEntities1();
                    var getmnampa = from a in ent.mpamnas where a.CatCode == catcodes && a.Cons == consss select a;
                    if (getmnampa.Count() > 0)
                    {
                        var mna = getmnampa.First();
                        txtName.Text = mna.Name;
                        refid = mna.RefId;
                    }
                    string ref_text = "STO/CMO/15/";

                    switch (this.txtCatcode.Text)
                    {
                        case "1":
                            ref_text += "AB";
                            break;
                        case "2":
                            ref_text += "AA";
                            break;
                        case "3":
                            ref_text += "SN";
                            break;
                        case "4":
                            ref_text += "OT";
                            break;
                    }
                    ref_text += "-" + txtCons.Text + "/" + "C" + "/" + "(" + txtFaNo.Text + ")";
                    //txtRef.Text = "Dir/AS(A)/CMS/09/" + txtCatcode.Text+"-"+txtCons.Text +"/"+"C"+"/"+ "("+txtFaNo.Text+")";
                    txtRef.Text = ref_text;
                }
                else
                {
                    int catcodes = Convert.ToInt32(txtCatcode.Text);
                    int consss = Convert.ToInt32(txtCons.Text);
                    DGEntities1 ent = new DGEntities1();
                    var getmnampa = from a in ent.mpamnas where a.CatCode == catcodes && a.Cons == consss select a;
                    if (getmnampa.Count() > 0)
                    {
                        var mna = getmnampa.First();
                        txtName.Text = mna.Name;
                        refid = mna.RefId;
                    }
                    string ref_text = "DIR/FA/CMO/15/";

                    switch (this.txtCatcode.Text)
                    {
                        case "1":
                            ref_text += "AB";
                            break;
                        case "2":
                            ref_text += "AA";
                            break;
                        case "3":
                            ref_text += "SN";
                            break;
                        case "4":
                            ref_text += "OT";
                            break;
                    }
                    ref_text += "-" + txtCons.Text + "/" + "C" + "/" + "(" + txtFaNo.Text + ")";
                    //txtRef.Text = "Dir/AS(A)/CMS/09/" + txtCatcode.Text+"-"+txtCons.Text +"/"+"C"+"/"+ "("+txtFaNo.Text+")";
                    txtRef.Text = ref_text;
                }
            }
            catch (Exception ex)
            {
            }
        }
    }
}
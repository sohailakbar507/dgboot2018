﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DGModel;

namespace DG.WebUI
{
    public partial class DgFormDirector : System.Web.UI.Page
    {
        int refid;

        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!IsPostBack)
            {
                txtFind.Enabled = false;
                btnFind.Enabled = false;
                btnSave.Visible = false;
                btnEdit.Visible = true;
                GetDepartments();
                GetStatus();
                GetSubjects();

                if (Request["DGID"] != null)
                {
                    this.ButtonsForEdit();
                    //EditRecord();
                }

                if (Request["DGID"] == null)
                {
                    DisabledFields();
                    GetFaNo();
                }
            }
            btnSave.Attributes.Add("onclick", "return confirm('Do you want to Save this record[Yes / No]?');");
            

        }

        private void GetFaNo()
        {
            DGEntities1 dg = new DGEntities1();
            int fano;
            var getfano = from a in dg.MAINCORRs select a.fano;
            if (getfano.Count() > 0)
            {
                var fansso = getfano.Max() + 1;
                fano = Convert.ToInt32(fansso);
            }
            else
            {
                fano = 1;
            }

            txtFaNo.Text = fano.ToString();

        }

        private void DisabledFields()
        {
            txtCons.Enabled = false;
            txtCatcode.Enabled = false;
            txtName.Enabled = false;
            txtFaNo.Enabled = false;
            date.Enabled = false;
            txtAppName.Enabled = false;
            txtContact.Enabled = false;
            txtAddress.Enabled = false;
            txtRef.Enabled = false;
            txtDistrict.Enabled = false;
            txtSubject.Enabled = false;            
            SentOn.Enabled = false;
            txtSubCat.Enabled = false;
            ddlType.Enabled = false;
            ddlDepartment.Enabled = false;
            ddlStatus.Enabled = false;
            TargetDate.Enabled = false;
            txtReplyno.Enabled = false;
            ReplyDate.Enabled = false;
            txtDCO.Enabled = false;
            txtIncome.Enabled = false;
            txtLetCode.Enabled = false;
            ASADate.Enabled = false;
            txtCNIC.Enabled = false;
            txtFamNo.Enabled = false;
            txtAmount.Enabled = false;
            txtAmtinWords.Enabled = false;
            SCMDate.Enabled = false;
            txtSCMFileNo.Enabled = false;
            BaitDate.Enabled = false;
            txtRemarks.Enabled = false;
            txtfYear.Enabled = false;
                       
        }

       

        private void ButtonsForEdit()
        {
            
            
        }

        private void GetSubjects()
        {
            DGEntities1 dg = new DGEntities1();
            var subj = (from s in dg.subjects select s);
            ddlType.DataSource = subj;
            ddlType.DataValueField = "SubjectCode";
            ddlType.DataTextField = "Description";
            ddlType.SelectedIndex = 0;
            ddlType.DataBind();
        }

        private void GetStatus()
        {
            DGEntities1 dg = new DGEntities1();
            var stat = (from st in dg.status select st);
            ddlStatus.DataSource = stat;
            ddlStatus.DataValueField = "StatusCode";
            ddlStatus.DataTextField = "Description";
            ddlStatus.SelectedIndex = 0;
            ddlStatus.DataBind();
            
        }

        private void GetDepartments()
        {
            DGEntities1 dg = new DGEntities1();
            var dept = (from d in dg.DEPTs select d);
            ddlDepartment.DataSource = dept;
            ddlDepartment.DataValueField = "DeptCode";
            ddlDepartment.DataTextField = "Deptname";
            ddlDepartment.SelectedIndex = 0;
            ddlDepartment.DataBind();
            
        }

        protected void txtCons_TextChanged(object sender, EventArgs e)
        {
            
            int catcodes = Convert.ToInt32(txtCatcode.Text);
            int consss = Convert.ToInt32(txtCons.Text);
            DGEntities1 ent = new DGEntities1();
            var getmnampa = from a in ent.mpamnas where a.CatCode == catcodes && a.Cons == consss select a;
            if (getmnampa.Count() > 0)
            {
                var mna = getmnampa.First();
                txtName.Text = mna.Name;
                refid = mna.RefId;
            }
            string ref_text = "DIR/FA/CMO/13/";

            switch (this.txtCatcode.Text)
            {
                case "1":
                    ref_text += "AB";
                    break;
                case "2":
                    ref_text += "AA";
                    break;
                case "3":
                    ref_text += "SN";
                    break;
                case "4":
                    ref_text += "OT";
                    break;
            }
            ref_text += "-" + txtCons.Text + "/" + "C" + "/" + "(" + txtFaNo.Text + ")";
            //txtRef.Text = "Dir/AS(A)/CMS/09/" + txtCatcode.Text+"-"+txtCons.Text +"/"+"C"+"/"+ "("+txtFaNo.Text+")";
            txtRef.Text = ref_text;

        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            Cls_Encrypt.fanod = 0;
            btnforAdd();
                        
        }

        private void btnforAdd()
        {
            
            EnabledFields();
            btnSave.Visible = true;
            btnAdd.Visible = false;
            btnEdit.Visible = false;
            txtCatcode.Focus();
            ClearField();
            GetFaNo();
        }

        private void ClearField()
        {
            txtCons.Text = "";
            txtCatcode.Text = "";
            txtName.Text = "";
            txtFaNo.Text = "";
            txtAppName.Text = "";
            txtContact.Text = "";
            txtAddress.Text = "";
            txtRef.Text = "";
            txtDistrict.Text = "";
            txtSubject.Text = "";
            //SentOn = "";
            txtSubCat.Text = "";
            //ddlType.Items.Clear();
            //ddlDepartment.Items.Clear();
            //ddlStatus.Items.Clear();
            //TargetDate.Enabled = "";
            txtReplyno.Text = "";
            //ReplyDate.Enabled = "";
            txtDCO.Text = "";
            txtIncome.Text = "";
            txtLetCode.Text = "";
            //ASADate = "";
            txtCNIC.Text = "";
            txtFamNo.Text = "";
            txtAmount.Text = "";
            txtAmtinWords.Text = "";
            //SCMDate.Enabled = "";
            txtSCMFileNo.Text = "";
            //BaitDate.Enabled = "";
            txtRemarks.Text = "";
        }

        private void EnabledFields()
        {
            txtCons.Enabled = true;
            txtCatcode.Enabled = true;
            txtName.Enabled = true;
            txtFaNo.Enabled = true;
            date.Enabled = true;
            txtAppName.Enabled = true;
            txtContact.Enabled = true;
            txtAddress.Enabled = true;
            txtRef.Enabled = true;
            txtDistrict.Enabled = true;
            txtSubject.Enabled = true;
            SentOn.Enabled = true;
            txtSubCat.Enabled = true;
            ddlType.Enabled = true;
            ddlDepartment.Enabled = true;
            ddlStatus.Enabled = true;
            TargetDate.Enabled = true;
            txtReplyno.Enabled = true;
            ReplyDate.Enabled = true;
            txtDCO.Enabled = true;
            txtIncome.Enabled = true;
            txtLetCode.Enabled = true;
            ASADate.Enabled = true;
            txtCNIC.Enabled = true;
            txtFamNo.Enabled = true;
            txtAmount.Enabled = true;
            txtAmtinWords.Enabled = true;
            SCMDate.Enabled = true;
            txtSCMFileNo.Enabled = true;
            BaitDate.Enabled = true;
            txtRemarks.Enabled = true;
            txtfYear.Enabled = true;
            
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            DGEntities1 dg = new DGEntities1();
            if (Cls_Encrypt.fanod !=1) 
            {
                MAINCORR M = new MAINCORR();
                M.catcode = txtCatcode.Text == string.Empty ? 0 : Convert.ToInt32(txtCatcode.Text);
                M.cons = txtCons.Text == string.Empty ? 0 : Convert.ToInt32(txtCons.Text);
                int catcodes = Convert.ToInt32(txtCatcode.Text == "" ? "0" : this.txtCatcode.Text);
                int conss = Convert.ToInt32(txtCons.Text == "" ? "0" : this.txtCons.Text);
                var getrefid = from a in dg.mpamnas where a.CatCode == catcodes && a.Cons == conss select a;
                if (getrefid.Count() > 0)
                {
                    var mna = getrefid.First();
                    M.RefId = mna.RefId;
                    M.CATCONSMAIN = mna.CatCons;
                }
                
                M.refno = txtRef.Text;
                M.refdate = date.SelectedDate;
                M.fano = txtFaNo.Text == string.Empty ? 0 : Convert.ToInt32(txtFaNo.Text);
                M.appname = txtAppName.Text;
                M.address = txtAddress.Text;
                M.contactno = txtContact.Text;
                M.district = txtDistrict.Text;
                M.subjcode = Convert.ToInt32(ddlType.SelectedItem.Value);
                M.deptcode = Convert.ToInt32(ddlDepartment.SelectedItem.Value);
                M.subcat = txtSubCat.Text;
                M.comdate = SentOn.SelectedDate;
                M.targetdate = TargetDate.SelectedDate;
                M.replno = txtReplyno.Text;
                M.repldate = ReplyDate.SelectedDate;
                M.subdept = txtDCO.Text;
                M.monthincom = txtIncome.Text == string.Empty ? 0 : Convert.ToInt32(txtIncome.Text);
                M.letcode = txtLetCode.Text == string.Empty ? 0 : Convert.ToInt32(txtLetCode.Text);
                M.statcode = Convert.ToInt32(ddlStatus.SelectedItem.Value);
                M.asadate = ASADate.SelectedDate;
                M.cnic = txtCNIC.Text;
                M.famno = txtFamNo.Text;
                M.amount = txtAmount.Text == string.Empty ? 0 : Convert.ToInt32(txtAmount.Text);
                M.amtinwords = txtAmtinWords.Text;
                M.scmdate = SCMDate.SelectedDate;
                M.scmfileno = txtSCMFileNo.Text;
                M.baitdate = BaitDate.SelectedDate;
                M.remarks = txtRemarks.Text;
                M.fyear = txtfYear.Text;
                dg.MAINCORRs.AddObject(M);
                if (M.letcode == 0) M.letcode = null;
                if (M.deptcode == 0) M.deptcode = null;
                if (M.statcode == 0) M.statcode = null;
                if (M.offcode == 0) M.offcode = null;
                if (M.subjcode == 0) M.subjcode = null;
                if (M.RefId == 0) M.RefId = null;
                dg.SaveChanges();
                //txtAddress.Text = "";
                //txtAppName.Text = "";
                //txtContact.Text = "";
                //DisabledFields();
                ClearField();
                btnFind.Enabled = false;
                txtFind.Enabled = false;
                btnAdd.Visible = true;
                btnSave.Visible = false;
                btnEdit.Visible = true;
                btnCancel.Visible = true;
            }
            else
            {
                int dgid = Convert.ToInt32(txtFind.Text);
                MAINCORR M = new MAINCORR();
                M = (from a in dg.MAINCORRs where a.fano == dgid select a).First();
                
                int conss = Convert.ToInt32(txtCons.Text == "" ? "0" : this.txtCons.Text);
                int catcodes = Convert.ToInt32(txtCatcode.Text == "" ? "0" : this.txtCatcode.Text);
                var getrefid = from a in dg.mpamnas where a.CatCode == catcodes && a.Cons == conss select a;
                if (getrefid.Count() > 0)
                {
                    var mna = getrefid.First();
                    M.RefId = mna.RefId;
                    M.CATCONSMAIN = mna.CatCons;
                }
                M.catcode = txtCatcode.Text == string.Empty ? 0 : Convert.ToInt32(txtCatcode.Text);
                M.cons = txtCons.Text == string.Empty ? 0 : Convert.ToInt32(txtCons.Text);
                M.refno = txtRef.Text;
                M.fano = txtFaNo.Text == string.Empty ? 0 : Convert.ToInt32(txtFaNo.Text);
                M.appname = txtAppName.Text;
                M.address = txtAddress.Text;
                M.contactno = txtContact.Text;
                M.district = txtDistrict.Text;
                M.subjcode = Convert.ToInt32(ddlType.SelectedItem.Value);
                M.deptcode = Convert.ToInt32(ddlDepartment.SelectedItem.Value);
                M.subcat = txtSubCat.Text;
                M.comdate = SentOn.DateInput.SelectedDate;
                M.targetdate = TargetDate.DateInput.SelectedDate;
                M.replno = txtReplyno.Text;
                M.refdate = date.SelectedDate;
                M.repldate = ReplyDate.SelectedDate;
                M.subdept = txtDCO.Text;
                M.monthincom = txtIncome.Text == string.Empty ? 0 : Convert.ToInt32(txtIncome.Text);
                M.letcode = txtLetCode.Text == string.Empty ? 0 : Convert.ToInt32(txtLetCode.Text);
                M.statcode = Convert.ToInt32(ddlStatus.SelectedItem.Value);
                M.asadate = ASADate.SelectedDate;
                M.cnic = txtCNIC.Text;
                M.famno = txtFamNo.Text;
                M.amount = txtAmount.Text == string.Empty ? 0 : Convert.ToInt32(txtAmount.Text);
                M.amtinwords = txtAmtinWords.Text;
                M.scmdate = SCMDate.SelectedDate;
                M.scmfileno = txtSCMFileNo.Text;
                M.baitdate = BaitDate.SelectedDate;
                M.remarks = txtRemarks.Text;
                M.fyear = txtfYear.Text;
                dg.SaveChanges();
                btnFind.Enabled = false;
                txtFind.Enabled = false;
                btnAdd.Visible = true;
                btnSave.Visible = false;
                btnEdit.Visible = true;
                btnCancel.Visible = true;
                
            }

                
                ClearField();
        }

        
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("Mainpage.aspx");
            btnAdd.Visible = true;
            btnSave.Visible = false;
            btnEdit.Visible = true;
            btnCancel.Visible = false;
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {

            Cls_Encrypt.fanod = 1;
            btnforEdit();

                        
        }

        private void btnforEdit()
        {
            txtFind.Focus();
            txtFind.Enabled = true;
            btnEdit.Visible = false;
            btnSave.Visible = true;
            btnCancel.Visible = true;
            btnAdd.Visible = false;
            btnFind.Enabled = true;

        }

        protected void btnFind_Click(object sender, EventArgs e)
        {
            DGEntities1 dg = new DGEntities1();
            int fano1 = Convert.ToInt32(txtFind.Text);
            var getfano = from a in dg.MAINCORRs where a.fano == fano1 select a;

            if (getfano.Count() > 0)
            {
                var fd = getfano.First();
                txtFaNo.Text = Convert.ToInt32(fano1).ToString();
                txtCons.Text = Convert.ToInt32(fd.cons).ToString();
                txtCatcode.Text = Convert.ToInt32(fd.catcode).ToString();
                txtAppName.Text = fd.appname;
                txtAddress.Text = fd.address;
                date.SelectedDate = fd.refdate;
                txtContact.Text = fd.contactno;
                txtRef.Text = fd.refno;
                txtDistrict.Text = fd.district;
                txtSubCat.Text = fd.subcat;
                SentOn.SelectedDate = fd.comdate;
                ddlType.SelectedIndex = ddlType.Items.IndexOf(ddlType.Items.FindByValue(fd.subjcode.ToString()));
                ddlDepartment.SelectedIndex = ddlDepartment.Items.IndexOf(ddlDepartment.Items.FindByValue(fd.deptcode.ToString()));
                TargetDate.SelectedDate= fd.targetdate;
                txtReplyno.Text = fd.replno;
                ReplyDate.SelectedDate= fd.repldate;
                txtDCO.Text = fd.subdept;
                txtIncome.Text = Convert.ToInt32(fd.monthincom).ToString();
                txtLetCode.Text = Convert.ToInt32(fd.letcode).ToString();
                ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByValue(fd.statcode.ToString()));
                ASADate.SelectedDate= fd.asadate;
                txtCNIC.Text = fd.cnic;
                txtFamNo.Text = fd.famno;
                txtAmount.Text = Convert.ToInt32(fd.amount).ToString();
                txtAmtinWords.Text = fd.amtinwords;
                SCMDate.SelectedDate= fd.scmdate;
                txtSCMFileNo.Text = fd.fileno;
                BaitDate.SelectedDate= fd.baitdate;
                txtRemarks.Text = fd.remarks;
                txtfYear.Text = fd.fyear;
                EnabledFields();
            }

        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DGModel;

namespace DG.WebUI
{
    public partial class FDUpdateForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadGrid();
            }
        }

        private void LoadGrid()
        {
            DGEntities1 dg = new DGEntities1();
            var dglist = (from a in dg.MAINCORRs where a.fano !=null && a.appname !=null && a.amount!=null && a.fdrefno!=null && a.fdrefdate!=null && a.sentto!=null && a.Balance != null && a.AmountIncured !=null 
                          select new { a.DGId,a.fano, a.appname, a.amount, a.AmountIncured, a.Balance, a.fdrefdate, a.fdrefno, a.sentto }). ToList();
            DGVFDUpdate.DataSource = dglist;
            DGVFDUpdate.DataBind();
            Session["dtData"] = dglist;
          
        }
        

        protected void DGVFDUpdate_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void DGVFDUpdate_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            DGVFDUpdate.PageIndex = e.NewPageIndex;
            DGVFDUpdate.DataSource = Session["dtData"];
            DGVFDUpdate.DataBind();
        }

        protected void lnkBtnSearch_Click(object sender, EventArgs e)
        {
            DGEntities1 DGE = new DGEntities1();

            if (txtFaNo.Text != "")
            {
                int fano = Convert.ToInt32(txtFaNo.Text);
                var fanowise = DGE.SearchFDFANowise(fano);
                Session["dtData"] = fanowise;
                DGVFDUpdate.DataSource = fanowise;
                DGVFDUpdate.DataBind();
            }
        }

        protected void DVGFDUpdate_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {

        }

        protected void DGVFDUpdate_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            Cls_Login login = (Cls_Login)Session["Login"];
            if (login.Isedit != true)
            {
                LinkButton editbutton = (LinkButton)DGVFDUpdate.Rows[e.RowIndex].FindControl("LnkBtnEdit");
                editbutton.Enabled = false;
                return;
            }

            else
            {
               Label dgid = (Label)DGVFDUpdate.Rows[e.RowIndex].FindControl("lblDGId");
               //int fano = Convert.ToInt32(dgid.Text);
               int dgidd = Convert.ToInt32(dgid.Text);

               Response.Redirect("FDForm.aspx?DGID=" + dgidd);
               
            }
        }
    }
}
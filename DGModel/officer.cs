//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DGModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class officer
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public officer()
        {
            this.MAINCORRs = new HashSet<MAINCORR>();
        }
    
        public int OffCode { get; set; }
        public string SectionCode { get; set; }
        public string DesignationCode { get; set; }
        public string SrNo { get; set; }
        public string Name { get; set; }
        public string Section { get; set; }
        public string Designation { get; set; }
        public string PhoneRes { get; set; }
        public string PhoneOffice { get; set; }
        public string MobileNo { get; set; }
        public string FaxNo { get; set; }
        public string Email { get; set; }
        public Nullable<int> MsgIdOfficer { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MAINCORR> MAINCORRs { get; set; }
    }
}

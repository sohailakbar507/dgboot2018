﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DGModel
{
    public class Cls_Login
    {
        private string _Username;// Member variables

        //public string UserName { get; set; }

        public string Username // Properties 
        {
            get { return _Username; }
            set { _Username = value; }
        }

        private int _healthaccessid;

        public int Healthaccessid
        {
            get { return _healthaccessid; }
            set { _healthaccessid = value; }
        }
        private int _userid;

        public int Userid
        {
            get { return _userid; }
            set { _userid = value; }
        }
        private int _Groupid;

        public int Groupid
        {
            get { return _Groupid; }
            set { _Groupid = value; }
        }

        private Boolean _Isedit;

        public Boolean Isedit
        {
            get { return _Isedit; }
            set { _Isedit = value; }
        }
        private Boolean _Isadd;

        public Boolean Isadd
        {
            get { return _Isadd; }
            set { _Isadd = value; }
        }
        private Boolean _Isdel;
        private string _Deptname;
        private string _Hospitalname;

        public Boolean Isdel
        {
            get { return _Isdel; }
            set { _Isdel = value; }
        }
        public string Deptname // Properties 
        {
            get { return _Deptname; }
            set { _Deptname = value; }
        }

        public string Hospitalname // Properties 
        {
            get { return _Hospitalname; }
            set { _Hospitalname = value; }
        }

        public Boolean IsUpdateForm // Auto Properties
        {
            get;
            set;
        }

        public int deptid { get; set; }
        public int hosptid { get; set; }
        //private int _age;
        //public int Age
        //{
        //  get
        // {
        //   if (this._age < 0) return 0;
        //  else return this._age;
        //}
        // set
        // {
        //   if (value < 0) this._age = 0;
        //  else this._age = value;
        //}
        // }

        //public Boolean IsAnalYearReport
        //{
        //    get;
        //    set;
        //}

        public string OfficerName { get; set; }
        public string Type { get; set; }
    }
}

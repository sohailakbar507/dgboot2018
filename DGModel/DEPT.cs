//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DGModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class DEPT
    {
        public int deptcode { get; set; }
        public string deptname { get; set; }
        public string officer_in { get; set; }
        public string contact { get; set; }
        public string fax { get; set; }
        public string mobile { get; set; }
        public string email { get; set; }
        public int deptid { get; set; }
        public string FocalPerson { get; set; }
        public string FocalDesg { get; set; }
        public string FocalMobile { get; set; }
        public string FocalOffice { get; set; }
        public string FocalEmail { get; set; }
        public Nullable<int> Allocations { get; set; }
        public Nullable<int> DepitId { get; set; }
        public string DeptOfficer { get; set; }
        public Nullable<int> MsgID { get; set; }
        public Nullable<bool> chk { get; set; }
        public string MobileNos { get; set; }
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DGModel
{
    using System;
    
    public partial class Senttohosp_Result
    {
        public Nullable<int> fano { get; set; }
        public string appname { get; set; }
        public string refno { get; set; }
        public Nullable<System.DateTime> refdate { get; set; }
        public string fdrefno { get; set; }
        public Nullable<System.DateTime> fdrefdate { get; set; }
        public string sentto { get; set; }
        public Nullable<int> amount { get; set; }
        public Nullable<int> amountincured { get; set; }
        public Nullable<int> balance { get; set; }
        public string subcat { get; set; }
        public string subdept { get; set; }
    }
}

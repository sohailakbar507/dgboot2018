//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DGModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class User_Groups
    {
        public int ID { get; set; }
        public string Group_Name { get; set; }
        public Nullable<int> AccessId { get; set; }
        public Nullable<bool> HealthSection1 { get; set; }
        public Nullable<bool> HealthSection2 { get; set; }
        public Nullable<bool> HealthSection3 { get; set; }
        public Nullable<int> HealthAccessid { get; set; }
    }
}

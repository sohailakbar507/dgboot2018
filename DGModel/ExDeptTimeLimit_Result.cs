//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DGModel
{
    using System;
    
    public partial class ExDeptTimeLimit_Result
    {
        public string fyear { get; set; }
        public Nullable<int> amount { get; set; }
        public string Description { get; set; }
        public Nullable<System.DateTime> refdate { get; set; }
        public string deptname { get; set; }
        public Nullable<System.DateTime> targetdate { get; set; }
        public Nullable<int> days { get; set; }
    }
}

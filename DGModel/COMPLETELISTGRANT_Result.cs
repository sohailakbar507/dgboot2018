//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DGModel
{
    using System;
    
    public partial class COMPLETELISTGRANT_Result
    {
        public Nullable<int> fano { get; set; }
        public Nullable<int> pageno { get; set; }
        public Nullable<System.DateTime> refdate { get; set; }
        public string Name { get; set; }
        public string PatientName { get; set; }
        public string refno { get; set; }
        public string fyear { get; set; }
        public Nullable<int> amount { get; set; }
        public string Chqno { get; set; }
        public Nullable<System.DateTime> ChqDate { get; set; }
        public Nullable<double> Approved { get; set; }
        public string RecvdChqno { get; set; }
        public Nullable<System.DateTime> RecvdChqDate { get; set; }
        public Nullable<int> AmountInc { get; set; }
        public Nullable<int> HealthAmount { get; set; }
        public string Hospital { get; set; }
        public string Disease { get; set; }
        public string PatientStatus { get; set; }
    }
}

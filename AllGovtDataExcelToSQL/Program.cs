﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.OleDb;
using System.IO;

namespace AllGovtDataExcelToSQL
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Start Exporting");

            try
            {
                string inputFolder = @"D:\AllgovExcelData";
                string outputFolder = @"D:\AllgovSQLData";
                List<string> columns = new List<string>();

                foreach (string excel_file in Directory.GetFiles(inputFolder, "*.xls"))
                {
                    Console.WriteLine(excel_file);
                    string sql_file = Path.Combine(outputFolder, Path.GetFileNameWithoutExtension(excel_file) + ".sql");
                    string connection_string = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + excel_file + ";Extended Properties=Excel 8.0";

                    using (OleDbConnection con = new OleDbConnection(connection_string))
                    {
                        string select_query = string.Format("SELECT * FROM [{0}$]", Path.GetFileNameWithoutExtension(excel_file));

                        using (OleDbDataAdapter da = new OleDbDataAdapter(select_query, con))
                        {
                            DataTable dt = new DataTable();
                            da.Fill(dt);
                            foreach (DataColumn col in dt.Columns)
                            {
                                if (!columns.Contains(col.ColumnName))
                                    columns.Add(col.ColumnName);
                            }
                        }
                    }
                }

                foreach (string excel_file in Directory.GetFiles(inputFolder, "*.xls"))
                {
                    Console.WriteLine(excel_file);
                    string sql_file = Path.Combine(outputFolder, Path.GetFileNameWithoutExtension(excel_file) + ".sql");
                    string connection_string = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + excel_file + ";Extended Properties=Excel 8.0";

                    using (OleDbConnection con = new OleDbConnection(connection_string))
                    {
                        string select_query = string.Format("SELECT * FROM [{0}$]", Path.GetFileNameWithoutExtension(excel_file));

                        using (OleDbDataAdapter da = new OleDbDataAdapter(select_query, con))
                        {
                            DataTable dt = new DataTable();
                            da.Fill(dt);
                            StringBuilder sb = new StringBuilder();

                            foreach (DataRow dr in dt.Rows)
                            {
                                string query = "";
                                string names = "";
                                bool first = true;
                                for (int col = 0; col < dt.Columns.Count; col++)
                                {
                                    if (!first)
                                    {
                                        query += ",";
                                        names += ",";
                                    }
                                    first = false;
                                    DataColumn dc = dt.Columns[col];
                                    string data_val = dr[col].ToString();

                                    
                                    data_val = data_val.Replace("'", "''");
                                    query += ("'" + data_val + "'");
                                    names += dc.ColumnName;
                                }
                                sb.AppendLine("INSERT INTO SummaryMain (" + names + ") VALUES(" + query + ")");
                            }

                            using (StreamWriter swSQL = new StreamWriter(sql_file))
                            {
                                    swSQL.WriteLine(sb.ToString());
                            }
                        }
                    }
                }


                /*
                string excelfilename = "D:\\SumExcelForExport.xls";
                string sqlfilename = "D:\\SumExcelForExport.sql";
                string selectQuery = "SELECT * FROM SumDataTable";


                if (File.Exists(excelfilename))
                {
                    if (Path.GetExtension(excelfilename) == ".xls")
                    {
                        OleDbConnection con = new OleDbConnection(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + excelfilename + ";Extended Properties=Excel 8.0");
                        OleDbDataAdapter da = new OleDbDataAdapter(selectQuery, con);
                        DataTable dt = new DataTable();
                        da.Fill(dt);

                        StreamWriter swSQL = new StreamWriter(sqlfilename);

                        foreach (DataRow dr in dt.Rows)
                        {
                            string query = "";
                            string names = "";
                            bool first = true;
                            for (int col = 0; col < dt.Columns.Count; col++)
                            {
                                if (!first)
                                {
                                    query += ",";
                                    names += ",";
                                }
                                first = false;
                                DataColumn dc = dt.Columns[col];
                                string data_val = dr[col].ToString();

                                switch (dc.ColumnName.ToLower())
                                {
                                    case "srno":
                                    case "contactid":
                                    case "colorid":
                                    case "ssno":
                                    case "offid":
                                    case "deptid":
                                    case "statid":
                                        //case "csno":
                                        if (data_val.Trim() == "")
                                        {
                                            query += "null";
                                        }
                                        else
                                        {
                                            query += data_val;
                                        }
                                        break;
                                    case "ssdate":
                                    case "offdate":
                                    case "officebkdate":
                                    case "commdate":
                                    case "targetdate":
                                        if (data_val.Trim() == "")
                                        {
                                            query += "null";
                                        }
                                        else
                                        {
                                            query += ("CONVERT(DateTime,'" + data_val + "')");
                                        }
                                        break;
                                    default:
                                        if (data_val.Trim() == "")
                                        {
                                            query += "null";
                                        }
                                        else
                                        {
                                            data_val = data_val.Replace("'", "''");
                                            query += ("'" + data_val + "'");
                                        }
                                        break;
                                }
                                names += dc.ColumnName;
                            }
                            query = "INSERT INTO SummaryMain (" + names + ") VALUES(" + query + ")";
                            Console.WriteLine(query);
                            swSQL.WriteLine(query);
                        }

                        swSQL.Close();
                    }
                }
                else
                {
                    Console.WriteLine("{0} does not exists.", excelfilename);
                }
                */
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception:{0}\n{1}", ex.Message, ex.StackTrace);
            }

            Console.WriteLine("Export completed.");
            Console.Read();
        }
    }
}
